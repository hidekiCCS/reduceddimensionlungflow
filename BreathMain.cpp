/*

 * main.cpp
 *
 *  Created on: Oct 28, 2014
 *      Author: fuji
 *
 */
#include <iostream>
#include <random>
#include <cstdio>
#include <cstdlib>
#include <mpi.h>
#include <metis.h>
#include "LungLobeFromAirwayTree.h"
#include "AirwayTreePartitioning.h"
#include "AirwayTreePartitioningParMetis.h"
#include "Bifurcation.h"
#include "Airway.h"
#include "AirwayTubeLaw.h"
#include "AirwayWithClosure.h"
#include "AirwayWithPlugPropagation.h"
#include "AcinusWithSurfactant.h"
#include "Surfactant.h"
#include "PleuralPressureWaveForm.h"
#include "InletPressureWaveForm.h"
#include "InletPleuralDataBase.h"
#include "LungFlowSolver.h"
#include "StateDataBaseHDF5.h"
#include "TrackLiquidPlugs.h"
#include "ParenchymalTethering.h"
#include "ParameterFile.h"
#include "LiquidSecretion.h"
#include "Timer.h"

#define AIR_VISCOSITY 1.86302844e-7
/// Viscosity of Liquid [cmH2O s]
#define LIQUID_VISCOSITY 1.0197e-5
/// Surface Tension [dyns/cm]
#define SURFACE_TENSION 0.0

/// Default value of \f$\epsilon\f$
#define EPS0 0.0001
/// percent difference for random perturbing of epsilon
#define PDIFF 0.0
/// airway surface liquid thickness [cm]
#define ASL 0.0009

/// Simulation Model : Elastic Airway
#define AIRWAY_TUBELAW
/// Simulation Model : Airway Closure
#define AIRWAY_CLOSURE
/// Simulation Model : Tracking Plugs
#define TRACKING_PLUGS

/// Simulation Model : Parenchymal Tethering
#define PARENCHYMAL_TETHERING

/// Simulation Model : Multi-layer Surfactant Model
#define MUTILAYER_SURFACTANT_MODEL

/// Liquid Secretion Model
#define LIQUID_SECRETION

/*!
 \mainpage Reduced Dimension Simulation of Air and Liquid Flows in Lung

 This set of code computes the air flow through a network of bifurcating airway model.

 The code is capable to compute following feature.
 - Elastic tube model due to a tube-law
 - Liquid lining that closes airways
 - Tracking liquid plugs.
 - Parenchymal Tethering airways

 For algorithm, look at main()

 */
/// Radius of An Alveolus At Rest
const double radiusOfAlveousArRest = 5.50321208149104e-3;

int main(int argc, char * argv[]) {
	if (argc < 2) {
		std::cout << argv[0] << "[Parameter File]" << std::endl;
		return -1;
	}

	/// - Initialize Petsc
	PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

	int myid = MPI::COMM_WORLD.Get_rank();
	int numproc = MPI::COMM_WORLD.Get_size();

	/// - Get Parameters From File
	ParameterFile *para = new ParameterFile(argv[1]);
	std::string fullname = para->getString("XMLDBFILE")->c_str();
	const double dt = para->getRealValue("DT");
	const double maxTime = para->getRealValue("MAXTIME");
	const double export_time_interval = para->getRealValue("EXPORT_TIME_INTERVAL");
	const int restart_count = para->getIntValue("RESTART_COUNT");
	const double acinus_surfactant_concentration = para->getRealValue("ACINUS_C0");
	const double alveolar_liquid_volume = para->getRealValue("ALVEOLAR_LIQUID_VOLUME");
	const double alveolar_liquid_volume_sd = para->getRealValue("ALVEOLAR_LIQUID_VOLUME_SD");
	double scale_size = para->getRealValue("SCALE_SIZE");
	if (scale_size == (double) NULL) scale_size = 1;
	if (myid == 0) {
		para->listParameters();
	}

	Timer t1;
	t1.start();
	int lastindex = fullname.find_last_of(".");
	std::string rawname = fullname.substr(0, lastindex);
	/// 1. LungLobeFromAirwayTree to reconstruct a lung lobe model from XML database file
	LungLobeFromAirwayTree *lung = new LungLobeFromAirwayTree(rawname.c_str());

	//lung->resetSize(radiusOfAlveousArRest); // WHAT ?

	t1.stop();
	if (myid == 0) {
		std::cout << "Number of MPI Processes=" << numproc << "\n";
		std::cout << "Reading XML file " << t1.getTime() << " sec\n";
	}

	/// 2. AirwayTreePartitioning to partition airway tree
	//AirwayTreePartitioning *graph = new AirwayTreePartitioning(lung);
	AirwayTreePartitioning *graph = new AirwayTreePartitioningParMetis(lung);
	if (myid == 0) {
		std::cout << "Partitioning begin" << std::endl;
	}
	t1.start();
	graph->partitioning();
	graph->exportPartionVTK(rawname.c_str());
	t1.stop();
	if (myid == 0) {
		std::cout << "Partitioning done " << t1.getTime() << " sec" << std::endl;
	}

	// Set up Model
	t1.start();
	/// 3. Create PleuralPressureWaveForm object that determines the pleural pressure
	PleuralPressureWaveForm *pl = new PleuralPressureWaveForm();
	pl->setTime(0.0);
	/// 4. Create InletPressureWaveForm object that determines the Inlet pressure that is constantly zero.
	InletPressureWaveForm *inlet = new InletPressureWaveForm();

	/// 5. Making a list of Bifurcation object
	/// 	- For each Bifurcation object
	///   	  - Set Parent Tube
	///   	  - Set Two Daughter Tubes
	///   	  - Set Initial pressure zero
	int numOfBifurcation = lung->get_NumberOfBifurcations();
	std::vector<Bifurcation *> bifurcationList;
	for (int i = 0; i < numOfBifurcation; i++) {
		if (graph->part_bifurcation(i) == myid) {
			Bifurcation *bif = new Bifurcation(i);
			bif->setParentAirwayIndex(lung->get_ParentTubeIndex(i));
			bif->setDaughterAirwayIndex1(lung->get_DaughterTube1Index(i));
			bif->setDaughterAirwayIndex2(lung->get_DaughterTube2Index(i));
			bif->setPressure(0.0);
			bifurcationList.push_back(bif);
		}
	}

	/// 6. Making a list of Airway object
	///  - use AirwayWithPlugPropagation derived from Airway class
	///  - For each Airway object
	///   - Set #LIQUID_VISCOSITY if using AirwayWithPlugPropagation
	///   - Set #SURFACE_TENSION if using AirwayWithPlugPropagation
	///   - Set Upper Bifurcation Index, if this is trachea set -1
	///   - Set Lower Bifurcation Index, if this terminal airway set -1
	///   - Set Acinus Index if this terminal airway, otherwise set -1
	///   - Set Generation Number
	///   - Set the maximum radius, \f$R_{max}\f$
	///   - Set airway length
	///   - Set #AIR_VISCOSITY
	///   - Set #LIQUID_VISCOSITY
	///   - Set #SURFACE_TENSION
	///   - Set the default film thickness, \f$\epsilon\f$ to #EPS0
	///   - Set initial pressure zero
	///   - Compute Liquid Volume by Airway::computeLiquidVolume()
	int numOfAirways = lung->get_numberOfAirways();
	std::vector<Airway *> airwayList;
#ifdef MUTILAYER_SURFACTANT_MODEL
	std::vector<Surfactant *> surfactantObjectList;
#endif
	for (int i = 0; i < numOfAirways; i++) {
		if (graph->part_airway(i) == myid) {
#ifdef AIRWAY_TUBELAW
#ifdef AIRWAY_CLOSURE
#ifdef TRACKING_PLUGS
			//  - use AirwayWithPlugPropagation derived from Airway class
			AirwayWithPlugPropagation *awPlug = new AirwayWithPlugPropagation(i, pl); // Elastic Tube with Closure and tracking plugs
			Airway *aw = awPlug;
			awPlug->setLiquidViscosityCm2H2O(LIQUID_VISCOSITY);
			awPlug->setSurfaceTension(SURFACE_TENSION);
#else
			// - use AirwayWithClosure derived from Airway class
			Airway *aw = new AirwayWithClosure(i,pl);// Elastic Tube with Closure by Liquid Bridge
#endif
#else
			// - use AirwayTubeLaw derived from Airway class
			Airway *aw = new AirwayTubeLaw(i,pl);
#endif
#else
			/// - use Airway
			Airway *aw = new Airway(i,pl);
#endif

			aw->setUpperBifurcationIndex(lung->get_UpperBifurcationIndex(i));
			aw->setLowerBifurcationIndex(lung->get_LowerBifurcationIndex(i));
			aw->setAicnusIndex(lung->get_AcinusIndex(i));
			aw->setGenerationNumber(lung->get_GenerationNumber(i));
			aw->setRmax(lung->get_AirwayRadius(i) * scale_size);
			aw->setRadius(lung->get_AirwayRadius(i) * scale_size);
			aw->setLength(lung->get_AirwayLength(i) * scale_size);
			aw->setEps(EPS0);
			aw->computeLiquidVolume();
			aw->setFluidViscosity(AIR_VISCOSITY);
			aw->setLiquidViscosityCm2H2O(LIQUID_VISCOSITY);
			aw->setSurfaceTension(SURFACE_TENSION);
			aw->setPressure(0.0);
			airwayList.push_back(aw);
		}
	}

	/// 7. Making a list of Acinus object
	///   - For each Acinus
	///   	- Set FRC volume
	///   	- Set Initial pressure zero
	///     -  this determine volume and shear modulus
	///   	- Set Terminal airway connected
	int numOfAcinus = lung->get_NumberOfAcinus();
	std::vector<Acinus *> acinusList;
	std::default_random_engine generator;
	std::normal_distribution<double> distribution(alveolar_liquid_volume, alveolar_liquid_volume_sd);
	for (int i = 0; i < numOfAcinus; i++) {
		if (graph->part_acinus(i) == myid) {
#ifdef MUTILAYER_SURFACTANT_MODEL
			Surfactant *sf = new Surfactant();
			surfactantObjectList.push_back(sf);
			Acinus *ac = new AcinusWithSurfactant(i, pl, sf);
			sf->setSurfaceTension(0.0);
#else
			Acinus *ac = new Acinus(i,pl);
#endif
			/*! - For each Acinus
			 - Set FRC volume (cm^3)
			 - Set the number of alveoli
			 - Set Initial pressure zero
			 - Set Terminal airway connected
			 */
			ac->setVolumeFrc(lung->get_AcinusVolumeFRC(i));
			//std::cout << ac->getVolume() << std::endl;
			ac->setNumberOfAlveoli(lung->get_NumberOfAlveoliInAcinus(i));
			ac->setPressure(0.0);
			ac->setIndexForTerminalAirway(lung->get_TerminalAirway(i));
#ifdef MUTILAYER_SURFACTANT_MODEL
			const double alvV = ac->getVolume() / ac->getNumberOfAlveoli();
			//std::cout << ac->getVolume() << " " << ac->getNumberOfAlveoli() << std::endl;
			const double area = 4 * M_PI * std::pow(3 * alvV / (4 * M_PI), 2.0 / 3);
			sf->setTotalConcentration(acinus_surfactant_concentration);  //
			sf->setLiquidVolume(distribution(generator));  //
			sf->initialize(area);
#endif
			ac->setPressure(0.0);
			//ac->setVolumeFrc(lung->get_AcinusVolumeFRC(i));
			acinusList.push_back(ac);
		}
	}
#ifdef PARENCHYMAL_TETHERING
	/// 8. Create ParenchymalTethering object
	ParenchymalTethering *tether = new ParenchymalTethering(graph, airwayList, acinusList);
	tether->updateParenchymalState();
#endif

	/// 8. Set Initial Airway Radius and \f$\epsilon\f$ Values
	for (int i = 0; i < airwayList.size(); i++) {
		Airway *aw = airwayList[i];
		aw->setPressure(0.0);
		aw->setSurfaceTension(0.0);
		aw->computeRadiusAtZeroPressure();
		double rad = aw->getRadius();
		aw->setEps(EPS0);
		//aw->setEps((ASL/rad) + pow(-1,rand() % 1)*(ASL/rad)*PDIFF*(rand() % 100)/99);
		aw->setSurfaceTension(SURFACE_TENSION);
		aw->computeLiquidVolume();
		aw->computeRadiusAtZeroPressure();

#ifdef DEBUG
		if (aw->getAicnusIndex() != -1) {
			std::cout << "rad="<<aw->getRadius()<<" Rnsf=" << rad<<" Rmax=" << aw->getRmax() <<" eps=" << aw->getEps() <<std::endl;
		}
#endif
	}

#ifdef LIQUID_SECRETION
	LiquidSecretion *liquidSecret = new LiquidSecretion(graph, airwayList);
#endif

	/// 9. Create StateDataBaseHDF5 object that manages exporting results to HDF5 file
	StateDataBaseHDF5 *statDB = new StateDataBaseHDF5(rawname);

	/// 10. Create LungFlowSolver object that provides a flow solver
	LungFlowSolver *lfs = new LungFlowSolver(graph, bifurcationList, airwayList, acinusList, pl, inlet);
	statDB->addDataBase(lfs);
#ifdef MUTILAYER_SURFACTANT_MODEL
	lfs->setSurfactantModel(true);
#endif

#ifdef TRACKING_PLUGS // Tracking plugs
	/// 1j. Create TrackLiquidPlugs object that manages plug propagation
	TrackLiquidPlugs *trackLps = new TrackLiquidPlugs(graph, bifurcationList, airwayList);
	statDB->addDataBase(trackLps);
#endif

	InletPleuralDataBase *ipDB = new InletPleuralDataBase(inlet, pl);
	statDB->addDataBase(ipDB);
#ifdef PARENCHYMAL_TETHERING
	statDB->addDataBase(tether);
#endif

	double time = 0.0;
	int count = 0;
	/// 12. Import Restart File or Export Initial status
	if (restart_count == 0){
		statDB->exportPHDF5(time);
	}else{
		time = statDB->importPHDF5(restart_count);
		count = restart_count;
	}
	double next_export_time = time + export_time_interval;

	t1.stop();
	if (myid == 0) {
		std::cout << "Setting up " << t1.getTime() << " sec\n";
	}

	/// 14. Begin Simulation Time stepping with count=0 and time=#INTIAL_TIME
	/// 	- Iteration for a time step
	/// 		-# Increment time=time+dt, count=count+1
	/// 		-# Set time to PleuralPressureWaveForm::setTime()
	/// 		-# Update Acinus volume by LungFlowSolver::updateAcinusVolume()
	/// 		-# Call ParenchymalTethering::updateParechymalState() if #PARENCHYMAL_TETHERING defined
	/// 		-# Call LungFlowSolver::solve() to solve flows
	/// 		-# Call TrackLiquidPlugs::updateLiquidPlugs() to update plugs
	/// 		-# Export Status if count=#EXPORT_INTERVAL * integer
	/// 		-# Repeat until time > #MAXTIME or system not converged
	while (1) {
		count++;
		time += dt;

		PetscPrintf(PETSC_COMM_WORLD, "Time=%e Count=%d\n", time, count);
		pl->setTime(time);
		lfs->updateAcinusVolume(dt);
#ifdef PARENCHYMAL_TETHERING // Parenchymal Tethering
		tether->updateParenchymalState();
#endif
		try {
			t1.start();
			lfs->solve();
			t1.stop();
			if (myid == 0) {
				std::cout << "Solver " << t1.getTime() << " sec\n";
			}
#ifdef TRACKING_PLUGS // Tracking plugs
			t1.start();
			trackLps->updateLiquidPlugs(dt);
			t1.stop();
			if (myid == 0) {
				std::cout << "Track plugs " << t1.getTime() << " sec\n";
			}
#endif

#ifdef LIQUID_SECRETION
			liquidSecret->airwayLiquidSecretion(dt);
#endif
			//
			//statDB->exportPHDF5(time);
			//break;
			//

			// export
			if (time >= next_export_time) {
				statDB->exportPHDF5(time);
				next_export_time = time + export_time_interval;
			}
		}
		catch (char *mes) {
			//Finalize
			std::cout << mes;
			delete statDB;
			return -1;
		}
		// stop
		if (time > maxTime) {
			break;
		}
		//if (count > 100) break;
	}

	/// 15. Close Database file
	delete statDB;

	/// 16. Cleanup
	delete lfs;
#ifdef TRACKING_PLUGS // Tracking plugs
	delete trackLps;
#endif
	delete graph;
	delete lung;

	PetscFinalize();
	return 0;
}
