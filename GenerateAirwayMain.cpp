/*
 * GenerateAirwayMain.cpp
 *
 *  Created on: June 14, 2015
 *      Author: fuji
 *      This program creates an airway tree using a Weibel model. The arguments are
 *      argv(0) Program Name
 *      argv(1) Basefile name (eg. Test.xml, Test.vtu)
 *      argv(2) Number of generations to be modeled
 *      argv(3) Asymmetry ratio

 */
#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include "AirwayTreeModel.h"

int main(int argc, const char *argv[]) {
	if (argc < 4){
		std::cout << argv[0] << " Usage incorrect -- should be \'Program Name Basefile name Generation number Asymmetry ratio\'\n";
		return 0;
	}
	AirwayTreeModel *tree = new AirwayTreeModel(argv[1]);
	int gen = std::atoi(argv[2]);
	double ratio = std::atof(argv[3]);
	if (gen < 3){
		std::cout << "[Weibel's Generation] > 2\n";
		return 0;
	}
	tree->generateSymmetricAirways(gen);
	tree->makeAsymmetric(ratio);
	std::cout << "export\n";
	tree->writeXML();
}
