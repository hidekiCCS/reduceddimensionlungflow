# -*- coding: utf-8 -*-
"""
Created on Fri May  5 17:28:53 2017

@author: fuji
"""
import numpy as np

mu = 1.86302844e-7 # Viscosity of air [cmH2O s]
Gen = 12 # Target Generation

rad = [0.8686,0.6142,0.4720,0.3732,0.2992,0.2296,0.1798,0.1410,0.1126,0.0889,0.0704,0.0567,0.0473,0.0416,0.0367,0.0331,0.0296]
len = [12.00,4.76,1.90,0.76,1.27,1.07,0.90,0.76,0.64,0.54,0.47,0.39,0.33,0.27, 0.23,0.20,0.17]

sum = 0.0
for i in range(0,Gen + 1):
    conductance = np.pi * np.power(rad[i],4) / (8 * mu * len[i]) 
    print "Gen=",i, "C=", conductance
    sum = sum + 1 / (np.power(2,i) * conductance)
    #
#
totalConductance = 1.0 / (np.power(2,Gen) * sum)
print "Total Conductance=", totalConductance
lengthEquiv = np.pi *  np.power(rad[Gen],4) * np.power(2,Gen) * sum / (8 * mu)
print "Equivarent Length = ", lengthEquiv
#