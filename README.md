# Generate Airway Model
## Compile
### Cypress
 module load vtk/6.1.0 cmake/3.11.2 gcc/4.9.4 eigen/3.3.4 parmetis/4.0.3 metis/5.1.0 hdf5-parallel/1.8.14 petsc/3.5.4 intel-psxe
 cmake -DCMAKE_CXX_COMPILER=icpc .
### MacOS
 need vtk petsc eigen hdf5 and cmake 
 cmake -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_C_COMPILER=mpicc  .
