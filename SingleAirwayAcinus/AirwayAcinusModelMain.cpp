/*
 * main.cpp
 *
 *  Created on: May 12, 2017
 *      Author: fuji
 * Simple Airway-Acinus Model
 *
 */
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "Bifurcation.h"
#include "AirwayDataBase.h"
#include "AcinusDataBase.h"
#include "Airway.h"
#include "AirwayTubeLaw.h"
#include "AirwayWithClosure.h"
#include "Acinus.h"
#include "PleuralPressureWaveForm.h"
#include "InletPressureWaveForm.h"
#include "InletPleuralDataBase.h"
#include "StateDataBaseHDF5.h"

/// Viscosity of air [cmH2O s]
#define AIR_VISCOSITY 1.86302844e-7
/// Viscosity of Liquid [cmH2O s]
#define LIQUID_VISCOSITY 1.0197e-5
/// Surface Tension [dyns/cm]
#define SURFACE_TENSION 17.0

/// Default value of \f$\epsilon\f$
#define EPS0 0.005

/// Time Step
#define DT 0.0001
/// End of Simulation Time
#define MAXTIME 20.0
/// Interval to export results
#define EXPORT_INTERVAL_TIME 0.1 
/// Initial Simulation Time
#define INTIAL_TIME 0.0

// model geometry
#define RMAX 1.0e-1
#define AW_LENGTH 1.0
#define AC_FRC 1.0

int main(int argc, char * argv[]) {
	if (argc < 1) {
		std::cout << argv[0] << std::endl;
		return -1;
	}
	std::string rawname = "simple";

	// Set up Model
	/// 3. Create PleuralPressureWaveForm object that determines the pleural pressure
	PleuralPressureWaveForm *pl = new PleuralPressureWaveForm();
	pl->setTime(INTIAL_TIME);
	/// 4. Create InletPressureWaveForm object that determines the Inlet pressure that is constantly zero.
	InletPressureWaveForm *inlet = new InletPressureWaveForm();

	/// 5. No Bifurcation

	/// 6. Making an Airway object
	Airway *aw = new AirwayTubeLaw(0,pl);
	aw->setUpperBifurcationIndex(-1);
	aw->setLowerBifurcationIndex(-1);
	aw->setAicnusIndex(0);
	aw->setGenerationNumber(0);
	aw->setRmax(RMAX);
	aw->setRadius(RMAX);
	aw->setEps(EPS0);
	aw->computeLiquidVolume();
	aw->setLength(AW_LENGTH);
	aw->setFluidViscosity(AIR_VISCOSITY);


    /// 7. Making an Acinus object
	Acinus *ac = new Acinus(0,pl);
	ac->setVolumeFrc(AC_FRC);
    ac->setPressure(0.0);
    ac->setIndexForTerminalAirway(0);
    std::vector<Acinus *> acinusList;
    acinusList.push_back(ac);

    /// 8. Set Initial Airway Radius and \f$\epsilon\f$ Values
    aw->setPressure(0.0);
    aw->setSurfaceTension(0.0);
    aw->computeRadiusAtZeroPressure();
    double rad = aw->getRadius();
    aw->setEps(EPS0);
    aw->setSurfaceTension(SURFACE_TENSION);
    aw->computeLiquidVolume();
    aw->computeRadiusAtZeroPressure();

    std::vector<Airway *> airwayList;
    airwayList.push_back(aw);

    /// 9. Create StateDataBaseHDF5 object that manages exporting results to HDF5 file
    StateDataBaseHDF5 *statDB = new StateDataBaseHDF5(rawname);
    AirwayDataBase *db_airway = new AirwayDataBase(airwayList);
    AcinusDataBase *db_acinus = new AcinusDataBase(acinusList);
    statDB->addDataBase(db_airway);
    statDB->addDataBase(db_acinus);
    InletPleuralDataBase *ipDB = new InletPleuralDataBase(inlet,pl);
    statDB->addDataBase(ipDB);

    /// 10. NO PLUG

    double time = INTIAL_TIME;
    const double dt = DT;
    int count = 0;
    /// 12. Export Initial status
    statDB->exportPHDF5(0,time);
    double export_time = time + EXPORT_INTERVAL_TIME;
 
    /// 13. Begin Simulation Time stepping with count=0 and time=#INTIAL_TIME
    while(1){
    	/*! - Iteration for a time step
    	 	 -# Increment time=time+dt, count=count+1
    	 	 -# Set time to PleuralPressureWaveForm::setTime()
    	 	 -# Update Acinus volume by LungFlowSolver::updateAcinusVolume()
    	 	 -# Call LungFlowSolver::solve() to solve flows
    	 	 -# Call TrackLiquidPlugs::updateLiquidPlugs() to update plugs
    	 	 -# Export Status if count=#EXPORT_INTERVAL * integer
    	 	 -# Repeat until time > #MAXTIME
    	 */
    	count++;
    	time += dt;

    	//printf("Time=%e Count=%d\n", time, count);
    	pl->setTime(time);
		/*! - solve
				-# Compute Airway Pressure
				-# Compute Airway Pressure
				-# Compute Airway Conductance
				-# Compute flowrate in Airways
				-# Update Acinus volume
		*/
    	double p0 = inlet->constant_pressure();
    	double p1 = ac->getPressure();
    	double aw_prs = (p0 + p1) * 0.5;
    	aw->setPressure(aw_prs);
    	aw->setPressureDrop(p0 - p1);
    	double aw_conduct = aw->computeAirwayConductance();
    	double aw_flowrare = aw_conduct * (p0 - p1);
    	aw->setFlowRate(aw_flowrare);
    	double ac_vol = ac->getVolume();
    	ac_vol += aw_flowrare * dt;
    	ac->setVolume(ac_vol);

    	// export
    	if (time >= export_time){
    		std::cout << " time=" << time;
    		std::cout << " Q=" << aw_flowrare;
    		std::cout << " C=" << aw_conduct;
    		std::cout << " P1=" << p1;
    		std::cout << " R=" << aw->getRadius();
    		std::cout << std::endl;
    		statDB->exportPHDF5(count,time);
    		statDB->closeFile();
                export_time += EXPORT_INTERVAL_TIME;
    	}

    	// stop
    	if (time > MAXTIME){
    		break;
    	}
    	//if (count > 100) break;
    }

    /// 14. Close Database file
    delete statDB;

    /// 15. Cleanup
	return 0;
}
