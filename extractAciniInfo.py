# -*- coding: utf-8 -*-
"""
Created on Fri Jun 24 17:38:04 2016

@author: fuji
"""


from pythonModule.LungWithPlugs import *
import h5py as h5

if (len(sys.argv) < 3):
    print "%s [xml airway Data] [First Count] [Last Count] [Step Size]\n" % (sys.argv[0])
    sys.exit()
if (False == os.path.isfile(sys.argv[1])):
    sys.exit("xml file not exist")
#
lung = LungWithModel(sys.argv[1])
lung.analyze()
#
baseFileName = lung.baseFileName
print "Base File Name : ", baseFileName
#
firstCount = int(sys.argv[2])
lastCount = firstCount
stepSize = 1
if (len(sys.argv) > 3):
    lastCount = int(sys.argv[3])
if (len(sys.argv) > 4):
    stepSize = int(sys.argv[4])
#    
#h5File = baseFileName + ".h5"
#h5f = h5.File(h5File,'r')
#
outTotalFile = open(baseFileName+'_LungVolume.dat', 'w')
#
#timeCounterList = h5f["TimeSeries"].keys()
#timeSeries = h5f["TimeSeries"]
for i in range(firstCount,lastCount + 1,stepSize):
    h5File = ("%s%06d.h5" % (baseFileName, i))            
    h5f = h5.File(h5File,'r')

    inletPressure = h5f["InletPressure"].value
    pleuralPressure = h5f["PleuralPressure"].value
    timeSec = h5f["Time"].value
    acinus = h5f["Acini"]
    acVol = acinus["Volume"].value
    acPrs = acinus["Pressure"].value
    #
    # export
    fileMode = 'w'
    if (i != 0):
        fileMode = 'a'
    for j, vol in enumerate(acVol):
        outFile = open(baseFileName+'_AC%06d.dat' % j, fileMode)
        outFile.write('%f %e %e %e\n' % (timeSec,inletPressure-pleuralPressure,acPrs[j],vol))
        outFile.close()
    #
    outTotalFile.write('%f %e %e\n' % (timeSec,inletPressure-pleuralPressure,acVol.sum()))
#
outTotalFile.close()
#