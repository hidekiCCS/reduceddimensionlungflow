# -*- coding: utf-8 -*-
"""
Created on Thu Jun 30 09:20:22 2016

@author: fuji
"""

from pythonModule.LungWithPlugs import *
import h5py as h5

if (len(sys.argv) < 3):
    print "%s [xml airway Data] [First Count] [Last Count] [Step Size]\n" % (sys.argv[0])
    sys.exit()
if (False == os.path.isfile(sys.argv[1])):
    sys.exit("xml file not exist")
#
lung = LungWithModel(sys.argv[1])
lung.analyze()
#
baseFileName = lung.baseFileName
print "Base File Name : ", baseFileName
#
firstCount = int(sys.argv[2])
lastCount = firstCount
stepSize = 1
if (len(sys.argv) > 3):
    lastCount = int(sys.argv[3])
if (len(sys.argv) > 4):
    stepSize = int(sys.argv[4])
#
#h5File = baseFileName + ".h5"
#h5f = h5.File(h5File,'r')
#
gInf = 3.1e-7
#
outSAFile = open(baseFileName+'_SA.csv', 'w')
outSTFile = open(baseFileName+'_ST.csv', 'w')
outGPFile = open(baseFileName+'_GP.csv', 'w')
outGSFile = open(baseFileName+'_GS.csv', 'w')
outBCFile = open(baseFileName+'_BC.csv', 'w')
#
#timeCounterList = h5f["TimeSeries"].keys()
#timeSeries = h5f["TimeSeries"]
for i in range(firstCount,lastCount + 1,stepSize):
    h5File = ("%s%06d.h5" % (baseFileName, i))            
    h5f = h5.File(h5File,'r')

    inletPressure = h5f["InletPressure"].value
    pleuralPressure = h5f["PleuralPressure"].value
    timeSec = h5f["Time"].value
    acinus = h5f["Acini"]
    acVol = acinus["Volume"].value
    acArea = acinus["SurfaceArea"].value
    acST = acinus["SurfaceTension"].value
    acG1 = acinus["GammaP"].value
    acG2 = acinus["GammaS"].value
    acBC = acinus["BulkC"].value
    acLV = acinus["LiquidVolume"].value
    #
    #totalSF = acBC + (acG1 + acG2) * acArea / acLV
    #print np.mean(totalSF) 
    # export
    outSAFile.write('%f' % (timeSec))
    outSTFile.write('%f' % (timeSec))
    outGPFile.write('%f' % (timeSec))
    outGSFile.write('%f' % (timeSec))
    outBCFile.write('%f' % (timeSec))
    for j, vol in enumerate(acVol):
        outSAFile.write(',%e' % (acArea[j]))
        outSTFile.write(',%e' % (acST[j]))
        outGPFile.write(',%e' % (acG1[j]/gInf))
        outGSFile.write(',%e' % (acG2[j]/gInf))
        outBCFile.write(',%e' % (acBC[j]))
    outSAFile.write('\n')
    outSTFile.write('\n')
    outGPFile.write('\n')
    outGSFile.write('\n')
    outBCFile.write('\n')
    h5f.close()
    #
outSAFile.close()
outSTFile.close()
outGPFile.close()
outGSFile.close()
outBCFile.close()
#
#