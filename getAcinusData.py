# -*- coding: utf-8 -*-
"""
Created on Mon May  8 11:21:36 2017

@author: fuji
"""

from pythonModule.xml2VTK import *
import h5py as h5

if (len(sys.argv) < 3):
    print "%s [xml airway Data] [First Count] [Last Count] [Step Size]\n" % (sys.argv[0])
    sys.exit()
if (False == os.path.isfile(sys.argv[1])):
    sys.exit("xml file not exist")
#
lung = xmlLungModel(sys.argv[1])
lung.analyze()
#
baseFileName = lung.baseFileName
print "Base File Name : ", baseFileName
#
firstCount = int(sys.argv[2])
lastCount = firstCount
stepSize = 1
if (len(sys.argv) > 3):
    lastCount = int(sys.argv[3])
if (len(sys.argv) > 4):
    stepSize = int(sys.argv[4])
#
#h5File = baseFileName + ".h5"
#h5f = h5.File(h5File,'r')
#
#timeCounterList = h5f["TimeSeries"].keys()
#timeSeries = h5f["TimeSeries"]
#
timeCounterList = range(firstCount,lastCount + 1,stepSize)    
timeVal = np.zeros((len(timeCounterList)))
inletPressure = np.zeros((len(timeCounterList)))
pleuralPressure = np.zeros((len(timeCounterList)))
#
for i in timeCounterList:
    h5File = ("%s%06d.h5" % (baseFileName, i))            
    h5f = h5.File(h5File,'r')
    
    ac = h5f["Acini"]
    prs = ac["Pressure"].value
    vol = ac["Volume"].value
    #
    if i == 0:
        pressure = np.zeros((len(timeCounterList),len(prs)))
        volume = np.zeros((len(timeCounterList),len(prs)))
    #
    pressure[i,:] = prs
    volume[i] = vol
    timeVal[i] = h5f["Time"].value
    #
    inletPressure[i] = h5f["InletPressure"].value
    pleuralPressure[i] = h5f["PleuralPressure"].value
    #
    h5f.close()
#
numberOfAcini = pressure[0,:].size
timeCount = len(timeCounterList)
for j in range(numberOfAcini):
    fileName = baseFileName + 'AC' + str(j).zfill(4) + '.dat'
    np.savetxt(fileName,np.column_stack((timeVal[:],pressure[:,j], volume[:,j])))
#
fileName = baseFileName + '_STAT.dat'
np.savetxt(fileName,np.column_stack((timeVal[:],inletPressure[:], pleuralPressure[:])))
