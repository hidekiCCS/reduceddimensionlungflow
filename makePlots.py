# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 09:16:26 2016

@author: fuji
"""
import sys
import os
import numpy as np
from pythonModule.LungWithPlugs import *
#
import matplotlib.pyplot as plt

def PVcurve(dataTotal):
    fig, axes = plt.subplots()
    axes.plot(dataTotal[:,1],dataTotal[:,2],'-')
    #axes.ticklabel_format(axis='y', style='sci', scilimits=(-3, -2))
    axes.set_ylabel(r"Volume($cm^3$)")
    axes.set_xlabel(r"$P_{inlet} -P_{pl} (cmH_2O)$")
    fig.savefig("PVcurve.png")
    #print dataTotal

def PVIndcurve(ppl,vol):
    numAcinus = len(vol[0]) - 1
    fig, axes = plt.subplots()
    for i in range(numAcinus):
        axes.plot(ppl[:,1],vol[:,i+1],'-')
    axes.set_ylabel(r"Volume($cm^3$)")
    axes.set_xlabel(r"$P_{inlet} -P_{pl} (cmH_2O)$")
    fig.savefig("PVIndcurve.png")
    
def gammaSA(dataST,dataSA):
    numAcinus = len(dataST[0]) - 1
    fig, axes = plt.subplots()
    for i in range(numAcinus):
        axes.plot(dataSA[:,i+1],dataST[:,i+1],'-')
    axes.set_ylabel(r"$\gamma$ ($dynes/cm^2$)")
    axes.set_xlabel(r"Surface Area ($cm^2$)")
    fig.savefig("SFAcurve.png")
    
def GammaSA(dataGP,dataSA):
    numAcinus = len(dataSA[0]) - 1
    fig, axes = plt.subplots()
    for i in range(numAcinus):
        axes.plot(dataSA[:,i+1],dataGP[:,i+1],'-')
    axes.set_ylabel(r"$\Gamma_1/\Gamma_\infty$")
    axes.set_xlabel(r"Surface Area ($cm^2$)")
    fig.savefig("GPAcurve.png")
 
def GammaTime(g1,g2):
    numAcinus = len(g2[0]) - 1
    fig, axes = plt.subplots()
    for i in range(numAcinus):
        axes.plot(g1[:,0],g1[:,i+1],'-')
        axes.plot(g2[:,0],g2[:,i+1],'-')
    axes.set_ylabel(r"$\Gamma/\Gamma_\infty$")
    axes.set_xlabel(r"time (sec)")
    fig.savefig("Gammas.png")   
    
# MAIN
if __name__=="__main__":
    if (len(sys.argv) < 2):
        print "%s [xml airway Data]\n" % (sys.argv[0])
        sys.exit()
    if (False == os.path.isfile(sys.argv[1])):
        sys.exit("xml file not exist")
    #
    lung = LungWithModel(sys.argv[1])
    lung.analyze()
    #
    baseFileName = lung.baseFileName
    print "Base File Name : ", baseFileName
    #
    dataSA = np.genfromtxt(baseFileName+'_SA.csv', delimiter=',')
    dataST = np.genfromtxt(baseFileName+'_ST.csv', delimiter=',')
    dataGP = np.genfromtxt(baseFileName+'_GP.csv', delimiter=',')
    dataGS = np.genfromtxt(baseFileName+'_GS.csv', delimiter=',')
    dataBC = np.genfromtxt(baseFileName+'_BC.csv', delimiter=',')
    dataTotal = np.genfromtxt(baseFileName+'_LungPV.csv', delimiter=',')
    dataVolume = np.genfromtxt(baseFileName+'_VAC.csv', delimiter=',')
    inpPrsFile = np.genfromtxt(baseFileName+'_PAC.csv', delimiter=',')
    #
    PVcurve(dataTotal)
    gammaSA(dataST[dataST[:,0] >= 55],dataSA[dataST[:,0] >= 55])
    GammaSA(dataGP[dataST[:,0] >= 55],dataSA[dataST[:,0] >= 55])
    PVIndcurve(dataTotal[dataST[:,0] >= 55],dataVolume[dataST[:,0] >= 55])
    GammaTime(dataGP[dataST[:,0] >= 55],dataGS[dataST[:,0] >= 55])