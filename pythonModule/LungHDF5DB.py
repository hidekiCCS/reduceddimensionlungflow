# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 16:37:17 2015

@author: fuji
"""

import os
import sys
import numpy as np
import h5py as h5

class LungHDF5DB(object):
    """
    READ HDF5 Lung Simulation Resuls
    """
    def __init__(self,fileName):
        """
        Open Database
        """
        self.h5fileName = fileName
        self.h5file = h5.File(self.h5fileName,'r')
        self.timeSeries = self.h5file["TimeSeries"]
    def selectTime(self,tcounter):
        """
        Select current data group to 'tcounter'
        """
        keys = self.timeSeries.keys()
        for key in keys:
            if (int(key) == tcounter):
                break
        self.tGroup = self.timeSeries[key]
        self.dataset = self.tGroup
        
    def selectDataSet(self, pathName):
        """
        Select current dataset
        """
        keys = self.dataset.keys()
        for key in keys:
            if (key == pathName):
                self.dataset = self.dataset[key]
        
    def getArray(self):
        """
        return numpy array of data
        """
        return self.dataset.value        
#
# MAIN
if __name__=="__main__":
    if (len(sys.argv) < 2):
        print "%s [HDF5 Simulation Data]\n" % (sys.argv[0])
        sys.exit()
    if (False == os.path.isfile(sys.argv[1])):
            sys.exit("HDF5 file not exist")
    #
    f = LungHDF5DB(sys.argv[1])
    g = f.selectTime(0)
    f.selectDataSet('Airways')
    f.selectDataSet('FlowRate')
    flowrate = f.getArray()
    print flowrate
            
    