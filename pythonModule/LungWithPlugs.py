# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 17:26:41 2015

@author: fuji
"""

import os
import sys
import vtk
import numpy as np
from xml.dom.minidom import getDOMImplementation
from xml.dom.minidom import parse
import matplotlib.pyplot as plt
from xml2VTK import xmlLungModel

class LungWithModel(xmlLungModel):
    """ Lung Model  """
    def __init__(self,fileName):
        """ read xml airway model data """
        xmlLungModel.__init__(self,fileName)
    
        self.plugAirwayIndex = []
        self.plugPosition = []
        
        self.plugScalarDataName = []
        self.plugScalarDataArray = []
    def addPlug(self, awIndex, position):
        """ Add a liquid plug """
        self.plugAirwayIndex.append(awIndex)
        self.plugPosition.append(position)
        #
    def addPlugs(self, awIndices, positions):
        """ Add a liquid plugs """
        if (len(awIndices) != len(positions)):
            print "data length doesn't match."
            return
        for i in range(len(awIndices)):
            self.addPlug(awIndices[i],positions[i])
        #
    def addPlugDataScalar(self,dataName,dataArray):
        """        
        Add a scalar cell data
        """
        if (len(self.plugAirwayIndex) == len(dataArray)):
            self.plugScalarDataName.append(dataName)
            self.plugScalarDataArray.append(dataArray)
        else:
            print "Number of data doesn't match with the numnber of Plugs"
        #
    def clearPlugs(self):
        """ delete all plugs """
        self.plugAirwayIndex = []
        self.plugPosition = []
        
        self.plugScalarDataName = []
        self.plugScalarDataArray = []     
        
    def export2VTK_Plugs(self,base):
        """
        export tree plot to VTK
        """
        print "number of plugs = ", len(self.plugAirwayIndex)
        vtkFile = base + '.vtu'
        if len(self.airwayCoordinate) <= 0:
            print "call export2VTK_Tree first"
            return
        #
        # set up VTK
        writer = vtk.vtkXMLUnstructuredGridWriter()
        plugs = vtk.vtkUnstructuredGrid()
        cells = vtk.vtkCellArray()
        nodes = vtk.vtkPoints()
        nodes.SetNumberOfPoints(len(self.plugAirwayIndex))
        scalarData = []
        for i, data in enumerate(self.plugScalarDataArray):
            scalarData.append(vtk.vtkDoubleArray())
            scalarData[i].SetNumberOfComponents(1);
            scalarData[i].SetNumberOfTuples(len(self.plugAirwayIndex))
            scalarData[i].SetName(self.plugScalarDataName[i])
        #
        for i, awIdx in enumerate(self.plugAirwayIndex):
            line = self.airwayCoordinate[awIdx]
            sp = np.array([line[0],line[1]])
            ep = np.array([line[2],line[3]])
            po = sp + (ep - sp) * self.plugPosition[i]
            nodes.InsertPoint(i, po[0], po[1], 0.0) 
            cells.InsertNextCell(1)
            cells.InsertCellPoint(i)
            for j, data in enumerate(self.plugScalarDataArray):
                scalarData[j].InsertTuple1(i,data[i].tolist())
        # export
        plugs.SetPoints(nodes)
        plugs.SetCells(vtk.VTK_VERTEX, cells)
        for data in scalarData:
            plugs.GetCellData().AddArray(data)
        writer.SetInput(plugs)
        writer.SetFileName(vtkFile)
        writer.Write();    
        
        