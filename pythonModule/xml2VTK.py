# -*- coding: utf-8 -*-
"""
Created on Thu Oct 16 16:00:45 2014

@author: fuji
"""
import os
import sys
import vtk
import numpy as np
from xml.dom.minidom import getDOMImplementation
from xml.dom.minidom import parse
import matplotlib.pyplot as plt

class xmlLungModel(object):
    """ Lung Model  """
    def __init__(self,fileName):
        """ read xml airway model data """
        self.xmlFile = fileName
        print "READ :", self.xmlFile
        self.baseFileName=os.path.splitext(os.path.basename(self.xmlFile))[0]
        # Create DOM object
        impl = getDOMImplementation()
        self.dom = impl.createDocument(self.xmlFile,
                                        "xmlLungModel",
                                        None)        
        self.dom = parse(self.xmlFile)
        doc = self.dom.documentElement
        vertices = doc.getElementsByTagName('VERTEX')
        self.numNodes = len(vertices)
        airways = doc.getElementsByTagName('AIRWAY')
        self.numberOfAirways = len(airways)
        self.generationNumber = [0] * self.numberOfAirways
        self.parentTube = [-1] * self.numberOfAirways
        self.daughterTube1 = [-1] * self.numberOfAirways
        self.daughterTube2 = [-1] * self.numberOfAirways
        self.length = [0.0] * self.numberOfAirways
        self.radius = [0.0] * self.numberOfAirways
        self.firstAirway = -1
        self.maxGeneration = -1
        for airway in airways:
            idx = int(airway.getAttributeNode('Number').nodeValue)
            self.generationNumber[idx] = int(airway.getElementsByTagName('GENERATION_NUMBER')[0].firstChild.nodeValue)
            self.parentTube[idx] = int(airway.getElementsByTagName('PARENT_TUBE')[0].firstChild.nodeValue)
            daughterTubeNodes = airway.getElementsByTagName('DAUGHTER_TUBE')
            self.daughterTube1[idx]= int(daughterTubeNodes[0].firstChild.nodeValue)
            if (len(daughterTubeNodes) > 1):
                self.daughterTube2[idx] = int(daughterTubeNodes[1].firstChild.nodeValue)
            #     
            if (self.maxGeneration < self.generationNumber[idx]):
                self.maxGeneration = self.generationNumber[idx]
                self.firstAirway = idx
            #
            self.length[idx] = float(airway.getElementsByTagName('LENGTH')[0].firstChild.nodeValue)
            self.radius[idx] = float(airway.getElementsByTagName('RADIUS')[0].firstChild.nodeValue)
            #
        print "First Airway :", self.firstAirway, ", Generation :", self.maxGeneration
        #self.generation_weibel = int(doc.getElementsByTagName('GENERATION_WEIBEL')[0].firstChild.nodeValue)
        #print "Equivalent Weibel's generation : ", self.generation_weibel
        #    
        self.scalarDataName = []
        self.scalarDataArray = []
        self.airwayCoordinate = []
        #
    def analyze(self):
        """
        Analyze the model
        """
        terminalBronchi = []
        for idx in range(self.numberOfAirways):
            if (self.daughterTube1[idx] == -1):
                terminalBronchi.append(idx)
        numberOfTerminalBronchi = len(terminalBronchi)                
        print "Number of Terminal Bronchi :", numberOfTerminalBronchi

        pathLength = [0.0] * numberOfTerminalBronchi        
        for idx in range(numberOfTerminalBronchi):
            pathLength[idx] = 0.0
            pidx = terminalBronchi[idx]
            while(1):
                pathLength[idx] = pathLength[idx] + self.length[pidx]
                if (self.parentTube[pidx] == -1):
                    break
                pidx = self.parentTube[pidx]
            #
        averageLength = np.mean(pathLength)
        sd = np.std(pathLength)
        print "Mean Path Length" , averageLength, "S.D. : ", sd
        
        alength = [12.00,4.76,1.90,0.76,1.27,1.07,0.90,0.76,0.64,0.54,0.47,0.39,0.33,0.27, 0.23,0.20,0.17]
        #weibelLength = np.sum(alength[:self.generation_weibel])
        #print "Equivalent Weibel's Airway Path Length :", weibelLength
        
        #binedges = [np.floor(weibelLength) - 10 + 2 * n for n in range(15)]
        
        #hist, binedges = np.histogram(pathLength,bins=binedges)
        #print "Histogram :",hist
        #print "Bin Edges :",binedges
        
        # resistance
        resistance = [0.0] * self.numberOfAirways
        for idx in range(self.numberOfAirways):
                resistance[idx] = self.length[idx] / np.power(self.radius[idx],4)
        for i in range(self.maxGeneration):
            for idx in range(self.numberOfAirways):
                resistance[idx] = self.length[idx] / np.power(self.radius[idx],4)
                if (self.daughterTube1[idx] != -1):
                    resistance[idx] = resistance[idx] + 1.0 / (1.0 / resistance[self.daughterTube1[idx]] + 1.0 / resistance[self.daughterTube2[idx]])
        print "Total Resistance :", resistance[0]
        
        #fig, axes = plt.subplots()
        #axes.bar(binedges[:-1], hist, width = 2)
        #axes.set_xlim([min(binedges), max(binedges)])
        #axes.set_xlabel('Path length from Trachea to Terminal Bronchi')
        #axes.set_ylabel('Frequency')
        #axes.grid(True)
        #fn = self.baseFileName + "_Hist.eps"
        #print "Export Histgram : ", fn
        #fig.savefig(fn)
        #plt.show()  
        #plt.hist(pathLength,bins=binedges)
        #plt.show()
    def addCellDataScalar(self,dataName,dataArray):
        """        
        Add a scalar cell data
        """
        if (self.numberOfAirways == len(dataArray)):
            self.scalarDataName.append(dataName)
            self.scalarDataArray.append(dataArray)
        else:
            print "Number of data doesn't match with the numnber of Airways"
        #
        #
    def updateCellDataScalar(self,dataName,dataArray):
        """        
        Update a scalar cell data already added
        """
        if (self.numberOfAirways == len(dataArray)):
            for i, name in enumerate(self.scalarDataName):
                if (name == dataName):
                    self.scalarDataArray[i] = dataArray
                    break
        else:
            print "Number of data doesn't match with the numnbero f Airways"
        #
        #
    def export2VTK_Tree(self,base):
        """
        export tree plot to VTK
        """
        vtkFile = base + '.vtu'
        self.airwayCoordinate = [np.array([0.0, 0.0, 0.0, 0.0])] * self.numberOfAirways
        # set up VTK
        writer = vtk.vtkXMLUnstructuredGridWriter()
        tree = vtk.vtkUnstructuredGrid()
        cells = vtk.vtkCellArray()
        nodes = vtk.vtkPoints()
        nodes.SetNumberOfPoints(self.numNodes)
        scalarArray = vtk.vtkDoubleArray()
        scalarArray.SetNumberOfComponents(1);
        scalarArray.SetNumberOfTuples(self.numberOfAirways)
        scalarArray.SetName('Generation')
        scalarData = []
        for i, data in enumerate(self.scalarDataArray):
            scalarData.append(vtk.vtkDoubleArray())
            scalarData[i].SetNumberOfComponents(1);
            scalarData[i].SetNumberOfTuples(self.numberOfAirways)
            scalarData[i].SetName(self.scalarDataName[i])
        
        #print Resistance
        airwayIdx = self.firstAirway
        nodes.InsertPoint(0, 0.0, 1.0, 0.0)
        vertexCount = 1
        vetexStack = []
        vetexStack.append(0)
        diectionStack = []
        diectionStack.append(np.array([0,-1]))
        lengthStack = []
        lengthStack.append(float(1.0))
        idxStack = []
        idxStack.append(self.firstAirway)
        fac = 0.7

        while(len(idxStack) > 0):
            airwayIdx = idxStack.pop()
            dirc = diectionStack.pop()
            length = lengthStack.pop()
            sidx = vetexStack.pop()
            # Draw an Airway
            sp = [0,0,0]
            nodes.GetPoint(sidx, sp)
            ep = np.array([sp[0],sp[1]]) + length * dirc
            self.airwayCoordinate[airwayIdx] = np.array([sp[0],sp[1], ep[0], ep[1]])
            nodes.InsertPoint(vertexCount, ep[0], ep[1], 0.0)
            cells.InsertNextCell(2)
            cells.InsertCellPoint(sidx)
            cells.InsertCellPoint(vertexCount)
            scalarArray.InsertValue(vertexCount-1,float(self.generationNumber[airwayIdx]))
            for i, data in enumerate(self.scalarDataArray):
                scalarData[i].InsertTuple1(vertexCount-1,data[airwayIdx].tolist())
            # put Daughter tubes on Stack
            if (self.daughterTube1[airwayIdx] != -1):
                vetexStack.append(vertexCount)
                vetexStack.append(vertexCount)
                idxStack.append(self.daughterTube1[airwayIdx])
                lengthStack.append(length * fac)
                diectionStack.append(np.array([dirc[1],dirc[0]]))
                idxStack.append(self.daughterTube2[airwayIdx])
                lengthStack.append(length * fac)
                diectionStack.append(np.array([-dirc[1],-dirc[0]]))
                #
            vertexCount += 1
        #    
        #
        # export
        tree.SetPoints(nodes)
        tree.SetCells(vtk.VTK_LINE, cells)
        #tree.SetCells(vtk.VTK_VERTEX, cells)
        tree.GetCellData().AddArray(scalarArray)
        for data in scalarData:
            tree.GetCellData().AddArray(data)
        if vtk.vtkVersion.GetVTKMajorVersion() >= 6:
            writer.SetInputData(tree)
        else:
            writer.SetInput(tree)
        writer.SetFileName(vtkFile)
        writer.Write();    
#   
# MAIN
if __name__=="__main__":
    if (len(sys.argv) < 2):
        print "%s [xml airway Data]\n" % (sys.argv[0])
        sys.exit()
    if (False == os.path.isfile(sys.argv[1])):
            sys.exit("xml file not exist")
    #
    lung = xmlLungModel(sys.argv[1])
    lung.analyze()
    lung.addCellDataScalar("Radius",np.array(lung.radius))
    lung.addCellDataScalar("Length",np.array(lung.length))
    lung.export2VTK_Tree(lung.baseFileName)
#
    
#
#
