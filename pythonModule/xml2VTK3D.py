# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23 12:09:58 2017

@author: fuji
"""

import os
import sys
import vtk
import numpy as np
from xml.dom.minidom import getDOMImplementation
from xml.dom.minidom import parse

class xmlLungModel3D(object):
    """ Lung Model 3D """
    def __init__(self,fileName):
        """ read xml airway model data """
        self.xmlFile = fileName
        print "READ :", self.xmlFile
        self.baseFileName=os.path.splitext(os.path.basename(self.xmlFile))[0]
        # Create DOM object
        impl = getDOMImplementation()
        self.dom = impl.createDocument(self.xmlFile,
                                        "xmlLungModel",
                                        None)        
        self.dom = parse(self.xmlFile)
        doc = self.dom.documentElement
        #
        vertices = doc.getElementsByTagName('VERTEX')
        self.numNodes = len(vertices)
        self.vertices = [[0.0, 0.0, 0.0]] * self.numNodes
        for vertex in vertices:
            idx = int(vertex.getAttributeNode('Number').nodeValue)
            vcord = vertex.getElementsByTagName('COORDINATE')[0]
            x = int(vcord.getElementsByTagName('X')[0].firstChild.nodeValue)
            y = int(vcord.getElementsByTagName('Y')[0].firstChild.nodeValue)
            z = int(vcord.getElementsByTagName('Z')[0].firstChild.nodeValue)
            self.vertices[idx] = [x,y,z]
        #
        airways = doc.getElementsByTagName('AIRWAY')
        self.numberOfAirways = len(airways)
        self.generationNumber = [0] * self.numberOfAirways
        self.vertex1 = [-1] * self.numberOfAirways
        self.vertex2 = [-1] * self.numberOfAirways
        self.parentTube = [-1] * self.numberOfAirways
        self.daughterTube1 = [-1] * self.numberOfAirways
        self.daughterTube2 = [-1] * self.numberOfAirways
        self.length = [0.0] * self.numberOfAirways
        self.radius = [0.0] * self.numberOfAirways
        self.firstAirway = -1
        self.maxGeneration = -1
        for airway in airways:
            idx = int(airway.getAttributeNode('Number').nodeValue)
            self.generationNumber[idx] = int(airway.getElementsByTagName('GENERATION_NUMBER')[0].firstChild.nodeValue)
            self.vertex1[idx] = int(airway.getElementsByTagName('VERTEX1')[0].firstChild.nodeValue)
            self.vertex2[idx] = int(airway.getElementsByTagName('VERTEX2')[0].firstChild.nodeValue)            
            self.parentTube[idx] = int(airway.getElementsByTagName('PARENT_TUBE')[0].firstChild.nodeValue)
            daughterTubeNodes = airway.getElementsByTagName('DAUGHTER_TUBE')
            self.daughterTube1[idx]= int(daughterTubeNodes[0].firstChild.nodeValue)
            if (len(daughterTubeNodes) > 1):
                self.daughterTube2[idx] = int(daughterTubeNodes[1].firstChild.nodeValue)
            #     
            if (self.maxGeneration < self.generationNumber[idx]):
                self.maxGeneration = self.generationNumber[idx]
                self.firstAirway = idx
            #
            self.length[idx] = float(airway.getElementsByTagName('LENGTH')[0].firstChild.nodeValue)
            self.radius[idx] = float(airway.getElementsByTagName('RADIUS')[0].firstChild.nodeValue)
            #
        print "First Airway :", self.firstAirway, ", Generation :", self.maxGeneration
        #    
        self.scalarDataName = []
        self.scalarDataArray = []
        self.arrayDataName = []
        self.arrayDataArray = []
    #
    def addCellDataScalar(self,dataName,dataArray):
        """        
        Add a scalar cell data
        """
        if (self.numberOfAirways == len(dataArray)):
            self.scalarDataName.append(dataName)
            self.scalarDataArray.append(dataArray)
        else:
            print "Number of data doesn't match with the numnber of Airways"
        #
        #
    def updateCellDataScalar(self,dataName,dataArray):
        """        
        Update a scalar cell data already added
        """
        if (self.numberOfAirways == len(dataArray)):
            for i, name in enumerate(self.scalarDataName):
                if (name == dataName):
                    self.scalarDataArray[i] = dataArray
                    break
        else:
            print "Number of data doesn't match with the numnbero f Airways"
        #
        #
    def addCellDataArray(self,dataName,dataArray):
        """        
        Add a array cell data
        """
        if (self.numberOfAirways == len(dataArray)):
            self.arrayDataName.append(dataName)
            self.arrayDataArray.append(dataArray)
        else:
            print "Number of data doesn't match with the numnber of Airways"
        #
        #
    def updateCellDataArray(self,dataName,dataArray):
        """        
        Update a array cell data already added
        """
        if (self.numberOfAirways == len(dataArray)):
            for i, name in enumerate(self.arrayDataName):
                if (name == dataName):
                    self.arrayDataArray[i] = dataArray
                    break
        else:
            print "Number of data doesn't match with the numnbero f Airways"
        #
        # 
    def getCellDataScalar(self,dataName):
        """        
        Get a scalar cell data
        """
        for k, name in enumerate(self.scalarDataName):
            if (name == dataName):
                return self.scalarDataArray[k]

        return [0]
        #
        #
    def export2VTK_3DTree(self,base):
        """
        export tree plot to VTK
        """
        numpc = 20
        cyl1 = [0.0] * numpc
        cyl2 = [0.0] * numpc
        for i in range(numpc):
            th = -np.pi + 2 * np.pi * float(i) / numpc
            cyl1[i] = np.cos(th);
            cyl2[i] = np.sin(th);
        
        radiusData = self.getCellDataScalar('Radius')
        if len(radiusData) < 2:
            radiusData = self.radius
        
        vtkFile = base + '.vtu'
        writer = vtk.vtkXMLUnstructuredGridWriter()
        tree = vtk.vtkUnstructuredGrid()
        cells = vtk.vtkCellArray()
        nodes = vtk.vtkPoints()
        nodes.SetNumberOfPoints(self.numNodes)
        gen = vtk.vtkDoubleArray()
        gen.SetNumberOfComponents(1);
        gen.SetNumberOfTuples(self.numberOfAirways)
        gen.SetName('Generation')
        scalarData = []
        for i, data in enumerate(self.scalarDataArray):
            scalarData.append(vtk.vtkDoubleArray())
            scalarData[i].SetNumberOfComponents(1);
            scalarData[i].SetNumberOfTuples(self.numberOfAirways)
            scalarData[i].SetName(self.scalarDataName[i])
        #
        arrayData = []
        for i, data in enumerate(self.arrayDataArray):
            arrayData.append(vtk.vtkDoubleArray())
            arrayData[i].SetNumberOfComponents(1);
            arrayData[i].SetNumberOfTuples(self.numberOfAirways)
            arrayData[i].SetName(self.arrayDataName[i])
        #
        vertexCount = 0
        for i in range(self.numberOfAirways):
            sp = np.array(self.vertices[self.vertex1[i]])
            ep = np.array(self.vertices[self.vertex2[i]])
            axis = ep - sp
            a1 = np.array([axis[1] - axis[2],10 * axis[2]-axis[0],-10 * axis[1]] + axis[0])
            a2 = np.cross(a1,axis)
            a1 = a1 / np.linalg.norm(a1)
            a2 = a2 / np.linalg.norm(a2)
            rad = radiusData[i]
            if len(self.arrayDataArray) > 0:
                num = len(self.arrayDataArray[0][0])
                #print num
                for d in range(num+1):
                    for j in range(numpc):
                        p = rad * cyl1[j] * a1 + rad * cyl2[j] * a2 + sp + (ep - sp) * float(d) / num
                        nodes.InsertPoint(vertexCount + numpc * d + j, p[0], p[1], p[2])
                        
                for d in range(num):
                    for j in range(numpc):
                        cells.InsertNextCell(4)
                        cells.InsertCellPoint(vertexCount + numpc * d + j)
                        cells.InsertCellPoint(vertexCount + numpc * (d + 1) + j)
                        cells.InsertCellPoint(vertexCount + numpc * (d + 1) + (j + 1) % numpc)
                        cells.InsertCellPoint(vertexCount + numpc * d + (j + 1) % numpc)
                        gen.InsertValue(i * numpc * num  + d * numpc + j,float(self.generationNumber[i]))
                        for k, data in enumerate(self.scalarDataArray):
                            scalarData[k].InsertTuple1(i * numpc * num + d * numpc + j,data[i].tolist())
                        for k, data in enumerate(self.arrayDataArray):
                            arrayData[k].InsertTuple1(i * numpc * num + d * numpc + j,data[i][d].tolist())
                vertexCount = vertexCount + (num + 1) * numpc
            #
            else:
                for j in range(numpc):
                    p = rad * cyl1[j] * a1 + rad * cyl2[j] * a2 + sp
                    nodes.InsertPoint(vertexCount + j, p[0], p[1], p[2])
                for j in range(numpc):
                    p = rad * cyl1[j] * a1 + rad * cyl2[j] * a2 + ep
                    nodes.InsertPoint(vertexCount + numpc + j, p[0], p[1], p[2])
                for j in range(numpc):
                    cells.InsertNextCell(4)
                    cells.InsertCellPoint(vertexCount + j)
                    cells.InsertCellPoint(vertexCount + j + numpc)
                    cells.InsertCellPoint(vertexCount + (j + 1) % numpc + numpc)
                    cells.InsertCellPoint(vertexCount + (j + 1) % numpc)
                    gen.InsertValue(i * numpc + j,float(self.generationNumber[i]))
                    for k, data in enumerate(self.scalarDataArray):
                        scalarData[k].InsertTuple1(i * numpc + j,data[i].tolist())
                vertexCount = vertexCount + numpc * 2
         # export
        tree.SetPoints(nodes)
        tree.SetCells(vtk.VTK_QUAD, cells)
        #tree.SetCells(vtk.VTK_VERTEX, cells)
        tree.GetCellData().AddArray(gen)
        for data in scalarData:
            tree.GetCellData().AddArray(data)
        for data in arrayData:
            tree.GetCellData().AddArray(data)
            
        if vtk.VTK_MAJOR_VERSION >= 6:
            writer.SetInputData(tree)
        else:
            writer.SetInput(tree)
            
        writer.SetFileName(vtkFile)
        writer.Write();
        
        #   
# MAIN
if __name__=="__main__":
    if (len(sys.argv) < 2):
        print "%s [xml airway Data]\n" % (sys.argv[0])
        sys.exit()
    if (False == os.path.isfile(sys.argv[1])):
            sys.exit("xml file not exist")
    #
    lung = xmlLungModel3D(sys.argv[1])
    lung.addCellDataScalar("Radius",np.array(lung.radius))
    lung.addCellDataScalar("Length",np.array(lung.length))
    lung.export2VTK_3DTree(lung.baseFileName)