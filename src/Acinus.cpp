/*
 * Acinus.cpp
 *
 *  Created on: Oct 28, 2014
 *      Author: fuji
 */

#include <cmath>
#include <stdexcept>
#include "Acinus.h"


Acinus::Acinus(int index, PleuralPressureWaveForm *pl) :
		index(index), pl(pl), pressureValue(0), volume(0), surfaceTensionObject(NULL) {
	/// - Define the pressure-volume curve of this acinus.
	/// \f$\frac{V}{V_{FRC}}=a+\frac{b}{1+\exp[-(\Delta P-c)/d]}\f$
	/// 'index' is the global index.
	a = 0.854; /// default a = 0.854
	b = 5.18; /// default b = 5.18
	c = 8.13; /// default c = 8.13
	d = 2.58; /// default d = 2.58
}
Acinus::Acinus(int index, PleuralPressureWaveForm *pl, SurfaceTension *surfaceTensionObject) :
		Acinus(index, pl) {
	this->surfaceTensionObject = surfaceTensionObject;
}

Acinus::~Acinus() {
}


void Acinus::setPressure(double pressureValue) {
	/// * Set Acinus pressure.
	///	* Computing Acinus volume from the pressure.
	volume = volumeFRC * computeVolumeFromPressure(pressureValue - pl->getPressure());
	this->pressureValue = pressureValue;
	/// * Compute Acinus Shear Modulus
	computeShearModulus(pressureValue - pl->getPressure());

	if (surfaceTensionObject != NULL) {
		const double v = volume / numberOfAlveoli;
		const double area = 4 * M_PI * std::pow(3 * v / (4 * M_PI), 2.0 / 3);
		surfaceTensionObject->setSurfaceArea(area);
	}
}


void Acinus::setVolume(double volume) {
	///	* Set Acinus volume.
	///	* Computing Acinus pressure from the volume.
	this->pressureValue = computePressureFromVolume(volume) + pl->getPressure();
	//this->volume = volume;
	this->volume = volumeFRC * computeVolumeFromPressure(this->pressureValue - pl->getPressure());
	/// * Compute Acinus Shear Modulus
	computeShearModulus(pressureValue - pl->getPressure());

	if (surfaceTensionObject != NULL) {
		const double v = this->volume / this->numberOfAlveoli;
		const double area = 4 * M_PI * std::pow(3.0 * v / (4 * M_PI), 2.0 / 3);
		surfaceTensionObject->setSurfaceArea(area);
		//std::cout << this->index << " v=" << v << " area=" << surfaceTensionObject->getSurfaceArea() << std::endl;
	}
	if (this->volume < 0.0){
		std::cout << "negative " << this->volume << std::endl;
	}
}

double Acinus::getPleuralPressure(){
	/// * Get Pleural pressure.
	return pl->getPressure();
}

double Acinus::computeVolumeFromPressure(double pres) {
#ifdef DEBUG
	if(surfaceTensionObject != NULL) {
		std::cout << "WHY COME HERE??\n";
	}
	if (pres <= this->c) {
		std::cout << "Acinus prs=" << pres << std::endl;
	}
#endif
       /// \f$\tilde{V}_{L}=a+\frac{b}{1+\exp[(\Delta P-c)/d]}\f$
       double NormalV = this->a + this->b / (1.0 + std::exp(-(pres - this->c)/this->d));

       return NormalV;
}

double Acinus::computePressureFromVolume(double vol) {
    /// * vol is the dimensional volume, \f$V_L\f$.
	///		+ The normalized volume is  \f$\tilde{V}_{L}=\frac{V_L}{V_{FRC}}\f$.
    double normalV = vol / this->volumeFRC;

#ifdef DEBUG
	if(surfaceTensionObject != NULL) {
		std::cout << "WHY COME HERE??\n";
	}
	if (normalV < this->a) {
		std::cout << "Acinus vol=" << normalV << " a=" << this->a<< std::endl;
	}
#endif

	//std::cout << "vol=" << normalV << std::endl;
	double deltaP;
	if (normalV <= this->a){
		normalV = this->a * 1.01;
	}else{
		if (normalV >= this->a + this->b){
			normalV = (this->a + this->b) * 0.99;
		}
	}
	deltaP = this->c - this->d * std::log(this->b / (normalV - this->a) - 1.0);
	//if (this->b / (normalV - this->a) <= 1.0) {
	//	std::cout << "Error - Log Failure" << " " << this->a << " " << " " << this->b << " " << normalV << std::endl;
	//	normalV = (this->b + this->a) * 0.9;
	//}

	//std::cout << "deltaP=" << deltaP << std::endl;
    
	return deltaP;
}

double Acinus::computeShearModulus(double transmuralPressure) {
	/// * Compute the shear modulus of parenchyma of Acinus
	/// * Using the expression by Fujioka et al. (2013)
	///		+ \f$ \mu=\chi V_{L}/C_{L} \f$, where \f$\chi \f$is a constant, \f$ V_{L} \f$ is the lung volume,
	///		and \f$ C_{L} \f$ is the lung compliance.
	///		+ \f$ C_{L}/V_{FRC}=\frac{d\tilde{V}_{L}}{dP}=\frac{b}{d}\frac{\exp\left[-\left(\Delta P_{tm}-c\right)/d\right]}{\left\{ 1+\exp\left[-\left(\Delta P_{tm}-c\right)/d\right]\right\} ^{2}} \f$
	///		+ \f$\chi\f$=0.893
	/// * Computing the compliance, \f$ C_{L}\f$
	double cl = this->b / this->d * std::exp(-(transmuralPressure - this->c)/this->d) / std::pow(1.0 + std::exp(-(transmuralPressure - this->c)/this->d),2);
	///	* Computing the normalized volume, \f$\tilde{V}_{L}=\frac{V_L}{V_{FRC}}\f$.
	double normalV = volume / this->volumeFRC;
	/// * Computing the shear modulus, \f$ \mu=\chi V_{L}/C_{L} \f$.
	shearModulus = chi * normalV / cl;
	return shearModulus;
}


