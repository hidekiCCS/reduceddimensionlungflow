/*
 * Acinus.h
 *
 *  Created on: Oct 28, 2014
 *      Author: fuji
 *  Modified on Jun 28, 2016
 */

#ifndef ACINUS_H_
#define ACINUS_H_

#include <iostream>
#include "PleuralPressureWaveForm.h"
#include "SurfaceTension.h"

/** This class to define Acinus model.
A sigmoidal equation by Venegas et al. (1998), which is given by

\f$\widetilde{V}_{L}=a+\frac{b}{1+\exp\left[-\left(P-c\right)/d\right]}\f$

where a, b, c, are d are constants,
\f$P=P_{ALV}-P_{PL}\f$. \f$P_{PL}\f$ is the pleural pressure that is provided by PleuralPressureWaveForm class.
This class provide the methods to compute Acinus pressure for a given Volume and Acinus volume for a given pressure.

**/
class Acinus {

public:
	/// Constructor takes a index and a pleural pressure waveform object.
	Acinus(int index, PleuralPressureWaveForm *pl);
	/// Destructor do nothing.
	virtual ~Acinus();

	/// Set the acinar pressure. The acinar volume is computed using the pressure-volume curve.
	void setPressure(double pressureValue);

	/// Get the pleural pressure.
	double getPleuralPressure();

	/// Set the acinar volume. The acinar pressure is computed using the pressure-volume curve.
	void setVolume(double tVolume);

	/// Get the coefficient 'a' of the pressure-volume curve.
	double getA() const {
		return a;
	}

	/// Set the coefficient 'a' of the pressure-volume curve. The default value is 0.854.
	void setA(double a = 0.854) {
		this->a = a;
	}

	/// Get the coefficient 'b' of the pressure-volume curve.
	double getB() const {
		return b;
	}

	/// Set the coefficient 'b' of the pressure-volume curve. The default value is 5.18.
	void setB(double b = 5.18) {
		this->b = b;
	}

	/// Get the coefficient 'c' of the pressure-volume curve.
	double getC() const {
		return c;
	}

	/// Set the coefficient 'c' of the pressure-volume curve. The default value is 8.13.
	void setC(double c = 8.13) {
		this->c = c;
	}

	/// Get the coefficient 'd' of the pressure-volume curve.
	double getD() const {
		return d;
	}

	/// Set the coefficient 'd' of the pressure-volume curve. The default value is 2.58.
	void setD(double d = 2.58) {
		this->d = d;
	}

	/// Get the index of terminal airway connected to this acinus.
	int getIndexForTerminalAirway() const {
		return indexForTerminalAirway;
	}

	/// Set the index of terminal airway connected to this acinus.
	void setIndexForTerminalAirway(int indexForTerminalAirway) {
		this->indexForTerminalAirway = indexForTerminalAirway;
	}

	/// Get the acinar pressure.
	double getPressure() const {
		return pressureValue;
	}

	/// Get the acinar volume.
	double getVolume() const {
		return volume;
	}

	/// Get the acinar volume at FRC.
	double getVolumeFrc() const {
		return volumeFRC;
	}

	/// Set the acinar volume at FRC.
	void setVolumeFrc(double volumeFrc) {
		if (volumeFrc <= 0) std::cout << "Zero FRC?\n";
		volumeFRC = volumeFrc;
		volume = volumeFRC * Acinus::computeVolumeFromPressure(0);
	}

	/// Get the index of this ainus.
	int getIndex() const {
		return index;
	}


	double getShearModulus() const {
		return shearModulus;
	}


	int getNumberOfAlveoli() const {
		return numberOfAlveoli;
	}

	void setNumberOfAlveoli(int numberOfAlveoli) {
		this->numberOfAlveoli = numberOfAlveoli;
	}

	SurfaceTension* getSurfaceTensionObject() const {
		return surfaceTensionObject;
	}

protected:
	Acinus(int index, PleuralPressureWaveForm *pl, SurfaceTension *surfaceTensionObject);
	/// Compute Volume from Pressure.
	virtual double computeVolumeFromPressure(double pres);
	/// Compute Shear Modulus from current Transmural Pressure and Volume.
	virtual double computePressureFromVolume(double vol);
	SurfaceTension *surfaceTensionObject;///< surface tension object


private:
	/// index
	int index;
	/// pressure
	double pressureValue;
	/// volume
	double volume;
	double volumeFRC;/// FRC volume (\f$cm^3\f$)
	int numberOfAlveoli;/// The number of Alveoli

	/// parameter for pressure-volume curve
	double a; /// default value
	double b; /// default value
	double c; /// default value
	double d; /// default value
	/// Index for connecting terminal airway.
	int indexForTerminalAirway;

	/// Pleural pressure object
	PleuralPressureWaveForm *pl;

	/// shearModulus
	double shearModulus;

	/// constant \f$\chi\f$ = 0.893 coefficient for the bulk modulus \f$ K=V_{L}/C_{L}\f$ to the shear modulus, \f$ \mu=\chi K \f$
	const double chi = 0.893;


	double computeShearModulus(double transmuralPressure);
};

#endif /* ACINUS_H_ */
