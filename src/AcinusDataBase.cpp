/*
 * AcinusDataBase.cpp
 *
 *  Created on: Feb 26, 2015
 *  Modified on: Jun 28, 2016
 *      Author: fuji
 */

#include "AcinusDataBase.h"

#ifndef NOMPI
AcinusDataBase::AcinusDataBase(AirwayTreePartitioning *graph, std::vector<Acinus*> acinusList)
:graph(graph),acinusList(acinusList),exportSurfactantStatus(false){
	/// - Create index list that process owns
	localNumberOfAcinus = acinusList.size();
	totalNumberOfAcinus = graph->getNumOfAcinus();
	data_indices = new hsize_t[localNumberOfAcinus+1];
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		data_indices[i] = acinusList.at(i)->getIndex();
	}
}
#endif

AcinusDataBase::AcinusDataBase(std::vector<Acinus*> acinusList):graph(NULL),acinusList(acinusList),exportSurfactantStatus(false) {
	/// - Create index list that process owns
	localNumberOfAcinus = acinusList.size();
	totalNumberOfAcinus = localNumberOfAcinus;
	data_indices = new hsize_t[localNumberOfAcinus+1];
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		data_indices[i] = acinusList[i]->getIndex();
	}
}

AcinusDataBase::~AcinusDataBase() {
	delete [] data_indices;
	/// Delete Acinus objects
	std::vector<Acinus *>::iterator iac = acinusList.begin();
	while (iac != acinusList.end()){
		delete *iac;
		iac = acinusList.erase(iac);
	}
}

bool AcinusDataBase::exportPHDF5dataspace(std::string pdir, hid_t& file) {
	/// - Create data group, "/Acini"
	std::ostringstream datasetName;
	//datasetName << pdir << "Acini/";
	datasetName << "/Acini/";
	hid_t gid;
	if (H5Lexists(file, datasetName.str().c_str(), H5P_DEFAULT) == 1) {
		gid = H5Gopen(file, datasetName.str().c_str(), NULL);
	} else {
		gid = H5Gcreate(file, datasetName.str().c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	}

	/// - Calling exportPHDF5dataspacePressure
	exportPHDF5dataspacePressure(datasetName.str(),file);
	/// - Calling exportPHDF5dataspaceVolume
	exportPHDF5dataspaceVolume(datasetName.str(),file);
	/// - Calling exportPHDF5dataspaceSurfactant if exportSurfactantStatus is true
	if (isExportSurfactantStatus()){
		 exportPHDF5dataspaceSurfaceTension(datasetName.str(),file);
		 exportPHDF5dataspaceSurfaceArea(datasetName.str(),file);
		 exportPHDF5dataspaceGammaP(datasetName.str(),file);
		 exportPHDF5dataspaceGammaS(datasetName.str(),file);
		 exportPHDF5dataspaceBulkC(datasetName.str(),file);
		 exportPHDF5dataspaceLiquidVolume(datasetName.str(),file);
	}
	/// - Close group
	H5Gclose(gid);
	return true;
}

bool AcinusDataBase::importHDF5dataspace(std::string pdir, hid_t &file){
	std::ostringstream datasetName;
	datasetName << "/Acini/";
	/// - Calling importHDF5dataspacePressure
	importHDF5dataspacePressure(datasetName.str(),file);
	/// - Calling exportPHDF5dataspaceVolume
	importHDF5dataspaceVolume(datasetName.str(),file);
	if (isExportSurfactantStatus()){
		 importHDF5dataspaceSurfaceTension(datasetName.str(),file);
		 importHDF5dataspaceSurfaceArea(datasetName.str(),file);
		 importHDF5dataspaceGammaP(datasetName.str(),file);
		 importHDF5dataspaceGammaS(datasetName.str(),file);
		 importHDF5dataspaceBulkC(datasetName.str(),file);
		 importHDF5dataspaceLiquidVolume(datasetName.str(),file);
	}
}

bool AcinusDataBase::exportPHDF5dataspacePressure(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> pressure(localNumberOfAcinus,0.0);
	/// - Loop over Acinus object to collect Pressure
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		pressure[i] = acinusList.at(i)->getPressure();
	}

	/// - Set Dataspace Name "./Pressure"
	std::string dataSpace = pdir + "/Pressure";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAcinus, localNumberOfAcinus, data_indices, pressure.data());
}

bool AcinusDataBase::exportPHDF5dataspaceVolume(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> vol(localNumberOfAcinus,0.0);
	/// - Loop over Acinus object to collect Pressure
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		vol[i] = acinusList.at(i)->getVolume();
	}

	/// - Set Dataspace Name "./Volume"
	std::string dataSpace = pdir + "/Volume";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAcinus, localNumberOfAcinus, data_indices, vol.data());
}

bool AcinusDataBase::exportPHDF5dataspaceSurfaceTension(std::string pdir, hid_t& file) {
	/// - Make an array of surface tension
	std::vector<double> gamma(localNumberOfAcinus,0.0);
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		gamma[i] = ac->getSurfaceTensionObject()->getSurfaceTension();
	}
	/// - Set Dataspace Name "./SurfaceTension"
	std::string dataSpace = pdir + "/SurfaceTension";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAcinus, localNumberOfAcinus, data_indices, gamma.data());
}

bool AcinusDataBase::exportPHDF5dataspaceSurfaceArea(std::string pdir, hid_t& file) {
	/// - Make an array of surface area
	std::vector<double> area(localNumberOfAcinus,0.0);
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		area[i] = ac->getSurfaceTensionObject()->getSurfaceArea();
	}
	/// - Set Dataspace Name "./SurfaceArea"
	std::string dataSpace = pdir + "/SurfaceArea";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAcinus, localNumberOfAcinus, data_indices, area.data());
}

bool AcinusDataBase::exportPHDF5dataspaceGammaP(std::string pdir, hid_t& file) {
	/// - Make an array of GammaP
	std::vector<double> gp(localNumberOfAcinus,0.0);
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		gp[i] = ac->getSurfaceTensionObject()->getGammaP();
	}
	/// - Set Dataspace Name "./GammaP"
	std::string dataSpace = pdir + "/GammaP";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAcinus, localNumberOfAcinus, data_indices, gp.data());
}

bool AcinusDataBase::exportPHDF5dataspaceGammaS(std::string pdir, hid_t& file) {
	/// - Make an array of GammaS
	std::vector<double> gs(localNumberOfAcinus,0.0);
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		gs[i] = ac->getSurfaceTensionObject()->getGammaS();
	}
	/// - Set Dataspace Name "./GammaS"
	std::string dataSpace = pdir + "/GammaS";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAcinus, localNumberOfAcinus, data_indices, gs.data());
}

bool AcinusDataBase::exportPHDF5dataspaceBulkC(std::string pdir, hid_t& file) {
	/// - Make an array of BulkC
	std::vector<double> bulkC(localNumberOfAcinus,0.0);
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		bulkC[i] = ac->getSurfaceTensionObject()->getBulkConcentration();
	}
	/// - Set Dataspace Name "./BulkC"
	std::string dataSpace = pdir + "/BulkC";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAcinus, localNumberOfAcinus, data_indices, bulkC.data());
}

bool AcinusDataBase::exportPHDF5dataspaceLiquidVolume(std::string pdir, hid_t& file) {
	/// - Make an array of Liquid Volume
	std::vector<double> lvol(localNumberOfAcinus,0.0);
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		lvol[i] = ac->getSurfaceTensionObject()->getLiquidVolume();
	}
	/// - Set Dataspace Name "./LiquidVolume"
	std::string dataSpace = pdir + "/LiquidVolume";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAcinus, localNumberOfAcinus, data_indices, lvol.data());
}

bool AcinusDataBase::importHDF5dataspacePressure(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> pressure(localNumberOfAcinus,0.0);
	/// - Set Dataspace Name "./Pressure"
	std::string dataSpace = pdir + "/Pressure";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAcinus, data_indices, pressure.data());

	/// - Loop over Acinus object to collect Pressure
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		acinusList.at(i)->setPressure(pressure[i]);
	}
	return stat;
}

bool AcinusDataBase::importHDF5dataspaceVolume(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> vol(localNumberOfAcinus,0.0);

	/// - Set Dataspace Name "./Volume"
	std::string dataSpace = pdir + "/Volume";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAcinus, data_indices, vol.data());
	/// - Loop over Acinus object to collect Pressure
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		acinusList.at(i)->setVolume(vol[i]);
	}
}

bool AcinusDataBase::importHDF5dataspaceSurfaceTension(std::string pdir, hid_t& file) {
	/// - Make an array of surface tension
	std::vector<double> gamma(localNumberOfAcinus,0.0);

	/// - Set Dataspace Name "./SurfaceTension"
	std::string dataSpace = pdir + "/SurfaceTension";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAcinus, data_indices, gamma.data());
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		ac->getSurfaceTensionObject()->setSurfaceTension(gamma[i]);
	}
	return stat;
}

bool AcinusDataBase::importHDF5dataspaceSurfaceArea(std::string pdir, hid_t& file) {
	/// - Make an array of surface area
	std::vector<double> area(localNumberOfAcinus,0.0);

	/// - Set Dataspace Name "./SurfaceArea"
	std::string dataSpace = pdir + "/SurfaceArea";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAcinus, data_indices, area.data());
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		ac->getSurfaceTensionObject()->setSurfaceArea(area[i]);
	}
	return stat;
}

bool AcinusDataBase::importHDF5dataspaceGammaP(std::string pdir, hid_t& file) {
	/// - Make an array of GammaP
	std::vector<double> gp(localNumberOfAcinus,0.0);

	/// - Set Dataspace Name "./GammaP"
	std::string dataSpace = pdir + "/GammaP";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAcinus, data_indices, gp.data());
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		ac->getSurfaceTensionObject()->setGammaP(gp[i]);
	}
	return stat;
}

bool AcinusDataBase::importHDF5dataspaceGammaS(std::string pdir, hid_t& file) {
	/// - Make an array of GammaS
	std::vector<double> gs(localNumberOfAcinus,0.0);

	/// - Set Dataspace Name "./GammaS"
	std::string dataSpace = pdir + "/GammaS";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAcinus, data_indices, gs.data());
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		ac->getSurfaceTensionObject()->setGammaS(gs[i]);
	}
	return stat;
}

bool AcinusDataBase::importHDF5dataspaceBulkC(std::string pdir, hid_t& file) {
	/// - Make an array of BulkC
	std::vector<double> bulkC(localNumberOfAcinus,0.0);

	/// - Set Dataspace Name "./BulkC"
	std::string dataSpace = pdir + "/BulkC";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAcinus, data_indices, bulkC.data());
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		ac->getSurfaceTensionObject()->setBulkConcentration(bulkC[i]);
	}
	return stat;
}

bool AcinusDataBase::importHDF5dataspaceLiquidVolume(std::string pdir, hid_t& file) {
	/// - Make an array of Liquid Volume
	std::vector<double> lvol(localNumberOfAcinus,0.0);

	/// - Set Dataspace Name "./LiquidVolume"
	std::string dataSpace = pdir + "/LiquidVolume";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAcinus, data_indices, lvol.data());
	for (int i = 0 ; i < localNumberOfAcinus ; i++){
		Acinus *ac = acinusList[i];
		ac->getSurfaceTensionObject()->setLiquidVolume(lvol[i]);
	}
	return stat;
}
