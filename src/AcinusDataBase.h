/*
 * AcinusDataBase.h
 *
 *  Created on: Feb 26, 2015
 *      Author: fuji
 */

#ifndef ACINUSDATABASE_H_
#define ACINUSDATABASE_H_

#include "DataBase.h"
#ifndef NOMPI
#include "AirwayTreePartitioning.h"
#endif
#include "Acinus.h"

/// Class for import/export Acinus status from/to file system
class AcinusDataBase: virtual public DataBase {
public:
#ifndef NOMPI
	/// Constructor takes AirwayTreePartitioning and a list of Acinus objects
	AcinusDataBase(AirwayTreePartitioning *graph,std::vector<Acinus *> acinusList);
#endif
	/// Constructor takes a list of Acinus objects (single process)
	AcinusDataBase(std::vector<Acinus *> acinusList);
	/// Destructor does nothing
	virtual ~AcinusDataBase();

	/// export Acinus status to HDF5 file
	virtual bool exportPHDF5dataspace(std::string pdir, hid_t &file);

	/// import Acinus status from HDF5 file
	virtual bool importHDF5dataspace(std::string pdir, hid_t &file);

	bool isExportSurfactantStatus() const {
		return exportSurfactantStatus;
	}

protected:
	void setExportSurfactantStatus(bool exportSurfactantStatus) {
		this->exportSurfactantStatus = exportSurfactantStatus;
	}

private:
	/// export Acinus Pressure to HDF5 file
	bool exportPHDF5dataspacePressure(std::string pdir, hid_t &file);
	/// export Acinus Volume to HDF5 file
	bool exportPHDF5dataspaceVolume(std::string pdir, hid_t &file);
	/// export Surface Tension in Acinus to HDF5 file
	bool exportPHDF5dataspaceSurfaceTension(std::string pdir, hid_t &file);
	/// export Surface Area in Acinus to HDF5 file
	bool exportPHDF5dataspaceSurfaceArea(std::string pdir, hid_t &file);
	/// export Primary Layer Surfactant Concentration in Acinus to HDF5 file
	bool exportPHDF5dataspaceGammaP(std::string pdir, hid_t &file);
	/// export Secondary Layer Surfactant Concentration in Acinus to HDF5 file
	bool exportPHDF5dataspaceGammaS(std::string pdir, hid_t &file);
	/// export Bulk Surfactant Concentration in Acinus to HDF5 file
	bool exportPHDF5dataspaceBulkC(std::string pdir, hid_t &file);
	/// export Liquid Volume in Acinus to HDF5 file
	bool exportPHDF5dataspaceLiquidVolume(std::string pdir, hid_t &file);

	/// import Acinus Pressure to HDF5 file
	bool importHDF5dataspacePressure(std::string pdir, hid_t &file);
	/// import Acinus Volume to HDF5 file
	bool importHDF5dataspaceVolume(std::string pdir, hid_t &file);
	/// export Surface Tension in Acinus to HDF5 file
	bool importHDF5dataspaceSurfaceTension(std::string pdir, hid_t &file);
	/// export Surface Area in Acinus to HDF5 file
	bool importHDF5dataspaceSurfaceArea(std::string pdir, hid_t &file);
	/// export Primary Layer Surfactant Concentration in Acinus to HDF5 file
	bool importHDF5dataspaceGammaP(std::string pdir, hid_t &file);
	/// export Secondary Layer Surfactant Concentration in Acinus to HDF5 file
	bool importHDF5dataspaceGammaS(std::string pdir, hid_t &file);
	/// export Bulk Surfactant Concentration in Acinus to HDF5 file
	bool importHDF5dataspaceBulkC(std::string pdir, hid_t &file);
	/// export Liquid Volume in Acinus to HDF5 file
	bool importHDF5dataspaceLiquidVolume(std::string pdir, hid_t &file);

	/// Object of AirwayTreePartitioning
	AirwayTreePartitioning *graph;
	/// List of Acinus object
	std::vector<Acinus *> acinusList;

	/// Data indices that process owns
	hsize_t *data_indices;
	/// number of local Acinus objects
	hsize_t localNumberOfAcinus;
	/// total number of Acinus objects
	hsize_t totalNumberOfAcinus;

	bool exportSurfactantStatus;///< Flag to export surfactant status, false in default

	static constexpr char dbName[] = "Acinus Database";///< Database Name
};

#endif /* ACINUSDATABASE_H_ */
