/*
 * AcinusWithSurfactant.cpp
 *
 *  Created on: Jun 27, 2016
 *      Author: fuji
 */

#include "AcinusWithSurfactant.h"
#include <cmath>

AcinusWithSurfactant::AcinusWithSurfactant(int index, PleuralPressureWaveForm* pl, Surfactant *surfaceTensionObject) :
		Acinus(index, pl, surfaceTensionObject) {
	/// Define the pressure-volume curve of this acinus.
	/// \f$\frac{V}{V_{FRC}}=a+\frac{b}{1+\exp[-(\Delta P-c)/d]}\f$
	/// Setting "Zero surface tension" lung
	/// 'index' is the global index.
	this->setA(1.03);	///- a=1.03
	this->setB(4.95);	///- b=4.95
	this->setC(5.04);	///- c=5.04
	this->setD(1.02);	///- d=1.02
}

AcinusWithSurfactant::~AcinusWithSurfactant() {
	// TODO Auto-generated destructor stub
}

double AcinusWithSurfactant::computeVolumeFromPressure(double pres) {
	const double surfaceTension = surfaceTensionObject->getSurfaceTension();
	const double alveolarVolume = this->getVolume() / this->getNumberOfAlveoli();
	const double alveolarRadius = std::pow(3 * alveolarVolume / (4 * M_PI), 1.0 / 3);
	const double liquidPressure = pres - 2 * surfaceTension / alveolarRadius / cmH2OtoDynsParSquareCm;
	//std::cout << "liquidPressure=" << liquidPressure << std::endl;
#ifdef DEBUG
	if (pres <= this->getC()) {
		std::cout << "Acinus Liquid Pressure =" << liquidPressure << std::endl;
	}
#endif
	double NormalV = this->getA() + this->getB() / (1.0 + std::exp(-(liquidPressure - this->getC()) / this->getD()));

	return NormalV;
}

double AcinusWithSurfactant::computePressureFromVolume(double vol) {
	double normalV = vol / this->getVolumeFrc();
#ifdef DEBUG
	if (normalV < this->getA()) {
		std::cout << "Acinus vol=" << normalV << " a=" << this->getA()<< std::endl;
	}
#endif
	double liquidPressure;
	if (normalV <= this->getA()){
		normalV = this->getA() * 1.01;
	}else{
		if (normalV >= this->getA() + this->getB()){
			normalV = (this->getA() + this->getB()) * 0.99;
		}
	}
	liquidPressure = this->getC() - this->getD() * std::log(this->getB() / (normalV - this->getA()) - 1.0);
	//if (this->getB() / (normalV - this->getA()) <= 1.0) {
	//	std::cout << "Error - Log Failure" << " " << this->getA() << " " << " " << this->getB() << " " << normalV << std::endl;
	//	normalV = std::min(normalV,this->getA() + this->getB() / (1.0 + std::exp(-(30 - this->getC()) / this->getD())));
	//	normalV = std::max(normalV,this->getA()*1.01);
	//}
	//const double liquidPressure = this->getC() - this->getD() * std::log(this->getB() / (normalV - this->getA()) - 1.0);
	const double surfaceTension = surfaceTensionObject->getSurfaceTension();
	const double alveolarVolume = this->getVolume() / this->getNumberOfAlveoli();
	const double alveolarRadius = std::pow(3 * alveolarVolume / (4 * M_PI), 1.0 / 3);
	const double airPressure = liquidPressure + 2 * surfaceTension / alveolarRadius / cmH2OtoDynsParSquareCm;
	return airPressure;
}
