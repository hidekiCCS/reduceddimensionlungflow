/*
 * AcinusWithSurfactant.h
 *
 *  Created on: Jun 27, 2016
 *      Author: fuji
 */

#ifndef ACINUSWITHSURFACTANT_H_
#define ACINUSWITHSURFACTANT_H_

#include "Acinus.h"
#include "Surfactant.h"

class AcinusWithSurfactant: public Acinus {
public:
	AcinusWithSurfactant(int index, PleuralPressureWaveForm *pl,Surfactant *surfaceTensionObject);
	virtual ~AcinusWithSurfactant();

protected:
	virtual double computeVolumeFromPressure(double pres);
	virtual double computePressureFromVolume(double vol);
private:
	/// Conversion \f$cmH_2O\f$ to \f$Dyns/cm^2\f$
	static constexpr double cmH2OtoDynsParSquareCm = 980.665;
};

#endif /* ACINUSWITHSURFACTANT_H_ */
