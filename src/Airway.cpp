/*
 * Airway.cpp
 *
 *  Created on: Oct 28, 2014
 *      Author: fuji
 */

#include <cmath>
#include <cstdlib>
#include "Airway.h"

Airway::Airway(int index, PleuralPressureWaveForm *pl): index(index),pl(pl),flowRate(0),pressure(0),conductance(1.0){
	surfaceTensionObject = new SurfaceTension();
}
Airway::Airway(int index, PleuralPressureWaveForm *pl, SurfaceTension *surfaceTensionObject)
: index(index),pl(pl),flowRate(0),pressure(0),surfaceTensionObject(surfaceTensionObject),conductance(1.0){
}

Airway::~Airway() {
}

void Airway::computeLiquidVolume() {
	liquidVolume = M_PI * length * std::pow(radius,2) * eps * (2 - eps);
}

double Airway::computeAirwayConductance(void) {
	/// compute conductance of a rigid tube with liquid lining.
	double r = radius * (1 - eps);
	conductance = (M_PI * std::pow(r,4)) / (8.0 * fluidViscosity * length);
    return conductance;
}

void Airway::computeEpsilon() {
	double sq = 1.0 - liquidVolume / (M_PI * length * std::pow(radius,2));
	if (sq <= 0.0){
		std::cout << "Airway " << this->getIndex() << " has too much liquid " << this->liquidVolume << std::endl;
		radius = std::sqrt(liquidVolume / (M_PI * length)) * 1.01;
		std::cout << "Force radius to " << radius << std::endl;
	}
	eps = 1.0 - std::sqrt(sq);
}

double Airway::computeRadiusAtZeroPressure() {
	return this->radius;
}
