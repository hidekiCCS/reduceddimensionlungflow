/*
 * Airway.h
 *
 *  Created on: Oct 28, 2014
 *  Modified on : Feb 9, 2015
 *      Author: fuji
 */

#ifndef AIRWAY_H_
#define AIRWAY_H_

#include <iostream>
#include "PleuralPressureWaveForm.h"
#include "SurfaceTension.h"

/// Airway Base Class
class Airway {
	public:
		/// Constructor takes a index and a pleural pressure waveform objects.
		Airway(int index, PleuralPressureWaveForm *pl);

		/// Constructor takes a index, a pleural pressure waveform and a surface tension objects.
		Airway(int index, PleuralPressureWaveForm *pl, SurfaceTension *surfaceTensionObject);

		/// Destructor do nothing.
		virtual ~Airway();

		/// Compute Airway Conductance = 1/Resistance
		virtual double computeAirwayConductance(void);

		/// Compute the liquid volume from a given \f$\epsilon\f$ value.
		void computeLiquidVolume();

		/// Compute \f$\epsilon\f$ from a given liquid volume.
		virtual void computeEpsilon();

		/// Compute Radius at Zero Pressure
		virtual double computeRadiusAtZeroPressure();

		/// If this airway is a terminal airway, this method returns the index of acinus connecting this airway, otherwise, returns -1.
		int getAicnusIndex() const {
			return aicnus_index;
		}

		/// Set the index of acinus.
		void setAicnusIndex(int aicnusIndex) {
			aicnus_index = aicnusIndex;
		}

		/// Get \f$\epsilon\f$ value.
		double getEps() const {
			return eps;
		}

		/// Set \f$\epsilon\f$ value.
		void setEps(double eps) {
			this->eps = eps;
			/// This method computes the liquid volume and set it to Airway::liquidVolume
			computeLiquidVolume();
		}

		/// Get the flowrate.
		double getFlowRate() const {
			return flowRate;
		}

		/// Set the flowrate.
		virtual void setFlowRate(double flowRate) {
			this->flowRate = flowRate;
		}

		/// Get the air viscosity.
		double getFluidViscosity() const {
			return fluidViscosity;
		}

		/// Set the air viscosity.
		void setFluidViscosity(double fluidViscosity) {
			this->fluidViscosity = fluidViscosity;
		}

		/// Get the bottom-up generation number.
		int getGenerationNumber() const {
			return generationNumber;
		}

		/// Set the bottom-up generation number.
		void setGenerationNumber(int generationNumber) {
			this->generationNumber = generationNumber;
		}

		/// Get the length of airway.
		double getLength() const {
			return length;
		}

		/// Set the length of airway.
		void setLength(double length) {
			this->length = length;
		}

		/// Get the volume of liquid.
		double getLiquidVolume() const {
			return liquidVolume;
		}

		/// Set the volume of liquid.
		void setLiquidVolume(double liqvol) {
			this->liquidVolume = liqvol;
			/// This method computes \f$\epsilon\f$ and set it to Airway::eps
			computeEpsilon();
		}

		/// Get the index of lower bifurcation object.
		int getLowerBifurcationIndex() const {
			return lower_bifurcation_index;
		}

		/// Set the index of lower bifurcation object.
		void setLowerBifurcationIndex(int lowerBifurcationIndex) {
			lower_bifurcation_index = lowerBifurcationIndex;
		}

		/// Get the inner pressure.
		double getPressure() const {
			return pressure;
		}

		/// Set the inner pressure.
		void setPressure(double pressure) {
			this->pressure = pressure;
		}

		/// Get the radius.
		double getRadius() const {
			return radius;
		}

		/// Set the radius.
		void setRadius(double radius) {
			this->radius = std::min(radius, rmax);
		}

		/// for tublelaw
		virtual double getRadiusRest() const {
			return radius;
		}

		/// Get the maximum radius.
		double getRmax() const {
			return rmax;
		}

		/// Set the maximum radius.
		virtual void setRmax(double rmax) {
			this->rmax = rmax;
		}

		/// Get the index of upper bifurcation object.
		int getUpperBifurcationIndex() const {
			return upper_bifurcation_index;
		}

		/// Set the index of upper bifurcation object.
		void setUpperBifurcationIndex(int upperBifurcationIndex) {
			upper_bifurcation_index = upperBifurcationIndex;
		}

		/// Get the index.
		int getIndex() const {
			return index;
		}

		/// Get the surface tension value.
		double getSurfaceTension() const {
			return surfaceTensionObject->getSurfaceTension();
		}

		/// Set the surface tension value.
		void setSurfaceTension(double surfaceTension) {
			surfaceTensionObject->setSurfaceTension(surfaceTension);
		}

		/// Get the pressure drop across the airway.
		double getPressureDrop() const {
			return delta_pres;
		}

		/// Set the pressure drop across the airway.
		void setPressureDrop(double deltaPres) {
			delta_pres = deltaPres;
		}

		double getConductance() const {
			return conductance;
		}

		double getTransmuralPressure() const {
			return transmuralPressure;
		}

		double getLiquidViscosityCm2H2O() const {
			return liquidViscosity_cm2H2O;
		}

		void setLiquidViscosityCm2H2O(double liquidViscosityCm2H2O) {
			liquidViscosity_cm2H2O = liquidViscosityCm2H2O;
		}

		SurfaceTension* getSurfaceTensionObject() const {
			return surfaceTensionObject;
		}

	protected:
		SurfaceTension *surfaceTensionObject; ///< surface tension object

		/// Global Airway Index.
		int index;
		/// Index of upper Bifurcation object
		int upper_bifurcation_index; /// -1 if not applicable
		/// Index of lower Bifurcation object
		int lower_bifurcation_index; /// -1 if not applicable
		/// Index of Acinus object
		int aicnus_index;            /// -1 if not applicable
		/// Bottom-up Generation number
		int generationNumber;
		/// Tube inner radius
		double radius;
		/// Luminal pressure
		double pressure;
		/// Pressure drop acress the airway
		double delta_pres;
		/// The maximum radius, \f$R_{max}\f$
		double rmax;
		/// Dimensionless film thickness
		double eps;

		///  Volume of Liquid lining, this excludes the volume of plugs.
		double liquidVolume;
		/// Airway Length
		double length;
		/// Fluid (air) Viscosity
		double fluidViscosity;
		/// Flow rate
		double flowRate;
		/// Conductance
		double conductance;
		/// Transmural Pressure
		double transmuralPressure;
		/// Liquid Viscosity in cm s H20
		double liquidViscosity_cm2H2O;

		/// Pleural Pressure Wave From Object
		PleuralPressureWaveForm *pl;

};

#endif /* AIRWAY_H_ */
