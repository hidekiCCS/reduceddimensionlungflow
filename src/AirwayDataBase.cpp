/*
 * AirwayDataBase.cpp
 *
 *  Created on: Feb 26, 2015
 *      Author: fuji
 */

#include "AirwayDataBase.h"

#ifndef NOMPI
AirwayDataBase::AirwayDataBase(AirwayTreePartitioning * graph, std::vector<Airway*> airwayList) : graph(graph),airwayList(airwayList){
	/// - Create index list that process owns
	localNumberOfAirway = airwayList.size();
	totalNumberOfAirway = graph->getNumOfAirways();
	data_indices = new hsize_t[localNumberOfAirway];
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		data_indices[i] = airwayList[i]->getIndex();
	}
}
#endif
AirwayDataBase::AirwayDataBase(std::vector<Airway*> airwayList) : graph(NULL),airwayList(airwayList){
	/// - Create index list that process owns
	localNumberOfAirway = airwayList.size();
	totalNumberOfAirway = localNumberOfAirway;
	data_indices = new hsize_t[localNumberOfAirway];
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		data_indices[i] = airwayList[i]->getIndex();
	}
}

AirwayDataBase::~AirwayDataBase() {
	delete [] data_indices;
	/// Delete Airway Objects
	std::vector<Airway *>::iterator iaw = airwayList.begin();
	while (iaw != airwayList.end()){
		delete *iaw;
		iaw = airwayList.erase(iaw);
	}
}

bool AirwayDataBase::exportPHDF5dataspace(std::string pdir, hid_t& file) {
	/// - Create data group, "/Airways"
	std::ostringstream datasetName;
	//datasetName << pdir << "/Airways/";
	datasetName << "/Airways/";
	hid_t gid;
	if (H5Lexists(file, datasetName.str().c_str(), H5P_DEFAULT) == 1) {
		gid = H5Gopen(file, datasetName.str().c_str(), NULL);
	} else {
		gid = H5Gcreate(file, datasetName.str().c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	}

	/// - Calling exportPHDF5dataspaceFlowRate
	exportPHDF5dataspaceFlowRate(datasetName.str(),file);
	/// - Calling exportPHDF5dataspacePressure
	exportPHDF5dataspacePressure(datasetName.str(),file);
	/// - Calling exportPHDF5dataspaceTransmuralPressure
	exportPHDF5dataspaceTransmuralPressure(datasetName.str(),file);
	/// - Calling exportPHDF5dataspaceRadius
	exportPHDF5dataspaceRadius(datasetName.str(),file);
	/// - Calling exportPHDF5dataspaceLength
	exportPHDF5dataspaceLength(datasetName.str(),file);
	/// - Calling exportPHDF5dataspaceEpsilon
	exportPHDF5dataspaceEpsilon(datasetName.str(),file);
	/// - Calling exportPHDF5dataspaceLiquidVolume
	exportPHDF5dataspaceLiquidVolume(datasetName.str(),file);
	/// - Calling exportPHDF5dataspaceConductance
	exportPHDF5dataspaceConductance(datasetName.str(),file);

	/// - Close group
	H5Gclose(gid);
	return true;
}

bool AirwayDataBase::importHDF5dataspace(std::string pdir, hid_t& file) {
	std::ostringstream datasetName;
	datasetName << "/Airways/";
	/// - Calling importHDF5dataspaceFlowRate
	importHDF5dataspaceFlowRate(datasetName.str(),file);
	/// - Calling importHDF5dataspacePressure
	importHDF5dataspacePressure(datasetName.str(),file);
	/// - Calling importHDF5dataspaceRadius
	importHDF5dataspaceRadius(datasetName.str(),file);
	/// - Calling importHDF5dataspaceLength
	importHDF5dataspaceLength(datasetName.str(),file);
	/// - Calling importHDF5dataspaceEpsilon
	importHDF5dataspaceEpsilon(datasetName.str(),file);
	/// - Calling eimportHDF5dataspaceLiquidVolume
	importHDF5dataspaceLiquidVolume(datasetName.str(),file);
}

bool AirwayDataBase::exportPHDF5dataspaceFlowRate(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> flowrate(localNumberOfAirway,0.0);
	/// - Loop over Airway object to collect FlowRate
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		flowrate[i] = airwayList[i]->getFlowRate();
	}

	/// - Set Dataspace Name "./FlowRate"
	std::string dataSpace = pdir + "/FlowRate";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAirway, localNumberOfAirway, data_indices, flowrate.data());
}

bool AirwayDataBase::exportPHDF5dataspacePressure(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> pressure(localNumberOfAirway,0.0);
	/// - Loop over Airway object to collect Pressure
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		pressure[i] = airwayList[i]->getPressure();
	}

	/// - Set Dataspace Name "./Pressure"
	std::string dataSpace = pdir + "/Pressure";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAirway, localNumberOfAirway, data_indices, pressure.data());
}


bool AirwayDataBase::exportPHDF5dataspaceTransmuralPressure(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> pressure(localNumberOfAirway,0.0);
	/// - Loop over Airway object to collect Pressure
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		pressure[i] = airwayList[i]->getTransmuralPressure();
	}

	/// - Set Dataspace Name "./TransmuralPressure"
	std::string dataSpace = pdir + "/TransmuralPressure";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAirway, localNumberOfAirway, data_indices, pressure.data());
}


bool AirwayDataBase::exportPHDF5dataspaceRadius(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> radius(localNumberOfAirway,0.0);
	/// - Loop over Airway object to collect Radius
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		radius[i] = airwayList[i]->getRadius();
	}

	/// - Set Dataspace Name "./Radius"
	std::string dataSpace = pdir + "/Radius";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAirway, localNumberOfAirway, data_indices, radius.data());
}

bool AirwayDataBase::exportPHDF5dataspaceEpsilon(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> eps(localNumberOfAirway,0.0);
	/// - Loop over Airway object to collect Epsilon value
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		eps[i] = airwayList[i]->getEps();
	}

	/// - Set Dataspace Name "./EPS"
	std::string dataSpace = pdir + "/EPS";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAirway, localNumberOfAirway, data_indices, eps.data());
}

bool AirwayDataBase::exportPHDF5dataspaceLiquidVolume(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> vol(localNumberOfAirway,0.0);
	/// - Loop over Airway object to collect Liquid Volume
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		vol[i] = airwayList[i]->getLiquidVolume();
	}

	/// - Set Dataspace Name "./LiquidVolume"
	std::string dataSpace = pdir + "/LiquidVolume";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAirway, localNumberOfAirway, data_indices, vol.data());
}

bool AirwayDataBase::exportPHDF5dataspaceLength(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> length(localNumberOfAirway,0.0);
	/// - Loop over Airway object to collect Radius
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		length[i] = airwayList[i]->getLength();
	}

	/// - Set Dataspace Name "./Length"
	std::string dataSpace = pdir + "/Length";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAirway, localNumberOfAirway, data_indices, length.data());
}

bool AirwayDataBase::exportPHDF5dataspaceConductance(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> cond(localNumberOfAirway,0.0);
	/// - Loop over Airway object to collect Conductance calling Airway::computeAirwayConductance
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		cond[i] = airwayList[i]->getConductance();
	}

	/// - Set Dataspace Name "./Conductance"
	std::string dataSpace = pdir + "/Conductance";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, totalNumberOfAirway, localNumberOfAirway, data_indices, cond.data());
}


bool AirwayDataBase::importHDF5dataspaceFlowRate(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> flowrate(localNumberOfAirway,0.0);

	/// - Set Dataspace Name "./FlowRate"
	std::string dataSpace = pdir + "/FlowRate";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAirway, data_indices, flowrate.data());
	/// - Loop over Airway object to collect FlowRate
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		airwayList[i]->setFlowRate(flowrate[i]);
	}
	return stat;
}

bool AirwayDataBase::importHDF5dataspacePressure(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> pressure(localNumberOfAirway,0.0);

	/// - Set Dataspace Name "./Pressure"
	std::string dataSpace = pdir + "/Pressure";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAirway, data_indices, pressure.data());
	/// - Loop over Airway object to collect Pressure
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		airwayList[i]->setPressure(pressure[i]);
	}
	return stat;
}

bool AirwayDataBase::importHDF5dataspaceRadius(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> radius(localNumberOfAirway,0.0);

	/// - Set Dataspace Name "./Radius"
	std::string dataSpace = pdir + "/Radius";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAirway, data_indices, radius.data());
	/// - Loop over Airway object to collect Radius
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		airwayList[i]->setRadius(radius[i]);
	}
	return stat;
}

bool AirwayDataBase::importHDF5dataspaceEpsilon(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> eps(localNumberOfAirway,0.0);

	/// - Set Dataspace Name "./EPS"
	std::string dataSpace = pdir + "/EPS";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAirway, data_indices, eps.data());
	/// - Loop over Airway object to collect Epsilon value
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		airwayList[i]->setEps(eps[i]);
	}
	return stat;
}

bool AirwayDataBase::importHDF5dataspaceLiquidVolume(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> vol(localNumberOfAirway,0.0);

	/// - Set Dataspace Name "./LiquidVolume"
	std::string dataSpace = pdir + "/LiquidVolume";
	bool stat =importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAirway, data_indices, vol.data());
	/// - Loop over Airway object to collect Liquid Volume
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		airwayList[i]->setLiquidVolume(vol[i]);
	}
	return stat;
}

bool AirwayDataBase::importHDF5dataspaceLength(std::string pdir, hid_t& file) {
	/// - Create a array
	std::vector<double> length(localNumberOfAirway,0.0);

	/// - Set Dataspace Name "./Length"
	std::string dataSpace = pdir + "/Length";
	bool stat = importHDF5dataspaceDouble1dIndex(dataSpace, file, localNumberOfAirway, data_indices, length.data());
	/// - Loop over Airway object to collect Radius
	for (int i = 0 ; i < localNumberOfAirway ; i++){
		airwayList[i]->setLength(length[i]);
	}
	return stat;
}

