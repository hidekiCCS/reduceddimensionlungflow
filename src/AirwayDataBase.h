/*
 * AirwayDataBase.h
 *
 *  Created on: Feb 26, 2015
 *      Author: fuji
 */

#ifndef AIRWAYDATABASE_H_
#define AIRWAYDATABASE_H_

#include "DataBase.h"
#ifndef NOMPI
#include "AirwayTreePartitioning.h"
#endif
#include "Airway.h"

/// Class for import/export Airway status from/to file system
class AirwayDataBase: virtual public DataBase {
public:
#ifndef NOMPI
	/// Constructor takes AirwayTreePartitioning and a list of Airway objects
	AirwayDataBase(AirwayTreePartitioning * graph,std::vector<Airway *> airwayList);
#endif
	/// Constructor takes a list of Airway objects (single process)
	AirwayDataBase(std::vector<Airway *> airwayList);
	/// Destructor does nothing important
	virtual ~AirwayDataBase();

	/// export Airway status to HDF5 file
	virtual bool exportPHDF5dataspace(std::string pdir, hid_t &file);
	/// import Airway status from HDF5 file
	virtual bool importHDF5dataspace(std::string pdir, hid_t &file);

private:
	/// export Airway FlowRate to HDF5 file
	bool exportPHDF5dataspaceFlowRate(std::string pdir, hid_t &file);
	/// export Airway Pressure to HDF5 file
	bool exportPHDF5dataspacePressure(std::string pdir, hid_t &file);
	/// export Airway Transmural Pressure to HDF5 file
	bool exportPHDF5dataspaceTransmuralPressure(std::string pdir, hid_t &file);
	/// export Airway Radius to HDF5 file
	bool exportPHDF5dataspaceRadius(std::string pdir, hid_t &file);
	/// export Airway Length to HDF5 file
	bool exportPHDF5dataspaceLength(std::string pdir, hid_t &file);
	/// export Airway Film Thickness to HDF5 file
	bool exportPHDF5dataspaceEpsilon(std::string pdir, hid_t &file);
	/// export Airway Liquid volume to HDF5 file
	bool exportPHDF5dataspaceLiquidVolume(std::string pdir, hid_t &file);
	/// export Airway Conductance to HDF5 file
	bool exportPHDF5dataspaceConductance(std::string pdir, hid_t &file);

	/// import Airway FlowRate to HDF5 file
	bool importHDF5dataspaceFlowRate(std::string pdir, hid_t &file);
	/// import Airway Pressure to HDF5 file
	bool importHDF5dataspacePressure(std::string pdir, hid_t &file);
	/// import Airway Radius to HDF5 file
	bool importHDF5dataspaceRadius(std::string pdir, hid_t &file);
	/// import Airway Length to HDF5 file
	bool importHDF5dataspaceLength(std::string pdir, hid_t &file);
	/// import Airway Film Thickness to HDF5 file
	bool importHDF5dataspaceEpsilon(std::string pdir, hid_t &file);
	/// import Airway Liquid volume to HDF5 file
	bool importHDF5dataspaceLiquidVolume(std::string pdir, hid_t &file);


	/// Object of AirwayTreePartitioning
	AirwayTreePartitioning *graph;
	/// List of Airway objects
	std::vector<Airway *> airwayList;

	/// Data indices that process owns
	hsize_t *data_indices;
	/// number of local Airway objects
	hsize_t localNumberOfAirway;
	/// total number of Airway objects
	hsize_t totalNumberOfAirway;

	static constexpr char dbName[] = "Airway Database";///< Database Name
};

#endif /* AIRWAYDATABASE_H_ */
