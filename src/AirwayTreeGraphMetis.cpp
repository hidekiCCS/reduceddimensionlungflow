/*
 * AirwayTreeGraphMetis.cpp
 *
 *  Created on: Oct 28, 2014
 *      Author: fuji
 */

#include <vector>
#include <fstream>
#include <iomanip>
#include <mpi.h>
#include "AirwayTreeGraphMetis.h"

AirwayTreeGraphMetis::AirwayTreeGraphMetis(LungLobeFromSimpleAirwayTree* lung) :
		lung(lung) {
    MPI_Comm_size(MPI_COMM_WORLD, &numproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	/// Get numbers
	numOfBifurcation = lung->get_NumberOfBifurcations();
	numOfAirways = lung->get_numberOfAirways();
	numOfAcinus = lung->get_NumberOfAcinus();
	/// allocate table
	part_table_bifurcation = new int[numOfBifurcation];
	part_table_airway = new int[numOfAirways];
	part_table_acinus = new int[numOfAcinus];
	part_table_inlet = -1;
#ifdef DEBUG
	std::cout << "# of Bifurcation " << numOfBifurcation << std::endl;
	std::cout << "# of Airways " << numOfAirways << std::endl;
	std::cout << "# of Acinus " << numOfAcinus << std::endl;
#endif
	///
	/// Making Graph data
	///
	xadj = new idx_t[numOfBifurcation + numOfAcinus + 2];
	adjency = new idx_t[numOfAirways * 2];
	element_count = 0;
	node_count = 0;
	std::vector<int> element_stack;
	int intel_element;
	/// bifurcation
	for (int i = 0; i < numOfBifurcation; i++) {
		xadj[node_count] = element_count;
		part_table_bifurcation[i] = node_count;
		int pt = lung->get_ParentTubeIndex(i);
		int dt1 = lung->get_DaughterTube1Index(i);
		int dt2 = lung->get_DaughterTube2Index(i);
		/// for parent tube
		int bif_up = lung->get_UpperBifurcationIndex(pt);
		part_table_airway[pt] = node_count;
		if (bif_up >= 0) {
			adjency[element_count] = bif_up;
			element_count++;
		} else {
			// this is the mouth
			part_table_inlet = pt;
			intel_element = element_count;
			element_count++;
		}
		/// for daughter tube 1
		int bif_low1 = lung->get_LowerBifurcationIndex(dt1);
		//part_table_airway[dt1] = element_count;
		if (bif_low1 >= 0) {
			adjency[element_count] = bif_low1;
			element_count++;
		} else {
			// this is a acinus
			adjency[element_count] = dt1;
			element_stack.push_back(element_count);
			element_count++;
		}
		/// for daughter tube 2
		int bif_low2 = lung->get_LowerBifurcationIndex(dt2);
		//part_table_airway[dt2] = element_count;
		if (bif_low2 >= 0) {
			adjency[element_count] = bif_low2;
			element_count++;
		} else {
			// this is a acinus
			adjency[element_count] = dt2;
			element_stack.push_back(element_count);
			element_count++;
		}
		node_count++;
	}
	/// acinus
	for (int i = 0; i < numOfAcinus; i++) {
		xadj[node_count] = element_count;
		part_table_acinus[i] = node_count;
		int teminal_bronchi = lung->get_TerminalAirway(i);
		part_table_airway[teminal_bronchi] = node_count;
		std::vector<int>::iterator itr = element_stack.begin();
		while (itr != element_stack.end()) {
			if (adjency[*itr] == teminal_bronchi) {
				adjency[element_count] = lung->get_UpperBifurcationIndex(teminal_bronchi);
				element_count++;
				adjency[*itr] = node_count;
				element_stack.erase(itr);
				break;
			} else {
				itr++;
			}
#ifdef DEBUG
			if (itr == element_stack.end()) {
				std::cout << "stranger things have happened\n";
			}
#endif
		}
		node_count++;
	}
	/// mouth
	xadj[node_count] = element_count;
	adjency[element_count] = lung->get_LowerBifurcationIndex(part_table_inlet);
	element_count++;
	adjency[intel_element] = node_count;
	part_table_inlet = node_count;
	node_count++;
	xadj[node_count] = element_count;

#ifdef DEBUG
	std::cout << "# of Nodes " << node_count << std::endl;
	std::cout << "# of Elements " << element_count << std::endl;
#endif
}

AirwayTreeGraphMetis::~AirwayTreeGraphMetis() {
	delete [] part_table_bifurcation;
	delete [] part_table_airway;
	delete [] part_table_acinus;
	delete [] xadj;
	delete [] adjency;

	delete [] vectorIndexOfBifurcationNode;
	delete [] vectorIndexOfAirwayNode;
	delete [] vectorIndexOfAcinusNode;
}

void AirwayTreeGraphMetis::partitioning(idx_t nparts) {
	if (nparts > 1) {
		idx_t nn = node_count;
		idx_t ne = xadj[node_count];
		idx_t objval;
		idx_t *epart = new idx_t[ne];
		idx_t *npart = new idx_t[nn];

		/// **Currently we are using METIS, which run serial. We have to use ParMETIS to parallelize the domain partitioning**
		if (myid == 0){
			int status = METIS_PartGraphKway(&nn, &ne, xadj, adjency, NULL, NULL, NULL, &nparts, NULL, NULL, NULL, &objval, npart);
			//int status = METIS_PartMeshNodal(&nn, &ne, xadj, adjency, NULL, NULL, &nparts, NULL, NULL, &objval, epart, npart);
			if (status != METIS_OK) {
				std::cout << "Metis returned with an error.\n";
				throw "Metis returned with an error.";
			}
		}
		MPI_Bcast((void *)npart, node_count * sizeof(idx_t), MPI_BYTE, 0, MPI_COMM_WORLD);

#ifdef DEBUG
		for (int i = 0; i < node_count; i++) {
			//std::cout << "node" << i << ": part" << npart[i] << std::endl;
		}
#endif

		/// setting process own
		for (int i = 0; i < numOfBifurcation; i++) {
			part_table_bifurcation[i] = npart[part_table_bifurcation[i]];
		}
		for (int i = 0; i < numOfAcinus; i++) {
			part_table_acinus[i] = npart[part_table_acinus[i]];
		}
		part_table_inlet = npart[part_table_inlet];

		for (int i = 0; i < numOfAirways; i++) {
			part_table_airway[i] = npart[part_table_airway[i]];
		}
		//for (int i = 0; i < numOfAirways; i++) {
		//	part_table_airway[i] = epart[part_table_airway[i]];
		//}
	} else {
		for (int i = 0; i < numOfBifurcation; i++) {
			part_table_bifurcation[i] = 0;
		}
		for (int i = 0; i < numOfAcinus; i++) {
			part_table_acinus[i] = 0;
		}
		part_table_inlet = 0;
		for (int i = 0; i < numOfAirways; i++) {
			part_table_airway[i] = 0;
		}
	}

	/// determine vector indices
	vectorIndexOfBifurcationNode = new int[numOfBifurcation];
	int index = 0;
	for (int proc = 0; proc < nparts; proc++) {
		for (int i = 0; i < numOfBifurcation; i++) {
			if (part_table_bifurcation[i] == proc) {
				vectorIndexOfBifurcationNode[i] = index;
				index++;
			}
		}
	}
	vectorIndexOfAirwayNode = new int[numOfAirways];
	index = 0;
	for (int proc = 0; proc < nparts; proc++) {
		for (int i = 0; i < numOfAirways; i++) {
			if (part_table_airway[i] == proc) {
				vectorIndexOfAirwayNode[i] = index;
				index++;
			}
		}
	}
	vectorIndexOfAcinusNode = new int[numOfAcinus];
	index = 0;
	for (int proc = 0; proc < nparts; proc++) {
		for (int i = 0; i < numOfAcinus; i++) {
			if (part_table_acinus[i] == proc) {
				vectorIndexOfAcinusNode[i] = index;
				index++;
			}
		}
	}
}

void AirwayTreeGraphMetis::exportGraphFile() {
	std::ofstream ofile1;
	ofile1.open("graph.dat");
	for (int i = 0; i < node_count; i++) {
		ofile1 << i << " :";
		for (int j = xadj[i]; j < xadj[i + 1]; j++) {
			ofile1 << adjency[j] << ",";
		}
		ofile1 << std::endl;
	}
	ofile1.close();
}

int AirwayTreeGraphMetis::part_bifurcation(int i) {
	return part_table_bifurcation[i];
}

int AirwayTreeGraphMetis::part_airway(int i) {
	return part_table_airway[i];
}

int AirwayTreeGraphMetis::part_acinus(int i) {
	return part_table_acinus[i];
}

int AirwayTreeGraphMetis::part_inlet() {
	return part_table_inlet;
}

int AirwayTreeGraphMetis::getVectorIndexOfBifurcationNode(int i) {
	return vectorIndexOfBifurcationNode[i];
}

int AirwayTreeGraphMetis::getVectorIndexOfAirwayNode(int i) {
	return vectorIndexOfAirwayNode[i];
}

int AirwayTreeGraphMetis::getVectorIndexOfAcinusNode(int i) {
	return vectorIndexOfAcinusNode[i];
}
