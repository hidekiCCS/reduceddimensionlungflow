/*
 * AirwayTreeGraphMetis.h
 *
 *  Created on: Oct 28, 2014
 *      Author: fuji
 */

#ifndef AIRWAYTREEGRAPHMETIS_H_
#define AIRWAYTREEGRAPHMETIS_H_

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <mpi.h>
#include <metis.h>
#include "LungLobeFromSimpleAirwayTree.h"

#ifndef IDXTYPEWIDTH
typedef int idx_t;
#endif

/// This class to partition the airway tree using METIS.
class AirwayTreeGraphMetis {
public:
	/// Constructor takes an object of 'LungLobeFromSimpleAirwayTree'
	AirwayTreeGraphMetis(LungLobeFromSimpleAirwayTree *lung);

	/// Destructor frees memory
	virtual ~AirwayTreeGraphMetis();

	/// Export a Graph Data to a File
	void exportGraphFile();

	/// Partitioning the model
	void partitioning(idx_t nparts);

	/// Get the partition number of the bifurcation of index, i.
	int part_bifurcation(int i);

	/// Get the partition number of the airway of index, i.
	int part_airway(int i);

	/// Get the partition number of the acinus of index, i.
	int part_acinus(int i);

	/// Get the partition number of the Trachea.
	int part_inlet();

	/// Get the global element number of PetSc Vector for Bifurcation node.
	int getVectorIndexOfBifurcationNode(int i);

	/// Get the global element number of PetSc Vector for Airway node.
	int getVectorIndexOfAirwayNode(int i);

	/// Get the global element number of PetSc Vector for Acinus node.
	int getVectorIndexOfAcinusNode(int i);

	/// Get the number of nodes in the graph
	int getNodeCount() const {
		return node_count;
	}

	/// Get the number of elements in the graph
	int getElementCount() const {
		return element_count;
	}

	/// Get the number of Acini
	int getNumOfAcinus() const {
		return numOfAcinus;
	}

	/// Get the number of Airways
	int getNumOfAirways() const {
		return numOfAirways;
	}

	/// Get the number of Bifurcation
	int getNumOfBifurcation() const {
		return numOfBifurcation;
	}

	/// Set an array of the global element number of PetSc Vector for Bifurcation node
	void setVectorIndexOfBifurcationNode(int* vectorIndexOfBifurcationNode) {
		this->vectorIndexOfBifurcationNode = vectorIndexOfBifurcationNode;
	}

	LungLobeFromSimpleAirwayTree*& getLung() {
		return lung;
	}

private:
	LungLobeFromSimpleAirwayTree* lung;
	int numOfAirways;
	int numOfBifurcation;
	int numOfAcinus;

	int node_count;
	int element_count;

	idx_t *xadj;
	idx_t *adjency;

	int *part_table_bifurcation;
	int *part_table_airway;
	int *part_table_acinus;
	int part_table_inlet;

	int *vectorIndexOfBifurcationNode;
	int *vectorIndexOfAirwayNode;
	int *vectorIndexOfAcinusNode;

	/// process id
	int myid;
	/// number of processes
	int numproc;
};

#endif /* AIRWAYTREEGRAPHMETIS_H_ */
