/*
 * AirwayTreeGraphMetis.cpp
 *
 *  Created on: Oct 28, 2014
 *      Author: fuji
 */

#include <vector>
#include <fstream>
#include <iomanip>
#include <mpi.h>
#include "AirwayTreeGraphMetis.h"

AirwayTreeGraphMetis::AirwayTreeGraphMetis(LungLobeFromSimpleAirwayTree* lung) :
		lung(lung) {
    MPI_Comm_size(MPI_COMM_WORLD, &numproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	/// Get numbers
	numOfBifurcation = lung->get_NumberOfBifurcations();
	numOfAirways = lung->get_numberOfAirways();
	numOfAcinus = lung->get_NumberOfAcinus();
	/// allocate table
	part_table_bifurcation = new int[numOfBifurcation];
	part_table_airway = new int[numOfAirways];
	part_table_acinus = new int[numOfAcinus];
	part_table_inlet = -1;
#ifdef DEBUG
	std::cout << "# of Bifurcation " << numOfBifurcation << std::endl;
	std::cout << "# of Airways " << numOfAirways << std::endl;
	std::cout << "# of Acinus " << numOfAcinus << std::endl;
#endif
	///
	/// Making Graph data
	///
	xadj = new idx_t[numOfBifurcation + numOfAcinus + 2];
	adjency = new idx_t[numOfAirways * 2];
	element_count = 0;
	node_count = 0;
	std::vector<int> element_stack;
	int intel_element;
	/// bifurcation
	for (int i = 0; i < numOfBifurcation; i++) {
		xadj[node_count] = element_count;
		part_table_bifurcation[i] = node_count;
		int pt = lung->get_ParentTubeIndex(i);
		int dt1 = lung->get_DaughterTube1Index(i);
		int dt2 = lung->get_DaughterTube2Index(i);
		/// for parent tube
		int bif_up = lung->get_UpperBifurcationIndex(pt);
		part_table_airway[pt] = node_count;
		if (bif_up >= 0) {
			adjency[element_count] = bif_up;
			element_count++;
		} else {
			// this is the mouth
			part_table_inlet = pt;
			intel_element = element_count;
			element_count++;
		}
		/// for daughter tube 1
		int bif_low1 = lung->get_LowerBifurcationIndex(dt1);
		//part_table_airway[dt1] = element_count;
		if (bif_low1 >= 0) {
			adjency[element_count] = bif_low1;
			element_count++;
		} else {
			// this is a acinus
			adjency[element_count] = dt1;
			element_stack.push_back(element_count);
			element_count++;
		}
		/// for daughter tube 2
		int bif_low2 = lung->get_LowerBifurcationIndex(dt2);
		//part_table_airway[dt2] = element_count;
		if (bif_low2 >= 0) {
			adjency[element_count] = bif_low2;
			element_count++;
		} else {
			// this is a acinus
			adjency[element_count] = dt2;
			element_stack.push_back(element_count);
			element_count++;
		}
		node_count++;
	}
	/// acinus
	for (int i = 0; i < numOfAcinus; i++) {
		xadj[node_count] = element_count;
		part_table_acinus[i] = node_count;
		int teminal_bronchi = lung->get_TerminalAirway(i);
		part_table_airway[teminal_bronchi] = node_count;
		std::vector<int>::iterator itr = element_stack.begin();
		while (itr != element_stack.end()) {
			if (adjency[*itr] == teminal_bronchi) {
				adjency[element_count] = lung->get_UpperBifurcationIndex(teminal_bronchi);
				element_count++;
				adjency[*itr] = node_count;
				element_stack.erase(itr);
				break;
			} else {
				itr++;
			}
#ifdef DEBUG
			if (itr == element_stack.end()) {
				std::cout << "stranger things have happened\n";
			}
#endif
		}
		node_count++;
	}
	/// mouth
	xadj[node_count] = element_count;
	adjency[element_count] = lung->get_LowerBifurcationIndex(part_table_inlet);
	element_count++;
	adjency[intel_element] = node_count;
	part_table_inlet = node_count;
	node_count++;
	xadj[node_count] = element_count;

#ifdef DEBUG
	std::cout << "# of Nodes " << node_count << std::endl;
	std::cout << "# of Elements " << element_count << std::endl;
#endif
}

AirwayTreeGraphMetis::~AirwayTreeGraphMetis() {
	delete [] part_table_bifurcation;
	delete [] part_table_airway;
	delete [] part_table_acinus;
	delete [] xadj;
	delete [] adjency;
}

void AirwayTreeGraphMetis::partitioning(idx_t nparts) {
	if (node_count < nparts){
		throw "The number of vertices is less than the number of partition.";
	}
	if (numproc != nparts){
		throw "The number of processes should be same with the number of partition.";
	}
	if (nparts > 1) {
		//idx_t nn = node_count;
		//idx_t ne = xadj[node_count];
		//idx_t objval;
		//idx_t *epart = new idx_t[ne];
		//idx_t *npart = new idx_t[nn];

		/// Use ParMETIS to partition the airway tree if you the number of process is greater than 1.
		idx_t *vtxdist = new idx_t[nparts+1];
		for (int i = 0 ; i < numproc ; i++){
			vtxdist[i] = i * (node_count / numproc) + (node_count % numproc > i ? i:node_count % numproc);
		}
		vtxdist[numproc] = node_count;
		for (int i = 0 ; i <= numproc ; i++){
			std::cout << "myid" << myid << "size" << i << "=" << vtxdist[i] << std::endl;
		}
		while(1);
		idx_t *xadj_local = new idx_t[vtxdist[myid + 1] - vtxdist[myid] + 1];
		for (int i = vtxdist[myid] ; i <= vtxdist[myid + 1] ; i++){
			xadj_local[i - vtxdist[myid]] = xadj[i];
		}
		idx_t *adjency_local = new idx_t[xadj_local[vtxdist[myid + 1] - vtxdist[myid]] - xadj_local[0]];
		for (int i = xadj[0] ; i < xadj[vtxdist[myid + 1] - vtxdist[myid]] ; i++){
			adjency_local[i - xadj[0]] = adjency[i];
		}

		idx_t wgtflag = 0;
		idx_t numflag = 0;
		idx_t ncon = 1;
		real_t *ubvec = new real_t[ncon];
		real_t *tpwgts = new real_t[ncon * nparts];
		for (int i = 0 ; i < ncon * nparts ; i++) tpwgts[i] = 1.0 / (real_t)nparts;
		idx_t *part = new idx_t[vtxdist[myid + 1] - vtxdist[myid] + 1];
		idx_t edgecut;
		idx_t options[10];
		real_t itr = 1000.0;

		ubvec[0] = 1.05;
		options[0] = 0;
		options[1] = 3;
		options[2] = 1;
		MPI_Comm comm;
		MPI_Comm_dup(MPI_COMM_WORLD, &comm);
		ParMETIS_V3_PartKway(vtxdist, xadj_local, adjency_local, NULL,
				NULL, &wgtflag, &numflag, &ncon, &nparts, tpwgts, ubvec,
				options, &edgecut, part, &comm);
		ParMETIS_V3_AdaptiveRepart(vtxdist, xadj_local, adjency_local, NULL, NULL,
				NULL, &wgtflag, &numflag,
				&ncon, &nparts, tpwgts, ubvec, &itr,
				options, &edgecut, part, &comm);
		std::cout << "myid" << myid << std::endl;
		idx_t *npart = new idx_t[node_count];
		int *count = new int[numproc];
		for (int i = 0 ; i < numproc ; i++){
			count[i] = vtxdist[myid + 1] - vtxdist[myid];
		}

        MPI_Allgatherv((void *)part, count[myid], MPI_INT,
                            (void *)npart, count, count,
                            MPI_INT, MPI_COMM_WORLD);

        delete [] count;
        delete [] ubvec;
        delete [] tpwgts;
        delete [] part;
        delete [] vtxdist;
        delete [] xadj_local;
        delete [] adjency_local;
#ifdef DEBUG
		for (int i = 0; i < node_count; i++) {
			//std::cout << "node" << i << ": part" << npart[i] << std::endl;
		}
#endif

		/// setting process own
		for (int i = 0; i < numOfBifurcation; i++) {
			part_table_bifurcation[i] = npart[part_table_bifurcation[i]];
		}
		for (int i = 0; i < numOfAcinus; i++) {
			part_table_acinus[i] = npart[part_table_acinus[i]];
		}
		part_table_inlet = npart[part_table_inlet];

		for (int i = 0; i < numOfAirways; i++) {
			part_table_airway[i] = npart[part_table_airway[i]];
		}
		//for (int i = 0; i < numOfAirways; i++) {
		//	part_table_airway[i] = epart[part_table_airway[i]];
		//}
	} else {
		for (int i = 0; i < numOfBifurcation; i++) {
			part_table_bifurcation[i] = 0;
		}
		for (int i = 0; i < numOfAcinus; i++) {
			part_table_acinus[i] = 0;
		}
		part_table_inlet = 0;
		for (int i = 0; i < numOfAirways; i++) {
			part_table_airway[i] = 0;
		}
	}

	/// determine vector indices
	vectorIndexOfBifurcationNode = new int[numOfBifurcation];
	int index = 0;
	for (int proc = 0; proc < nparts; proc++) {
		for (int i = 0; i < numOfBifurcation; i++) {
			if (part_table_bifurcation[i] == proc) {
				vectorIndexOfBifurcationNode[i] = index;
				index++;
			}
		}
	}
	vectorIndexOfAirwayNode = new int[numOfAirways];
	index = 0;
	for (int proc = 0; proc < nparts; proc++) {
		for (int i = 0; i < numOfAirways; i++) {
			if (part_table_airway[i] == proc) {
				vectorIndexOfAirwayNode[i] = index;
				index++;
			}
		}
	}
	vectorIndexOfAcinusNode = new int[numOfAcinus];
	index = 0;
	for (int proc = 0; proc < nparts; proc++) {
		for (int i = 0; i < numOfAcinus; i++) {
			if (part_table_acinus[i] == proc) {
				vectorIndexOfAcinusNode[i] = index;
				index++;
			}
		}
	}
}

void AirwayTreeGraphMetis::exportGraphFile() {
	std::ofstream ofile1;
	ofile1.open("graph.dat");
	for (int i = 0; i < node_count; i++) {
		ofile1 << i << " :";
		for (int j = xadj[i]; j < xadj[i + 1]; j++) {
			ofile1 << adjency[j] << ",";
		}
		ofile1 << std::endl;
	}
	ofile1.close();
}

int AirwayTreeGraphMetis::part_bifurcation(int i) {
	return part_table_bifurcation[i];
}

int AirwayTreeGraphMetis::part_airway(int i) {
	return part_table_airway[i];
}

int AirwayTreeGraphMetis::part_acinus(int i) {
	return part_table_acinus[i];
}

int AirwayTreeGraphMetis::part_inlet() {
	return part_table_inlet;
}

int AirwayTreeGraphMetis::getVectorIndexOfBifurcationNode(int i) {
	return vectorIndexOfBifurcationNode[i];
}

int AirwayTreeGraphMetis::getVectorIndexOfAirwayNode(int i) {
	return vectorIndexOfAirwayNode[i];
}

int AirwayTreeGraphMetis::getVectorIndexOfAcinusNode(int i) {
	return vectorIndexOfAcinusNode[i];
}
