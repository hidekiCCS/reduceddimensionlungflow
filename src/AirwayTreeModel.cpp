//
//  AirwayTreeModel.cpp
//  LungFluidFlow2
//
//  Created by Hideki Fujioka on 6/15/15.
//
//

#include "AirwayTreeModel.h"
#include <cstdlib>
#include <cmath>
#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

AirwayTreeModel::AirwayTreeModel(const char* bfn) : generation(0),lungVolume_FRC(1000),firstGeneration(0){
	/// * Assuming FRC = 1000 ml
	/// * The first generation is 0 (trachea)
	baseFileName.assign(bfn);
}

AirwayTreeModel::~AirwayTreeModel() {
	/// * Clean up listOfAirways
	for (std::vector<airway *>::iterator it = listOfAirways.begin() ; it != listOfAirways.end() ; ){
		delete *it;
		it = listOfAirways.erase(it);
	}
	/// * Clean up listOfAcinus
	for (std::vector<acinus *>::iterator it = listOfAcinus.begin() ; it != listOfAcinus.end() ; ){
		delete *it;
		it = listOfAcinus.erase(it);
	}
}

void AirwayTreeModel::generateSymmetricAirways(int generation) {
	/// * Cleanup for safety
	for(std::vector<airway *>::iterator it = listOfAirways.begin() ; it != listOfAirways.end();){
		delete *it;
		it = listOfAirways.erase(it);
	}
	/// * Generate a symmetric airway Tree
	this->generation = generation;
	airway *firstAirway = new airway();
	listOfAirways.push_back(firstAirway);

	std::stack<airway *> airwayWorkingStack;
	airwayWorkingStack.push(firstAirway);

	while(!airwayWorkingStack.empty()){
		airway *aw = airwayWorkingStack.top();
		airwayWorkingStack.pop();
		int wgen = aw->weibelsGenerationNumber;
		for (int i = 0 ; i < 2 ; i++){
			airway *daughterAirway = new airway();
			daughterAirway->parentAirway = aw;
			daughterAirway->weibelsGenerationNumber = wgen + 1;
			listOfAirways.push_back(daughterAirway);
			if (i == 0) {
				aw->daughterAirway1 = daughterAirway;
			} else {
				aw->daughterAirway2 = daughterAirway;
			}
			if (wgen + 1 < generation - 1) {
				airwayWorkingStack.push(daughterAirway);
			}
		}
	}

	/// * Determine index, generation number, radius, and length.
	GenerationNumbering();

	std::cout << "The number of airways : " << listOfAirways.size() << std::endl;
}

void AirwayTreeModel::makeAsymmetric(double ratio) {
	int numberOfAirways = listOfAirways.size();
	/// * make a list of terminal airways.
	std::vector<airway *> listOfTermianlBronchi;
	for (int i = 0; i < numberOfAirways; i++) {
		if (listOfAirways[i]->daughterAirway1 == NULL){
			listOfTermianlBronchi.push_back(listOfAirways[i]);
		}
	}
	std::cout << "Terminal Bronchi : " << listOfTermianlBronchi.size() << std::endl;

	/// * make a list of free bifurcation (both daughter tubes are terminal airways.)
	std::vector<airway *> listOfFreeBifurcation;
	for (int i = 0; i < numberOfAirways; i++) {
		if (listOfAirways[i]->daughterAirway1 != NULL){
			if ((listOfAirways[i]->daughterAirway1->daughterAirway1 == NULL) && (listOfAirways[i]->daughterAirway2->daughterAirway1 == NULL)){
				listOfFreeBifurcation.push_back(listOfAirways[i]);
			}
		}
	}
	std::cout << "Free Bifurcation (before): " << listOfFreeBifurcation.size() << std::endl;
	std::cout << "Terminal Bronchi : " << listOfTermianlBronchi.size() << std::endl;

	/// * Determine the number of picking-up free-bifurcation based on the input argument 'ratio'.
	int frequency = static_cast<double>(listOfTermianlBronchi.size()) * ratio;
	for (int count = 0 ; count < frequency ; count++){
		/// * Loop for the number of picking-up
		// 		+ Randomly pick one.
		int bifIdx = std::min(listOfFreeBifurcation.size() * std::rand() / RAND_MAX,listOfFreeBifurcation.size()-1);
		int tbIdx;
		while(1){
			/// + pick one terminal airway.
			tbIdx = std::min(listOfTermianlBronchi.size() * std::rand() / RAND_MAX,listOfTermianlBronchi.size()-1);
			if (listOfTermianlBronchi[tbIdx]->parentAirway != listOfFreeBifurcation[bifIdx]){
				break;
			}
		}

		/// 	+ Move the free-bifurcation under the terminal airway.
		airway *d1 = listOfFreeBifurcation[bifIdx]->daughterAirway1;
		airway *d2 = listOfFreeBifurcation[bifIdx]->daughterAirway2;
		listOfFreeBifurcation[bifIdx]->daughterAirway1 = NULL;
		listOfFreeBifurcation[bifIdx]->daughterAirway2 = NULL;
		listOfTermianlBronchi[tbIdx]->daughterAirway1 = d1;
		listOfTermianlBronchi[tbIdx]->daughterAirway2 = d2;
		d1->parentAirway = listOfTermianlBronchi[tbIdx];
		d2->parentAirway = listOfTermianlBronchi[tbIdx];
		d1->daughterAirway1 = NULL;
		d1->daughterAirway2 = NULL;
		d2->daughterAirway1 = NULL;
		d2->daughterAirway2 = NULL;
		d1->weibelsGenerationNumber = d1->parentAirway->weibelsGenerationNumber + 1;
		d2->weibelsGenerationNumber = d2->parentAirway->weibelsGenerationNumber + 1;

		/// 	+ erase and push lists
		airway *tb = listOfTermianlBronchi[tbIdx];
		airway *bif = listOfFreeBifurcation[bifIdx];
		listOfTermianlBronchi.erase(listOfTermianlBronchi.begin() + tbIdx);
		listOfFreeBifurcation.erase(listOfFreeBifurcation.begin() + bifIdx);
		listOfTermianlBronchi.push_back(bif);
		listOfFreeBifurcation.push_back(tb);
		if ((bif->parentAirway->daughterAirway1->daughterAirway1 == NULL) && (bif->parentAirway->daughterAirway2->daughterAirway1 == NULL)){
			listOfFreeBifurcation.push_back(bif->parentAirway);
		}
		for (int i = 0 ; i < listOfFreeBifurcation.size() ; i++){
			if (tb->parentAirway == listOfFreeBifurcation[i]){
				listOfFreeBifurcation.erase(listOfFreeBifurcation.begin() + i);
			}
		}
		if (listOfFreeBifurcation.size() < 2) break;
	}
	std::cout << "Free Bifurcation (after): " << listOfFreeBifurcation.size() << std::endl;

	/// * Determine index, generation number, radius, and length.
	GenerationNumbering();

	std::cout << "Trachea Generation Number: " << listOfAirways[0]->GenerationNumber << std::endl;
}

void AirwayTreeModel::readXML() {
	char fn[256];
	xmlDoc *doc = NULL;
	xmlNode *cur_node = NULL;
	xmlNode *root_node = NULL;
	xmlNode *a_node = NULL;
	xmlNode *t_node = NULL;

	/// * Read xml file using xmlParser
	xmlInitParser();
	sprintf(fn, "%s.xml", baseFileName.c_str());
	doc =xmlReadFile(fn, NULL , 0);
	if (doc == NULL){
		std::cout << "ERROR: FILE %s NOT FOUND " <<  fn << std::endl;
		return;
	}
	this->generation = -1;
	double totalVolume = 0;
	root_node = xmlDocGetRootElement(doc);
	for (a_node = root_node ; a_node ; a_node = a_node->next){
		if (xmlStrEqual(a_node->name, BAD_CAST "Lung_Lobe_Model") == 1){
			/// * load Comments
			for (t_node = a_node->children ; t_node ; t_node = t_node->next){
				if (xmlStrEqual(t_node->name, BAD_CAST "Comments") == 1){
					for (cur_node = t_node->children ; cur_node ; cur_node = cur_node->next){
						if (xmlStrEqual(cur_node->name, BAD_CAST "TEXT") == 1){
							std::cout << (cur_node->children->content) << std::endl;
						}
					}
				}
			}

			/// * Loop over entries and create objects.
			for (t_node = a_node->children ; t_node ; t_node = t_node->next){
				if (xmlStrEqual(t_node->name, BAD_CAST "AIRWAY") == 1){
					int anum =  (int)xmlXPathCastStringToNumber(xmlGetProp(t_node, BAD_CAST "Number"));
					airway *aw = new airway();
					aw->index = anum;
					listOfAirways.push_back(aw);
				}
				if (xmlStrEqual(t_node->name, BAD_CAST "ACINUS") == 1){
					int anum =  (int)xmlXPathCastStringToNumber(xmlGetProp(t_node, BAD_CAST "Number"));
					acinus *ac = new acinus();
					ac->index = anum;
					listOfAcinus.push_back(ac);
				}
				// Tawhai Model doesn't contain next two entries.
				if (xmlStrEqual(t_node->name, BAD_CAST "GENERATION_WEIBEL") == 1){
					 this->generation = (int)xmlXPathCastStringToNumber(t_node->children->content);
				}

				if (xmlStrEqual(t_node->name, BAD_CAST "LUNG_VOLUME_FRC") == 1){
					 this->lungVolume_FRC = (double)xmlXPathCastStringToNumber(t_node->children->content);
				}
			}

			/// * Make Index -> Object table for airway
			std::vector<airway *> airwayLooupTable (listOfAirways.size());
			for (int i = 0 ; i < listOfAirways.size() ; i++){
				airwayLooupTable[listOfAirways[i]->index] = listOfAirways[i];
			}
			/// * Make Index -> Object table for acinus
			std::vector<acinus *> acinusLooupTable (listOfAcinus.size());
			for (int i = 0 ; i < listOfAcinus.size() ; i++){
				acinusLooupTable[listOfAcinus[i]->index] = listOfAcinus[i];
			}

			/// * Loop over entries second times
			for (t_node = a_node->children ; t_node ; t_node = t_node->next){
				if (xmlStrEqual(t_node->name, BAD_CAST "AIRWAY") == 1){
					int anum =  (int)xmlXPathCastStringToNumber(xmlGetProp(t_node, BAD_CAST "Number"));
					airway *aw = airwayLooupTable[anum];
					bool firstDaughter = true;
					for (xmlNode *i_node = t_node->children ; i_node ; i_node = i_node->next){
						///	* Loop over AIRWAY entries
						///		+ get GENERATION_NUMBER entry
						if (xmlStrEqual(i_node->name, BAD_CAST "GENERATION_NUMBER") == 1){
							aw->GenerationNumber = (int)xmlXPathCastStringToNumber(i_node->children->content);
						}
						///		+ get PARENT_TUBE entry
						if (xmlStrEqual(i_node->name, BAD_CAST "PARENT_TUBE") == 1){
							int idx = (int)xmlXPathCastStringToNumber(i_node->children->content);
							if (idx >= 0){
								aw->parentAirway = airwayLooupTable[idx];
							}else{
								aw->parentAirway = NULL;
							}
						}
						///		+ get DAUGHTER_TUBE entries
						if (xmlStrEqual(i_node->name, BAD_CAST "DAUGHTER_TUBE") == 1){
							int idx = (int)xmlXPathCastStringToNumber(i_node->children->content);
							if (idx >= 0){
								if (firstDaughter){
									aw->daughterAirway1 = airwayLooupTable[idx];
									firstDaughter = false;
								}else{
									aw->daughterAirway2 = airwayLooupTable[idx];
								}
							}else{
								aw->daughterAirway1 = NULL;
								aw->daughterAirway2 = NULL;
							}
						}
						///		+ get WEIBEL_GENERATION_NUMBER entry
						// Tawhai Model doesn't contain this entry.
						if (xmlStrEqual(i_node->name, BAD_CAST "WEIBEL_GENERATION_NUMBER") == 1) {
							aw->weibelsGenerationNumber = (int) xmlXPathCastStringToNumber(i_node->children->content);
						}
						///		+ get RADIUS entry
						if (xmlStrEqual(i_node->name, BAD_CAST "RADIUS") == 1) {
							aw->radius = (double) xmlXPathCastStringToNumber(i_node->children->content);
						}
						///		+ get LENGTH entry
						if (xmlStrEqual(i_node->name, BAD_CAST "LENGTH") == 1) {
							aw->length = (double) xmlXPathCastStringToNumber(i_node->children->content);
						}
						///		+ get NUMBER_OF_ALVEOLI_IN_ROI entries
						// Simple Airway Model doesn't contain this entry
						if (xmlStrEqual(i_node->name, BAD_CAST "NUMBER_OF_ALVEOLI_IN_ROI") == 1) {
							int acidx = (int) xmlXPathCastStringToNumber(xmlGetProp(i_node, BAD_CAST "ACINUS"));
							aw->listOfAciniROI.push_back(acinusLooupTable[acidx]);
							aw->listOfTNumberOfAlveoliROI.push_back((int) xmlXPathCastStringToNumber(i_node->children->content));
						}
					}
				}
				if (xmlStrEqual(t_node->name, BAD_CAST "ACINUS") == 1) {
					int anum = (int) xmlXPathCastStringToNumber(xmlGetProp(t_node, BAD_CAST "Number"));
					acinus *ac = acinusLooupTable[anum];
					for (xmlNode *i_node = t_node->children; i_node; i_node = i_node->next) {
						///	* Loop over ACINUS entries
						///		+ get TERNIMAL_BRONCHI entry
						if (xmlStrEqual(i_node->name, BAD_CAST "TERNIMAL_BRONCHI") == 1) {
							ac->terminalBronch = airwayLooupTable[(int) xmlXPathCastStringToNumber(i_node->children->content)];
						}
						///		+ get NUMBER_OF_ALVEOLI entry
						if (xmlStrEqual(i_node->name, BAD_CAST "NUMBER_OF_ALVEOLI") == 1) {
							ac->numberOfAlveoli = (int) xmlXPathCastStringToNumber(i_node->children->content);
						}
						///		+ get FRC_VOLUME entry
						if (xmlStrEqual(i_node->name, BAD_CAST "FRC_VOLUME") == 1) {
							ac->volumeFRC = (double) xmlXPathCastStringToNumber(i_node->children->content);
							totalVolume += ac->volumeFRC;
						}
					}
				}
			}
		}
	}
	/*free the document */
	xmlFreeDoc(doc);

	/*
	 *Free the global variables that may
	 *have been allocated by the parser.
	 */
	xmlCleanupParser();

	if (this->generation == -1){
		this->lungVolume_FRC = totalVolume;
		std::cout << "This is a Tawhai Model\n";
	}
}

void AirwayTreeModel::writeXML() {
	int rc;
	xmlTextWriterPtr writer;
	xmlNodePtr node;
	char cnum[256];

	std::string comments = "Generation of an Airways";

	/// * Define aicnus;
	determineAcini();
	/// * Map acinus tethering airways.
	mapAciniTetheringAirway();

	///  * Export Lung Lobe Model
	char fn[256];
	sprintf(fn, "%s.xml", baseFileName.c_str());
	writer = xmlNewTextWriterFilename(fn, 0);
	if (writer == NULL) {
		printf(" Error creating the xml writer\n");
		return;
	}

	/*! Start the document with the xml default for the version,
	 * encoding ISO 8859-1 and the default for the standalone
	 * declaration. */
	rc = xmlTextWriterStartDocument(writer, NULL, "ISO-8859-1", NULL);
	rc = xmlTextWriterStartElement(writer, BAD_CAST "Lung_Lobe_Model");
	rc = xmlTextWriterStartElement(writer, BAD_CAST "Comments");
	rc = xmlTextWriterWriteElement(writer, BAD_CAST "TEXT", BAD_CAST comments.c_str());
	rc = xmlTextWriterEndElement(writer);// Comments

	/// 	+ Make Airway Entry
	for (int i = 0; i < listOfAirways.size(); i++) {
		/// 	* Loop over airway
		rc = xmlTextWriterStartElement(writer, BAD_CAST "AIRWAY");
		sprintf(cnum, "%d", listOfAirways[i]->index);
		rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "Number", BAD_CAST cnum);
		{
			///		+ add entry GENERATION_NUMBER
			rc = xmlTextWriterStartElement(writer, BAD_CAST "GENERATION_NUMBER");
			rc = xmlTextWriterWriteFormatString(writer, "%d", listOfAirways[i]->GenerationNumber);
			rc = xmlTextWriterEndElement(writer);

			///		+ add entry WEIBEL_GENERATION_NUMBER
			rc = xmlTextWriterStartElement(writer, BAD_CAST "WEIBEL_GENERATION_NUMBER");
			rc = xmlTextWriterWriteFormatString(writer, "%d", listOfAirways[i]->weibelsGenerationNumber);
			rc = xmlTextWriterEndElement(writer);

			///		+ add entry PARENT_TUBE
			rc = xmlTextWriterStartElement(writer, BAD_CAST "PARENT_TUBE");
			if (listOfAirways[i]->parentAirway == NULL) {
				rc = xmlTextWriterWriteFormatString(writer, "%d", -1);
			} else {
				rc = xmlTextWriterWriteFormatString(writer, "%d", listOfAirways[i]->parentAirway->index);
			}
			rc = xmlTextWriterEndElement(writer);

			///		+ add entry DAUGHTER_TUBE
			if (listOfAirways[i]->GenerationNumber == 1) {
				rc = xmlTextWriterStartElement(writer, BAD_CAST "DAUGHTER_TUBE");
				rc = xmlTextWriterWriteFormatString(writer, "%d", -1);
				rc = xmlTextWriterEndElement(writer);
			} else {
				rc = xmlTextWriterStartElement(writer, BAD_CAST "DAUGHTER_TUBE");
				rc = xmlTextWriterWriteFormatString(writer, "%d", listOfAirways[i]->daughterAirway1->index);
				rc = xmlTextWriterEndElement(writer);
				rc = xmlTextWriterStartElement(writer, BAD_CAST "DAUGHTER_TUBE");
				rc = xmlTextWriterWriteFormatString(writer, "%d", listOfAirways[i]->daughterAirway2->index);
				rc = xmlTextWriterEndElement(writer);
			}
			///		+ add entry RADIUS
			rc = xmlTextWriterStartElement(writer, BAD_CAST "RADIUS");
			rc = xmlTextWriterWriteFormatString(writer, "%e", listOfAirways.at(i)->radius);
			rc = xmlTextWriterEndElement(writer);
			///		+ add entry LENGTH
			rc = xmlTextWriterStartElement(writer, BAD_CAST "LENGTH");
			rc = xmlTextWriterWriteFormatString(writer, "%e", listOfAirways.at(i)->length);
			rc = xmlTextWriterEndElement(writer);

			///		+ add entry NUMBER_OF_ALVEOLI_IN_ROI
			for (int j = 0; j < listOfAirways.at(i)->listOfAciniROI.size(); j++) {
				rc = xmlTextWriterStartElement(writer, BAD_CAST "NUMBER_OF_ALVEOLI_IN_ROI");
				sprintf(cnum, "%d", listOfAirways.at(i)->listOfAciniROI.at(j)->index);
				rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "ACINUS", BAD_CAST cnum);
				rc = xmlTextWriterWriteFormatString(writer, "%d", listOfAirways.at(i)->listOfTNumberOfAlveoliROI[j]);
				rc = xmlTextWriterEndElement(writer);
			}
		}
		rc = xmlTextWriterEndElement(writer);
	}

	/// 	+  Make Acinus entry
	for (int i = 0; i < listOfAcinus.size(); i++) {
		/// 	* Loop over acinus
		rc = xmlTextWriterStartElement(writer, BAD_CAST "ACINUS");
		sprintf(cnum, "%d", listOfAcinus[i]->index);
		rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "Number", BAD_CAST cnum);
		{
			///		+ add entry TERNIMAL_BRONCHI
			rc = xmlTextWriterStartElement(writer, BAD_CAST "TERNIMAL_BRONCHI");
			rc = xmlTextWriterWriteFormatString(writer, "%d", listOfAcinus[i]->terminalBronch->index);
			rc = xmlTextWriterEndElement(writer);
			///		+ add entry NUMBER_OF_ALVEOLI
			rc = xmlTextWriterStartElement(writer, BAD_CAST "NUMBER_OF_ALVEOLI");
			rc = xmlTextWriterWriteFormatString(writer, "%d", listOfAcinus[i]->numberOfAlveoli);
			rc = xmlTextWriterEndElement(writer);
			///		+ add entry FRC_VOLUME
			rc = xmlTextWriterStartElement(writer, BAD_CAST "FRC_VOLUME");
			rc = xmlTextWriterWriteFormatString(writer, "%f", listOfAcinus[i]->volumeFRC);
			rc = xmlTextWriterEndElement(writer);
		}
		rc = xmlTextWriterEndElement(writer);
	}
	/// 	+  Make GENERATION_WEIBEL entry
	rc = xmlTextWriterStartElement(writer, BAD_CAST "GENERATION_WEIBEL");
	rc = xmlTextWriterWriteFormatString(writer, "%d", this->generation);
	rc = xmlTextWriterEndElement(writer);
	/// 	+  Make LUNG_VOLUME_FRC entry
	rc = xmlTextWriterStartElement(writer, BAD_CAST "LUNG_VOLUME_FRC");
	rc = xmlTextWriterWriteFormatString(writer, "%f", this->lungVolume_FRC);
	rc = xmlTextWriterEndElement(writer);

	rc = xmlTextWriterEndDocument(writer);
	xmlFreeTextWriter(writer);
}

void AirwayTreeModel::mapAciniTetheringAirway() {
	/// * Cleanup for safety
	for (int i = 0; i < listOfAirways.size(); i++) {
		listOfAirways.at(i)->listOfAciniROI.clear();
		listOfAirways.at(i)->listOfTNumberOfAlveoliROI.clear();
	}
	/// * Determine acinus volume at FRC assuming all acini are same size.
	int numberOfAcinus = listOfAcinus.size();
	double acinusVolumeFRC = lungVolume_FRC / (double) numberOfAcinus;
	double aliveolar_volume = 4.0 * M_PI * std::pow(alveolarRadiusFRC, 3) / 3.0;
	int numberOfAlveoli = acinusVolumeFRC / aliveolar_volume;

	/// * Loop over all acini
	for (int i = 0; i < listOfAcinus.size(); i++) {
		int k = 0;
		airway *aw = (airway *) (listOfAcinus.at(i)->terminalBronch);
		/// * Tracing airway from terminal airway to trachea
		/// 	+ determine the number of alveoli to  \f$2^{-k}\f$ times total number of alveoli, where k is the number of lower airways towards the acinus.
		while (1) {
			aw->listOfAciniROI.push_back(listOfAcinus[i]);
			aw->listOfTNumberOfAlveoliROI.push_back(numberOfAlveoli * std::pow(2, -k));
			if (aw->parentAirway == NULL) break;
			k++;
			aw = aw->parentAirway;
		}
	}
}

void AirwayTreeModel::GenerationNumbering() {
	/// * Generation numbering with bottom-up manner.
	while (1) {
		int c = 0;
		for (int i = 0; i < listOfAirways.size(); i++) {
			if (listOfAirways.at(i)->daughterAirway1 != NULL) {
				if (listOfAirways.at(i)->daughterAirway2 == NULL) {
					std::cout << "stranger things have happened" << std::endl;
				}
				int g = std::max(listOfAirways.at(i)->daughterAirway1->GenerationNumber, listOfAirways.at(i)->daughterAirway2->GenerationNumber) + 1;
				if (g != listOfAirways.at(i)->GenerationNumber) {
					//std::cout << g << " " << listOfAirways[i]->GenerationNumber << std::endl;
					listOfAirways.at(i)->GenerationNumber = g;
					c++;
				}
			} else {
				listOfAirways.at(i)->GenerationNumber = 1;
			}
		}
		//std::cout << "c=" << c << std::endl;
		if (c == 0) break;
		//break;
	}

	/// * Compute Weibel's generation ordering
	/// * Set Airway Radius and Length
	int numberOfAirways = listOfAirways.size();
	int ngen = listOfAirways.at(0)->GenerationNumber;
	for (int i = 0; i < numberOfAirways; i++) {
		listOfAirways.at(i)->index = i;
		double weibelGenNum = firstGeneration
				+ static_cast<double>((this->generation - firstGeneration - 1) * (ngen - listOfAirways.at(i)->GenerationNumber)) / (ngen - 1);
		listOfAirways.at(i)->radius = Radius(weibelGenNum);
		listOfAirways.at(i)->length = Length(weibelGenNum);
	}
}

double AirwayTreeModel::Radius(double s) {
	/// * Radius as a function of Weibel's generation number, s, which is a real number (not an integer).
	/// * Interpolating from a table.
	const double R []={0.8686,0.6142,0.4720,0.3732,0.2992,0.2296,0.1798,0.1410,0.1126,0.0889,\
	        0.0704,0.0567,0.0473,0.0416,0.0367,0.0331,0.0296}; //[cm]
	int i = std::min((int)std::floor(s),15);
	double ds = s - i;
	return R[i] + (R[i+1] - R[i]) * ds;
}

void AirwayTreeModel::determineAcini() {
	/// * Cleanup for safety
	for(std::vector<acinus *>::iterator it = listOfAcinus.begin() ; it != listOfAcinus.end();){
		delete *it;
		it = listOfAcinus.erase(it);
	}
	/// * Look for terminal airways
	int index = 0;
	for (int i = 0; i < listOfAirways.size(); i++) {
		if (listOfAirways.at(i)->GenerationNumber == 1) {
			acinus *ac = new acinus();
			ac->index = index;
			ac->terminalBronch = listOfAirways[i];
			listOfAcinus.push_back(ac);
			index++;
		}
	}
	/// * Determine acinus volume at FRC assuming all acini are same size.
	int numberOfAcinus = listOfAcinus.size();
	double acinusVolumeFRC = lungVolume_FRC / (double) numberOfAcinus;
	double aliveolar_volume = 4.0 * M_PI * std::pow(alveolarRadiusFRC, 3) / 3.0;
	int numberOfAlveoli = acinusVolumeFRC / aliveolar_volume;
	for (int i = 0; i < listOfAcinus.size(); i++) {
		listOfAcinus.at(i)->volumeFRC = acinusVolumeFRC;
		listOfAcinus.at(i)->numberOfAlveoli = numberOfAlveoli;
	}
	std::cout << "Number Of Acini = " << numberOfAcinus << std::endl;
}

double AirwayTreeModel::Length(double s) {
	/// * Length as a function of Weibel's generation number, s, which is a real number (not an integer).
	/// * Interpolating from a table.
	const double L[] = { 12.00, 4.76, 1.90, 0.76, 1.27, 1.07, 0.90, 0.76, 0.64, 0.54, 0.47, 0.39, 0.33, 0.27, 0.23, 0.20, 0.17 }; //[cm]
	int i = std::min((int) std::floor(s), 15);
	double ds = s - i;
	return L[i] + (L[i + 1] - L[i]) * ds;
}
