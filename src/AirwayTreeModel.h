//
//  AirwayTreeModel.h
//  LungFluidFlow2
//
//  Created by Hideki Fujioka on 6/15/15.
//
//

#ifndef __LungFluidFlow2__AirwayTreeModel__
#define __LungFluidFlow2__AirwayTreeModel__

#include <iostream>
#include <string>
#include <vector>
#include <stack>

/// Generate an Airway Tree Model and Acini.
class AirwayTreeModel {
 public:
  /// Constructor takes base file name.
  AirwayTreeModel(const char *bfn);

  /// Destructor
  virtual ~AirwayTreeModel();

  /// Generate a symmetric airway
  virtual void generateSymmetricAirways(int generation);

  /// Make airway tree asymmetric
  virtual void makeAsymmetric(double ratio);

  /// import xml file
  void readXML();

  /// export xml file
  virtual void writeXML();

  /// get base file name
  const std::string& getBaseFileName() const {
    return baseFileName;
  }

  /// Get FRC
  double getLungVolumeFrc() const {
    return lungVolume_FRC;
  }

  /// Re-Assign FRC
  virtual void setLungVolumeFrc(double lungVolumeFrc) {
    lungVolume_FRC = lungVolumeFrc;
  }

 protected:
  /// Determine the area ratio of acinus attaching to airways.
  void mapAciniTetheringAirway();
  /// Determine acini connecting to terminal airways.
  void determineAcini();

  /// Base Generations of Airway Tree
  int generation;

  /// First Generation of Airway in Weibel's generation ordering.
  /// Default is 0 (trachea).
  int firstGeneration;

  /// FRC
  double lungVolume_FRC;

  /// Base File Name
  std::string baseFileName;
  /// container object
  class container {
  public:
    container(){
      index = 0;
    }
    int index;
  };
  /// acinus container class
  class acinus : public container {
  public:
    acinus() {
      terminalBronch = NULL;
      volumeFRC = 0;
      numberOfAlveoli = 0;
    }
    container *terminalBronch;
    double volumeFRC;
    int numberOfAlveoli;
  };

  /// airway container class
  class airway: public container {
  public:
    airway() {
      weibelsGenerationNumber = 0;
      parentAirway = NULL;
      daughterAirway1 = NULL;
      daughterAirway2 = NULL;
      GenerationNumber = 1;
      length = 0.0;
      radius = 0.0;
    }
    int weibelsGenerationNumber;
    int GenerationNumber;
    double length;
    double radius;
    airway *parentAirway;
    airway *daughterAirway1;
    airway *daughterAirway2;
    std::vector<acinus *> listOfAciniROI;
    std::vector<int> listOfTNumberOfAlveoliROI;
		};

  /// list of airway container
  std::vector<airway *> listOfAirways;

  /// list of acinus container
  std::vector<acinus *> listOfAcinus;

  /// Determine Generation numbering
  void GenerationNumbering();

  /// Airway Radius
  /// s is the Weibel's generation ordering
  double Radius(double s);
  /// Airway Length
  /// s is the Weibel's generation ordering
  double Length(double s);

#if __cplusplus > 199711L
#define _CONST_TLM constexpr
#else
#define _CONST_TLM const
#endif
  /// Alveolar Radius at FRC (cm)
  static _CONST_TLM double alveolarRadiusFRC = 5.50321208149104e-3;
#undef _CONST_TLM
};

#endif /* defined(__LungFluidFlow2__AirwayTreeModel__) */
