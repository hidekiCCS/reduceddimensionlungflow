/*
 * AirwayTreePartitioning.cpp
 *
 *  Created on: Jun 2, 2015
 *      Author: fuji
 */

#include "AirwayTreePartitioning.h"
#include <mpi.h>
#include <cmath>
#include <stdexcept>
#include <vtkSmartPointer.h>
#include <vtkXMLReader.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLStructuredGridWriter.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLRectilinearGridReader.h>
//#include <vtkXMLHyperOctreeReader.h>
#include <vtkXMLCompositeDataReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLImageDataReader.h>
#include <vtkDataSetReader.h>
#include <vtkDataSet.h>
#include <vtkUnstructuredGrid.h>
#include <vtkRectilinearGrid.h>
//#include <vtkHyperOctree.h>
#include <vtkImageData.h>
#include <vtkPolyData.h>
#include <vtkStructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkFieldData.h>
#include <vtkCellTypes.h>
#include <vtksys/SystemTools.hxx>
#include <vtkDoubleArray.h>
#include <vtkIntArray.h>
#include <vtkCellArray.h>
#include <Eigen/Core>
#include <Eigen/Dense>

AirwayTreePartitioning::AirwayTreePartitioning(LungLobeFromAirwayTree* lung) :
		lung(lung) {
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	/// Get numbers
	numOfBifurcation = lung->get_NumberOfBifurcations();
	numOfAirways = lung->get_numberOfAirways();
	numOfAcinus = lung->get_NumberOfAcinus();
	/// allocate table
	part_table_bifurcation = new int[numOfBifurcation];
	part_table_airway = new int[numOfAirways];
	part_table_acinus = new int[numOfAcinus];
	part_table_inlet = -1;
#ifdef DEBUG
	std::cout << "# of Bifurcation " << numOfBifurcation << std::endl;
	std::cout << "# of Airways " << numOfAirways << std::endl;
	std::cout << "# of Acinus " << numOfAcinus << std::endl;
#endif
}

AirwayTreePartitioning::~AirwayTreePartitioning() {
	delete[] part_table_bifurcation;
	delete[] part_table_airway;
	delete[] part_table_acinus;

	delete[] vectorIndexOfBifurcationNode;
	delete[] vectorIndexOfAirwayNode;
	delete[] vectorIndexOfAcinusNode;
}

void AirwayTreePartitioning::partitioning() {
	if (std::pow(2.0, std::floor(std::log2((double) numproc))) != (numproc)) {
		std::cout << "The number of processes must be an integer powers of 2.";
		throw std::runtime_error("The number of processes must be an integer powers of 2.");
	}
	/// Set all to the first process
	for (int i = 0; i < numOfBifurcation; i++) {
		part_table_bifurcation[i] = 0;
	}
	for (int i = 0; i < numOfAcinus; i++) {
		part_table_acinus[i] = 0;
	}
	part_table_inlet = 0;
	for (int i = 0; i < numOfAirways; i++) {
		part_table_airway[i] = 0;
	}

	if (numproc > 1) {
		int tracheaIndex = lung->get_IndexOfTrachea();
		int upperMainGeneration = std::floor(std::log2((double) numproc));
#ifdef DEBUG
		std::cout << "Upper main Generation = " << upperMainGeneration << std::endl;
		std::cout << "Trachea Airway Index = " << tracheaIndex << std::endl;
#endif

		int awIndex = tracheaIndex;
		part_table_airway[awIndex] = 0;
		for (int i = 1; i <= upperMainGeneration; i++) {
			int lowBif = lung->get_LowerBifurcationIndex(awIndex);
			int daughterTubeIndx1 = lung->get_DaughterTube1Index(lowBif);
			int daughterTubeIndx2 = lung->get_DaughterTube2Index(lowBif);
			int step = std::pow(2, (upperMainGeneration - i));
			part_table_airway[daughterTubeIndx1] = part_table_airway[awIndex];
			part_table_airway[daughterTubeIndx2] = part_table_airway[awIndex] + step;
			if ((myid >= part_table_airway[daughterTubeIndx1]) && (myid < part_table_airway[daughterTubeIndx2])) {
				awIndex = daughterTubeIndx1;
			} else {
				awIndex = daughterTubeIndx2;
			}
		}
		recursivelySetProcessID_part_table_airway(awIndex);
		int *part_table_airway_bak = new int[numOfAirways];
		MPI_Allreduce(part_table_airway, part_table_airway_bak, numOfAirways, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
		delete[] part_table_airway;
		part_table_airway = part_table_airway_bak;

		/// setting process own
		for (int i = 0; i < numOfBifurcation; i++) {
			part_table_bifurcation[i] = part_table_airway[lung->get_ParentTubeIndex(i)];
		}
		for (int i = 0; i < numOfAcinus; i++) {
			part_table_acinus[i] = part_table_airway[lung->get_TerminalAirway(i)];
		}
		part_table_inlet = part_table_airway[tracheaIndex];
	}

	/// determine vector indices
	vectorIndexOfBifurcationNode = new int[numOfBifurcation];
	int index = 0;
	for (int proc = 0; proc < numproc; proc++) {
		for (int i = 0; i < numOfBifurcation; i++) {
			if (part_table_bifurcation[i] == proc) {
				vectorIndexOfBifurcationNode[i] = index;
				index++;
			}
		}
	}
	vectorIndexOfAirwayNode = new int[numOfAirways];
	index = 0;
	for (int proc = 0; proc < numproc; proc++) {
		for (int i = 0; i < numOfAirways; i++) {
			if (part_table_airway[i] == proc) {
				vectorIndexOfAirwayNode[i] = index;
				index++;
			}
		}
	}
	vectorIndexOfAcinusNode = new int[numOfAcinus];
	index = 0;
	for (int proc = 0; proc < numproc; proc++) {
		for (int i = 0; i < numOfAcinus; i++) {
			if (part_table_acinus[i] == proc) {
				vectorIndexOfAcinusNode[i] = index;
				index++;
			}
		}
	}
}

void AirwayTreePartitioning::recursivelySetProcessID_part_table_airway(int awIdx) {
	int lowBif = lung->get_LowerBifurcationIndex(awIdx);
	if (lowBif < 0) return;

	int daughterTubeIndx1 = lung->get_DaughterTube1Index(lowBif);
	int daughterTubeIndx2 = lung->get_DaughterTube2Index(lowBif);

	part_table_airway[daughterTubeIndx1] = myid;
	part_table_airway[daughterTubeIndx2] = myid;

	this->recursivelySetProcessID_part_table_airway(daughterTubeIndx1);
	this->recursivelySetProcessID_part_table_airway(daughterTubeIndx2);
}

int AirwayTreePartitioning::part_bifurcation(int i) {
	return part_table_bifurcation[i];
}

int AirwayTreePartitioning::part_airway(int i) {
	return part_table_airway[i];
}

int AirwayTreePartitioning::part_acinus(int i) {
	return part_table_acinus[i];
}

int AirwayTreePartitioning::part_inlet() {
	return part_table_inlet;
}

int AirwayTreePartitioning::getVectorIndexOfBifurcationNode(int i) {
	return vectorIndexOfBifurcationNode[i];
}

int AirwayTreePartitioning::getVectorIndexOfAirwayNode(int i) {
	return vectorIndexOfAirwayNode[i];
}

int AirwayTreePartitioning::getVectorIndexOfAcinusNode(int i) {
	return vectorIndexOfAcinusNode[i];
}

void AirwayTreePartitioning::exportPartionVTK(const char *baseFileName) {
	if (myid == 0) {
		std::string fileName = baseFileName;
		fileName += "_Partition.vtu";

		vtkXMLUnstructuredGridWriter *writer = vtkXMLUnstructuredGridWriter::New();
		vtkUnstructuredGrid *unstructured_grid = vtkUnstructuredGrid::New();
		vtkCellArray *cells = vtkCellArray::New();
		vtkPoints *positions = vtkPoints::New();
		vtkDoubleArray *proc = vtkDoubleArray::New();
		proc->SetNumberOfComponents(1);
		proc->SetName("Process");

		int numVertices = lung->get_NumberOfVertices();
		for (int i = 0; i < numVertices; i++) {
			Eigen::Vector3d cord = lung->get_VertexCoordinate(i);
			positions->InsertNextPoint(cord(0), cord(1), cord(2));
		}
		unstructured_grid->SetPoints(positions);

		vtkIdType cell[2];
		for (int i = 0; i < numOfAirways; i++) {
			cell[0] = lung->get_IndexOfAirwayUpperVertex(i);
			cell[1] = lung->get_IndexOfAirwayLowerVertex(i);
			cells->InsertNextCell(2, cell);
			double p = part_table_airway[i];
			proc->InsertNextTuple(&p);
		}
		unstructured_grid->SetCells(VTK_LINE, cells);
		unstructured_grid->GetCellData()->AddArray(proc);
#if (VTK_MAJOR_VERSION >=6)
		writer->SetInputData(unstructured_grid);
#else
		writer->SetInput(unstructured_grid);
#endif
		writer->SetFileName(fileName.c_str());
		writer->SetDataModeToAscii();
		writer->Write();

		writer->Delete();

		cells->Delete();
		proc->Delete();
		unstructured_grid->Delete();
		positions->Delete();
	}
	MPI_Barrier(MPI_COMM_WORLD);
}
