/*
 * AirwayTreePartitioning.h
 *
 *  Created on: Jun 2, 2015
 *      Author: fuji
 */

#ifndef AIRWAYTREEPARTITIONING_H_
#define AIRWAYTREEPARTITIONING_H_

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <mpi.h>
#include "LungLobeFromAirwayTree.h"

/// This class to partition the airway tree with a simple way.
/// The number of processes must be a integer powers of 2.
class AirwayTreePartitioning {
public:
	/// Constructor takes an object of 'LungLobeFromAirwayTree'
	AirwayTreePartitioning(LungLobeFromAirwayTree *lung);

	/// Destructor frees memory
	virtual ~AirwayTreePartitioning();

	/// Partitioning the model
	virtual void partitioning();

	/// Get the partition number of the bifurcation of index, i.
	int part_bifurcation(int i);

	/// Get the partition number of the airway of index, i.
	int part_airway(int i);

	/// Get the partition number of the acinus of index, i.
	int part_acinus(int i);

	/// Get the partition number of the Trachea.
	int part_inlet();

	/// Get the global element number of PetSc Vector for Bifurcation node.
	int getVectorIndexOfBifurcationNode(int i);

	/// Get the global element number of PetSc Vector for Airway node.
	int getVectorIndexOfAirwayNode(int i);

	/// Get the global element number of PetSc Vector for Acinus node.
	int getVectorIndexOfAcinusNode(int i);

	/// Get the number of nodes in the graph
	int getNodeCount() const {
		return node_count;
	}

	/// Get the number of elements in the graph
	int getElementCount() const {
		return element_count;
	}

	/// Get the number of Acini
	int getNumOfAcinus() const {
		return numOfAcinus;
	}

	/// Get the number of Airways
	int getNumOfAirways() const {
		return numOfAirways;
	}

	/// Get the number of Bifurcation
	int getNumOfBifurcation() const {
		return numOfBifurcation;
	}

	/// Set an array of the global element number of PetSc Vector for Bifurcation node
	void setVectorIndexOfBifurcationNode(int* vectorIndexOfBifurcationNode) {
		this->vectorIndexOfBifurcationNode = vectorIndexOfBifurcationNode;
	}

	LungLobeFromAirwayTree*& getLung() {
		return lung;
	}

	/// Export Partioning Resutl to VTK
	void exportPartionVTK(const char *baseFileName);

protected:
	LungLobeFromAirwayTree* lung;
	int numOfAirways;
	int numOfBifurcation;
	int numOfAcinus;

	int node_count;
	int element_count;

	int *part_table_bifurcation;
	int *part_table_airway;
	int *part_table_acinus;
	int part_table_inlet;

	int *vectorIndexOfBifurcationNode;
	int *vectorIndexOfAirwayNode;
	int *vectorIndexOfAcinusNode;

	/// process id
	int myid;
	/// number of processes
	int numproc;
private:
	void recursivelySetProcessID_part_table_airway(int awIdx);
};

#endif /* AIRWAYTREEPARTITIONING_H_ */
