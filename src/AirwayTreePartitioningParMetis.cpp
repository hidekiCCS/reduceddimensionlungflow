/*
 * AirwayTreePartitioningParMetis.cpp
 *
 *  Created on: May 21, 2019
 *      Author: fuji
 */

#include "AirwayTreePartitioningParMetis.h"

AirwayTreePartitioningParMetis::AirwayTreePartitioningParMetis(LungLobeFromAirwayTree* lung) :
		AirwayTreePartitioning(lung) {
	///
	/// Making Graph data
	///
	xadj = new idx_t[numOfBifurcation + numOfAcinus + 2];
	adjency = new idx_t[numOfAirways * 2];
	element_count = 0;
	node_count = 0;
	std::vector<int> element_stack;
	int intel_element;
	/// bifurcation
	for (int i = 0; i < numOfBifurcation; i++) {
		xadj[node_count] = element_count;
		part_table_bifurcation[i] = node_count;
		int pt = lung->get_ParentTubeIndex(i);
		int dt1 = lung->get_DaughterTube1Index(i);
		int dt2 = lung->get_DaughterTube2Index(i);
		/// for parent tube
		int bif_up = lung->get_UpperBifurcationIndex(pt);
		part_table_airway[pt] = node_count;
		if (bif_up >= 0) {
			adjency[element_count] = bif_up;
			element_count++;
		} else {
			// this is the mouth
			part_table_inlet = pt;
			intel_element = element_count;
			element_count++;
		}
		/// for daughter tube 1
		int bif_low1 = lung->get_LowerBifurcationIndex(dt1);
		//part_table_airway[dt1] = element_count;
		if (bif_low1 >= 0) {
			adjency[element_count] = bif_low1;
			element_count++;
		} else {
			// this is a acinus
			adjency[element_count] = dt1;
			element_stack.push_back(element_count);
			element_count++;
		}
		/// for daughter tube 2
		int bif_low2 = lung->get_LowerBifurcationIndex(dt2);
		//part_table_airway[dt2] = element_count;
		if (bif_low2 >= 0) {
			adjency[element_count] = bif_low2;
			element_count++;
		} else {
			// this is a acinus
			adjency[element_count] = dt2;
			element_stack.push_back(element_count);
			element_count++;
		}
		node_count++;
	}
	/// acinus
	for (int i = 0; i < numOfAcinus; i++) {
		xadj[node_count] = element_count;
		part_table_acinus[i] = node_count;
		int teminal_bronchi = lung->get_TerminalAirway(i);
		part_table_airway[teminal_bronchi] = node_count;
		std::vector<int>::iterator itr = element_stack.begin();
		while (itr != element_stack.end()) {
			if (adjency[*itr] == teminal_bronchi) {
				adjency[element_count] = lung->get_UpperBifurcationIndex(teminal_bronchi);
				element_count++;
				adjency[*itr] = node_count;
				element_stack.erase(itr);
				break;
			} else {
				itr++;
			}
#ifdef DEBUG
			if (itr == element_stack.end()) {
				std::cout << "stranger things have happened\n";
			}
#endif
		}
		node_count++;
	}
	/// mouth
	xadj[node_count] = element_count;
	adjency[element_count] = lung->get_LowerBifurcationIndex(part_table_inlet);
	element_count++;
	adjency[intel_element] = node_count;
	part_table_inlet = node_count;
	node_count++;
	xadj[node_count] = element_count;

#ifdef DEBUG
	std::cout << "# of Nodes " << node_count << std::endl;
	std::cout << "# of Elements " << element_count << std::endl;
#endif

}

AirwayTreePartitioningParMetis::~AirwayTreePartitioningParMetis() {
	// TODO Auto-generated destructor stub
}

void AirwayTreePartitioningParMetis::partitioning() {
	idx_t nparts = numproc;
	if (nparts > 1) {
		idx_t nn = node_count;
		idx_t ne = xadj[node_count];

		idx_t *vtxdist = new idx_t[numproc + 1];
		for (int i = 0; i < numproc; i++) {
			vtxdist[i] = i * nn / numproc;
		}
		vtxdist[numproc] = nn;
		idx_t *p_xadj = new idx_t[vtxdist[myid + 1] - vtxdist[myid] + 1];
		for (int i = 0; i <= nn; i++) {
			if ((i >= vtxdist[myid]) && (i <= vtxdist[myid + 1])) {
				p_xadj[i - vtxdist[myid]] = xadj[i];
			}
		}
		idx_t *p_adjency = new idx_t[p_xadj[vtxdist[myid + 1] - vtxdist[myid]] - p_xadj[0]];
		for (int i = 0; i < ne; i++) {
			if ((i >= p_xadj[0]) && (i < p_xadj[vtxdist[myid + 1] - vtxdist[myid]])) {
				p_adjency[i - p_xadj[0]] = adjency[i];
			}
		}
		// weight
		idx_t *vwgt = new idx_t[vtxdist[myid + 1] - vtxdist[myid]];
		std::fill(vwgt,vwgt+vtxdist[myid + 1] - vtxdist[myid],1);
		for (int i = 0; i < numOfAcinus; i++) {
			if ((part_table_acinus[i] >= vtxdist[myid]) && (part_table_acinus[i] < vtxdist[myid + 1])) {
				vwgt[part_table_acinus[i] - vtxdist[myid]] = 2;
			}
		}

		/// adjust xadj
		int offset = p_xadj[0];
		for (int i = vtxdist[myid]; i <= vtxdist[myid + 1]; i++) {
			p_xadj[i - vtxdist[myid]] -= offset;
		}

		idx_t wgtflag = 2; // Weight on the vertices only
		idx_t numflag = 0;
		idx_t ncon = 1;
		idx_t nparts = numproc;
		real_t *ubvec = new real_t[ncon];
		real_t *tpwgts = new real_t[ncon * nparts];
		for (int i = 0; i < ncon * nparts; i++)
			tpwgts[i] = 1.0 / (real_t) nparts;
		idx_t *part = new idx_t[vtxdist[myid + 1] - vtxdist[myid] + 1];
		idx_t edgecut;
		idx_t options[10];
		real_t itr = 1000.0;
		if (myid == 0) {
			std::cout << "ParMetis...";
		}
		ubvec[0] = 1.05;
		options[0] = 0;
		options[1] = 3;
		options[2] = 1;
		MPI_Comm comm;
		MPI_Comm_dup(MPI_COMM_WORLD, &comm);
		ParMETIS_V3_PartKway(vtxdist, p_xadj, p_adjency, vwgt, NULL, &wgtflag, &numflag, &ncon, &nparts, tpwgts, ubvec, options, &edgecut, part, &comm);
		ParMETIS_V3_AdaptiveRepart(vtxdist, p_xadj, p_adjency, vwgt, NULL, NULL, &wgtflag, &numflag, &ncon, &nparts, tpwgts, ubvec, &itr, options, &edgecut, part, &comm);
		if (myid == 0) {
			std::cout << "done\n";
		}

		int *npart = new int[nn + 1];
		for (int i = vtxdist[myid]; i < vtxdist[myid + 1]; i++) {
			npart[i] = part[i - vtxdist[myid]];
		}
		for (int proc = 0; proc < numproc; proc++) {
			MPI_Bcast(&npart[vtxdist[proc]], vtxdist[proc + 1] - vtxdist[proc], MPI_INT, proc, MPI_COMM_WORLD);
		}

#ifdef DEBUG
		for (int i = 0; i < node_count; i++) {
			//std::cout << "node" << i << ": part" << npart[i] << std::endl;
		}
#endif

		/// setting process own
		for (int i = 0; i < numOfBifurcation; i++) {
			part_table_bifurcation[i] = npart[part_table_bifurcation[i]];
		}
		for (int i = 0; i < numOfAcinus; i++) {
			part_table_acinus[i] = npart[part_table_acinus[i]];
		}
		part_table_inlet = npart[part_table_inlet];

		for (int i = 0; i < numOfAirways; i++) {
			part_table_airway[i] = npart[part_table_airway[i]];
		}
		//for (int i = 0; i < numOfAirways; i++) {
		//	part_table_airway[i] = epart[part_table_airway[i]];
		//}
		//cleanup
		delete[] vwgt;
		delete[] vtxdist;
		delete[] p_xadj;
		delete[] p_adjency;
		delete[] ubvec;
		delete[] tpwgts;
		delete[] part;
	} else {
		for (int i = 0; i < numOfBifurcation; i++) {
			part_table_bifurcation[i] = 0;
		}
		for (int i = 0; i < numOfAcinus; i++) {
			part_table_acinus[i] = 0;
		}
		part_table_inlet = 0;
		for (int i = 0; i < numOfAirways; i++) {
			part_table_airway[i] = 0;
		}
	}

	/// determine vector indices
	vectorIndexOfBifurcationNode = new int[numOfBifurcation];
	int index = 0;
	for (int proc = 0; proc < nparts; proc++) {
		for (int i = 0; i < numOfBifurcation; i++) {
			if (part_table_bifurcation[i] == proc) {
				vectorIndexOfBifurcationNode[i] = index;
				index++;
			}
		}
	}
	vectorIndexOfAirwayNode = new int[numOfAirways];
	index = 0;
	for (int proc = 0; proc < nparts; proc++) {
		for (int i = 0; i < numOfAirways; i++) {
			if (part_table_airway[i] == proc) {
				vectorIndexOfAirwayNode[i] = index;
				index++;
			}
		}
	}
	vectorIndexOfAcinusNode = new int[numOfAcinus];
	index = 0;
	for (int proc = 0; proc < nparts; proc++) {
		for (int i = 0; i < numOfAcinus; i++) {
			if (part_table_acinus[i] == proc) {
				vectorIndexOfAcinusNode[i] = index;
				index++;
			}
		}
	}

}
