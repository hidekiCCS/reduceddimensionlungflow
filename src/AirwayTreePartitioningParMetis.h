/*
 * AirwayTreePartitioningParMetis.h
 *
 *  Created on: May 21, 2019
 *      Author: fuji
 */

#ifndef SRC_AIRWAYTREEPARTITIONINGPARMETIS_H_
#define SRC_AIRWAYTREEPARTITIONINGPARMETIS_H_
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <mpi.h>
#include <parmetis.h>
#include "LungLobeFromAirwayTree.h"
#include "AirwayTreePartitioning.h"

/// This class to partition the airway tree using ParMetis.
class AirwayTreePartitioningParMetis: public AirwayTreePartitioning {
public:
	AirwayTreePartitioningParMetis(LungLobeFromAirwayTree* lung);
	virtual ~AirwayTreePartitioningParMetis();

	/// Partitioning the model
	virtual void partitioning();

private:
	idx_t *xadj;
	idx_t *adjency;
};

#endif /* SRC_AIRWAYTREEPARTITIONINGPARMETIS_H_ */
