/*
 * AirwayTubeLaw.cpp
 *
 *  Created on: Nov 7, 2014
 *  Modified on: June 25, 2015
 *      Author: fuji
 */

#include <iostream>
#include <cmath>
#include "AirwayTubeLaw.h"

AirwayTubeLaw::AirwayTubeLaw(int index, PleuralPressureWaveForm *pl) :
		Airway(index, pl), radiusConverged(true) {
	/// Set parenchymal tethering flag. Default is false
	parenchymaTether = false;
	/// Default \f$ \beta \f$ determined by Fujioka et al. (2013).
	beta_correct = -0.391;
	parenchymalShearModulus = 0.0;
	periAirwayPressureValue = pl->getPressure();
}

AirwayTubeLaw::~AirwayTubeLaw() {
}

double AirwayTubeLaw::computeAirwayConductance(void) {
	/// - Surface Tension in cm.cmH2O \f$\gamma\f$ / Airway::cmH2OtoDynsParSquareCm
	const double surfaceTension = surfaceTensionObject->getSurfaceTension();
	double sigma = surfaceTension / cmH2OtoDynsParSquareCm;

	double tol = 1.0e-3; /// - Set Tolerance \a tol=1.0e-3
	double r, dr;

	///	- Compute \f$ \frac{\Delta R_{AW}}{R_{AW,u}}\f$, where \f$ \Delta R_{AW} = R_{AW} - R_{AW,u}\f$,
	///			\f$R_{AW,u}\f$ is the parenchymal hole radius at uniform strain state.
	double dru = (radius - parenchymalHoleRadius) / parenchymalHoleRadius;
	//std::cout << "Radius=" << radius << " Hole=" << parenchymalHoleRadius << " Rest=" << radiusRest << std::endl;

	///	- Compute Peri-Airway Pressure by calling AirwayTubeLaw::periAirwayPressure
	periAirwayPressureValue = periAirwayPressure(dru);
	///	- Compute the transmural pressure from given luminal pressure (\f$P_{AW}\f$), and peri-airway pressures (\f$P_{PA}\f$),
	///		surface tension (\f$\sigma\f$), radius (\f$R_{AW}\f$) and \f$\epsilon\f$.
	///			\f$\Delta P_{tm}=P_{AW}-\frac{\sigma}{R_{AW}(1-\epsilon)} - P_{PA}\f$

	transmuralPressure = this->getPressure() - sigma / (this->radius * (1 - this->eps)) - periAirwayPressureValue;
	///	- Update Airway::radius by Tube-Law by Lambert et al.(1982)
	///		\f$
	///		\alpha=\left\{ \begin{array}{ll}
	/// 	1-\left(1-\alpha_{0}\right)\left(1-\frac{\Delta P_{tm}}{P_{2}}\right)^{-n_{2}} & \Delta P_{tm}\geq0\\
	/// 	\alpha_{0}\left(1-\frac{\Delta P_{tm}}{P_{1}}\right)^{-n_{1}} & \Delta P_{tm}<0
	/// 	\end{array}\right.
	///		\f$

	if (transmuralPressure < 0) {
		r = this->rmax * std::sqrt(alpha0 * std::pow(1 - transmuralPressure / p1, -n1));
	} else {
		r = this->rmax * std::sqrt(1 - (1 - alpha0) * std::pow(1 - transmuralPressure / p2, -n2));
	}
	dr = (r - this->radius);
	/// - Check convergence
	///		- If the change of radius from the value at previous iteration step is less than Tolerance, \a tol,
	/// 	  set AirwayTubeLaw::radiusConverged = true
	///		- Otherwise set AirwayTubeLaw::radiusConverged = false
	if (std::abs(dr / this->rmax) < tol) {
		radiusConverged = true;
	} else {
		radiusConverged = false;
	}
	this->radius += underrelaxation * (r - this->radius);

	/// - Update \f$\epsilon\f$ assuming total liquid volume to be conserved.

	computeEpsilon();

	/// - Compute conductance with Poiseuille's law.
	conductance = (M_PI * std::pow(this->radius * (1 - this->eps), 4)) / (8.0 * this->fluidViscosity * this->length);
	return conductance;
}

void AirwayTubeLaw::setRmax(double rmax) {
	/// * From UpdateRadius.h by JR
	this->rmax = rmax;
	alpha0 = 1.367533004907444 * pow(this->rmax, 0.911482014812055);
	if (this->rmax > 0.614) {
		alpha0 = 0.882;
	}
	alpha0p = 2.88e-4 * std::pow(1.3e-2, this->rmax) / 0.001019716213;
	if (this->rmax >= 0.6142) {
		n1 = 0.5;
	} else if (this->rmax >= 0.3732 && this->rmax < 0.6142) {
		n1 = 0.6;
	} else if (this->rmax >= 0.2992 && this->rmax < 0.3732) {
		n1 = 0.7;
	} else if (this->rmax >= 0.2296 && this->rmax < 0.2992) {
		n1 = 0.8;
	} else if (this->rmax >= 0.1798 && this->rmax < 0.2296) {
		n1 = 0.9;
	} else {
		n1 = 1.0;
	}
	if (this->rmax >= 0.0704) {
		n2 = 10.0;
	} else if (this->rmax >= 0.0567 && this->rmax < 0.0704) {
		n2 = 9.0;
	} else if (this->rmax >= 0.0367 && this->rmax < 0.0567) {
		n2 = 8.0;
	} else {
		n2 = 7.0;
	}

	p1 = (alpha0 * n1) / alpha0p;
	p2 = (-n2 * (1 - alpha0)) / alpha0p;

	/// * Compute the airway radius at rest \f$ \sqrt{\alpha}R_{max}\f$
	radiusRest = this->rmax * std::sqrt(alpha0);
}

double AirwayTubeLaw::computeRadiusAtZeroPressure() {
	/// * Bakcup Current Pressure value
	double prs = pressure;

#ifdef DEBUG
	std::cout << "R / Rmax = " << this->radius << " / " << this->rmax << " eps=" << this->eps << std::endl;
#endif
	/// * Compute Radius at Zero Airway Pressure

	pressure = 0.0;
	const int mItr = 1000;
	const double tol = 1.0e-6;
	double r = this->radius;
	double dr;
	for (int i = 0; i < mItr; i++) {
		AirwayTubeLaw::computeAirwayConductance();
		dr = this->radius - r;
		if (std::abs(dr) < tol) break;
		r = this->radius;
	}

#ifdef DEBUG
	std::cout << "R / Rmax = " << this->radius << " / " << this->rmax << " Ptm=" << transmuralPressure << std::endl;
#endif

	/// * Restore pressure
	pressure = prs;
}

double AirwayTubeLaw::periAirwayPressure(double dru) {
	/// * If parenchymaTether is true, compute the peri-airway pressure.
	/// * If false, return the pleural pressure.
	double ppa = pl->getPressure();
	if (parenchymaTether) {
		///	* Computing the peri-airway pressure, \f$ P_{PA}\f$ from the relation by Lai-Fook (1979).
		/// \f$\frac{\Delta R_{AW}}{R_{AW,u}}+\beta\left(\frac{\Delta R_{AW}}{R_{AW,u}}\right)^{2}=\frac{P_{PA}-P_{PL}}{2\mu}\f$
		///	where \f$\mu\f$ is the parenchymal shear modulus, and \f$ \beta \f$ is the coefficient for correction term.
		/// * \f$\mu\f$ and \f$ \beta \f$ can be determined by Fujioka et al. (2013).
		///		+ AirwayTubeLaw::parenchymalShearModulus stores \f$\mu\f$ that may assigned by void ParenchymalTethering::updateParechymalShearModulus
		///		+ AirwayTubeLaw::beta_correct stores \f$ \beta \f$
		double tetherPress = 2.0 * parenchymalShearModulus * (dru + beta_correct * dru * dru);
#ifdef DEBUG
		std::cout << "Parenchymal Shear Modulus = " << parenchymalShearModulus << std::endl;
		std::cout << "Pleural Pressure = " << ppa << " Tether Pressure = " << tetherPress << " dR = " << dru << std::endl;
#endif
		ppa += tetherPress;
	}
	return ppa;
}
