/*
 * AirwayTubeLaw.h
 *
 *  Created on: Nov 7, 2014
 *  Modified on: June 25, 2015
 *      Author: fuji
 */

#ifndef AIRWAYTUBELAW_H_
#define AIRWAYTUBELAW_H_

#include "Airway.h"

/** Airway model with the tube-law based on model proposed by Lambert et al. (1982).

\f$\alpha=\left\{ \begin{array}{ll}
1-\left(1-\alpha_{0}\right)\left(1-\frac{\Delta P_{tm}}{P_{2}}\right)^{-n_{2}} & \Delta P_{tm}\geq0\\
\alpha_{0}\left(1-\frac{\Delta P_{tm}}{P_{1}}\right)^{-n_{1}} & \Delta P_{tm}<0
\end{array}\right.\f$

where \f$\alpha=A_{AW}/A_{AW,max}\f$, \f$A\f$
is the cross-sectional area of the airway and \f$A_{AW,max}\f$
is its maximum, and \f$\Delta P_{tm}\f$ is the transmural pressure across the airway,
which is equivalent to the elastic recoil pressure in the airway tissue at static conditions.
The values of the constant parameters, \f$\alpha_{0}\f$, \f$P_{1}\f$, \f$P_{2}\f$, \f$n_{1}\f$
and \f$n_{2}\f$ are different for each airway generation and are tabulated in Lambert et al.(1982).
AirwayTubeLaw::setRmax sets those parameters as a function of \f$R_{max}\f$.

**/
class AirwayTubeLaw: public Airway {
public:
	/// Constructor takes a index and a pleural pressure waveform object.
	AirwayTubeLaw(int index, PleuralPressureWaveForm *pl);

	/// Destructor do nothing.
	virtual ~AirwayTubeLaw();

	/// Compute Airway Conductance = 1/Resistance
	virtual double computeAirwayConductance(void );

	/// Set the maximum radius and
	/// values of the constant parameters, \f$\alpha_{0}\f$, \f$P_{1}\f$, \f$P_{2}\f$, \f$n_{1}\f$
	/// and \f$n_{2}\f$
	virtual void setRmax(double rmax);

	/// Compute Radius at Zero Pressure
	virtual double computeRadiusAtZeroPressure();

	bool isParenchymaTether() const {
		return parenchymaTether;
	}

	void setParenchymaTether(bool parenchymaTether) {
		this->parenchymaTether = parenchymaTether;
	}

	double getParenchymalShearModulus() const {
		return parenchymalShearModulus;
	}

	void setParenchymalShearModulus(double parenchymalShearModulus) {
		this->parenchymalShearModulus = parenchymalShearModulus;
	}

	double getBetaCorrect() const {
		return beta_correct;
	}

	void setBetaCorrect(double betaCorrect) {
		beta_correct = betaCorrect;
	}

	double getParenchymalHoleRadius() const {
		return parenchymalHoleRadius;
	}

	void setParenchymalHoleRadius(double parenchymalHoleRadius) {
		this->parenchymalHoleRadius = parenchymalHoleRadius;
	}

	virtual double getRadiusRest() const {
		return radiusRest;
	}

	double getPeriAirwayPressureValue() const {
		return periAirwayPressureValue;
	}

	bool isRadiusConverged() const {
		return radiusConverged;
	}

private:
	/// Compute Peri-Airway Pressure
	double periAirwayPressure(double dru);

	/// Tube law parameters
	double n1,n2,p1,p2,alpha0,alpha0p;

	/// Peri-Airway Pressure
	double periAirwayPressureValue;
	/// Parenchymal Shear Modulus
	double parenchymalShearModulus;
	///  Parenchymal Hole Radius at Uniform stress state.
	double parenchymalHoleRadius;
	/// Radius at rest computed from \f$ R_{max} \f$ and Tube-Law.
	double radiusRest;
	/// coefficient for correction term
	double beta_correct;

	/// Flag for Parechymal Tethering (default false)
	bool parenchymaTether;

	/// Flag for convergence of Radius updating in AirwayTubeLaw::computeAirwayConductance
	bool radiusConverged;

#if __cplusplus > 199711L
#define _CONST_TLM constexpr
#else
#define _CONST_TLM const
#endif
	/// Under Relaxation for updating Peri-Airway Pressure
	static _CONST_TLM double underrelaxation = 0.1;
	/// \f$cmH_2O\f$ to \f$Dyns/cm^2\f$
	static _CONST_TLM  double cmH2OtoDynsParSquareCm = 980.665;
#undef  _CONST_TLM

};

#endif /* AIRWAYTUBELAW_H_ */
