/*
 * AirwayWithClosure.cpp
 *
 *  Created on: Feb 10, 2015
 *      Author: fuji
 */

#include "AirwayWithClosure.h"

AirwayWithClosure::AirwayWithClosure(int index, PleuralPressureWaveForm* pl):AirwayTubeLaw(index,pl){
	/// Calling upper class constructors.
	closure=false;
}

AirwayWithClosure::~AirwayWithClosure() {
	// TODO Auto-generated destructor stub
}

double AirwayWithClosure::computeAirwayConductance(void) {
	/// * Check if the airway closed already, looking at AirwayWithClosure::closure.
	///		+ If closed already, return zero conductance.
	if (closure){
		return 0.0;
	}
	/// * Calling the upper class method to obtain the flow conductance and compute radius and \f$\epsilon\f$.
	double tube_conductance = AirwayTubeLaw::computeAirwayConductance();

	/// * Check if \f$\epsilon \geq \epsilon_{critical}\f$.
	///		+ If so, set AirwayWithClosure::closure = true and return zero conductance.
	if (eps > eps_critical){
		closure = true;
		tube_conductance = 0.0;
	}
	return tube_conductance;
}
