/*
 * AirwayWithClosure.h
 *
 *  Created on: Feb 10, 2015
 *      Author: fuji
 */

#ifndef AIRWAYWITHCLOSURE_H_
#define AIRWAYWITHCLOSURE_H_

#include "AirwayTubeLaw.h"

/// Airway Model with closure due to the formation of a liquid plug.

/// * Plug will form when the film thickness exceeds the critical value, \f$\epsilon_{critical}\f$.
/// * Liquid plug won't move and cause zero air-flow conductance.
/// * When once the airway closed, it never reopen and its radius remains fixed.
/// * While airway open, it behaves same as AirwayTubeLaw (tube-law based on model proposed by Lambert et al. (1982))
class AirwayWithClosure: public AirwayTubeLaw {
public:
	/// Constructor takes a index and a pleural pressure waveform object.
	/// It calls upper class constructors
	AirwayWithClosure(int index, PleuralPressureWaveForm *pl);

	/// Destructor does nothing
	virtual ~AirwayWithClosure();

	/// Compute Airway Conductance = 1/Resistance
	virtual double computeAirwayConductance(void );

private:

#if __cplusplus > 199711L
#define _CONST_TLM constexpr
#else
#define _CONST_TLM const
#endif
	/// The critical dimensionless film thickness \f$\epsilon_{critical}\f$ based on Halpern and Grotberg (19??)
	static _CONST_TLM double eps_critical = 0.13;
	/// Flag for closure. the default is false.
	/// When the film thickness exceeds the critical value, \f$\epsilon_{critical}\f$,
	/// it becomes true and makes the air-flow conductance to be zero.
#if __cplusplus > 199711L
	bool closure = false;
#else  
	bool closure;
#endif
#undef _CONST_TLM
};

#endif /* AIRWAYWITHCLOSURE_H_ */
