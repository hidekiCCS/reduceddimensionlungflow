/*
 * AirwayWithPlugPropagation.cpp
 *
 *  Created on: Feb 10, 2015
 *      Author: fuji
 */

#include "AirwayWithPlugPropagation.h"
#include <stdexcept>
#include <Eigen/Core>
#include <Eigen/Dense>

AirwayWithPlugPropagation::AirwayWithPlugPropagation(int index, PleuralPressureWaveForm* pl):AirwayTubeLaw(index,pl) {
	/// * Calling upper class constructors.
	/// * Default Liquid Viscosity is 1.0197e-5 cmsH20
	liquidViscosity_cm2H2O = 1.0197e-5;
}


AirwayWithPlugPropagation::~AirwayWithPlugPropagation() {
	/// Delete LiquidPlug Objects
	std::vector<LiquidPlug *>::iterator itlp = listOfPlugsInThisAirway.begin();
	while (itlp != listOfPlugsInThisAirway.end()){
		delete *itlp;
		itlp = listOfPlugsInThisAirway.erase(itlp);
	}
}

double AirwayWithPlugPropagation::computeAirwayConductance(void) {
	/// * If \f$\epsilon \geq \epsilon_{critical}\f$ and there is no plug in this airway,
	///  create a liquid plug and compute the flow conductance due to the plug.
	///		+ If plugs existing already, compute the flow conductance due to plugs.
	if ((eps > eps_critical) && (listOfPlugsInThisAirway.size() == 0)){
		createLiquidPlug();
	}
	if (listOfPlugsInThisAirway.size() > 0){
		computeConductanceOfLiquidPlugs();
	}else{
		///	* If no plug exists and \f$\epsilon < \epsilon_{critical}\f$,
		/// 	Calling AirwayTubeLaw::computeAirwayConductance() to obtain the flow conductance and compute radius and \f$\epsilon\f$.
		conductance = AirwayTubeLaw::computeAirwayConductance();
	}

	return conductance;
}


void AirwayWithPlugPropagation::computeConductanceOfLiquidPlugs() {
	/// ## Compute Total Conductance with Newton's Method
	/// The resistance of a single liquid plug can be obtained by calling LiquidPlug::computeResistance.
	/// If there is only one plug in this Airway, this method simply returns the inverse of the resistance.
	/// 	- If only one plug in the airway, set *setLeadingPlug* on.
	int numOfPlugs = this->getNumberOfLiquidPlugs();
	double totalPressureDrop = this->getPressureDrop();
	if (numOfPlugs == 1){
		LiquidPlug *lp = listOfPlugsInThisAirway[0];
		lp->setLeadingPlug(true);
		//std::cout << lp->getPosition() << std::endl;
		double resistance = lp->computeResistance(totalPressureDrop);
		if (resistance <= 0.0){
			throw std::runtime_error("Plug resistance is zero or negative.");
		}
		/// * add air-phase resistance
		resistance += (8.0 * this->fluidViscosity * std::max(this->length - lp->getPlugLength(),0.0)) / (M_PI * std::pow(this->radius,4));
		conductance = 1.0 / resistance;
		return;
	}
#ifdef DEBUG
	std::cout << "Multiple plugs in an airway " << numOfPlugs << std::endl;
#endif
	/// When there are more than one plug in this airway, this method does following procedure to determine the total conductance.
	/// * With initial guesses of pressure drop across each plug, \f$\Delta P_i\f$, computing conductance of each plug.
	/// * Since the flow-rates are same for all plugs, \f$Q=C_1\Delta p_1 = C_2\Delta p_2 = \cdots\f$.
	///
	/// \f$\left[\begin{array}{c}
	/// Q_{1}\\
	/// Q_{2}\\
	/// \vdots\\
	/// Q_{n}
	/// \end{array}\right]=\left[\begin{array}{cccc}
	/// C_1 & 0 & \cdots & 0 \\
	/// 0 & C_2 & \vdots & 0 \\
	///	0 & 0 & \ddots & 0 \\
	///	0 & 0 & \cdots & C_n
	/// \end{array}\right]\left[\begin{array}{c}
	/// \Delta p_{1}\\
	/// \Delta p_{2}\\
	/// \vdots\\
	/// \Delta p_{n}
	/// \end{array}\right]\f$
	/// 	- where \f$Q_{i}\f$ is the flow-rate for i-th plug.
	/// *  The flow-rates are same for all plug, this yields,
	///
	/// \f$\left[\begin{array}{c}r_{1}\\
	/// r_{2}\\
	/// \vdots\\
	/// r_{n-1}
	/// \end{array}\right]=\left[\begin{array}{ccccc}1 & 0 & \cdots & 0 & -1\\
	/// 0 & 1 & 0 & \cdots & -1\\
	/// \vdots &  & \ddots &  & \vdots\\
	/// 0 & \cdots & 0 & 1 & -1
	/// \end{array}\right]\left[\begin{array}{c}Q_{1}\\
	/// Q_{2}\\
	/// \vdots\\
	/// Q_{n}
	/// \end{array}\right]\f$
	///		- In above system, \f$\mathbf{r}=\mathbf{SQ}\f$, \f$\mathbf{S}\f$ is \f$(n-1)\times n\f$ matrix.
	///		- \f$\mathbf{r}=\mathbf{SQ} = \mathbf{SC \Delta P}\f$.
	/// * The sum of the pressure drop across plugs, \f$\Delta p_{1} + \Delta p_{2} + \cdots + \Delta p_{n} = \Delta P_{total}\f$.
	/// Therefore,
	///
	/// \f$\left[\begin{array}{cccc}
	/// &  &  & \\
	/// & \mathbf{SC} &  & \\
	///	&  &  & \\
	///	1 & 1 & \cdots & 1
	/// \end{array}\right]\left[\begin{array}{c}
	/// \Delta p_{1}\\
	/// \Delta p_{2}\\
	/// \vdots\\
	/// \Delta p_{n}
	/// \end{array}\right]-
	/// \left[\begin{array}{c}
	/// 0\\
	/// 0\\
	/// \vdots\\
	/// \Delta P_{total}
	/// \end{array}\right]=\left[\begin{array}{c}r_{1}\\
	/// r_{2}\\
	/// \vdots\\
	/// r_{n}
	/// \end{array}\right]\f$
	///
	/// * For above system, \f$\mathbf{S_C}\Delta \mathbf{P_0} - \Delta \mathbf{P_{total}}=\mathbf{r_0}\f$,
	/// find \f$\Delta \mathbf{P_0}\f$ that results \f$\mathbf{r_0}=0\f$ by following steps,
	/// 1. Solve \f$\mathbf{S_C}(\Delta \mathbf{P_0} - \Delta \mathbf{P}) =\mathbf{r_0}\f$ for \f$\Delta \mathbf{P}\f$.
	/// 2. Update \f$\Delta \mathbf{P_0} = \Delta \mathbf{P}\f$ and compute \f$\mathbf{r_0}\f$.
	/// 3. Repeat until \f$|\mathbf{r_0}| <\f$ AirwayWithPlugPropagation::tolerance .
	Eigen::MatrixXd matS(numOfPlugs - 1, numOfPlugs);
	Eigen::MatrixXd matC(numOfPlugs, numOfPlugs);
	Eigen::MatrixXd matJac(numOfPlugs, numOfPlugs);
	Eigen::VectorXd vecDp = Eigen::VectorXd::Constant(numOfPlugs, totalPressureDrop / numOfPlugs);
	Eigen::VectorXd vecDpTotal(numOfPlugs);
	Eigen::VectorXd vecR0(numOfPlugs);
	// Total Pressure Drop
	vecDpTotal(numOfPlugs-1) = totalPressureDrop;
	// Setup S
	for (int i = 0 ; i < numOfPlugs - 1 ; i++){
		matS(i, i) = 1.0;
		matS(i, numOfPlugs - 1) = -1.0;
	}
	// Newton's Iteration
	for (int itc = 0 ; itc < maxIteration ; itc++){
		// Setup C
		for (int i = 0 ; i < numOfPlugs ; i++){
			LiquidPlug *lp = listOfPlugsInThisAirway[i];
			matC(i,i) = 1.0 / lp->computeResistance(vecDp.coeff(i));
		}
		// Set Jacobian
		matJac.topLeftCorner(numOfPlugs - 1, numOfPlugs) = matS * matC;
		for (int i = 0 ; i < numOfPlugs ; i++){
			matJac(numOfPlugs - 1, i) = 1.0;
		}
		// Compute Residual
		vecR0 = matJac * vecDp - vecDpTotal;
		if (vecR0.norm() < tolerance) break;

		// Solve
		vecDp = vecDp - matJac.householderQr().solve(vecR0);
	}

	/// * Determine AirwayWithPlugPropagation::tube_conductance
	///		- The total conductance is \f$\frac{1}{C_{total}} =  \frac{1}{C_{1}} + \frac{1}{C_{2}} + \frac{1}{C_{3}} + \cdots\f$
	double resistance = 0.0;
	double totalPlugLength = 0.0;
	for (int i = 0 ; i < numOfPlugs ; i++){
		resistance += 1.0 / matC(i,i);
		LiquidPlug *lp = listOfPlugsInThisAirway[i];
		totalPlugLength += lp->getPlugLength();
	}

	/// * Examine Leading Plug
	/// 	- If leading plug, set flag on, if not off.
	double max_po = listOfPlugsInThisAirway[0]->getPosition();
	double min_po = listOfPlugsInThisAirway[0]->getPosition();
	int max_idx = 0;
	int min_idx = 0;
	for (int i = 0 ; i < numOfPlugs ; i++){
		LiquidPlug *lp = listOfPlugsInThisAirway[i];
		if (max_po < lp->getPosition()){
			max_idx = i;
			max_po = lp->getPosition();
		}
		if (min_po > lp->getPosition()){
			min_idx = i;
			min_po = lp->getPosition();
		}
		lp->setLeadingPlug(false);
	}
	if (flowRate >= 0.0){
		listOfPlugsInThisAirway[max_idx]->setLeadingPlug(true);
	}else{
		listOfPlugsInThisAirway[min_idx]->setLeadingPlug(true);
	}

	/// * Compute Conductance
	if (resistance <= 0.0){
		throw std::runtime_error("Resistance is zero/negative");
	}

	/// * Finally, add air-phase resistance
	resistance += (8.0 * this->fluidViscosity * std::max(this->length - totalPlugLength,0.0)) / (M_PI * std::pow(this->radius,4));
	conductance = 1.0 / resistance;
}

LiquidPlug *AirwayWithPlugPropagation::addLiquidPlug(double volume, double position) {
	const double surfaceTension = surfaceTensionObject->getSurfaceTension();
	/// Add a Liquid Plug in this airway.
#ifdef DH_RESISTANCE_MODEL
	LiquidPlug *lp = new LiquidPlugDH(volume, position, this->radius, this->length,this->eps * this->radius);
#else
	LiquidPlug *lp = new LiquidPlug(volume, position, this->radius, this->length,this->eps * this->radius);
#endif
	lp->setSurfaceTension(surfaceTension);
	lp->setLiquidViscositycmsH2O(liquidViscosity_cm2H2O);
	lp->setAirwayFlowRate(this->flowRate);

	/// * Assign Airway::surfaceTension to LiquidPlug::surfaceTension (dynes/cm)
	lp->setSurfaceTension(surfaceTension);
	listOfPlugsInThisAirway.push_back(lp);
	return lp;
}

void AirwayWithPlugPropagation::removeLiquidPlug(LiquidPlug *lp){
	/// Remove an object from AirwayWithPlugPropagation::listOfPlugsInThisAirway .
	std::vector<LiquidPlug *>::iterator it = listOfPlugsInThisAirway.begin();
	while ( it !=  listOfPlugsInThisAirway.end()){
		if (*it == lp) {
			it = listOfPlugsInThisAirway.erase(it);
			return;
		}else{
			it++;
		}
	}
	//if (it ==  listOfPlugsInThisAirway.end()){
	std::cout << "No plug found : Stranger thing has happened\n";
	throw std::runtime_error("No plug found : Stranger thing has happened");
	//}
	/// This method doesn't destroy the LiquidPlug object.
}

bool AirwayWithPlugPropagation::examinePlugCreation(double vol) {
	double core = vol - 2.0 * M_PI * std::pow(radius - eps * radius, 3) / 3.0;
	double len = core / (M_PI * std::pow(radius - eps * radius, 2));
	return (len > 0.0);
}

void AirwayWithPlugPropagation::createLiquidPlug() {
	/// * This routine is called when \f$\epsilon \geq \epsilon_{critical}\f$.
	/// * Create an instance of LiquidPlug object with whole liquid lining on this Airway object.
	///		+ The ratio defined by AirwayWithPlugPropagation::ratioVolume of liquid volume creates a plug.
	///		+ Initial position is 0.5; the middle of airway
	double po = 0.5;
	double plugVol = liquidVolume - length * M_PI * epsRemainingFluid * radius * radius * (2.0 - epsRemainingFluid);
	/// * Subtract the plug volume from the lining liquid volume.
	liquidVolume -= plugVol;
	/// * Update \f$\epsilon\f$
	computeEpsilon();
	LiquidPlug *lp;
	if(examinePlugCreation(plugVol)){
		lp = addLiquidPlug(plugVol, po);
		lp->setPrecursorFilmThickness(eps * radius);
		lp->setAirwayFlowRate(flowRate);
	}else{
		/// *	If not enough volume, plug is not created.
		liquidVolume += plugVol;
		computeEpsilon();
	}
}

void AirwayWithPlugPropagation::setFlowRate(double frate) {
	/// - Examine if flow direction changes
	bool reversed = (this->flowRate * frate <= 0.0);
	/// - Set the flowrate.
	this->flowRate = frate;
	/// - Set Plug Velocity
	for (int i = 0 ; i < listOfPlugsInThisAirway.size() ; i++){
		listOfPlugsInThisAirway[i]->setAirwayFlowRate(this->flowRate);
		/// 	- Re-assign precursor film thickness when flow direction reversed.
		if (reversed){
			std::cout << "Flow Direction Reversed\n";
			listOfPlugsInThisAirway[i]->flowReversed();
		}
	}
}

void AirwayWithPlugPropagation::computeEpsilon() {
	double sq = 1.0 - liquidVolume / (M_PI * length * std::pow(this->radius,2));
	if (sq <= 0.0){
		/// * When airway becomes fully occluded liquid, force set eps=0.99 and compute Radius.

		///		- Then it may create a plug.
		std::cout << "Airway Collapsed\n";
		std::cout << this->index << " R = " << this->radius << " V=" << this->liquidVolume << " length=" <<  this->length << std::endl;

		eps = 0.99;
		radius = std::sqrt(liquidVolume / (M_PI * length * eps * (2.0 - eps)));
		return;
	}
	eps = 1.0 - std::sqrt(sq);
}
