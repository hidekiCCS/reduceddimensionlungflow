/*
 * AirwayWithPlugPropagation.h
 *
 *  Created on: Feb 10, 2015
 *      Author: fuji
 */
/// Airway Model with closure due to the formation of a liquid plug that can propagate.

#ifndef AIRWAYWITHPLUGPROPAGATION_H_
#define AIRWAYWITHPLUGPROPAGATION_H_

#include <iostream>
#include <vector>
#include "AirwayTubeLaw.h"
#include "LiquidPlug.h"

#define DH_RESISTANCE_MODEL
//#undef DH_RESISTANCE_MODEL
#ifdef DH_RESISTANCE_MODEL
#include "LiquidPlugDH.h"
#endif

/// * A liquid plug will form when the liquid film thickness exceeds the critical value, \f$\epsilon_{critical}\f$.
/// * The liquid film remains with \f$\epsilon\f$ = AirwayWithPlugPropagation::epsRemainingFluid after plug creation. 
/// * The liquid plug can move due to the pressure drop across the plug.
/// * Plugs may leave this airway or enter from other airways.
/// * While there are no plugs in this airway, it behaves same as AirwayTubeLaw (tube-law based on model proposed by Lambert et al. (1982))
class AirwayWithPlugPropagation: public AirwayTubeLaw {
public:
	/// Constructor takes a index, and a pleural pressure waveform.
	/// It calls upper class constructors
	AirwayWithPlugPropagation(int index, PleuralPressureWaveForm *pl);
	/// Destructor does nothing so far
	virtual ~AirwayWithPlugPropagation();

	/// Compute Airway Conductance = 1/Resistance.
	///		* When \f$\epsilon \geq \epsilon_{critical}\f$, a plug forms creating LiquidPlug object.
	///		* If the size of AirwayWithPlugPropagation::listOfPlugsInThisAirway is zero, calling the upper class method to determine the air-flow conductance.
	///		* If the size of AirwayWithPlugPropagation::listOfPlugsInThisAirway is not zero, compute total air-flow conductance .
	virtual double computeAirwayConductance(void );

	/// Add a LiquidPlug.
	/// This method should be called by TrackLiquidPlugs object when a plug enters into this Airway from other Airway.
	LiquidPlug * addLiquidPlug(double volume, double position);

	/// Remove a LiquidPlug object from the list.
	/// This method should be called by TrackLiquidPlugs object when a plug leaves this Airway or ruptures.
	void removeLiquidPlug(LiquidPlug *lp);

	/// Get the number of liquid plugs
	int getNumberOfLiquidPlugs(){
		return listOfPlugsInThisAirway.size();
	}
	/// Get an instance of LiquidPlug object
	LiquidPlug * getLiquidPlug(int i){
		return listOfPlugsInThisAirway[i];
	}

	/// Examine if plug can be created by a given liquid volume
	bool examinePlugCreation(double vol);

	/// Compute \f$\epsilon\f$ from a given liquid volume. Override Base.
	virtual void computeEpsilon();

	/// Set the flowrate.
	virtual void setFlowRate(double flowRate);
private:
	/// Compute total conductance of liquid plugs, the resistance of each plug is computed by LiquidPlug::computeResistance()
	void computeConductanceOfLiquidPlugs();
	/// Create a Liquid Plug
	void createLiquidPlug();


#if __cplusplus > 199711L
#define _CONST_TLM constexpr
#else
#define _CONST_TLM const
#endif
	/// The critical dimensionless film thickness \f$\epsilon_{critical}\f$ based on Halpern and Grotberg (19??)
	static _CONST_TLM double eps_critical = 0.13;
	/// List of LiquidPlug object in this Airway object
	std::vector<LiquidPlug *> listOfPlugsInThisAirway;
	/// tolerance for Newton's method
	static _CONST_TLM double tolerance = 1.0e-8;
	/// maximum iteration for Newton's method
	static _CONST_TLM int maxIteration = 1000;
	/// Ratio of liquid volume to create a plug NOT USED
	static _CONST_TLM double ratioVolume = 0.7;
	/// EPS value of remaining liquid lining after a plug creation.
	static _CONST_TLM double epsRemainingFluid = 0.01;

#undef _CONST_TLM
};

#endif /* AIRWAYWITHPLUGPROPAGATION_H_ */
