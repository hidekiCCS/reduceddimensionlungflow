/*
 * Bifurcation.h
 *
 *  Created on: Oct 28, 2014
 *      Author: fuji
 */

#ifndef BIFURCATION_H_
#define BIFURCATION_H_

/// This class to define bifurcation objects.
class Bifurcation {
public:
	/// Constructor takes the index of bifurcation.
	Bifurcation(int index);

	/// Destructor does nothing.
	virtual ~Bifurcation();

	/// Get the index of the daughter tube 1.
	int getDaughterAirwayIndex1() const {
		return daughter_airway_index1;
	}

	/// Get the index of the daughter tube 2.
	int getDaughterAirwayIndex2() const {
		return daughter_airway_index2;
	}

	/// Get the index of the parent tube.
	int getParentAirwayIndex() const {
		return parent_airway_index;
	}

	/// Get the pressure at bifurcation.
	double getPressure() const {
		return pressure;
	}

	/// Set the pressure at bifurcation.
	void setPressure(double pressure) {
		this->pressure = pressure;
	}

	/// Set the index of the daughter tube 1.
	void setDaughterAirwayIndex1(int daughterAirwayIndex1) {
		daughter_airway_index1 = daughterAirwayIndex1;
	}

	/// Set the index of the daughter tube 2.
	void setDaughterAirwayIndex2(int daughterAirwayIndex2) {
		daughter_airway_index2 = daughterAirwayIndex2;
	}

	/// Set the index of the parent tube.
	void setParentAirwayIndex(int parentAirwayIndex) {
		parent_airway_index = parentAirwayIndex;
	}

	/// Get the index.
	int getIndex() const {
		return index;
	}

private:
	/// Global Bifurcation index
	int index;
	/// Parent Tube Index
	int parent_airway_index;
	/// Daughter Tube 1 Index
	int daughter_airway_index1;
	/// Daughter Tube 2 Index
	int daughter_airway_index2;
	/// Pressure at Bifurcation
	double pressure;
};

#endif /* BIFURCATION_H_ */
