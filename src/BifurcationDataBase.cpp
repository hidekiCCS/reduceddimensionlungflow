/*
 * BifurcationDataBase.cpp
 *
 *  Created on: Feb 26, 2015
 *      Author: fuji
 */

#include "BifurcationDataBase.h"

BifurcationDataBase::BifurcationDataBase(AirwayTreePartitioning* graph, std::vector<Bifurcation*> bifurcationList) :graph(graph), bifurcationList(bifurcationList){
	/// - Create index list that process owns
	localNumberOfBifurcation = bifurcationList.size();
	totalNumberOfBifurcation = graph->getNumOfBifurcation();
	data_indices = new hsize_t[localNumberOfBifurcation];
	for (int i = 0 ; i < localNumberOfBifurcation ; i++){
		data_indices[i] = bifurcationList[i]->getIndex();
	}
}

BifurcationDataBase::BifurcationDataBase(std::vector<Bifurcation*> bifurcationList) :graph(NULL), bifurcationList(bifurcationList){
	/// - Create index list that process owns
	localNumberOfBifurcation = bifurcationList.size();
	totalNumberOfBifurcation = graph->getNumOfBifurcation();
	data_indices = new hsize_t[localNumberOfBifurcation];
	for (int i = 0 ; i < localNumberOfBifurcation ; i++){
		data_indices[i] = bifurcationList[i]->getIndex();
	}
}

BifurcationDataBase::~BifurcationDataBase() {
	delete [] data_indices;
	/// Delete Airway Objects
	std::vector<Bifurcation *>::iterator ibf = bifurcationList.begin();
	while (ibf != bifurcationList.end()){
		delete *ibf;
		ibf = bifurcationList.erase(ibf);
	}
}

bool BifurcationDataBase::exportPHDF5dataspace(std::string pdir, hid_t& file) {
	/// - Create data group, "/Bifurcations"
	std::ostringstream datasetName;
	//datasetName << pdir << "Bifurcation/";
	datasetName << "/Bifurcations/";
	hid_t gid;
	if (H5Lexists(file, datasetName.str().c_str(), H5P_DEFAULT) == 1) {
		gid = H5Gopen(file, datasetName.str().c_str(), NULL);
	} else {
		gid = H5Gcreate(file, datasetName.str().c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	}

	/// - Calling exportPHDF5dataspacePressure
	exportPHDF5dataspacePressure(datasetName.str(),file);

	/// - Close group
	H5Gclose(gid);
	return true;
}

bool BifurcationDataBase::exportPHDF5dataspacePressure(std::string pdir, hid_t& file) {
	/// - Create a array
	double *pressure = new double[localNumberOfBifurcation];
	/// - Loop over Bifurcation object to collect Pressure
	for (int i = 0 ; i < localNumberOfBifurcation ; i++){
		pressure[i] = bifurcationList[i]->getPressure();
	}

	/// - Set Dataspace Name "./Pressure"
	std::string dataSpace = pdir + "/Pressure";


	int offset = 0;
#ifndef NOMPI
	int myid,numproc;
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	if (myid > 0){
		// Receive
		MPI_Status status;
		MPI_Recv(&offset,1,MPI_INT,myid-1,123,MPI_COMM_WORLD,&status);
	}
	if(myid < numproc - 1){
		// send
		int offsetNext = offset + localNumberOfBifurcation;
		MPI_Send(&offsetNext,1,MPI_INT,myid+1,123,MPI_COMM_WORLD);
	}
#endif
	exportPHDF5dataspaceDouble1d(dataSpace, file, totalNumberOfBifurcation, localNumberOfBifurcation, offset, pressure);

	delete [] pressure;
	return true;
}
