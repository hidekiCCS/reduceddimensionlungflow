/*
 * BifurcationDataBase.h
 *
 *  Created on: Feb 26, 2015
 *      Author: fuji
 */

#ifndef BIFURCATIONDATABASE_H_
#define BIFURCATIONDATABASE_H_

#include "DataBase.h"
#ifndef NOMPI
#include "AirwayTreePartitioning.h"
#endif
#include "Bifurcation.h"

/// Class for import/export Bifurcation status from/to file system
class BifurcationDataBase: virtual public DataBase {
public:
#ifndef NOMPI
	/// Constructor takes AirwayTreePartitioning and a list of Bifurcation objects
	BifurcationDataBase(AirwayTreePartitioning *graph,std::vector<Bifurcation *> bifurcationList);
#endif
	/// Constructor takes a list of Bifurcation objects (single process)
	BifurcationDataBase(std::vector<Bifurcation *> bifurcationList);
	/// Destructor does nothing
	virtual ~BifurcationDataBase();

	/// export Bifurcation status to HDF5 file
	virtual bool exportPHDF5dataspace(std::string pdir, hid_t &file);

private:
	/// export Bifurcation Pressure to HDF5 file
	bool exportPHDF5dataspacePressure(std::string pdir, hid_t &file);

	/// Object of AirwayTreePartitioning
	AirwayTreePartitioning *graph;
	/// List of Bifurcation objects
	std::vector<Bifurcation *> bifurcationList;

	/// Data indices that process owns
	hsize_t *data_indices;
	/// number of local Bifurcation objects
	hsize_t localNumberOfBifurcation;
	/// total number of Bifurcation objects
	hsize_t totalNumberOfBifurcation;

	static constexpr char dbName[] = "Bifurcation Database";///< Database Name
};

#endif /* BIFURCATIONDATABASE_H_ */
