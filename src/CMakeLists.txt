CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
set(CMAKE_VERBOSE_MAKEFILE 1)
#
find_package(MPI REQUIRED)
find_package(HDF5 REQUIRED)
find_package(PETSc REQUIRED)
find_package(VTK REQUIRED)
find_package(LibXml2 REQUIRED)
include(${VTK_USE_FILE})
INCLUDE_DIRECTORIES(${LIBXML2_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(${PETSC_INCLUDES})
INCLUDE_DIRECTORIES(${MPI_INCLUDE_PATH})
ADD_DEFINITIONS(${PETSC_DEFINITIONS})
#
SET(CLASSLIBS Airway.cpp AirwayTubeLaw.cpp AirwayWithClosure.cpp AirwayWithPlugPropagation.cpp Acinus.cpp AcinusWithSurfactant.cpp Bifurcation.cpp SurfaceTension.cpp Surfactant.cpp AirwayDataBase.cpp AcinusDataBase.cpp BifurcationDataBase.cpp InletPleuralDataBase.cpp DataBase.cpp StateDataBaseHDF5.cpp AirwayTreeModel.cpp LungLobeFromAirwayTree.cpp AirwayTreePartitioning.cpp AirwayTreePartitioningParMetis.cpp InletPressureWaveForm.cpp LungFlowSolver.cpp PleuralPressureWaveForm.cpp LiquidPlug.cpp LiquidPlugDH.cpp TrackLiquidPlugs.cpp ParenchymalTethering.cpp ParameterFile.cpp   LiquidSecretion.cpp)
#
#
if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()
#
set(CMAKE_CXX_FLAGS_RELEASE "-O3")
#
set(CMAKE_CXX_STANDARD 11)  # enable C++11 standard
#
ADD_LIBRARY(Breath STATIC ${CLASSLIBS})
#
