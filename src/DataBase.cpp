/*
 * DataBase.cpp
 *
 *  Created on: Feb 25, 2015
 *  Modified on: Jun 28, 2016
 *      Author: fuji
 */

#include "DataBase.h"

DataBase::DataBase() {
	// TODO Auto-generated constructor stub

}

DataBase::~DataBase() {
	// TODO Auto-generated destructor stub
}

bool DataBase::exportPHDF5dataspaceInteger1d(std::string dataSpace, hid_t& file, int totalNum, int localNum, int offset, int* data) {
	/// - setup dimensionality object of one data space
	hsize_t numdata = totalNum;
	hid_t sid1 = H5Screate_simple (1, &numdata, NULL);

	/// - create a dataset collectively
	hid_t dataset1 = H5Dcreate(file, dataSpace.c_str(), H5T_NATIVE_INT, sid1, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	/// - create a file dataspace independently
	hid_t file_dataspace = H5S_ALL;
	hid_t mem_dataspace = H5S_ALL;
	hid_t xfer_plist = H5P_DEFAULT;

#ifndef NOMPI
	hsize_t numdataLocal = localNum;
	/// - stores data parallel.
	hsize_t offsetH5 = offset;
	file_dataspace = H5Dget_space (dataset1);
	if(numdataLocal > 0){
		H5Sselect_hyperslab(file_dataspace, H5S_SELECT_SET, &offsetH5, NULL, &numdataLocal, NULL);
	}else{
		H5Sselect_none(file_dataspace);
	}
	/// - create a memory dataspace independently
	mem_dataspace = H5Screate_simple (1, &numdataLocal, NULL);

	/// - set up the collective transfer properties list
	xfer_plist = H5Pcreate (H5P_DATASET_XFER);
	H5Pset_dxpl_mpio(xfer_plist, H5FD_MPIO_COLLECTIVE);
#endif
	/// - write data collectively
	H5Dwrite(dataset1, H5T_NATIVE_INT, mem_dataspace, file_dataspace, xfer_plist, data);

	/// - Close
#ifndef NOMPI
	H5Sclose(file_dataspace);
	H5Sclose(mem_dataspace);
	H5Pclose(xfer_plist);
#endif
	H5Dclose(dataset1);
	H5Sclose(sid1);

	return true;
}

bool DataBase::exportPHDF5dataspaceDouble1d(std::string dataSpace, hid_t& file, int totalNum, int localNum, int offset, double* data) {
	/// - setup dimensionality object of one data space
	hsize_t numdata = totalNum;
	hid_t sid1 = H5Screate_simple (1, &numdata, NULL);

	/// - create a dataset collectively
	hid_t dataset1 = H5Dcreate(file, dataSpace.c_str(), H5T_NATIVE_DOUBLE, sid1, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	hid_t file_dataspace = H5S_ALL;
	hid_t mem_dataspace = H5S_ALL;
	hid_t xfer_plist = H5P_DEFAULT;

#ifndef NOMPI
	/// - create a file dataspace independently
	hsize_t numdataLocal = localNum;
	/// - stores data parallel.
	hsize_t offsetH5 = offset;
	file_dataspace = H5Dget_space (dataset1);
	if (localNum > 0){
		H5Sselect_hyperslab(file_dataspace, H5S_SELECT_SET, &offsetH5, NULL, &numdataLocal, NULL);
	}else{
		H5Sselect_none(file_dataspace);
	}
	/// - create a memory dataspace independently
	mem_dataspace = H5Screate_simple (1, &numdataLocal, NULL);

	/// - set up the collective transfer properties list
	xfer_plist = H5Pcreate (H5P_DATASET_XFER);
	H5Pset_dxpl_mpio(xfer_plist, H5FD_MPIO_COLLECTIVE);
#endif

	/// - write data collectively
	H5Dwrite(dataset1, H5T_NATIVE_DOUBLE, mem_dataspace, file_dataspace, xfer_plist, data);

	/// - Close
#ifndef NOMPI
	H5Sclose(file_dataspace);
	H5Sclose(mem_dataspace);
	H5Pclose(xfer_plist);
#endif
	H5Dclose(dataset1);
	H5Sclose(sid1);

	return true;
}

bool DataBase::exportPHDF5dataspaceDouble1dIndex(std::string dataSpace, hid_t& file, int totalNum, int localNum, hsize_t* data_index, double* data) {
	/// - setup dimensionality object of one data space
	hsize_t numdata = totalNum;
	hid_t sid1 = H5Screate_simple (1, &numdata, NULL);

	/// - create a dataset collectively
	hid_t dataset1 = H5Dcreate(file, dataSpace.c_str(), H5T_NATIVE_DOUBLE, sid1, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	hid_t file_dataspace = H5S_ALL;
	hid_t mem_dataspace = H5S_ALL;
	hid_t xfer_plist = H5P_DEFAULT;

#ifndef NOMPI
	/// - create a file dataspace independently
	file_dataspace = H5Dget_space (dataset1);
	hsize_t localnumdata = localNum;
	if (localnumdata > 0){
		H5Sselect_elements(file_dataspace, H5S_SELECT_SET, localnumdata, data_index);
	}else{
		H5Sselect_none(file_dataspace);
	}
	/// - create a memory dataspace independently
	mem_dataspace = H5Screate_simple (1, &localnumdata, NULL);

	/// - set up the collective transfer properties list
	xfer_plist = H5Pcreate (H5P_DATASET_XFER);
	H5Pset_dxpl_mpio(xfer_plist, H5FD_MPIO_COLLECTIVE);
#endif
	/// - write data collectively
	H5Dwrite(dataset1, H5T_NATIVE_DOUBLE, mem_dataspace, file_dataspace, xfer_plist, data);

	/// - Close
#ifndef NOMPI
	H5Sclose(file_dataspace);
	H5Sclose(mem_dataspace);
	H5Pclose(xfer_plist);
#endif
	H5Dclose(dataset1);
	H5Sclose(sid1);

	return true;
}

bool DataBase::importHDF5dataspaceInteger1d(std::string dataSpace, hid_t &file, int totalNum,int *data){
	/// Read all data on each process
	if (H5Lexists(file, dataSpace.c_str(), H5P_DEFAULT)) {
		hid_t dataset = H5Dopen1(file, dataSpace.c_str());
		H5Dread(dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
		H5Dclose(dataset);
		return true;
	}
	return false;
}

bool DataBase::importHDF5dataspaceDouble1d(std::string dataSpace, hid_t &file, int totalNum,double *data){
	/// Read all data on each process
	if (H5Lexists(file, dataSpace.c_str(), H5P_DEFAULT)) {
		hid_t dataset = H5Dopen1(file, dataSpace.c_str());
		H5Dread(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
		H5Dclose(dataset);
		return true;
	}
	return false;
}

bool DataBase::importHDF5dataspaceDouble1dIndex(std::string dataSpace, hid_t &file, int localNumber, hsize_t* data_index, double *data){
	if (H5Lexists(file, dataSpace.c_str(), H5P_DEFAULT)) {
		hid_t dataset = H5Dopen2(file, dataSpace.c_str(), H5P_DEFAULT);
		hid_t file_dataspace = H5Dget_space(dataset);
		hsize_t num = localNumber;
		H5Sselect_elements(file_dataspace, H5S_SELECT_SET, num, data_index);
		hid_t mem_dataspace = H5Screate_simple(1, &num, NULL);
		H5Dread(dataset, H5T_NATIVE_DOUBLE, mem_dataspace, file_dataspace, H5P_DEFAULT, data);

		H5Sclose(file_dataspace);
		H5Sclose(mem_dataspace);
		H5Dclose(dataset);
		return true;
	}
	return false;
}
