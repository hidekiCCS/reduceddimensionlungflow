/*
 * DataBase.h
 *
 *  Created on: Feb 25, 2015
 *  Modified on Jun 28, 2016
 *      Author: fuji
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include <iostream>
#include <vector>
#include <hdf5.h>
#ifndef NOMPI
	#include <H5FDmpio.h>
#else
	#undef H5_HAVE_PARALLEL
#endif

/// Base Class for Lung Network Database
class DataBase {
public:
	/// Constructor don't do anything
	DataBase();

	/// This method overridden by upper class
	virtual bool exportPHDF5dataspace(std::string pdir, hid_t &file){
		/// Intentionally blank
		return false;
	}
	/// This method overridden by upper class
	virtual bool importHDF5dataspace(std::string pdir, hid_t &file){
		/// Intentionally blank
		return false;
	}

	/// Export 1-dimensional Integer Data to Parallel HDF5 data space, must call collectively
	bool exportPHDF5dataspaceInteger1d(std::string dataSpace, hid_t &file, int totalNum, int localNum, int offset, int *data);
	/// Export 1-dimensional Double Data to Parallel HDF5 data space, must call collectively
	bool exportPHDF5dataspaceDouble1d(std::string dataSpace, hid_t &file, int totalNum, int localNum, int offset, double *data);
	/// Export 1-dimensional Double Data to Parallel HDF5 data space with indexing, must call collectively
	bool exportPHDF5dataspaceDouble1dIndex(std::string dataSpace, hid_t &file, int totalNum, int localNum, hsize_t *data_index, double *data);

	/// Import 1-dimensional Integer Data from HDF5 data space
	bool importHDF5dataspaceInteger1d(std::string dataSpace, hid_t &file, int totalNum,int *data);
	/// Import 1-dimensional Double Data from HDF5 data space
	bool importHDF5dataspaceDouble1d(std::string dataSpace, hid_t &file, int totalNum, double *data);
	/// Import 1-dimensional Double Data from HDF5 data space
	bool importHDF5dataspaceDouble1dIndex(std::string dataSpace, hid_t &file, int locallNum, hsize_t* data_index, double *data);

	virtual ~DataBase();

private:
	static constexpr char dbName[] = "Base Database";///< Database Name
};

#endif /* DATABASE_H_ */
