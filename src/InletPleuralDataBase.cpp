/*
 * InletPleuralDataBase.cpp
 *
 *  Created on: Feb 28, 2015
 *      Author: fuji
 */

#include "InletPleuralDataBase.h"
#ifndef NOMPI
#include <mpi.h>
#endif

InletPleuralDataBase::InletPleuralDataBase(InletPressureWaveForm* inlet, PleuralPressureWaveForm* pleural): inlet(inlet),pleural(pleural),myid(0),numproc(1){
#ifndef NOMPI
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
#endif
}

InletPleuralDataBase::~InletPleuralDataBase() {
	// TODO Auto-generated destructor stub
}

bool InletPleuralDataBase::exportPHDF5dataspace(std::string pdir, hid_t& file) {
	/// - Calling exportPHDF5dataspaceInletPressure
	exportPHDF5dataspaceInletPressure(pdir,file);
	/// - Calling exportPHDF5dataspacePleuralPressure
	exportPHDF5dataspacePleuralPressure(pdir,file);
	return true;
}

bool InletPleuralDataBase::exportPHDF5dataspaceInletPressure(std::string pdir, hid_t& file) {
	hsize_t idx[1] = {0};
	hsize_t numdata = 1;
	double pressure = inlet->constant_pressure();
	if (myid != 0){
		numdata = 0;
	}
	/// - Set Dataspace Name "./InletPressure"
	std::string dataSpace = pdir + "/InletPressure";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, 1, numdata, idx, &pressure);
}

bool InletPleuralDataBase::exportPHDF5dataspacePleuralPressure(std::string pdir, hid_t& file) {
	hsize_t idx[1] = {0};
	hsize_t numdata = 1;
	double pressure = pleural->getPressure();
	if (myid != 0){
		numdata = 0;
	}
	/// - Set Dataspace Name "./PleuralPressure"
	std::string dataSpace = pdir + "/PleuralPressure";
	return exportPHDF5dataspaceDouble1dIndex(dataSpace, file, 1, numdata, idx, &pressure);
}
