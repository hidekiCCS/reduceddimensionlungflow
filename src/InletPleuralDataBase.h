/*
 * InletPleuralDataBase.h
 *
 *  Created on: Feb 28, 2015
 *      Author: fuji
 */

#ifndef INLETPLEURALDATABASE_H_
#define INLETPLEURALDATABASE_H_

#include "DataBase.h"
#include "PleuralPressureWaveForm.h"
#include "InletPressureWaveForm.h"

/// Class for import/export Intel&Pleural status from/to file system
class InletPleuralDataBase: virtual public DataBase {
public:
	/// Constructor takes InletPressureWaveForm and PleuralPressureWaveForm
	InletPleuralDataBase(InletPressureWaveForm *inlet, PleuralPressureWaveForm *pleural);
	virtual ~InletPleuralDataBase();

	/// export Intel&Pleural status to HDF5 file
	virtual bool exportPHDF5dataspace(std::string pdir, hid_t &file);

private:
	/// export Intel Pressure to HDF5 file
	bool exportPHDF5dataspaceInletPressure(std::string pdir, hid_t &file);
	/// export Pleural Pressure to HDF5 file
	bool exportPHDF5dataspacePleuralPressure(std::string pdir, hid_t &file);
	/// Object of InletPressureWaveForm
	InletPressureWaveForm *inlet;
	/// Object of PleuralPressureWaveForm
	PleuralPressureWaveForm *pleural;

	/// process id
	int myid;
	/// number of processes
	int numproc;

	static constexpr char dbName[] = "Inlet & Pleural Database";///< Database Name
};

#endif /* INLETPLEURALDATABASE_H_ */
