/*
 * InletPressureWaveForm.h
 *
 *  Created on: Oct 29, 2014
 *      Author: fuji
 */

#ifndef INLETPRESSUREWAVEFORM_H_
#define INLETPRESSUREWAVEFORM_H_

/// This class defines the pressure at inlet
class InletPressureWaveForm {
public:
	/// Constructor
	InletPressureWaveForm();
	/// Destructor
	virtual ~InletPressureWaveForm();

	/// this returns constant pressure = 0
	virtual double constant_pressure(void);
};

#endif /* INLETPRESSUREWAVEFORM_H_ */
