/*
 * LiquidPlug.cpp
 *
 *  Created on: Feb 10, 2015
 *      Author: fuji
 */

#include "LiquidPlug.h"
#include <cmath>

LiquidPlug::~LiquidPlug() {
	// TODO Auto-generated destructor stub
}

LiquidPlug::LiquidPlug(double initialLiquidVolume, double initialPosition, double tubeRadius, double tubeLength, double precursorFilmThickness) :
		liquidVolume(initialLiquidVolume), position(initialPosition), tubeRadius(tubeRadius), tubeLength(tubeLength), precursorFilmThickness(
				precursorFilmThickness),leadingPlug(true) {
	/// * Set zero Initial velocity.
	plugVelocity = 0.0;
	comutePlugLength();
	rateOfplugVolumeChange = 0.0;
	trailingFilmThickness = precursorFilmThickness;
	travelDistance = 0.0;
	liquidVolumeLeft = 0.0;

	/// * Default Surface Tension 25 dyns/cm
	surfaceTension = 25.0;
	/// * Default Liquid Viscosity is 1.0197e-5 \f$ cmH20 \cdot s \f$ = \f$ 10^-2 dyne/cm^2 \cdot s / 980.665 \f$
	liquidViscositycmsH2O = 1.0197e-5;
	liquidViscosityPoise = liquidViscositycmsH2O * cmH2OtoDynsParSquareCm;
	/// * Default Plug Flow Resistance
	plugResistance = 1.0e+5;
}

double LiquidPlug::computeResistance(double pressureDrop) {
	/// - THIS IS A TEST ROUTINE : Poiseuille's law
	double resistance = (8.0 * liquidViscositycmsH2O * plugLength) / (M_PI * std::pow(tubeRadius, 4));
	/// - This method determine the plug velocity.
	plugVelocity = (pressureDrop / resistance) / (M_PI * std::pow(tubeRadius, 2));
	capillaryNumber = std::abs(plugVelocity * liquidViscosityPoise / surfaceTension);
	return resistance;
}

double LiquidPlug::propagationOfPlug(double dt) {
	/// - Compute the Liquid deposited by this plug,  LiquidPlug::rateOfplugVolumeChange \f$\times dt\f$.
	double dvol = dt * rateOfplugVolumeChange;
	/// - If there are multiple plugs in this airway and this is not a leading plug, no volume change.
	if (!(this->isLeadingPlug())) dvol = 0.0;
	liquidVolume += dvol;

	/// - Update the liquid volume this plug left behind.
	double ep2 = trailingFilmThickness / tubeRadius;
	liquidVolumeLeft += std::abs(this->plugVelocity) * (M_PI * tubeRadius * tubeRadius) * (2.0 * ep2 - ep2 * ep2) * dt;
	/// - Update the traveling distance.
	travelDistance += std::abs(plugVelocity) * dt;

	/// - Update the plug position by Euler's method (This is scaled position 0~1)
	position += plugVelocity * dt / tubeLength;
	//std::cout << "po=" << position << std::endl;
	//std::cout << "len=" << tubeLength << std::endl;
	//std::cout << "vel=" << plugVelocity << std::endl;
	//std::cout << "plen=" << plugLength << std::endl;
	comutePlugLength();
	/// - Return the Liquid deposited by this plug
	return -dvol;
}

double LiquidPlug::comutePlugLength() {
	/// The plug length is determined assuming hemi-spherical meniscus shapes.
	double core = liquidVolume - 2.0 * M_PI * std::pow(tubeRadius - precursorFilmThickness, 3) / 3.0;
	plugLength = core / (M_PI * std::pow(tubeRadius - precursorFilmThickness, 2));
	return plugLength;
}

void LiquidPlug::flowReversed() {
	/// * This method is called when airway flow reverses.
	///		- Compute the mean trailing film thickness.
	///		- Assign the mean trailing film thickness to the precursor film thickness.
	///		- Re-set *travelDistance* and *liquidVolumeLeft*.
	double meanTR = trailingFilmThickness;
	if (travelDistance > 0.0){
		double b2 = tubeRadius * tubeRadius - liquidVolumeLeft / travelDistance / M_PI;
		if (b2 < 0.0){
			throw "Stranger things have happened : Plug has deposited too much liquid.";
		}
		meanTR = tubeRadius - std::sqrt(b2);
		//std::cout << "mean tr = " << meanTR << std::endl;
	}
	precursorFilmThickness = meanTR;
	travelDistance = 0.0;
	liquidVolumeLeft = 0.0;
}
