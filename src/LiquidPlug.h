/*
 * LiquidPlug.h
 *
 *  Created on: Feb 10, 2015
 *      Author: fuji
 */
/// State of a liquid Plug. One object takes one plug.

#ifndef LIQUIDPLUG_H_
#define LIQUIDPLUG_H_

#include <iostream>
#include <vector>
#include <cmath>

/// * LiquidPlug object manages one liquid plug.
/// * The object is created by AirwayWithPlugPropagation object.
/// * TrackLiquidPlugs object manages LiquidPlug objects.
class LiquidPlug {
public:
	/// Constructor takes the initial liquid volume, the initial position, and Airway object.
	LiquidPlug(double initialLiquidVolume, double intialPosition, double tubeRadius, double tubeLength,double precursorFilmThickness);
	/// Destructor does nothing, this must called by TrackLiquidPlug object.
	virtual ~LiquidPlug();
	/// Compute the flow resistance
	virtual double computeResistance(double pressureDrop);
	/// Update Plug Position by \f$x= x + v dt\f$. This return the liquid volume deposited in the trailing film.
	double propagationOfPlug(double dt);

	/// Call this when flow reverses.
	void flowReversed();

	/// Get Liquid Volume
	double getLiquidVolume()  {
		return liquidVolume;
	}

	/// Get plug velocity
	double getPlugVelocity() {
		return plugVelocity;
	}

	/// get plug position
	double getPosition()  {
		return position;
	}

	/// Get Precursor film thickness
	double getPrecursorFilmThickness() {
		return precursorFilmThickness;
	}

	void setPrecursorFilmThickness(double precursorFilmThickness) {
		this->precursorFilmThickness = precursorFilmThickness;
		this->comutePlugLength();
	}

	double getPlugLength() const {
		return plugLength;
	}

	double getTubeLength() const {
		return tubeLength;
	}

	double getTubeRadius() const {
		return tubeRadius;
	}

	virtual void setAirwayFlowRate(double flowrate) {
		this->plugVelocity = flowrate / (M_PI * tubeRadius * tubeRadius);
	}

	double getSurfaceTension() const {
		return surfaceTension;
	}

	void setSurfaceTension(double surfaceTension) {
		this->surfaceTension = surfaceTension;
	}

	double getCapillaryNumber() const {
		return capillaryNumber;
	}

	double getLiquidViscositycmsH2O() const {
		return liquidViscositycmsH2O;
	}

	void setLiquidViscositycmsH2O(double liquidViscositycmsH2O) {
		this->liquidViscositycmsH2O = liquidViscositycmsH2O;
		this->liquidViscosityPoise = liquidViscositycmsH2O * cmH2OtoDynsParSquareCm;
	}

	double getLiquidViscosityPoise() const {
		return liquidViscosityPoise;
	}

	double getPlugResistance() const {
		return plugResistance;
	}

	bool isLeadingPlug() const {
		return leadingPlug;
	}

	void setLeadingPlug(bool leadingPlug) {
		this->leadingPlug = leadingPlug;
	}

	double getLiquidVolumeLeft() const {
		return liquidVolumeLeft;
	}

	double getPrecursorFilmThickness() const {
		return precursorFilmThickness;
	}

	double getTrailingFilmThickness() const {
		return trailingFilmThickness;
	}

	double getTravelDistance() const {
		return travelDistance;
	}

protected:

	/// Liquid Viscosity (cmH2O s)
	double liquidViscositycmsH2O;
	/// Viscosity in Poise (\f$1P=1dyns/cm^2\f$)
	double liquidViscosityPoise;
	/// Method to compute plug length from liquid volume
	double comutePlugLength();
	/// Plug Length
	double plugLength;
	/// Rate of Plug Volume change
	double rateOfplugVolumeChange;
	/// Liquid Volume
	double liquidVolume;
	/// Tube Radius
	double tubeRadius;
	/// Tube Length
	double tubeLength;
	/// Plug Velocity
	double plugVelocity;
	/// Normalized Location within the Airway object. 0 < position < 1.
	double position;
	/// Precursor film thickness when this plug created. (Re-assigned when flow reverses)
	double precursorFilmThickness;
	/// Trailing film thickness. (Re-set when flow reverses)
	double trailingFilmThickness;
	/// Travel Distance after this plug created. (Re-set when flow reverses)
	double travelDistance;
	/// Total liquid volume this plug left behind. (Re-set when flow reverses)
	double liquidVolumeLeft;
	/// Surface Tension (dynes/cm)
	double surfaceTension;
	/// Capillary Number
	double capillaryNumber;
	/// Resistance
	double plugResistance;
	/// Flag of Leading Plug
	bool leadingPlug;

#if __cplusplus > 199711L
#define _CONST_TLM constexpr
#else
#define _CONST_TLM const
#endif
	/// Pressure unit ratio
	static _CONST_TLM  double cmH2OtoDynsParSquareCm = 980.665;
#undef _CONST_TLM
};

#endif /* LIQUIDPLUG_H_ */
