/*
 * LiquidPlugDH.cpp
 *
 *  Created on: Mar 31, 2015
 *      Author: fuji
 */

#include "LiquidPlugDH.h"
#include <cmath>

LiquidPlugDH::LiquidPlugDH(double initialLiquidVolume, double intialPosition, double tubeRadius, double tubeLength, double precursorFilmThickness) : LiquidPlug(initialLiquidVolume, intialPosition, tubeRadius, tubeLength, precursorFilmThickness){
	plugResistance = plugFlowResistance(minCa) / cmH2OtoDynsParSquareCm;
}

LiquidPlugDH::~LiquidPlugDH() {
	// TODO Auto-generated destructor stub
}

double LiquidPlugDH::computeResistance(double pressureDrop) {
	/// - If the pressure drop is zero, return a large value
	if (pressureDrop * pressureDrop <= 0.0){
#ifdef DEBUG
		std::cout << "Pressure Drop is zero\n";
#endif
		return plugFlowResistance(minCa) / cmH2OtoDynsParSquareCm;
	}
	/// - Compute the initial guess of Capillary Number from LiquidPlug::plugVelocity.
	double mc = minCa;
	capillaryNumber = std::max(capillaryNumber, mc);

#ifdef DEBUG
	std::cout << "viscosityPoise=" << liquidViscosityPoise << std::endl;
	std::cout << "plugVelocity=" << plugVelocity << std::endl;
	std::cout << "surfaceTension=" << surfaceTension << std::endl;
#endif
	/// - Convert the unit for pressure drop across the plug from \f$cmH_2O\f$ to \f$dyns/cm^2\f$.
	pressureDropAcrossPlug = pressureDrop * cmH2OtoDynsParSquareCm;
	bool negitiveFlag = pressureDropAcrossPlug < 0.0;
	pressureDropAcrossPlug = std::abs(pressureDropAcrossPlug);
#ifdef DEBUG
	std::cout << "PressureDrop=" << pressureDropAcrossPlug << " dyns/cm2" << std::endl;
	std::cout << "capillaryNumber=" << capillaryNumber << std::endl;
#endif

    /// - Compute the plug velocity
    plugVelocity = (negitiveFlag ? -1.0:1.0) * capillaryNumber * surfaceTension / liquidViscosityPoise;

    /// - Return the flow resistance, converting to cmH2O/(cm/s)
    plugResistance += underRelaxation * (plugFlowResistance(capillaryNumber) / cmH2OtoDynsParSquareCm - plugResistance);
#ifdef DEBUG
    std::cout << "Resistance=" << plugFlowResistance(capillaryNumber) << std::endl;
#endif
	return plugResistance;
}

double LiquidPlugDH::plugFlowResistance(double ca) {
	/// \f$F_2=\f$ LiquidPlugDH::fitfun(\f$\epsilon / (3Ca)^{2/3}\f$)
	double epsilon2 = precursorFilmThickness / tubeRadius;
    double a2 =epsilon2 / std::pow(3.0 * ca, 2.0/3.0);
    /// \f$f_2 = f_2(10^{-2})$ when $\frac{\epsilon}{(3Ca)^{2/3}}>10^{-2}\f$
    double f2=fitfun(std::min(a2,1.0e-2));
	/// Non-dimensional Resistance due to the front meniscus, \f$2 \cdot 3^{2/3} f_2 * \cdot Ca^{-1/3}\f$.
	double resistanceFront = -2.0 * std::pow(3.0, 2.0/3.0) * f2 * std::pow(ca,-1.0/3.0);
	/// Non-dimensional Resistance due to the rear meniscus, \f$2 * A * Ca^{-1/3}\f$.
	double resistanceRear = 2.0 * param_A * std::pow(ca, -1.0/3.0);
	/// Resistance due to the viscous dissipation in plug core.
	double resistanceViscousCore = 8.0 * plugLength / tubeRadius;
	/// Return the total
	return (resistanceFront + resistanceRear + resistanceViscousCore) * liquidViscosityPoise / (M_PI * std:: pow(tubeRadius,3));
}

double LiquidPlugDH::rearh1(double ca) {
	/// Trailing film thickness : \f$1.337Ca^{2/3}\f$
	return 1.337*std::pow(ca,2.0/3.0);
}

double LiquidPlugDH::fitfun(double x) {
	const int orderpoly = 5;
	/// David's polynomial coefficients
	double b[orderpoly+1] = {-0.148998179024024,2.041755671089916,0.570213033078381,0.232768341308782,0.064004773055983,0.006891194223464};

	double logx=std::log10(x);
	/// - use Horner's rule to evaluate polynomial
	double res = 0.0;
	for (int i = orderpoly ; i >= 0 ; i--){
		res = res * logx + b[i];
	}
	return res;
}

void LiquidPlugDH::setAirwayFlowRate(double flowrate) {
	this->plugVelocity = flowrate / (M_PI * tubeRadius * tubeRadius);

	capillaryNumber = std::min(std::abs(plugVelocity * liquidViscosityPoise / surfaceTension), 1.0e-2);
	/// - Computing the trailing film thickness calling LiquidPlugDH::rearh1.
    double epsilon1 = rearh1(capillaryNumber);
    trailingFilmThickness = epsilon1 * tubeRadius;
    double epsilon2 = precursorFilmThickness / tubeRadius;
    /// - Computing the rate of the plug volume change.
    ///		\f$ |Q|(2\epsilon_2 - 2\epsilon_1 - \epsilon_2^2 + \epsilon_1^2)\f$
    rateOfplugVolumeChange = std::abs(flowrate) * (2.0 * epsilon2 - 2.0 * epsilon1 - epsilon2 * epsilon2 + epsilon1 * epsilon1);
}
