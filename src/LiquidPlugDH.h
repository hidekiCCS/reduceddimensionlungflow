/*
 * LiquidPlugDH.h
 *
 *  Created on: Mar 24, 2015
 *      Author: fuji
 */

#ifndef LIQUIDPLUGDH_H_
#define LIQUIDPLUGDH_H_

#include "LiquidPlug.h"

/// Liquid Plug Flow Resistance Model by David Halpern

class LiquidPlugDH: public LiquidPlug {
public:
	LiquidPlugDH(double initialLiquidVolume, double intialPosition, double tubeRadius, double tubeLength,double precursorFilmThickness);
	virtual ~LiquidPlugDH();

	/// Compute the flow resistance
	virtual double computeResistance(double pressureDrop);

	virtual void setAirwayFlowRate(double flowrate);

private:
	/// Total Plug Flow Resistance
	double plugFlowResistance(double ca);

	/// Pressure Drop Across Plug \f$dyn/cm^2\f$
	double pressureDropAcrossPlug;
	/// Trailing film thickness as a function of Capillary number.
	double rearh1(double ca);
	/// Fitting Function
	double fitfun(double x);

	/// Minimum Pressure Drop
	double minPressureDrop;

#if __cplusplus > 199711L
#define _CONST_TLM constexpr
#else
#define _CONST_TLM const
#endif
	/// Tolerance1
	static _CONST_TLM  double rootdat_tol1 = 1.0e-6;
	/// Tolerance2
	static _CONST_TLM  double rootdat_tol2 = 1.0e-6;
	/// denominator for finite difference approximation
	static _CONST_TLM  double rootdat_diffx= 1.0e-12;
	/// max number of iterations- Newt
	static _CONST_TLM int rootdat_maxrootsteps = 1000;
	/// Parameter A = 3.73
	static _CONST_TLM  double param_A = 3.73;
	/// Minimum Capillary Number
	static _CONST_TLM  double minCa = 1.0e-15;
	/// Under Relaxation Factor for updating plug resistance
	static _CONST_TLM double underRelaxation = 0.9;
#undef _CONST_TLM
};

#endif /* LIQUIDPLUGDH_H_ */
