/*
 * LiquidSecretion.cpp
 *
 *  Created on: Nov 7, 2017
 *      Author: fuji
 */

#include "LiquidSecretion.h"


LiquidSecretion::LiquidSecretion(AirwayTreePartitioning* graph, std::vector<Airway*> airwayList)
:graph(graph),airwayList(airwayList){
}

LiquidSecretion::~LiquidSecretion() {
	// TODO Auto-generated destructor stub
}

void LiquidSecretion::airwayLiquidSecretion(double dt) {
	const int numberOfAirwayInLocal = airwayList.size();
    for (int i = 0 ; i < numberOfAirwayInLocal ; i++){
    	Airway *aw = airwayList[i];
    	double liquidVolume = aw->getLiquidVolume();
    	double radius = aw->getRadiusRest();
    	double length = aw->getLength();
    	double addtionalLiquidVolume = secreteRate * length * 2 * M_PI * radius * dt;
    	liquidVolume += addtionalLiquidVolume;
    	aw->setLiquidVolume(liquidVolume);
    }
}
