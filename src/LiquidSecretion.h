/*
 * LiquidSecretion.h
 *
 *  Created on: Nov 7, 2017
 *      Author: fuji
 */

#include <iostream>
#include <vector>
#include <cmath>
#include "DataBase.h"
#include "AirwayTreePartitioning.h"
#include "Airway.h"

#ifndef LIQUIDSECRETION_H_
#define LIQUIDSECRETION_H_

class LiquidSecretion {
public:
	LiquidSecretion(AirwayTreePartitioning *graph, std::vector<Airway *> airwayList);
	virtual ~LiquidSecretion();

	/// Make Liquid Secretion
	void airwayLiquidSecretion(double dt);
private:
	static constexpr double secreteRate = 1.0e-4;//< Liquid Secretion Rate cm/s (Volumr / Area / Time)
	AirwayTreePartitioning *graph; //< Airway Tree Partition
	std::vector<Airway *> airwayList; //< List of Airway Object
};

#endif /* LIQUIDSECRETION_H_ */
