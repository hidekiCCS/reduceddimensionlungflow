/*
 * LungFlowSolver.cpp
 *
 *  Created on: Oct 29, 2014
 *  Modifed on: June 28, 2016
 *      Author: fuji
 */

#include "LungFlowSolver.h"
#include <iostream>
#include <string>
#include <stdexcept>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <sstream>
#include <omp.h>

LungFlowSolver::LungFlowSolver(AirwayTreePartitioning *graph, std::vector<Bifurcation*> bifurcationList,
		std::vector<Airway*> airwayList, std::vector<Acinus*> acinusList,
		PleuralPressureWaveForm* pl, InletPressureWaveForm* inlet)
: BifurcationDataBase(graph,bifurcationList), AirwayDataBase(graph,airwayList),
  AcinusDataBase(graph,acinusList), graph(graph),bifurcationList(bifurcationList),
  airwayList(airwayList),acinusList(acinusList),pl(pl),inlet(inlet),surfactantModel(false){

	/// - Default setting for Newton's iteration
	///		- LungFlowSolver::maxStepSize = 0.01
	///		- LungFlowSolver::tolerance = 1.0e-8
	///		- LungFlowSolver::maxIteration = 100
	maxStepSize = 0.01;
	tolerance = 1.0e-8;
	maxIteration = 50000;

	/// - Setting up Petsc, calling LungFlowSolver::setupPetSc
	trachea = NULL;
	scallJac = MAT_INITIAL_MATRIX;
	setupPetSc();

	/// - Set Initial node pressure vector, extract from Bifurcation objects.
	for (std::vector<Bifurcation*>::iterator it = bifurcationList.begin() ; it != bifurcationList.end() ; it++){
		Bifurcation *bif = *it;
		VecSetValue(vecP, graph->getVectorIndexOfBifurcationNode(bif->getIndex()), bif->getPressure(), INSERT_VALUES);
	}
    VecAssemblyBegin(vecP);
    /* may do something */
    VecAssemblyEnd(vecP);

    baseFileName = "lung";
}

LungFlowSolver::~LungFlowSolver() {
	/// Calling Base Class Destructors
	MatDestroy(&matA);
	MatDestroy(&matB);
	MatDestroy(&matC);
	MatDestroy(&matAave);
	MatDestroy(&matDpres);
	MatDestroy(&matJac);
	MatDestroy(&matQtoAcinus);
	VecDestroy(&vecConductance);
	VecDestroy(&vecConductance0);
	VecDestroy(&vecP);
	VecDestroy(&vecR);
	VecDestroy(&vecF);
	VecDestroy(&vecQ);
	VecDestroy(&vecAPb);
	VecDestroy(&vecAwP);
	VecDestroy(&vecAwP0);
	VecDestroy(&vecAwDp);
	VecDestroy(&vecAcinusVolume);
}

int LungFlowSolver::solve() {
	/// 1. Set Pressure at Terminal Airways.
	this->setTerminalPressureVector();

	///  2. Create the PetSC KSP linear solver context
	KSP            ksp;     /* linear solver context */
	PC             pc;
	KSPCreate(PETSC_COMM_WORLD,&ksp);
	KSPSetFromOptions(ksp);

	///  3. Newton's method iteration
	/// 	- Compute the flowrate residual
	///			+ Compute Airway Pressure, calling LungFlowSolver::computeAirwayPressure
	///			+ Compute Airway Pressure, calling LungFlowSolver::computeAirwayPressureDrop
	///			+ Compute Airway Conductance, calling LungFlowSolver::computeConductance
	///			+ Compute flowrate in Airways, calling LungFlowSolver::computeFlowRate
	///			+ Compute flowrate residual at Bifurcations, calling LungFlowSolver::computeFlowRateResidual
	/// 	- Check if the flowrate residual is smaller than LungFlowSolver::tolerance, \f$|\vec{r}| <\f$ LungFlowSolver::tolerance
	///			+ If satisfied, end iteration.
	///		- Solve the linear system, \f$\mathbf{B}\mathbf{C}\mathbf{D}\vec{x} = \vec{r}(\vec{p_0})\f$
	///		- Update the pressure at Bifurcation, \f$\vec{p} = \vec{p_0} - \vec{x}\f$
	///			+ If \f$|\vec{x}|\f$ is larger than LungFlowSolver::maxStepSize, scale it to be \f$|\vec{x}|<\f$LungFlowSolver::maxStepSize.
	///		- Repeat until \f$|\vec{r}| <\f$ LungFlowSolver::tolerance or \a itr \f$\geq\f$ LungFlowSolver::maxIteration
	this->computeAirwayPressure();
	int itr;
	double maxR = 0.0;
	for (itr = 0 ; itr < maxIteration ; itr++){
		this->computeAirwayPressure();
		this->computeAirwayPressureDrop();
		if (itr < maxIteration / 2){
			this->computeConductance();
//			VecAXPY(vecConductance0, -1.0, vecConductance);
//			PetscReal dcon;
//			VecNorm(vecConductance0, NORM_2, &dcon);
//			PetscPrintf(PETSC_COMM_WORLD, "Dcon= %e\n", dcon);
		}
		this->computeFlowRate();
		this->computeFlowRateResidual();

		PetscReal f;
		VecNorm(vecF, NORM_2, &f);
		if (f < this->tolerance){
			break;
		}


		//- Compute the Jacobian, calling LungFlowSolver::computeJacobian
		this->computeJacobian();
#if PETSC_VERSION_GE(3,5,0)
		KSPSetOperators(ksp,matJac,matJac);
#else
		KSPSetOperators(ksp,matJac,matJac,DIFFERENT_NONZERO_PATTERN);
#endif
		//  - Solve the linear system, \f$\mathbf{B}\mathbf{C}\mathbf{D}\vec{x} = \vec{r}(\vec{p_0})\f$
		KSPSolve(ksp, vecF, vecR);

		// - Update the pressure at Bifurcation, \f$\vec{p} = \vec{p_0} - \vec{x}\f$
		//	- If \f$|\vec{x}|\f$ is larger than LungFlowSolver::maxStepSize, scale it to be \f$|\vec{x}|<\f$LungFlowSolver::maxStepSize.
		VecNorm(vecR,NORM_INFINITY,&maxR);
		double scale = 0.5 * maxStepSize / std::max(maxStepSize, maxR);
		//VecAXPY(vecP, -scale, vecR);
		VecAXPY(vecP, -0.5, vecR);
	}

	PetscPrintf(PETSC_COMM_WORLD, "Number of Iteration = %d\n", itr);
	PetscReal maxC=0,minC=0;
	PetscInt iaw=0;

	VecMax(vecConductance, &iaw, &maxC);
	PetscPrintf(PETSC_COMM_WORLD, "Max Conductance=%e at %d\n", maxC,iaw);
	VecMin(vecConductance, &iaw, &minC);
	PetscPrintf(PETSC_COMM_WORLD, "Min Conductance=%e at %d\n", minC,iaw);
	if (itr >= maxIteration / 2){
		PetscReal f;
		VecNorm(vecF, NORM_2, &f);
		if (itr >= maxIteration){
			PetscPrintf(PETSC_COMM_WORLD, "Flow Solver Not Converged, NORM_2 Flowrate residual = %e\n", f);
		}else{
			PetscPrintf(PETSC_COMM_WORLD, "Flow Solver stopped updating conductance, NORM_2 Flowrate residual = %e\n", f);
		}
		PetscReal rMax,rMin;
		PetscInt irMax,irMin;
		VecMax(vecF,&irMax,&rMax);
		VecMin(vecF,&irMin,&rMin);
		PetscPrintf(PETSC_COMM_WORLD, "Max flowrate residual = %e at %d\n", rMax,irMax);
		PetscPrintf(PETSC_COMM_WORLD, "Min flowrate residual = %e at %d\n", rMin,irMin);
		PetscPrintf(PETSC_COMM_WORLD, "Maximum Pressure = %e\n", maxR);
		PetscReal qMax,qMin;
		PetscInt iMax,iMin;
		VecMax(vecQ,&iMax,&qMax);
		VecMin(vecQ,&iMin,&qMin);
		PetscPrintf(PETSC_COMM_WORLD, "Max flowrate = %e at %d\n", qMax,iMax);
		PetscPrintf(PETSC_COMM_WORLD, "Min flowrate = %e at %d\n", qMin,iMin);
//		VecMax(vecConductance0,&iMax,&qMax);
//		VecMin(vecConductance0,&iMin,&qMin);
//		PetscPrintf(PETSC_COMM_WORLD, "Max dConductance = %e at %d\n", qMax,iMax);
//		PetscPrintf(PETSC_COMM_WORLD, "Min dConductance = %e at %d\n", qMin,iMin);
	}
	KSPDestroy(&ksp);
	/// 4. Update the state variables of objects, calling LungFlowSolver::setResultsBackToObject
	this->setResultsBackToObject() ;
#ifdef DEBUG
	PetscPrintf(PETSC_COMM_WORLD, "# of Iteration = %d err=%e\n", itr,maxR);
	PetscReal qMax,qMin;
	PetscInt iMax,iMin;
	VecMax(vecQ,&iMax,&qMax);
	VecMin(vecQ,&iMin,&qMin);
	PetscPrintf(PETSC_COMM_WORLD, "Max flowrate = %e at %d\n", qMax,iMax);
	PetscPrintf(PETSC_COMM_WORLD, "Min flowrate = %e at %d\n", qMin,iMin);
#endif
	return itr;
}

void LungFlowSolver::setupPetSc() {
	/// - Declare the vector of node pressure at Bifurcation, \f$\vec{p}\f$.
	VecCreateMPI(PETSC_COMM_WORLD, bifurcationList.size(), PETSC_DETERMINE, &vecP);
	VecSet(vecP,0.0);
	/// - Declare a vector of Airway conductance
	VecCreateMPI(PETSC_COMM_WORLD, airwayList.size(), PETSC_DETERMINE, &vecConductance);
	VecSet(vecConductance,0.0);
	VecDuplicate(vecConductance, &vecConductance0);
	/// - Declare a vector of flowrate residual at Bifurcation, \f$\vec{r}\f$.
	VecDuplicate(vecP, &vecF);
	/// - Declare the solution vector for linear system.
	VecDuplicate(vecP, &vecR);
	/// - Declare the flowrate vector in Airway
	VecDuplicate(vecConductance, &vecQ);
	/// - Declare a vector of pressure in Acinus
	VecDuplicate(vecConductance, &vecAPb);
	/// - Declare a vector of pressure in Airway
	VecDuplicate(vecConductance, &vecAwP);
	VecDuplicate(vecConductance, &vecAwP0);
	VecSet(vecAwP,0.0);
	/// - Declare a vector of pressure Drop in Airway
	VecDuplicate(vecConductance, &vecAwDp);
	VecSet(vecAwDp,0.0);
	/// - Declare a vector of Acinus volume
	VecCreateMPI(PETSC_COMM_WORLD, acinusList.size(), PETSC_DETERMINE, &vecAcinusVolume);
	/// - Declare a matrix, \f$\mathbf{D}\f$
	MatCreateAIJ(PETSC_COMM_WORLD, airwayList.size(), bifurcationList.size(), PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matA);
	/// - Declare a matrix, \f$\mathbf{B}\f$
	MatCreateAIJ(PETSC_COMM_WORLD, bifurcationList.size(), airwayList.size(), PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matB);
	/// - Declare a matrix, \f$\mathbf{C}\f$
	MatCreateAIJ(PETSC_COMM_WORLD, airwayList.size(), airwayList.size(), PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matC);
	MatCreateAIJ(PETSC_COMM_WORLD, airwayList.size(), airwayList.size(), PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matDC);
	/// - Declare a matrix, \f$\mathbf{D_{AC}}\f$
	MatCreateAIJ(PETSC_COMM_WORLD, airwayList.size(), bifurcationList.size(), PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matAave);
	/// - Declare a matrix, \f$\mathbf{D_{pres}}\f$
	MatCreateAIJ(PETSC_COMM_WORLD, airwayList.size(), bifurcationList.size(), PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matDpres);
	/// - Declare matrix to extract flowrate to Acinus from vecQ
	MatCreateAIJ(PETSC_COMM_WORLD, acinusList.size(), airwayList.size(), PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matQtoAcinus);

	/// - Setup matrix \f$\mathbf{D}\f$ elements : Matrix to get pressure difference
	for (std::vector<Airway*>::iterator it = airwayList.begin() ; it != airwayList.end() ; it++){
		Airway *aw = *it;
		int bif_up = aw->getUpperBifurcationIndex();
		int bif_low = aw->getLowerBifurcationIndex();
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw->getIndex());
		if (bif_up >= 0){
			PetscInt col = graph->getVectorIndexOfBifurcationNode(bif_up);
			MatSetValue(matA, row, col, 1.0, INSERT_VALUES);
		}else{
			// this is trachea
			trachea = aw;
		}
		if (bif_low >= 0){
			PetscInt col = graph->getVectorIndexOfBifurcationNode(bif_low);
			MatSetValue(matA, row, col, -1.0, INSERT_VALUES);
		}else{
			// this is a terminal bronchi
		}
	}
	MatAssemblyBegin(matA, MAT_FINAL_ASSEMBLY);

	/// - Setup matrix \f$\mathbf{B}\f$ elements : Matrix to get flowrate residuals
	for (std::vector<Bifurcation*>::iterator it = bifurcationList.begin() ; it != bifurcationList.end() ; it++){
		Bifurcation *bif = *it;
		int pt = bif->getParentAirwayIndex();
		int dt1 = bif->getDaughterAirwayIndex1();
		int dt2 = bif->getDaughterAirwayIndex2();
		PetscInt row = graph->getVectorIndexOfBifurcationNode(bif->getIndex());
		PetscInt col;
		col = graph->getVectorIndexOfAirwayNode(pt);
		MatSetValue(matB, row, col, 1.0, INSERT_VALUES);
		col = graph->getVectorIndexOfAirwayNode(dt1);
		MatSetValue(matB, row, col, -1.0, INSERT_VALUES);
		col = graph->getVectorIndexOfAirwayNode(dt2);
		MatSetValue(matB, row, col, -1.0, INSERT_VALUES);
	}
	MatAssemblyBegin(matB, MAT_FINAL_ASSEMBLY);

	/// - Setup matrix \f$\mathbf{D_{AC}}\f$ elements : Matrix to get Airway pressure
	/// - Setup matrix \f$\mathbf{D_{pres}}\f$ elements : Matrix to get Pressure drop across Airway, DP=P_up - P_low
	for (std::vector<Airway*>::iterator it = airwayList.begin() ; it != airwayList.end() ; it++){
		Airway *aw = *it;
		int bif_up = aw->getUpperBifurcationIndex();
		int bif_low = aw->getLowerBifurcationIndex();
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw->getIndex());
		if (bif_up >= 0){
			PetscInt col = graph->getVectorIndexOfBifurcationNode(bif_up);
			MatSetValue(matAave, row, col, 0.5, INSERT_VALUES);
			MatSetValue(matDpres, row, col, 1.0, INSERT_VALUES);
		}
		if (bif_low >= 0){
			PetscInt col = graph->getVectorIndexOfBifurcationNode(bif_low);
			MatSetValue(matAave, row, col, 0.5, INSERT_VALUES);
			MatSetValue(matDpres, row, col, -1.0, INSERT_VALUES);
		}
	}
	MatAssemblyBegin(matAave, MAT_FINAL_ASSEMBLY);
	MatAssemblyBegin(matDpres, MAT_FINAL_ASSEMBLY);

	/// - Setup a matrix that extracts the flowrate to Acinus from the Airway flowrate vector.
	for (std::vector<Acinus*>::iterator it = acinusList.begin() ; it != acinusList.end() ; it++){
		Acinus *ac = *it;
		PetscInt row = graph->getVectorIndexOfAcinusNode(ac->getIndex());
		int tb = ac->getIndexForTerminalAirway();
		PetscInt col = graph->getVectorIndexOfAirwayNode(tb);

		MatSetValue(matQtoAcinus, row, col, 1, INSERT_VALUES);
	}
	MatAssemblyBegin(matQtoAcinus, MAT_FINAL_ASSEMBLY);

	MatAssemblyEnd(matA, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(matB, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(matAave, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(matDpres, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(matQtoAcinus, MAT_FINAL_ASSEMBLY);
#ifdef DEBUG
	MatView(matA, PETSC_VIEWER_STDOUT_WORLD);
	MatView(matB, PETSC_VIEWER_STDOUT_WORLD);
	MatView(matAave, PETSC_VIEWER_STDOUT_WORLD);
	MatView(matQtoAcinus, PETSC_VIEWER_STDOUT_WORLD);
#endif
}

void LungFlowSolver::computeJacobian() {
	/// - Convert the conductance vector to a diagonal matrix
	MatDiagonalSet(matC, vecConductance, INSERT_VALUES);
	/// - Compute \f$\mathbf{B}\mathbf{C}\mathbf{D}\f$
	MatMatMatMult(matB, matC, matA,scallJac, PETSC_DEFAULT, &matJac);

	///
//	VecAYPX(vecConductance0,-1.0,vecConductance);
//	VecAYPX(vecAwP0,-1.0,vecAwP);
//	PetscScalar *dc;
//	PetscScalar *dpaw;
//	VecGetArray(vecConductance0, &dc);
//	VecGetArray(vecAwP0, &dpaw);
//	int numberOfAirwaysInLocal = airwayList.size();
//	for (int i = 0 ; i < numberOfAirwaysInLocal ; i++){
//		if (dpaw[i] != 0.0){
//			dc[i] /= dpaw[i];
//		}else{
//			dc[i] = 0.0;
//		}
//	}
//	VecRestoreArray(vecConductance0, &dc);
//	VecRestoreArray(vecAwP0, &dpaw);
//	VecPointwiseMult(vecConductance0, vecConductance0,vecAwDp);
//	MatDiagonalSet(matDC, vecConductance0, INSERT_VALUES);
//	MatMatMatMult(matB, matDC,matAave,scallJac, PETSC_DEFAULT, &matDJac);
//
//	MatAXPY(matJac,1.0,matDJac,SAME_NONZERO_PATTERN);

	scallJac =  MAT_REUSE_MATRIX;
}

void LungFlowSolver::computeAirwayPressure() {
	VecCopy(vecAwP,vecAwP0);
	/// - Compute Airway pressure by averaging pressures at both ends.
	MatMult(matAave, vecP, vecAwP);
	VecAXPY(vecAwP,0.5,vecAPb); // this sill don't have mouth pressure

	/// - Set the Airway pressure to Airway objects
	PetscInt myBegin;
	VecGetOwnershipRange(vecAwP,&myBegin,NULL);
	PetscScalar *airway_pressure;
	VecGetArray(vecAwP, &airway_pressure);
	if (trachea != NULL){
		PetscInt row = graph->getVectorIndexOfAirwayNode(trachea->getIndex());
		airway_pressure[row - myBegin] += 0.5 * inlet->constant_pressure();
	}
	/// - Parallel loop over Airway objects
	int numberOfAirwaysInLocal = airwayList.size();

	for (int i = 0 ; i < numberOfAirwaysInLocal ; i++){
		Airway *aw = airwayList[i];
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw->getIndex());
		aw->setPressure(airway_pressure[row - myBegin]);
	}
	VecRestoreArray(vecAwP, &airway_pressure);
}

void LungFlowSolver::computeAirwayPressureDrop() {
	/// - Compute the pressure drop across the airway by
	/// 	subtracting pressure at lower bifurcation from pressure at upper bifurcation,
	/// 	\f$\Delta P= \mathbf{D_{pres}}P_{bifurcation}\f$.
	MatMult(matDpres, vecP, vecAwDp);
	VecAXPY(vecAwDp,-1.0,vecAPb); // this sill don't have mouth pressure
	/// - Set the Airway pressure-drop to Airway objects
	PetscInt myBegin;
	VecGetOwnershipRange(vecAwP,&myBegin,NULL);
	PetscScalar *airway_pressureDrop;
	VecGetArray(vecAwDp, &airway_pressureDrop);
	if (trachea != NULL){
		PetscInt row = graph->getVectorIndexOfAirwayNode(trachea->getIndex());
		airway_pressureDrop[row - myBegin] += inlet->constant_pressure();
	}
	/// - Parallel loop over Airway objects
	int numberOfAirwaysInLocal = airwayList.size();

	for (int i = 0 ; i < numberOfAirwaysInLocal ; i++){
		Airway *aw = airwayList[i];
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw->getIndex());
		aw->setPressureDrop(airway_pressureDrop[row - myBegin]);
	}
	VecRestoreArray(vecAwDp, &airway_pressureDrop);
}
void LungFlowSolver::computeConductance() {
	VecCopy(vecConductance,vecConductance0);
	PetscInt myBegin;
	VecGetOwnershipRange(vecConductance,&myBegin,NULL);
	PetscScalar *airway_conductance;
	VecGetArray(vecConductance, &airway_conductance);
	/// - For each Airway object, calling Airway::computeAirwayConductance
	/// - Parallel loop over Airway objects
	const int numberOfAirwaysInLocal = airwayList.size();

	for (int i = 0 ; i < numberOfAirwaysInLocal ; i++){
		Airway *aw = airwayList[i];
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw->getIndex());
		airway_conductance[row - myBegin] = aw->computeAirwayConductance();
	}
	VecRestoreArray(vecConductance, &airway_conductance);
}

void LungFlowSolver::computeFlowRateResidual() {
	/// - Compute, \f$\vec{r} = \mathbf{B}\vec{q}\f$
	MatMult(matB, vecQ, vecF);
}

void LungFlowSolver::computeFlowRate() {
	/// - Compute the pressure drop each Airway, \f$\vec{dp} = \mathbf{D}\vec{p} + \vec{d_t}P_T + \mathbf{D_{AC}}\vec{p_{ac}}\f$
	/// 	- Using pressures at Bifurcation, Acinus, and Inlet.
	///		- To get the inlet pressure, calling InletPressureWaveForm::constant_pressure
	MatMult(matA, vecP, vecQ);
	VecAXPY(vecQ,-1.0,vecAPb);

	PetscInt myBegin;
	VecGetOwnershipRange(vecQ,&myBegin,NULL);
	PetscScalar *airway_Dpressure;
	VecGetArray(vecQ, &airway_Dpressure);
	if (trachea != NULL){
		PetscInt row = graph->getVectorIndexOfAirwayNode(trachea->getIndex());
		airway_Dpressure[row - myBegin] += inlet->constant_pressure();
	}
	VecRestoreArray(vecQ, &airway_Dpressure);

	/// - Compute the flowrate, \f$\vec{q} = \mathbf{C}\vec{dp}\f$
	VecPointwiseMult(vecQ, vecConductance, vecQ);

	/// - Store the flowrate to each Airway object, calling Airway::setFlowRate
	PetscScalar *airway_flowrate;
	VecGetArray(vecQ, &airway_flowrate);
	const int numberOfAirwaysInLocal = airwayList.size();

	for (int i = 0 ; i < numberOfAirwaysInLocal ; i++){
		Airway *aw = airwayList[i];
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw->getIndex());
		aw->setFlowRate(airway_flowrate[row - myBegin]);
	}
	VecRestoreArray(vecQ, &airway_flowrate);
}

void LungFlowSolver::setTerminalPressureVector() {
	/// - Determine vecAPb, getting the pressure from each Acinus object, calling Acinus::getPressure
	VecSet(vecAPb,0.0);
	// loop for acini
	const PetscInt numberOfAcinusInLocal = acinusList.size();
	PetscInt *ix = new PetscInt[numberOfAcinusInLocal];
	PetscScalar *pr = new PetscScalar[numberOfAcinusInLocal];

	for (PetscInt i = 0 ; i < numberOfAcinusInLocal ; i++){
		Acinus *ac = acinusList[i];
		int tb = ac->getIndexForTerminalAirway();
		ix[i] = graph->getVectorIndexOfAirwayNode(tb);
		pr[i] = ac->getPressure();
	}
	VecSetValues(vecAPb, numberOfAcinusInLocal, ix, pr, INSERT_VALUES);
    VecAssemblyBegin(vecAPb);
    /* may do something */
    delete [] ix;
    delete [] pr;
    VecAssemblyEnd(vecAPb);
}

void LungFlowSolver::updateAcinusVolume(double dt) {
	/// - Extract flowrate into Acinus from the flowrate vector, \f$\vec{q}\f$
	MatMult(matQtoAcinus, vecQ, vecAcinusVolume);
	VecScale(vecAcinusVolume,dt);

	/// - Update LungFlowSolver::vecAcinusVolume and store the volume to each Acinus object, calling Acinus::setVolume
	PetscInt myBegin = 0;
	VecGetOwnershipRange(vecAcinusVolume,&myBegin,NULL);
	PetscScalar *acinusVolume_rate;
	VecGetArray(vecAcinusVolume, &acinusVolume_rate);
	const PetscInt numberOfAcinusInLocal = acinusList.size();

	for (PetscInt i = 0 ; i < numberOfAcinusInLocal ; i++){
		Acinus *ac = acinusList[i];
		int idx = ac->getIndex();
		PetscInt row = graph->getVectorIndexOfAcinusNode(idx);
		double vol = ac->getVolume();
		//std::cout << "vol=" << vol << " dv=" << acinusVolume_rate[row - myBegin] << std::endl;
		vol += acinusVolume_rate[row - myBegin];
		if (vol <= ac->getVolumeFrc()){
			std::cout << "Acinus " << ac->getIndex()
					<< " vol = " << vol << " less than FRC "
					<< ac->getVolumeFrc() << std::endl;
			vol = ac->getVolumeFrc();
		}
		ac->setVolume(vol);
		acinusVolume_rate[row - myBegin] = ac->getVolume();
	}
	VecRestoreArray(vecAcinusVolume, &acinusVolume_rate);

	/// - Solve Surfactant Model if included.
	if (isSurfactantModel()){
		/// * Solve for Surfactant in Acinus

		for (PetscInt i = 0 ; i < numberOfAcinusInLocal ; i++){
			Acinus *ac = acinusList[i];
			SurfaceTension *sf = ac->getSurfaceTensionObject();
			sf->computeConcentration(dt);
			sf->incrementTime();
		}
	}
#ifdef DEBUG
	// check
	VecGetOwnershipRange(vecQ,&myBegin,NULL);
	PetscScalar *airway_flowrate;
	VecGetArray(vecQ, &airway_flowrate);
	for (std::vector<Airway*>::iterator it = airwayList.begin() ; it != airwayList.end() ; it++){
		Airway *aw = *it;
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw->getIndex());
		double rad = aw->getRadius();
		double vel = airway_flowrate[row - myBegin] / (M_PI * rad * rad);
		double len = aw->getLength();

		if (len * 0.1 < std::abs(vel) * dt){
			std::cout << "Velocity in Airway " << aw->getIndex() << " is too large " << vel << "cm/s\n";
		}
	}
	VecRestoreArray(vecQ, &airway_flowrate);
#endif
}


bool LungFlowSolver::exportPHDF5dataspace(std::string pdir, hid_t& file) {
	hid_t gid;

	/// - Create data group, "./Airways"
	std::string airwayDataSpace = pdir + "/Airways";
	if (H5Lexists(file, airwayDataSpace.c_str(), H5P_DEFAULT) == 1) {
		gid = H5Gopen(file, airwayDataSpace.c_str(), NULL);
	} else {
		gid = H5Gcreate(file, airwayDataSpace.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	}

	/// - Calling AirwayDataBase::exportPHDF5dataspace
	AirwayDataBase::exportPHDF5dataspace(airwayDataSpace,file);
	/// - Close group
	H5Gclose(gid);

	/// - Create data group, "./Bifurcations"
	std::string bifurcationDataSpace = pdir + "/Bifurcations";
	if (H5Lexists(file, bifurcationDataSpace.c_str(), H5P_DEFAULT) == 1) {
		gid = H5Gopen(file, bifurcationDataSpace.c_str(), NULL);
	} else {
		gid = H5Gcreate(file, bifurcationDataSpace.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	}
	/// - Calling BifurcationDataBase::exportPHDF5dataspace
	BifurcationDataBase::exportPHDF5dataspace(bifurcationDataSpace,file);
	/// - Close group
	H5Gclose(gid);

	/// - Create data group, "./Acini"
	std::string aciniDataSpace = pdir + "/Acini";
	if (H5Lexists(file, aciniDataSpace.c_str(), H5P_DEFAULT) == 1) {
		gid = H5Gopen(file, aciniDataSpace.c_str(), NULL);
	} else {
		gid = H5Gcreate(file, aciniDataSpace.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	}
	/// - Calling AcinusDataBase::exportPHDF5dataspace
	AcinusDataBase::exportPHDF5dataspace(aciniDataSpace,file);
	/// - Close group
	H5Gclose(gid);

	return true;
}

bool LungFlowSolver::importHDF5dataspace(std::string pdir, hid_t &file){
	std::string airwayDataSpace = pdir + "/Airways";
	/// - Calling AirwayDataBase::exportPHDF5dataspace
	AirwayDataBase::importHDF5dataspace(airwayDataSpace,file);

	/// - Create data group, "./Bifurcations"
	std::string bifurcationDataSpace = pdir + "/Bifurcations";
	/// - Calling BifurcationDataBase::exportPHDF5dataspace
	BifurcationDataBase::importHDF5dataspace(bifurcationDataSpace,file);

	/// - Create data group, "./Acini"
	std::string aciniDataSpace = pdir + "/Acini";
	/// - Calling AcinusDataBase::exportPHDF5dataspace
	AcinusDataBase::importHDF5dataspace(aciniDataSpace,file);

	return true;
}

void LungFlowSolver::setResultsBackToObject() {
	/// - Store the flowrate to each Airway object, calling Airway::setFlowRate
	PetscInt myBegin;
	VecGetOwnershipRange(vecQ,&myBegin,NULL);
	PetscScalar *airway_flowrate;
	VecGetArray(vecQ, &airway_flowrate);
	const int numberOfAirwaysInLocal = airwayList.size();

	for (int i = 0 ; i < numberOfAirwaysInLocal ; i++){
		Airway *aw = airwayList[i];
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw->getIndex());
		aw->setFlowRate(airway_flowrate[row - myBegin]);
	}
	VecRestoreArray(vecQ, &airway_flowrate);

	/// - Store the pressure to each Bifurcation object, calling Bifurcation::setPressure
	VecGetOwnershipRange(vecP,&myBegin,NULL);
	PetscScalar *bif_pressure;
	VecGetArray(vecP, &bif_pressure);
	const int numberOfBifurcationInLocal = bifurcationList.size();

	for (int i = 0 ; i < numberOfBifurcationInLocal ; i++){
		Bifurcation *bif = bifurcationList[i];
		PetscInt row = graph->getVectorIndexOfBifurcationNode(bif->getIndex());
		bif->setPressure(bif_pressure[row - myBegin]);
	}
	VecRestoreArray(vecP, &bif_pressure);
}
