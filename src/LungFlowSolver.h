/*
 * LungFlowSolver.h
 *
 *  Created on: Oct 29, 2014
 *  MOdified on: June 28, 2016
 *      Author: fuji
 */
/// Airway Flow Solver

#ifndef LUNGFLOWSOLVER_H_
#define LUNGFLOWSOLVER_H_

#include <iostream>
#include <vector>
#include "AirwayTreePartitioning.h"
#include "BifurcationDataBase.h"
#include "AirwayDataBase.h"
#include "AcinusDataBase.h"
#include "PleuralPressureWaveForm.h"
#include "InletPressureWaveForm.h"
#include "petsc.h"

/// # Solve for pressure and flowrate in a lung-lobe model #
/// ## A Parent and two Daughter tubes connecting at a Bifurcation ##
/// ![A Bifurcation](https://docs.google.com/drawings/d/e/2PACX-1vTtU0J5DertyYxQOGn_s-0q63X0kNlnOiJ_TWJURYKubn5vNnSeIqD1REfA7Q1jf9KEcAvZ6BcFQeQC/pub?w=682&h=510)
///  - \f$P_P, P_B, P_{D1}, P_{D2}\f$ are pressures at Bifurcation.
///  - \f$Q_P, Q_B, Q_{D1}, Q_{D2}\f$ are flow rates in Airway.
///  - \f$C_P, C_B, C_{D1}, C_{D2}\f$ are coductanes in Airway, inverse of resistances.
///
/// ## Flow Rate in Airway ##
///  - \f$Q_P = C_P(P_B - P_P) \f$
///  - \f$Q_{D1} = C_{D1}(P_{D1} - P_B) \f$
///  - \f$Q_{D2} = C_{D2}(P_{D2} - P_B) \f$
///
/// ## Flow Rate Residual at Bifurcation ##
///  - \f$Q_P - Q_{D1} - Q_{D2} = R = 0 \f$
///
/// ## Flow in Airway Tree ##
/// - Assuming there are \f$N_b\f$ number of Bifurcation, \f$N_{aw}\f$ number of Airway, and \f$N_{ac}\f$ number of Acinus.\n
/// ![Airway Tree](https://docs.google.com/drawings/d/e/2PACX-1vTAXuZpekGVONfspg8Dt_lQ8wVe6cbt4dgTrtXOt4YhAjUKagdbAuDSog6D0R5GNLjP0mLtfWSV-FL0/pub?w=766&h=603)
/// 	- \f$P_T\f$ : the pressure at inlet.
/// 	- \f$P_{AC}\f$ : the pressure at Acinus.\n
/// - The pressure difference in each Airway is
/// 	- \f$\vec{dp} = \mathbf{D}\vec{p} + \vec{d_t}P_T + \mathbf{D_{AC}}\vec{p_{ac}} \f$\n
/// 	where \f$\mathbf{D}\f$ is a \f$N_{aw} \times N_b\f$ matrix, \f$\vec{p}\f$ is a \f$N_b \times 1\f$ vector of node pressure at Bifurcation,
/// \f$\vec{dp}\f$ is a \f$N_{aw} \times 1\f$ vector of pressure difference in Airway.\n
/// - The Flow Rate in Airway is:
/// 	- \f$\vec{q}=\mathbf{C}\vec{dp} = \mathbf{C}(\mathbf{D}\vec{p} + \vec{d_t}P_T + \mathbf{D_{AC}}\vec{p_{ac}})\f$\n
/// 	where \f$\vec{q}\f$ is \f$N_{aw} \times 1\f$ vector of flowrate, Q in Airway, \f$\mathbf{C}\f$ is a \f$N_{aw} \times N_{aw}\f$ diagonal matrix of Airway conductance, C.\n
/// - The flow rate residual at Bifurcation should be zero for the mass conservation, that is:
///  	- \f$\vec{r}=\mathbf{B}\vec{q} = 0\f$\n
/// 	where \f$\vec{r}\f$ is \f$N_b \times 1\f$ vector of flow rate residual, \f$\mathbf{B}\f$ is a \f$N_b \times N_{aw}\f$ matrix.
///
/// ### Solve for pressure with Newton's method ###
/// - Flow rate residual at Bifurcation is,
/// 	- \f$\vec{r}=\mathbf{B}\vec{q} = \mathbf{B}\mathbf{C}(\mathbf{D}\vec{p} + \vec{d_t}P_T + \mathbf{D_{AC}}\vec{p_{ac}})\f$\n
/// - Changes of \f$\vec{r}\f$ by perturbation of \f$\vec{p}\f$
///		- \f$\vec{\partial r} = \mathbf{B}\mathbf{C}\mathbf{D}\vec{\partial p} + \mathbf{B}\mathbf{\partial C}(\mathbf{D}\vec{p} + \vec{d_t}P_T + \mathbf{D_{AC}}\vec{p_{ac}})\f$\n
/// - Assuming that the Airway conductances are constant.
///		- \f$\vec{\partial r} = \mathbf{B}\mathbf{C}\mathbf{D}\vec{\partial p}\f$\n
///		where \f$\mathbf{B}\mathbf{C}\mathbf{D}\f$ is a \f$N_b \times N_b\f$ matrix.
///
/// #### Newton's iteration step ####
/// 1. For a given pressure at Bifurcation, \f$\vec{p_0}\f$, compute the residual, \f$\vec{r}(\vec{p_0})\f$
/// 2. If \f$|\vec{r}(\vec{p_0})|\f$ is less than LungFlowSolver::tolerance
/// or the number of iteration exceeds LungFlowSolver::maxIteration,
/// exit this iteration loop.
/// 3. Solve the linear system, \n
///		\f$\mathbf{B}\mathbf{C}\mathbf{D}\vec{x} = \vec{r}(\vec{p_0})\f$
///		where \f$\vec{x} = \vec{p_0} - \vec{p}\f$.
/// 4. Update \f$\vec{p_0}\f$ with \f$\vec{p}\f$ and goto 1.
class LungFlowSolver:  public AirwayDataBase,  public BifurcationDataBase,  public AcinusDataBase {
public:
	/// Constructor takes 'AirwayTreePartitioning' object, lists of bifurcation objects and airway onjects and acinus objects, the pleural and intel pressure objects
	LungFlowSolver(AirwayTreePartitioning *graph,std::vector<Bifurcation *> bifurcationList,std::vector<Airway *> airwayList,std::vector<Acinus *> acinusList,PleuralPressureWaveForm *pl,InletPressureWaveForm *inlet);
	/// Destructor does nothing
	virtual ~LungFlowSolver();

	/// Solve for pressures at bifurcation and flowrate in airways
	int solve();
	/// Update the volume of each Acinus
	void updateAcinusVolume(double dt);

	/// Export Data to Parallel HDF5 data space, must call collectively
	virtual bool exportPHDF5dataspace(std::string pdir, hid_t &file);
	/// Import Data from HDF5 data space, must call collectively
	virtual bool importHDF5dataspace(std::string pdir, hid_t &file);

	/// Get base-file name
	const std::string& getBaseFileName() const {
		return baseFileName;
	}

	/// Set base-file name
	void setBaseFileName(const std::string& baseFileName) {
		this->baseFileName = baseFileName;
	}

	bool isSurfactantModel() const {
		return surfactantModel;
	}

	void setSurfactantModel(bool surfactantModel) {
		this->surfactantModel = surfactantModel;
		this->setExportSurfactantStatus(surfactantModel);
	}
private:
	/// Setup PetSc
	void setupPetSc();
	/// Compute Airway Pressure
	void computeAirwayPressure();
	/// Compute Airway Pressure Drop
	void computeAirwayPressureDrop();
	/// Compute Jacobian
	void computeJacobian();
	/// Compute Airway Conductance
	void computeConductance();
	/// Compute Flow Rate in Airway
	void computeFlowRate();
	/// Compute Flow Rate Residual at Bifurcation
	void computeFlowRateResidual();
	/// Set Acinus Pressures to the end of terminal Airway
	void setTerminalPressureVector();
	/// Update State Variables of \ objects.
	void setResultsBackToObject();
	/// Matrix, \f$\mathbf{D}\f$
	Mat matA;
	/// Matrix, \f$\mathbf{B}\f$
	Mat matB;
	/// Matrix, \f$\mathbf{C}\f$
	Mat matC;
	Mat matDC;
	/// Matrix, \f$\mathbf{D_{AC}}\f$
	Mat matAave;
	/// Matrix, \f$\mathbf{D_{pres}}\f$ to get pressure drop across the airway, \f$\Delta P= \mathbf{D_{pres}}P_{bifurcation}\f$.
	Mat matDpres;
	/// Jacobian, \f$\mathbf{B}\mathbf{C}\mathbf{D}\f$
	Mat matJac;
	Mat matDJac;
	/// Matrix to extract flowrate to Acinus from vecQ
	Mat matQtoAcinus;
	/// Vector of Airway conductance
	Vec vecConductance;
	/// Vector of Airway conductance at Previous Iteration
	Vec vecConductance0;
	/// Vector of pressure at Bifucation
	Vec vecP;
	/// Solution vector of the linear system
	Vec vecR;
	/// Vector of the flowrate residual at Bifurcation
	Vec vecF;
	/// Vector of flowrate in Airway
	Vec vecQ;
	/// Vector of pressure in Aicinus
	Vec vecAPb;
	/// Vector of pressure in Airway
	Vec vecAwP;
	/// Vector of pressure in Airway at Previous Iteration
	Vec vecAwP0;
	/// Vector of pressure drop in Airway
	Vec vecAwDp;
	/// Vector of Acinus volume
	Vec vecAcinusVolume;
	/// Control flag for Jacobi Matrix
	MatReuse scallJac;
	/// Object of AirwayTreePartitioning
	AirwayTreePartitioning *graph;
	/// List of Bifurcation objects
	std::vector<Bifurcation *> bifurcationList;
	/// List of Airway objects
	std::vector<Airway *> airwayList;
	/// List of Acinus object
	std::vector<Acinus *> acinusList;
	/// Object of PleuralPressureWaveForm
	PleuralPressureWaveForm *pl;
	/// Object of InletPressureWaveForm
	InletPressureWaveForm *inlet;
	/// Object of Trachea Airway object
	Airway *trachea;

	/// The maximum step size of pressure update
	double maxStepSize;
	/// The tolerance of residual to determine the convergence
	double tolerance;
	/// The maximum number of iterations
	int maxIteration;
	/// Base File Name
	std::string baseFileName;
	/// Flag to solve Surfactant Model (false in default)
	bool surfactantModel;

	static constexpr char dbName[] = "Flow Solver Database";///< Database Name
};

#endif /* LUNGFLOWSOLVER_H_ */
