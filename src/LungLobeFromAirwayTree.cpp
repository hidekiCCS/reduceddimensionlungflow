/*
 * LungLobeFromAirwayTree.cpp
 *
 *  Created on: Jun 24, 2015
<<<<<<< HEAD
=======
 *  Modified on: Jun 29, 2016
>>>>>>> SurfactantEffect
 *      Author: fuji
 */

#include "LungLobeFromAirwayTree.h"

LungLobeFromAirwayTree::LungLobeFromAirwayTree(const char* bfn) :
		AirwayTreeModel(bfn) {
	/// - Constructor reads XML file to build a lung lobe model.
	readXML();

	/// - Creating bifurcation info.
	int bifidx = 0;
	maxGeneration = 1;
	for (int i = 0; i < listOfAirways.size(); i++) {
		if (listOfAirways.at(i)->daughterAirway1 != NULL) {
			bifurcation *bif = new bifurcation();
			bif->index = bifidx;
			bif->parentAirway = i;
			bif->daughterAirway1 = listOfAirways.at(i)->daughterAirway1->index;
			bif->daughterAirway2 = listOfAirways.at(i)->daughterAirway2->index;
			listOfBifurcation.push_back(bif);
			bifidx++;
		}
		if (maxGeneration < listOfAirways.at(i)->GenerationNumber) {
			maxGeneration = listOfAirways.at(i)->GenerationNumber;
			firstAirwayIndex = i;
		}
	}
	generateGeometory1();
}

LungLobeFromAirwayTree::~LungLobeFromAirwayTree() {
	/// * Clean up listOfBifurcation
	for (std::vector<bifurcation *>::iterator it = listOfBifurcation.begin(); it != listOfBifurcation.end();) {
		delete *it;
		it = listOfBifurcation.erase(it);
	}
}


void LungLobeFromAirwayTree::resetSize(const double radiusOfAlveolus){
	/// Reset Size of Airways and Acini
	const double volumeOfAlveolus = 4 * std::pow(radiusOfAlveolus,3) * M_PI / 3;
	for (int i = 0; i < listOfAirways.size(); i++) {
		listOfAirways[i]->radius *= radiusOfAlveolus;
		listOfAirways[i]->length *= radiusOfAlveolus;
	}
	double lungVolume = 0.0;
	for (int i = 0; i < listOfAcinus.size(); i++) {
		listOfAcinus[i]->volumeFRC = listOfAcinus[i]->numberOfAlveoli * volumeOfAlveolus;
		lungVolume += listOfAcinus[i]->volumeFRC;
	}
	this->setLungVolumeFrc(lungVolume);
}

void LungLobeFromAirwayTree::generateSymmetricAirways(int generation) {
	/// This method disabled
	std::cout << "DO NOT CALL\n";
}

void LungLobeFromAirwayTree::makeAsymmetric(double ratio) {
	/// This method disabled
	std::cout << "DO NOT CALL\n";
}

int LungLobeFromAirwayTree::get_NumberOfBifurcations() {
	return listOfBifurcation.size();
}

int LungLobeFromAirwayTree::get_ParentTubeIndex(int i) {
	return listOfBifurcation.at(i)->parentAirway;
}

int LungLobeFromAirwayTree::get_DaughterTube1Index(int i) {
	return listOfBifurcation.at(i)->daughterAirway1;
}

int LungLobeFromAirwayTree::get_DaughterTube2Index(int i) {
	return listOfBifurcation.at(i)->daughterAirway2;
}

int LungLobeFromAirwayTree::get_numberOfAirways() {
	return listOfAirways.size();
}

int LungLobeFromAirwayTree::get_GenerationNumber(int i) {
	return listOfAirways.at(i)->GenerationNumber;
}

int LungLobeFromAirwayTree::get_UpperBifurcationIndex(int i) {
	/// Input argument is an Airway index. Return index of an upper Bifurcation index.
	for (int n = 0; n < listOfBifurcation.size(); n++) {
		if (listOfBifurcation.at(n)->daughterAirway1 == i) {
			return n;
		}
		if (listOfBifurcation.at(n)->daughterAirway2 == i) {
			return n;
		}
	}
	/// If not found, return -1. Input airway is trachea.
	return -1;
}

int LungLobeFromAirwayTree::get_LowerBifurcationIndex(int i) {
	/// Input argument is an Airway index. Return index of an lower Bifurcation index.
	for (int n = 0; n < listOfBifurcation.size(); n++) {
		if (listOfBifurcation.at(n)->parentAirway == i) {
			return n;
		}
	}
	/// If not found, return -1. Input airway is terminal airway.
	return -1;
}

int LungLobeFromAirwayTree::get_AcinusIndex(int i) {
	/// Input argument is an Airway index. Return index of the connecting acinus index.
	for (int n = 0; n < listOfAcinus.size(); n++) {
		airway *aw = (airway *) (listOfAcinus.at(n)->terminalBronch);
		if (aw->index == i) {
			return n;
		}
	}
	/// If not found, return -1. Input airway is NOT terminal airway.
	return -1;
}

double LungLobeFromAirwayTree::get_AirwayLength(int i) {
	return listOfAirways[i]->length;
}

double LungLobeFromAirwayTree::get_AirwayRadius(int i) {
	return listOfAirways.at(i)->radius;
}

int LungLobeFromAirwayTree::get_NumberOfAcinus() {
	return listOfAcinus.size();
}

int LungLobeFromAirwayTree::get_TerminalAirway(int i) {
	airway *aw = (airway *) (listOfAcinus[i]->terminalBronch);
	return aw->index;
}

double LungLobeFromAirwayTree::get_AcinusVolumeFRC(int i) {
	return listOfAcinus.at(i)->volumeFRC;
}

int LungLobeFromAirwayTree::get_NumberOfAlveoliInAcinus(int i) {
	return listOfAcinus.at(i)->numberOfAlveoli;
}

void LungLobeFromAirwayTree::generateGeometory1() {
	/// Generate Geometry Data for Visualization
	geometryNodes.clear();
	indexOfUpperVertexOfAirway.resize(get_numberOfAirways());
	indexOfLowerVertexOfAirway.resize(get_numberOfAirways());
	std::stack<int> nodeStack;
	std::stack<Eigen::Vector3d> directionStack;
	std::stack<double> lengthStack;
	std::stack<int> indexStack;

	Eigen::Vector3d inlet(0.0, 1.0, 0.0);
	geometryNodes.push_back(inlet);
	nodeStack.push(0);
	Eigen::Vector3d dirc(0.0, -1.0, 0.0);
	directionStack.push(dirc);
	lengthStack.push(1.0);
	indexStack.push(firstAirwayIndex);

	Eigen::Matrix3d tr1, tr2;
	tr1 << 0, 1, 0, 1, 0, 0, 0, 0, 1;
	tr2 << 0, -1, 0, -1, 0, 0, 0, 0, 1;
	const double fac = 0.7;
	while (!(indexStack.empty())) {
		int airwayIdx = indexStack.top();
		Eigen::Vector3d direction = directionStack.top();
		double length = lengthStack.top();
		int nodeIdx = nodeStack.top();
		indexStack.pop();
		directionStack.pop();
		lengthStack.pop();
		nodeStack.pop();

		Eigen::Vector3d airwayBeginPoint = geometryNodes[nodeIdx];
		Eigen::Vector3d airwayEndPoint = airwayBeginPoint + length * direction;
		//std::cout << "length=" << length << std::endl;
		//std::cout << "direction=" << direction << std::endl;
		geometryNodes.push_back(airwayEndPoint);
		indexOfUpperVertexOfAirway[airwayIdx] = nodeIdx;
		indexOfLowerVertexOfAirway[airwayIdx] = geometryNodes.size() - 1;

		if (listOfAirways.at(airwayIdx)->daughterAirway1 != NULL) {
			nodeStack.push(geometryNodes.size() - 1);
			nodeStack.push(geometryNodes.size() - 1);
			directionStack.push(tr1 * direction);
			directionStack.push(tr2 * direction);
			lengthStack.push(length * fac);
			lengthStack.push(length * fac);
			indexStack.push(listOfAirways.at(airwayIdx)->daughterAirway1->index);
			indexStack.push(listOfAirways.at(airwayIdx)->daughterAirway2->index);
		}
	}
}

int LungLobeFromAirwayTree::get_IndexOfAirwayUpperVertex(int idx) {
	/// List generated by generateGeometry
	return indexOfUpperVertexOfAirway[idx];
}

int LungLobeFromAirwayTree::get_IndexOfAirwayLowerVertex(int idx) {
	/// List generated by generateGeometry
	return indexOfLowerVertexOfAirway[idx];
}

int LungLobeFromAirwayTree::get_IndexOfBifurcationVertex(int idx) {
	/// It is parent tube lower vertex index
	return get_IndexOfAirwayLowerVertex(get_ParentTubeIndex(idx));
}

int LungLobeFromAirwayTree::get_IndexOfAcinusVertex(int idx) {
	/// It is terminal airways lower vertex index;
	return get_IndexOfAirwayLowerVertex(get_TerminalAirway(idx));
}

int LungLobeFromAirwayTree::get_IndexOfInletVertex() {
	/// Inlet vertex index is zero
	return 0;
}

int LungLobeFromAirwayTree::get_IndexOfTrachea() {
	int inletIndex = get_IndexOfInletVertex();
	for (int i = 0; i < listOfAirways.size(); i++) {
		if (get_IndexOfAirwayUpperVertex(i) == inletIndex) {
			return i;
		}
	}
	throw "Trachea not found";
	return -1;
}

/// Get number of Acini within ROI of the airway
int LungLobeFromAirwayTree::get_numberOfAciniInROI(int airway_idx){
	return listOfAirways[airway_idx]->listOfAciniROI.size();
}

/// Get an Acinus index in ROI of the airway
int LungLobeFromAirwayTree::get_indexOfAciniInROI(int airway_idx, int idx){
	return listOfAirways[airway_idx]->listOfAciniROI[idx]->index;
}

/// Get the ratio of volume of acinus within ROI to ROI
double LungLobeFromAirwayTree::get_volumeRatioOfAciniROI(int airway_idx, int idx){
	int n = listOfAirways[airway_idx]->listOfAciniROI.size();
	int total = 0;
	for (int i = 0 ; i < n ; i++)  total += listOfAirways[airway_idx]->listOfTNumberOfAlveoliROI[i];

	return static_cast<double>(listOfAirways[airway_idx]->listOfTNumberOfAlveoliROI[idx]) / total;
}

Eigen::Vector3d LungLobeFromAirwayTree::get_VertexCoordinate(int idx) {
	return geometryNodes[idx];
}
