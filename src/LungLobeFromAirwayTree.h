/*
 * LungLobeFromAirwayTree.h
 *
 *  Created on: Jun 24, 2015
 *      Author: fuji
 */

#ifndef LUNGLOBEFROMAIRWAYTREE_H_
#define LUNGLOBEFROMAIRWAYTREE_H_

#include "AirwayTreeModel.h"
#include <Eigen/Core>
#include <Eigen/Dense>

class LungLobeFromAirwayTree: public AirwayTreeModel {
public:
	/// Constructor takes the name of XML file.
	LungLobeFromAirwayTree(const char *bfn);
	/// Destructor
	virtual ~LungLobeFromAirwayTree();

	/// Reset Size of Airways and Acini
	void resetSize(const double radiusOfAlveolus);
	/// DO NOT CALL
	virtual void generateSymmetricAirways(int generation);

	/// DO NOT CALL
	virtual void makeAsymmetric(double ratio);

	/// Get the NUmber of Bifurcation
	int get_NumberOfBifurcations();
	/// Get the index of parent tube of airway, i
	int get_ParentTubeIndex(int i);
	/// Get the index of daughter tube 1 of airway, i
	int get_DaughterTube1Index(int i);
	/// Get the index of daughter tube 2 of airway, i
	int get_DaughterTube2Index(int i);
	/// Get the number of airways
	int get_numberOfAirways();
	/// Get the bottom-up generation number of airway, i
	int get_GenerationNumber(int i);
	/// Get the index of upper-bifurcation of airway, i
	int get_UpperBifurcationIndex(int i);
	/// Get the index of lower-bifurcation of airway, i
	int get_LowerBifurcationIndex(int i);
	/// Get the index of acinus of airway, i, if it is a terminal airway, otherwise -1.
	int get_AcinusIndex(int i);
	/// Get the length of airway, i
	double get_AirwayLength(int i);
	/// Get the radius of airway, i
	double get_AirwayRadius(int i);
	/// Get the number of acini
	int get_NumberOfAcinus();
	/// Get index of terminal airways.
	int get_TerminalAirway(int i);
	 /// Get the volume of acinus,i at FRC
	double get_AcinusVolumeFRC(int i);
	/// Get the number of alveoli in acinus,i
	int get_NumberOfAlveoliInAcinus(int i);

	/// Generate Geometry for Visualization
	void generateGeometory1();
	/// Get Number Of Vertices
	int get_NumberOfVertices(){
		return get_NumberOfBifurcations() + get_NumberOfAcinus() + 1;
	}
	/// Get Index Of Airway Upper Vertex
	int get_IndexOfAirwayUpperVertex(int idx);
	/// Get Index Of Airway Lower Vertex
	int get_IndexOfAirwayLowerVertex(int idx);
	/// Get Index Of Bifurcation Vertex
	int get_IndexOfBifurcationVertex(int idx);
	/// Get Index Of Acinus Vertex
	int get_IndexOfAcinusVertex(int idx);
	/// Get Index Of Inlet
	int get_IndexOfInletVertex();
	/// Get Airway Index Of Trachea
	int get_IndexOfTrachea();

	/// Get number of Acini within ROI of the airway
	int get_numberOfAciniInROI(int airway_idx);
	/// Get an Acinus index in ROI of the airway
	int get_indexOfAciniInROI(int airway_idx, int idx);
	/// Get the ratio of volume of acinus within ROI to ROI
	double get_volumeRatioOfAciniROI(int airway_idx, int idx);

	/// Get Vertex Coordinate
	Eigen::Vector3d get_VertexCoordinate(int idx);

private:
	/// bifurcation container class
	class bifurcation {
	public:
		bifurcation() {
			parentAirway = -1;
			daughterAirway1 = -1;
			daughterAirway2 = -2;
			index = 0;
		}
		int index;
		int parentAirway;
		int daughterAirway1;
		int daughterAirway2;
	};

	/// list of bifurcation
	std::vector<bifurcation *> listOfBifurcation;

	/// Geometry
	std::vector<Eigen::Vector3d> geometryNodes;
	/// max generation number
	int maxGeneration;
	/// First Airway
	int firstAirwayIndex;
	/// Index of Upper Vertex of Airway
	std::vector<int> indexOfUpperVertexOfAirway;
	/// Index of Lower Vertex of Airway
	std::vector<int> indexOfLowerVertexOfAirway;
};

#endif /* LUNGLOBEFROMAIRWAYTREE_H_ */
