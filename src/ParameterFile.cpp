///
///  Read Parameter File 
///
///  05/04/2010 
///  09/28/2015 Modified
///
#include"ParameterFile.h"

/// read input file
ParameterFile::ParameterFile(char *fn) {
	FILE *fp;
	char buf[1024];

	fp = fopen(fn, "r");
	if (fp == NULL) {
		printf("File %s not found\n", fn);
		exit(-1);
	}

	while (1) {
		if (NULL == fgets(buf, 1024, fp))
			break;
		if (buf[0] == '#')
			continue; /// skip comment line
		if (strlen(buf) < 3)
			continue; /// skip comment line

		char *v1 = strtok(buf, "=");
		char *v2 = strtok(NULL, "#");
		if ((strlen(v1) < 1) || (strlen(v2) < 1))
			continue;

		string *key = new string(v1);
		string *val = new string(v2);

		if ((key->length() < 1) || (val->length() < 1)) {
			delete key;
			delete val;
			continue;
		}

		/// get rid of space
		for (string::iterator key_it = key->begin(); key_it != key->end();) {
			if (isspace((int) *key_it) != 0) {
				key_it = key->erase(key_it);
			} else {
				key_it++;
			}
		}
		for (string::iterator val_it = val->begin(); val_it != val->end();) {
			if (isspace((int) *val_it) != 0) {
				val_it = val->erase(val_it);
			} else {
				val_it++;
			}
		}

		/// push into database
		database.push_back(key);
		database.push_back(val);

		//    cout << *key << "(" << *val << ")"<< endl;

	}
	fclose(fp);
}

/// destroy data base
ParameterFile::~ParameterFile() {
	vector<string*>::iterator database_it;
	for (database_it = database.begin(); database_it != database.end();) {
		delete *database_it;
		database_it = database.erase(database_it);
	}
	database.clear();
}

/// get a string from database
string *ParameterFile::getString(string key) {
	vector<string*>::iterator database_it;
	for (database_it = database.begin(); database_it != database.end();) {
		if ((*database_it)->find(key) != string::npos) {
			if (key.find((*database_it)->data()) != string::npos) {
				++database_it;
				return *database_it;
			}
		}
		++database_it;
		++database_it;
	}

	//cout << "key=" << key << " not found in database" << endl;
	return (string *) NULL;
}
/// get a integer value from database
int ParameterFile::getIntValue(string key) {
	vector<string*>::iterator database_it;
	for (database_it = database.begin(); database_it != database.end();) {
		if ((*database_it)->find(key) != string::npos) {
			if (key.find((*database_it)->data()) != string::npos) {
				++database_it;
				return atoi((*database_it)->c_str());
			}
		}
		++database_it;
		++database_it;
	}

	//cout << "key=" << key << " not found in database" << endl;
	return (int) NULL;
}
/// get a double value from database
double ParameterFile::getRealValue(string key) {
	vector<string*>::iterator database_it;
	for (database_it = database.begin(); database_it != database.end();) {
		if ((*database_it)->compare(key) == 0) {
			++database_it;
			return atof((*database_it)->c_str());
		}
		++database_it;
		++database_it;
	}

	//cout << "key=" << key << " not found in database" << endl;
	return (double) NULL;
}

/// add a parameter if success returns true
bool ParameterFile::addParameter(string keyin, string value) {
	string *key = new string(keyin);
	string *val = new string(value);
	database.push_back(key);
	database.push_back(val);
	return true;
}

/// remove a parameter from database if success returns true
bool ParameterFile::removeParameter(string key) {
	vector<string*>::iterator database_it;
	for (database_it = database.begin(); database_it != database.end();) {
		if ((*database_it)->find(key) != string::npos) {
			delete *database_it;
			database_it = database.erase(database_it);
			delete *database_it;
			database_it = database.erase(database_it);
			return true;
		}
		++database_it;
		++database_it;
	}

	//cout << "key=" << key << " not found in database" << endl;
	return false;
}

void ParameterFile::listParameters() {
	vector<string*>::iterator database_it;
	for (database_it = database.begin(); database_it != database.end();) {
		std::cout << (*database_it)->c_str() << " = ";
		database_it++;
		std::cout << (*database_it)->c_str() << std::endl;
		database_it++;
	}
}
