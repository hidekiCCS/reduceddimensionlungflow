///
///  Read Parameter File 
///
///  05/04/2010 
///
#ifndef __ParameterFile_h
#define __ParameterFile_h

#include<iostream>
#include<vector>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

using namespace std;

class ParameterFile{
  vector<string *> database;

 public:
  ParameterFile(char *fn); /// read input file
  ~ParameterFile();  /// destroy data base
  
  string *getString(string key); /// get a string from database
  int getIntValue(string key);  /// get a integer value from database
  double getRealValue(string key); /// get a double value from database

  bool addParameter(string key, string value); /// add a parameter if success returns true
  bool removeParameter(string key); /// remove a parameter from database if success returns true

  void listParameters();/// Display keys and values
};

#endif // __ParametersFile_h
