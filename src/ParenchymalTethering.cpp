/*
 * ParenchymalTethering.cpp
 *
 *  Created on: Jun 25, 2015
 *      Author: fuji
 */

#include "ParenchymalTethering.h"
#include "AirwayTubeLaw.h"
#include <mpi.h>

ParenchymalTethering::ParenchymalTethering(AirwayTreePartitioning* graph, std::vector<Airway*> airwayList, std::vector<Acinus*> acinusList) :
		graph(graph), airwayList(airwayList), acinusList(acinusList) {
	myid = MPI::COMM_WORLD.Get_rank();
	numproc = MPI::COMM_WORLD.Get_size();
	///  * Setup PetSc Matrix ParenchymalTethering::setupPetSc
	setupPetSc();

	/// * Turn parenchymal tethering flag on by AirwayTubeLaw::setParenchymaTether()
	const int localNumberOfAirways = airwayList.size();
	for (int i = 0; i < localNumberOfAirways; i++) {
		AirwayTubeLaw *aw = dynamic_cast<AirwayTubeLaw *>(airwayList[i]);
		aw->setParenchymaTether(true);
	}
}

ParenchymalTethering::~ParenchymalTethering() {
	MatDestroy(&matNij);
	VecDestroy(&vecPressureInAcini);
	VecDestroy(&vecPressureInROI);
}

void ParenchymalTethering::updateParenchymalState(void) {
	/// Update Shear Modulus and \f$ R_{AW,u} \f$ for each Airway
	updateParenchymaStateOfAirways();
}

bool ParenchymalTethering::exportPHDF5dataspace(std::string pdir, hid_t& file) {
	hid_t gid;
	/// - Set Dataspace Name "./Airways"
	std::string dataSpace = pdir + "/Airways";
	if (H5Lexists(file, dataSpace.c_str(), H5P_DEFAULT) == 1) {
		gid = H5Gopen(file, dataSpace.c_str(), NULL);
	} else {
		gid = H5Gcreate(file, dataSpace.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	}

	/// - Gather data to store.
	std::vector<double> shearModulud;
	std::vector<double> holeRadius;
	std::vector<double> periAWPressure;

	const int localNumberOfAirways = airwayList.size();
	for (int i = 0; i < localNumberOfAirways; i++) {
		AirwayTubeLaw *aw = dynamic_cast<AirwayTubeLaw *>(airwayList[i]);
		shearModulud.push_back(aw->getParenchymalShearModulus());
		holeRadius.push_back(aw->getParenchymalHoleRadius());
		periAWPressure.push_back(aw->getPeriAirwayPressureValue());
	}
	const int totalNumberOfAirways = graph->getNumOfAirways();

	///	- Make Index
	hsize_t *data_indices = new hsize_t[localNumberOfAirways];
	for (int i = 0; i < localNumberOfAirways; i++) {
		data_indices[i] = airwayList[i]->getIndex();
	}

	std::string dataSpaceShearModulus = dataSpace + "/ShearModulus";
	exportPHDF5dataspaceDouble1dIndex(dataSpaceShearModulus, file, totalNumberOfAirways, localNumberOfAirways, data_indices, shearModulud.data());
	std::string dataSpaceHoleRdius = dataSpace + "/HoleRdius";
	exportPHDF5dataspaceDouble1dIndex(dataSpaceHoleRdius, file, totalNumberOfAirways, localNumberOfAirways, data_indices, holeRadius.data());
	std::string dataSpacePeriAirwayPressure = dataSpace + "/PeriAirwayPressure";
	exportPHDF5dataspaceDouble1dIndex(dataSpacePeriAirwayPressure, file, totalNumberOfAirways, localNumberOfAirways, data_indices, periAWPressure.data());

	/// - Close group
	H5Gclose(gid);
	return true;
	delete[] data_indices;
}


void ParenchymalTethering::updateParenchymaStateOfAirways(void){
	PetscInt myBegin;
	VecGetOwnershipRange(vecPressureInAcini, &myBegin, NULL);
	PetscScalar *acinus_pressures;
	VecGetArray(vecPressureInAcini, &acinus_pressures);
	const int numberOfAcinusInLocal = acinusList.size();

	for (int i = 0; i < numberOfAcinusInLocal; i++) {
		/// * Loop for local Acinus objects in local
		Acinus *ac = acinusList[i];
		///		- Get Acinus index
		int ac_index = ac->getIndex();
		PetscInt row = graph->getVectorIndexOfAcinusNode(ac_index);
		///		- Get Pressure of Acinus - Pleural Pressure
		double pres = ac->getPressure();
		double p_pl = ac->getPleuralPressure();
		/// 	- Store in the vector.
		acinus_pressures[row - myBegin] = pres - p_pl;
	}
	VecRestoreArray(vecPressureInAcini, &acinus_pressures);

	/// * Compute the volume-weighted average of Pressure in ROI of airways
	MatMult(matNij, vecPressureInAcini, vecPressureInROI);

	VecGetOwnershipRange(vecPressureInROI, &myBegin, NULL);
	PetscScalar *airway_ROI_pressure;
	VecGetArray(vecPressureInROI, &airway_ROI_pressure);
	const int numberOfAirwayInLocal = airwayList.size();

	for (int i = 0; i < numberOfAirwayInLocal; i++) {
		/// * Loop for local Airway objects in local
		AirwayTubeLaw *aw = dynamic_cast<AirwayTubeLaw *>(airwayList[i]);
		///		- Get Airway index
		int aw_index = aw->getIndex();
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw_index);
		///		- Get the averaged pressure in ROI of Airway from PetSc vector
		double ROIpressure = airway_ROI_pressure[row - myBegin];
		///		- Set the Effective Shear Modulus to Airway object
		double shearModulus = this->effectiveShearModulusROI(ROIpressure);
		aw->setParenchymalShearModulus(shearModulus);
		///		- Get \f$ R_{AW,0} \f$ from Airway object.
		double radiusRest = aw->getRadiusRest();
		///		- Set \f$ R_{AW,e}\f$ to Airway object.
		double radiusHole = this->equilibriumParenchymalHoleRadius(ROIpressure,radiusRest);
		aw->setParenchymalHoleRadius(radiusHole);
	}
	VecRestoreArray(vecPressureInROI, &airway_ROI_pressure);
}

void ParenchymalTethering::setupPetSc() {
	/// * Obtains LungLobeFromAirwayTree object
	LungLobeFromAirwayTree *lung = graph->getLung();

	/// - Declare Matrix of Area Ratios
	MatCreateAIJ(PETSC_COMM_WORLD, airwayList.size(), acinusList.size(), PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matNij);
	/// - Declare the vector of Acinus Pressures.
	VecCreateMPI(PETSC_COMM_WORLD, acinusList.size(), PETSC_DETERMINE, &vecPressureInAcini);
	/// - Declare the vector of averaged Pressure in ROI
	VecCreateMPI(PETSC_COMM_WORLD, airwayList.size(), PETSC_DETERMINE, &vecPressureInROI);

	/// - Setup Matrix of Volume Ratio in ROI
	for (std::vector<Airway*>::iterator it = airwayList.begin(); it != airwayList.end(); it++) {
		/// * Loop for local Airway objects
		Airway *aw = *it;
		///		- Get Airway index
		int aw_index = aw->getIndex();
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw_index);
		///		- Get the number of Acinus surrounding the Airway by calling LungLobeFromAirwayTree::get_numberOfAciniSurrounding
		int numberOfAincusSurround = lung->get_numberOfAciniInROI(aw_index);
		double totalVol = 0.0;
		for (int i = 0; i < numberOfAincusSurround; i++) {
			///		* Loop for the surrounding Acinus
			///		- Get Acinus index by calling LungLobeFromAirwayTree::get_indexOfAciniSurrounding
			int ac_index = lung->get_indexOfAciniInROI(aw_index, i);
			///		- Get the area ratio by calling LungLobeFromAirwayTree::get_areaRatioOfAciniSurrounding
			double volumeRatio = lung->get_volumeRatioOfAciniROI(aw_index, i);
			PetscInt col = graph->getVectorIndexOfAcinusNode(ac_index);
			/// 	- Store the area ratio to the Matrix.
			MatSetValue(matNij, row, col, volumeRatio, INSERT_VALUES);
			totalVol += volumeRatio;
		}
	}
	/// * Assemble the Matrix.
	MatAssemblyBegin(matNij, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(matNij, MAT_FINAL_ASSEMBLY);
}


double ParenchymalTethering::effectiveShearModulusROI(double tp_pressure_average_ROI){
	/// \f$ G_{eff} = 4.47\exp(5.83\times 10^{-3} P_{TP,ave}^2) \f$
	return 4.47 * std::exp(5.83e-3 * tp_pressure_average_ROI * tp_pressure_average_ROI);
}

double ParenchymalTethering::equilibriumParenchymalHoleRadius(double tp_pressure_average_ROI, double radiusRest){
	const int a = 0.854;
	const int b = 5.18;
	const int c = 8.13;
	const int d = 2.58;
	/// \f$\tilde{V}_{L}=a+\frac{b}{1+\exp[(\Delta P-c)/d]}\f$
	double NormalV = a + b / (1.0 + std::exp(-(tp_pressure_average_ROI -c) / d));
	/// \f$ R_{AW,e} = R_{AW,0} * \tilde{V}_{L}^{1/3} \f$
	return radiusRest * std::pow(NormalV,1/3.0);
}
