/*
 * ParenchymalTethering.h
 *
 *  Created on: Jun 25, 2015
 *      Author: fuji
 */

#ifndef PARENCHYMALTETHERING_H_
#define PARENCHYMALTETHERING_H_

#include <iostream>
#include <vector>
#include "DataBase.h"
#include "PleuralPressureWaveForm.h"
#include "AirwayTreePartitioning.h"
#include "AirwayDataBase.h"
#include "AcinusDataBase.h"
#include "petsc.h"

/// There may be more than one Acinus wrapping an Airway.
/// This class computes an effective shear modulus by the volume-weighted averaging of alveolar pressure within the region of interest (ROI).
/// The detail is shown in Jason Ryans, Hideki Fujioka, and Donald Gaver, "Micro-scale to Meso-scale Analysis of Parenchymal Tethering Mechanics",
/// \it{Journal of Applied Physiology} (2018).
/// The averaged pressure in ROI is computed by the volume-weighted averaging of Acini pressures within the region of interest (ROI) as
/// \f$
///  \overline{P_{ROI}}_{i}=\sum_{j}N_{ij}P_{AC_{j}}
/// \f$
///  where \f$ P_{AC_{j}} \f$ is the representative pressure of acinus \f$ j \f$, \f$ N_{ij}=\left(V_{AC_{j}}\bigcap V_{ROI_{i}}\right)/V_{ROI_{i}} \f$.
///  Total is \f$ \sum_{j}N_{ij}=1 \f$.
///
/// Since Airway and Acinus objects are distributed over processes, this class uses PetSc to compute the averaged pressure in ROI,
///	\f$
/// \left[\begin{array}{c}
///	\overline{P_{ROI}}_{0}\\
///	\overline{P_{ROI}}_{1}\\
///	\vdots
///	\end{array}\right]_{Airway}=\left[\begin{array}{ccc}
///	N_{00} & N_{01} & \cdots\\
///	N_{10} & \ddots\\
///	\vdots
///	\end{array}\right]\left[\begin{array}{c}
///	P_{AC_{0}}\\
///	P_{AC_{1}}\\
///	P_{AC_{2}}
///	\end{array}\right]_{Acinus}
///	\f$
///
/// This class also computes the parenchymal hole radius at uniform stress state, \f$ R_{AW,u}\f$ for each Airway.
/// The cross-sectional area of parenchymal hole is assumed in proportion to the 2/3 powers of Acinus volume, that is:
/// \f$ \left(\frac{R_{AW,u}}{R_{AW,0}}\right)^2 = \left(\frac{V_{L}}{V_{L0}}\right)^{2/3}\f$
/// where \f$R_{AW,0} \f$ and \f$V_{L0}\f$ are the radius and volume at rest.
/// Since Airway is surrounded by several Acinus, \f$ \left(\frac{R_{AW,u}}{R_{AW,0}}\right)^2 \f$ is computed by area-weighted averaging
/// \f$ \left(\frac{V_{L}}{V_{L0}}\right)^{2/3}\f$.
class ParenchymalTethering: public DataBase {
public:
	/// Constructor takes 'AirwayTreePartitioning' object, lists of Airway objects and Acinus objects.
	ParenchymalTethering(AirwayTreePartitioning *graph, std::vector<Airway *> airwayList, std::vector<Acinus *> acinusList);
	/// Destructor
	virtual ~ParenchymalTethering();

	/// This method calls ParenchymalTethering::updateParechymalShearModulus and ParenchymalTethering::updateParechymalHoleRadius
	/// This must be called collectively at each time step.
	void updateParenchymalState(void);

	/// Export Data to Parallel HDF5 data space, must call collectively
	virtual bool exportPHDF5dataspace(std::string pdir, hid_t &file);
private:
	/// Setup PetSc Mat and Vec
	void setupPetSc();


	/// This method computes the averaged pressure with ROI for each airway, and determines the effective shear modulus, \f$ G_{eff,i} \f$
	/// and the radius of parenchyma hole at an equilibrium stress state, \f$ R_{AW,e,i} \f$.
	void updateParenchymaStateOfAirways(void);

	/// Fitted function of the Effective Shear Modulus, \f$ G_{eff,i} \f$ as a function of the averaged trans-pulmonary pressure within ROI.
	double effectiveShearModulusROI(double tp_pressure_average_ROI);

	/// Empirical curve of the parenchymal hole radius in equilibrium state, \f$ R_{AW,e,i} \f$ as a function of the averaged trans-pulmonary pressure within ROI.
	double equilibriumParenchymalHoleRadius(double tp_pressure_average_ROI, double radiusRest);

	Mat matNij; //< Matrix of Volume ratio in ROI
	Vec vecPressureInAcini; //< Vector of Pressure in Acini
	Vec vecPressureInROI; //< Vector of Pressure in ROI of airways

	/// Object of AirwayTreePartitioning
	AirwayTreePartitioning *graph;
	/// List of Airway objects
	std::vector<Airway *> airwayList;
	/// List of Acinus objects
	std::vector<Acinus *> acinusList;

	/// process id
	int myid;
	/// number of processes
	int numproc;
};

#endif /* PARENCHYMALTETHERING_H_ */
