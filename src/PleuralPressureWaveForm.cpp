/*
 * PleuralPressureWaveForm.cpp
 *
 *  Created on: Oct 29, 2014
 *  Modified on: June 29, 2016
 *      Author: fuji
 */

#include "PleuralPressureWaveForm.h"

PleuralPressureWaveForm::PleuralPressureWaveForm() {
	// TODO Auto-generated constructor stub
	time = 0.0;
	pressure = Sinusoidal_Pleural_Pressure(0.0);
}

PleuralPressureWaveForm::~PleuralPressureWaveForm() {
	// TODO Auto-generated destructor stub
}


void PleuralPressureWaveForm::setTime(double time) {
	this->pressure = Sinusoidal_Pleural_Pressure(time);
	this->time = time;
}

double PleuralPressureWaveForm::Sinusoidal_Pleural_Pressure(double time) {
	const double Bpm = 12; /// Breaths par Minute
	const double pplFRC = 0; //-2;/// Pleural Pressure at FRC
	const double pplAmp = 0;//8;///  Amplitude
	const double pt = 60.0 / Bpm;
	/// Waveform \f$P_{pl}=pplFRC - \frac{1}{2}pplAmp (1 - \cos(2\pi \cdot time / pt))\f$
	double ppl = pplFRC - pplAmp * 0.5 * (1 - std::cos(2.0 * M_PI * time / pt));

	return ppl;
}
