/*
 * PleuralPressureWaveForm.h
 *
 *  Created on: Oct 29, 2014
 *      Author: fuji
 */

#ifndef PLEURALPRESSUREWAVEFORM_H_
#define PLEURALPRESSUREWAVEFORM_H_

#include <iostream>
#include <cmath>

/// This class define pleural pressure wave form, 
/// ppl = pplFRC - pplAmp * 0.5 * (1 - cos(2\f$\pi\f$ * time / pt - \f$\pi\f$)) \n
/// Bpm = 12 	Breaths par Minute\n
/// pplFRC = -2 (cmH2O) 	Pleural Pressure at FRC\n
/// pplAmp = 8 (cmH2O) 	Amplitude\n
/// pt = 60.0 / Bpm
class PleuralPressureWaveForm {
public:
	/// Constructor set time=0
	PleuralPressureWaveForm();

	/// Destructor does nothing
	virtual ~PleuralPressureWaveForm();

	/// Set time
	void setTime(double time);

	/// Get time
	double getTime() const {
		return time;
	}

	/// Get the pleural pressure
	double getPressure() const {
		return pressure;
	}

private:
	double pressure;
	double time;

	double Sinusoidal_Pleural_Pressure(double time);
};

#endif /* PLEURALPRESSUREWAVEFORM_H_ */
