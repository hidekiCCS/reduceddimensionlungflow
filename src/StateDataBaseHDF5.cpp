/*
 * StateDataBaseHDF5.cpp
 *
 *  Created on: Feb 9, 2015
 *      Author: fuji
 */

#include "StateDataBaseHDF5.h"
#include <fstream>
#include <iomanip>
#include <sstream>
#include <ctime>
#include <unistd.h>

StateDataBaseHDF5::StateDataBaseHDF5(std::string baseFileName)
:baseFileName(baseFileName),exportCount(0),exportTime(0.0),myid(0),numproc(1) {
#ifndef NOMPI
    MPI_Comm_size(MPI_COMM_WORLD, &numproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
#endif
}

StateDataBaseHDF5::~StateDataBaseHDF5() {
}

void StateDataBaseHDF5::addDataBase(DataBase* db) {
	/// Add a database object to the list
	listDB.push_back(db);
}

void StateDataBaseHDF5::exportPHDF5(double time) {
	exportTime = time;

	/// - create file
    std::ostringstream fileNameHDF5;
    fileNameHDF5 << baseFileName << std::setfill('0') << std::setw(numberOfDigits) << exportCount << ".h5";
    if (myid == 0){
		hid_t file = H5Fcreate(fileNameHDF5.str().c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
		H5Fclose(file);
    }

	hid_t acc_tpl1 = H5P_DEFAULT;
#ifndef NOMPI
	MPI_Barrier(MPI_COMM_WORLD);
	/// - setup file access template with parallel IO access.
	acc_tpl1 = H5Pcreate (H5P_FILE_ACCESS);
	/// - set Parallel access with communicator
	MPI_Comm comm = MPI_COMM_WORLD;
	MPI_Info info = MPI_INFO_NULL;
	herr_t ret = H5Pset_fapl_mpio(acc_tpl1, comm, info);
#endif
	/// - Open the file collectively with read-write mode.
	fileHDF5LungDB = H5Fopen(fileNameHDF5.str().c_str(), H5F_ACC_RDWR, acc_tpl1);
	if (fileHDF5LungDB < 0)  throw "cannot create/open HDF file.";

	/// - Release parallel IO access template
#ifndef NOMPI
	H5Pclose(acc_tpl1);
#endif

    try{
    	for (std::vector<DataBase *>::iterator it = listDB.begin(); it != listDB.end() ; it++){
    		/// 	+ Call "exportPHDF5dataspace" method for each
    		(*it)->exportPHDF5dataspace("/", fileHDF5LungDB);
    	}
    	this->exportPHDF5timeCounter("/", fileHDF5LungDB);
    	this->exportPHDF5time("/", fileHDF5LungDB);
    }
    catch(char *str){
    	std::cout << "ERROR : " << str << std::endl;
    	throw "Database write failed";
    }

	exportCount++;
	H5Fclose(fileHDF5LungDB);
}

double StateDataBaseHDF5::importPHDF5(int count) {
	/// - create file
    std::ostringstream fileNameHDF5;
    fileNameHDF5 << baseFileName << std::setfill('0') << std::setw(numberOfDigits) << count << ".h5";
    exportCount = count;
    //std::cout << "READ " << fileNameHDF5.str() << std::endl;

    /* open the file individually */
	fileHDF5LungDB = H5Fopen(fileNameHDF5.str().c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	if (fileHDF5LungDB < 0) {
		std::cout << "File not found, " << fileNameHDF5.str() << std::endl;
		return 0.0;
	}
	double restartTime = 0.0;
    try{
    	for (std::vector<DataBase *>::iterator it = listDB.begin(); it != listDB.end() ; it++){
    		/// 	+ Call "importHDF5dataspace" method for each
    		(*it)->importHDF5dataspace("/", fileHDF5LungDB);
    	}
    	exportCount = this->importHDF5timeCounter("/", fileHDF5LungDB);
    	restartTime  = this->importHDF5time("/", fileHDF5LungDB);
    }
    catch(char *str){
    	std::cout << "ERROR : " << str << std::endl;
    	throw "Database write failed";
    }
    H5Fclose(fileHDF5LungDB);
	//if (myid == 0){
	//	std::cout << "Restart from " << restartTime << std::endl;
	//}
    return restartTime;
}

bool StateDataBaseHDF5::exportPHDF5timeCounter(std::string pdir, hid_t& file) {
	/// - Set Dataspace Name "./TimeCounter"
	std::string dataSpace = pdir + "/TimeCounter";
	int localNum = 1;
	int offset = 0;
#ifndef NOMPI
	/// - Only rank=0 stores data.
	if (myid == 0){
		localNum = 1;
	}else{
		localNum = 0;
	}
#endif
	DataBase db;
	db.exportPHDF5dataspaceInteger1d(dataSpace, file, 1,  localNum,  offset, &exportCount);

	return true;
}


bool StateDataBaseHDF5::exportPHDF5time(std::string pdir, hid_t& file) {
	/// - Set Dataspace Name "./Time"
	std::string dataSpace = pdir + "/Time";
	int localNum = 1;
	int offset = 0;
#ifndef NOMPI
	/// - Only rank=0 stores data.
	if (myid == 0){
		localNum = 1;
	}else{
		localNum = 0;
	}
#endif
	DataBase db;
	db.exportPHDF5dataspaceDouble1d(dataSpace, file, 1,  localNum,  offset, &exportTime);

	return true;
}

/// Import time counter  to database
int StateDataBaseHDF5::importHDF5timeCounter(std::string pdir, hid_t& file){
	hid_t dataset = H5Dopen(file, "TimeCounter", H5P_DEFAULT);
	hid_t dataspace = H5Dget_space(dataset);
	int count;
	H5Dread(dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &count);
	H5Dclose(dataset);
	return count;
}
/// Import time  to database
double StateDataBaseHDF5::importHDF5time(std::string pdir, hid_t& file){
	hid_t dataset = H5Dopen(file, "Time", H5P_DEFAULT);
	hid_t dataspace = H5Dget_space(dataset);
	double ti;
	H5Dread(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &ti);
	H5Dclose(dataset);
	return ti;
}
