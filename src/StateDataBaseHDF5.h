/*
 * StateDataBaseHDF5.h
 *
 *  Created on: Feb 9, 2015
 *      Author: fuji
 */

#ifndef STATEDATABASEHDF5_H_
#define STATEDATABASEHDF5_H_

#include <iostream>
#include <vector>
#include "DataBase.h"
#include "hdf5.h"

/// This class provides methods to export/import status data in/from HDF5 files.
class StateDataBaseHDF5 {
public:
	/// Constructor takes base file name
	StateDataBaseHDF5(std::string baseFileName);
	/// Destructor does nothing
	virtual ~StateDataBaseHDF5();

	/// Add database
	void addDataBase(DataBase *db);

	/// Export state variables to database
	void exportPHDF5(double time);

	/// Import state variables from database.
	double importPHDF5(int count);

	/// Get base-file name
	const std::string& getBaseFileName() const {
		return baseFileName;
	}

	int getExportCount() const {
		return exportCount;
	}

	void setExportCount(int exportCount) {
		this->exportCount = exportCount;
	}

	double getExportTimeCount() const {
		return exportTime;
	}

private:
	/// Export time counter  to database
	bool exportPHDF5timeCounter(std::string pdir, hid_t& file);
	/// Export time  to database
	bool exportPHDF5time(std::string pdir, hid_t& file);

	/// Import time counter  to database
	int importHDF5timeCounter(std::string pdir, hid_t& file);
	/// Import time  to database
	double importHDF5time(std::string pdir, hid_t& file);

	std::string baseFileName;///< Base File Name
	std::vector<DataBase *> listDB;///< List of Database

	int exportCount;///< counter for export
	double exportTime;///< last export time
	hid_t fileHDF5LungDB;///< File Handler

	int myid;///< process id
	int numproc;///< number of processes

	static constexpr int numberOfDigits=6;///< Number of digits in dataset name.
};

#endif /* STATEDATABASEHDF5_H_ */
