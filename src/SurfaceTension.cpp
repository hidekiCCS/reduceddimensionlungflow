/*
 * SurfaceTension.cpp
 *
 *  Created on: Jun 28, 2016
 *      Author: fuji
 */

#include "SurfaceTension.h"

SurfaceTension::SurfaceTension() :surfaceTension(25.0){
	/// Nothing to do
}

SurfaceTension::SurfaceTension(double surfaceTension):surfaceTension(surfaceTension) {
	/// Nothing to do
}

SurfaceTension::~SurfaceTension() {
	/// Nothing to do
}

double SurfaceTension::getSurfaceTension() const {
	return surfaceTension;
}

void SurfaceTension::computeConcentration(double dt) {
	/// Nothing to do
}

void SurfaceTension::incrementTime() {
	/// Nothing to do
}

double SurfaceTension::getBulkConcentration() const {
	/// Nothing to do
}
void SurfaceTension::setBulkConcentration(double bc) {
	/// Nothing to do
}
double SurfaceTension::getGammaP() const {
	/// Nothing to do
}

double SurfaceTension::getGammaS() const {
	/// Nothing to do
}
void SurfaceTension::setGammaP(double gammaP) {
	/// Nothing to do
}

void SurfaceTension::setGammaS(double gammaS) {
	/// Nothing to do
}

double SurfaceTension::getLiquidVolume() const {
	/// Nothing to do
}

void SurfaceTension::setLiquidVolume(double liquidVolume) {
	/// Nothing to do
}

double SurfaceTension::getSurfaceArea() const {
	/// Nothing to do
}

void SurfaceTension::setSurfaceArea(double surfaceArea)  {
	/// Nothing to do
}

double SurfaceTension::getTotalConcentration() const {
	/// Nothing to do
}

void SurfaceTension::setTotalConcentration(double totalConcentration) {
	/// Nothing to do
}
