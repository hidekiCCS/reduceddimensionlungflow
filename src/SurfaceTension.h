/*
 * SurfaceTension.h
 *
 *  Created on: Jun 28, 2016
 *      Author: fuji
 */

#ifndef SURFACETENSION_H_
#define SURFACETENSION_H_
#include <iostream>

/// This is a dummy class providing a constant surface tension.
class SurfaceTension {
public:
	SurfaceTension();
	SurfaceTension(double surfaceTension);
	virtual ~SurfaceTension();

	/// get surface tension
	double getSurfaceTension() const;

	/// Force to set the surface tension
	void setSurfaceTension(double surfaceTension) {
		this->surfaceTension = surfaceTension;
	}

	/// dummy virtual functions
	virtual void computeConcentration(double dt);
	virtual void incrementTime();
	virtual double getBulkConcentration() const;
	virtual void setBulkConcentration(double bulkC);
	virtual double getGammaP() const;
	virtual double getGammaS() const;
	virtual void setGammaP(double gammaP);
	virtual void setGammaS(double gammaS);
	virtual double getLiquidVolume() const;
	virtual void setLiquidVolume(double liquidVolume);
	virtual double getSurfaceArea() const;
	virtual void setSurfaceArea(double surfaceArea) ;
	virtual double getTotalConcentration() const;
	virtual void setTotalConcentration(double totalConcentration);

protected:
	double surfaceTension; ///< Surface Tension, \f$\gamma (dyens/cm^2)\f$
};

#endif /* SURFACETENSION_H_ */
