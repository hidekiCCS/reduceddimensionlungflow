/*
 * Surfactant.cpp
 *
 *  Created on: Jun 27, 2016
 *      Author: fuji
 */

#include "Surfactant.h"
#include <cmath>

#define SURFINACTANT
//#undef SURFINACTANT
#ifdef SURFINACTANT
#define CONSTANT_SURFACE_TENSION 0.0
#endif

Surfactant::Surfactant()
:totalConcentration(5e-3), liquidVolume(1.288461538462e-7), surfaceArea(3.80576847e-4) {
	initialize(surfaceArea);
	/// - Default value of Total Concentration, \f$C_0=M_T/V_L=5\times10^{-3}(g/cm^3)\f$.
	/// - Default value of Liquid Volume, \f$V_L=1.288461538462\times10^{-7}(cm^3)\f$.
	/// - Initial Surface Area, \f$A=3.80576847\times10^{-4}\f$.
}

Surfactant::Surfactant(double totalConcentration, double liquidVolume,double surfaceArea)
:totalConcentration(totalConcentration),liquidVolume(liquidVolume),surfaceArea(surfaceArea)
{
	initialize(surfaceArea);
}

void Surfactant::initialize(const double sArea){
	surfaceArea = sArea;
	/// - Compute the equilibrium concentration
	/// \f$\Gamma_{eq}=\frac{1}{2}\left[\frac{C_{0}V_{L}}{A}+\frac{V_{L}k_{d}}{Ak_{a}}+\Gamma_{\infty}-\sqrt{\left(\frac{C_{0}V_{L}}{A}+\frac{V_{L}k_{d}}{Ak_{a}}+\Gamma_{\infty}\right)^{2}-4\frac{C_{0}V_{L}}{A}\Gamma_{\infty}}\right]\f$
	const double b = totalConcentration * liquidVolume / surfaceArea + surfaceArea * rateDesorpP / rateAbsorpP + gammaPInfinity;
	const double c = gammaPInfinity * totalConcentration * liquidVolume / surfaceArea;
	if ((b * b - 4 * c) < 0){
		std::cout << "Something wrong in Initial amount of surfactant\n";
		std::exit(-1);
	}
	/// * If \f$\Gamma_{eq}<\Gamma_{\infty}\f$,
	///set \f$\Gamma_{1}=\Gamma_{eq}\f$, \f$\Gamma_{2}=0\f$, and \f$C=C_{0}-\frac{A}{V_{L}}\Gamma_{1}\f$.

	/// * If \f$\Gamma_{eq}\geq\Gamma_{\infty}\f$, set \f$\Gamma_{1}=\Gamma_{\infty}\f$, \f$\Gamma_{2}=\Gamma_{eq}-\Gamma_{\infty}\f$,
	///and \f$C=C_{0}-\frac{A}{V_{L}}\left(\Gamma_{1}+\Gamma_{2}\right)\f$.
	gammaP = 0.5 * (b - std::sqrt(b * b - 4 * c));
	if (gammaP > gammaPInfinity){
		gammaS = gammaP - gammaPInfinity;
	}else{
		gammaS = 0.0;
	}
	bulkConcentration = totalConcentration - surfaceArea * (gammaP + gammaS) / liquidVolume;

	gammaP0 = gammaP;
	gammaS0 = gammaS;
	bulkConcentration0 = bulkConcentration;
	surfaceArea0 = surfaceArea;
	surfaceTension = computeSurfaceTension();

	std::cout << "Total Concentration=" << totalConcentration << std::endl;
	std::cout << "Liquid Volume=" << liquidVolume << std::endl;
	std::cout << "Surface Area=" << surfaceArea << std::endl;
	std::cout << "gammaP0=" << gammaP0 << std::endl;
	std::cout << "gammaS0=" << gammaS0 << std::endl;
	std::cout << "bulkConcentration0=" << bulkConcentration0 << std::endl;
	std::cout << "surfaceArea0=" << surfaceArea0 << std::endl;
	std::cout << "surfaceTension=" << surfaceTension << std::endl;
}

Surfactant::~Surfactant() {
	// TODO Auto-generated destructor stub
}

void Surfactant::computeConcentration(double dt) {
	/// - Update \f$\Gamma_1\f$, \f$\Gamma_2\f$ and \f$C\f$ within a time step dt
	/// 	* Divide dt into Surfactant::timeDivision
	const double ddt = dt / timeDivision;
	double saT = surfaceArea0;
	double gammaPT = gammaP0;
	double gammaST = gammaS0;
	/// - Loop for Surfactant::timeDivision times
	for (int i = 0 ; i < timeDivision ; i++){
		/// - Compute Rate of Area change
		/// 	* \f$\frac{dA}{dt}\simeq (A-A_0)/\Delta t\f$
		double sa = surfaceArea0 + (surfaceArea - surfaceArea0) * (i + 1) / timeDivision;
		const double dAdt = (sa - saT) / ddt;
		//reSpreadingFlag = (dAdt > 0.0) & (reSpreadingFlag | (gammaPT < gammaPmls));

		/// - Compute Fluxes * Area
		double jA_s1 = (gammaPT > gammaST * 0.5) ? (-rateDesorpP * (gammaPT - gammaST * 0.5)) : 0.0;
		jA_s1 += (gammaPT < gammaPInfinity) ? rateAbsorpP * bulkConcentration * (gammaPInfinity - gammaPT) : 0.0;
		jA_s1 *= saT;
		const double jA_s2 = -rateDesorpS * gammaST * saT;
		const double jA_c1 = (gammaPT > gammaPmax) ? (-dAdt * gammaPT) : 0.0;
		const double jA_c2 = (gammaST > gammaSmax) ? (-dAdt * gammaST) : 0.0;
		const double jA_r2 = (gammaPT < gammaPmls) ? (std::max(dAdt,0.0) * gammaST) : 0.0;

		/// - Update Concentrations
		/// 	* \f$\frac{A\Gamma_{1}-A_0\Gamma_{1,0}}{\Delta t}=A_0\left(j_{1}^{s}-j_{1}^{c}+j_{2}^{r}\right)\f$
		///		* \f$\frac{A\Gamma_{2}-A_0\Gamma_{2,0}}{\Delta t}=A_0\left(j_{2}^{s}+j_{1}^{c}-j_{2}^{c}-j_{2}^{r}\right)\f$
		gammaP = (gammaPT * saT + (jA_s1 - jA_c1 + jA_r2) * ddt ) / sa;
		gammaS = (gammaST * saT + (jA_s2 + jA_c1 - jA_r2 - jA_c2) * ddt ) / sa;
		saT= sa;
		gammaPT = gammaP;
		gammaST = gammaS;
		bulkConcentration = totalConcentration - surfaceArea * (gammaP + gammaS) / liquidVolume;
	}
}

void Surfactant::incrementTime() {
	gammaP0 = gammaP;
	gammaS0 = gammaS;
	bulkConcentration0 = bulkConcentration;
	surfaceArea0 = surfaceArea;
	surfaceTension = computeSurfaceTension();
#ifdef SURFINACTANT
	surfaceTension = CONSTANT_SURFACE_TENSION;
#endif
}

double Surfactant::computeSurfaceTension() {
	return waterSurfaceTension - surfaceElasticity * gammaP;
}
