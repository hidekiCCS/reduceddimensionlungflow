/*
 * Surfactant.h
 *
 *  Created on: Jun 27, 2016
 *      Author: fuji
 */

#ifndef SURFACTANT_H_
#define SURFACTANT_H_
#include <iostream>
#include "SurfaceTension.h"

/**
# Model of Surfactant Transport #
The interfacial surfactant concentrations satisfy the following transport equation:

\f$\frac{d}{dt}\left(A\Gamma_{1}\right)=A\left(j_{1}^{s}-j_{1}^{c}+j_{2}^{r}\right)\f$

\f$\frac{d}{dt}\left(A\Gamma_{2}\right)=A\left(j_{2}^{s}+j_{1}^{c}-j_{2}^{c}-j_{2}^{r}\right)\f$

where \f$\Gamma_{1}\f$ and \f$\Gamma_{2}\f$ are the concentrations of surfactant
on primary and secondary layers present at the air-interface,
\f$A\f$ is the surface area of the interface exposed to the surfactant,
and the \f$j\f$ denote several fluxes:
The ones with a superscript ”s” represent sorption and desorption fluxes between the layers and the subsurface;
the ones with a ”c” represent fluxes during the collapse phase of either layer;
and ”r” represents the flux during the respreading phase of the secondary layer.

We use Langmuir kinetics to describe the sorptive and desorptive fluxes:

\f$j_{1}^{s}=\left\{ \begin{array}{rr}
k_{a}C\left(\Gamma_{\infty}-\Gamma_{1}\right)-k_{d}\left(\Gamma_{1}-\Gamma_{2}/2\right), & \textrm{for } \Gamma_{1}<\Gamma_{\infty} \textrm{ and } \Gamma_{1}>\Gamma_{2}/2\\
-k_{d}\left(\Gamma_{1}-\Gamma_{2}/2\right), & \textrm{for } \Gamma_{1}\geq\Gamma_{\infty} \textrm{ and } \Gamma_{1}>\Gamma_{2}/2\\
0, & \textrm{for } \Gamma_{1}\leq\Gamma_{2}/2
\end{array}\right.\f$

 and

\f$j_{2}^{s}=-k_{d2}\Gamma_{2}\f$

where \f$C\f$ is the bulk surfactant concentration.
Here \f$k_{a}\f$ and \f$k_{d}\f$ are adsorption and desorption coefficients.
The collapse and respreading fluxes are given by

\f$j_{1}^{c}=-\frac{1}{A}\frac{dA}{dt}\Gamma_{1},\qquad\Gamma_{1}\geq\Gamma_{max1}\f$

\f$j_{2}^{c}=-\frac{1}{A}\frac{dA}{dt}\Gamma_{2},\qquad\Gamma_{2}\geq\Gamma_{max2}\f$

\f$j_{2}^{r}=-\frac{1}{A}\frac{dA}{dt}\Gamma_{2},\qquad\Gamma_{1}\leq\Gamma_{mls}, \frac{dA}{dt}>0 \f$

 where \f$\Gamma_{max2}=2\Gamma_{max1}\f$.

 The bulk surfactant concentration can be expressed in terms of the interfacial concentration
 by using the mass conservation law

 \f$C=\frac{M_{T}}{V_{L}}-\frac{A}{V_{L}}\left(\Gamma_{1}+\Gamma_{2}\right)\f$

where \f$M_{T}\f$ is the total amount of surfactant, and \f$V_{L}\f$ is the liquid volume.
And \f$M_{T}=C_{0}V_{L}\f$, where \f$C_{0}\f$ is the total concentration.

For mass conservation, the bulk surfactant concentration is:

\f$C=C_{0}-\frac{A}{V_{L}}\left(\Gamma_{1}+\Gamma_{2}\right)\f$

## Initial Setup ##
A constructor takes the total surfactant concentration \f$C_0\f$,
the liquid volume \f$V_L\f$, and the surface area \f$A\f$ as initial condition.
The constructor computes \f$\Gamma_{1}\f$, \f$\Gamma_{2}\f$, and \f$C\f$.

In equilibrium, assuming \f$\Gamma_{2}=0\f$

\f$k_{a}C\left(\Gamma_{\infty}-\Gamma_{1}\right)-k_{d}\Gamma_{1}=0\f$,

so coupled with

\f$C=C_{0}-\frac{A}{V_{L}}\Gamma_{1}\f$

The solution is

\f$\Gamma_{eq}=\frac{1}{2}\left[\frac{C_{0}V_{L}}{A}+\frac{V_{L}k_{d}}{Ak_{a}}+\Gamma_{\infty}-\sqrt{\left(\frac{C_{0}V_{L}}{A}+\frac{V_{L}k_{d}}{Ak_{a}}+\Gamma_{\infty}\right)^{2}-4\frac{C_{0}V_{L}}{A}\Gamma_{\infty}}\right]\f$

If \f$\Gamma_{eq}<\Gamma_{\infty}\f$,
set \f$\Gamma_{1}=\Gamma_{eq}\f$, \f$\Gamma_{2}=0\f$, and \f$C=C_{0}-\frac{A}{V_{L}}\Gamma_{1}\f$.

If \f$\Gamma_{eq}\geq\Gamma_{\infty}\f$, set \f$\Gamma_{1}=\Gamma_{\infty}\f$, \f$\Gamma_{2}=\Gamma_{eq}-\Gamma_{\infty}\f$,
and \f$C=C_{0}-\frac{A}{V_{L}}\left(\Gamma_{1}+\Gamma_{2}\right)\f$.

## Surface tension Model ##
The surface tension \f$\gamma\f$ is assumed to be a linear function of the primary layer concentration, \f$\Gamma_1\f$.

\f$\gamma = \gamma_0 - E\Gamma_1\f$

where \f$\gamma_0\f$ is the surface tension of surfactant-free interface and \f$E\f$ is the surface elasticity number.
 **/
class Surfactant:public SurfaceTension {
public:
	Surfactant();
	Surfactant(double totalConcentration,double liquidVolume,double surfaceArea);
	virtual ~Surfactant();

	/// Reset Initial Condition
	void initialize(const double sArea);

	/// Compute Evolution of concentration
	void computeConcentration(double dt);

	/// Increment time step
	void incrementTime();

	double getBulkConcentration() const {
		return bulkConcentration;
	}
	void setBulkConcentration(double bulkC) {
		this->bulkConcentration = bulkC;
	}

	double getGammaP() const {
		return gammaP;
	}
	void setGammaP(double gammaP)  {
		this->gammaP = gammaP;
	}

	double getGammaS() const {
		return gammaS;
	}
	void setGammaS(double gammaS)  {
		this->gammaS = gammaS;
	}

	double getLiquidVolume() const {
		return liquidVolume;
	}

	void setLiquidVolume(double liquidVolume) {
		this->liquidVolume = liquidVolume;
	}

	double getSurfaceArea() const {
		return surfaceArea;
	}

	virtual void setSurfaceArea(double surfaceArea) {
		this->surfaceArea = surfaceArea;
	}

	double getTotalConcentration() const {
		return totalConcentration;
	}

	void setTotalConcentration(double totalConcentration) {
		this->totalConcentration = totalConcentration;
	}

private:
	double computeSurfaceTension();///< compute surface tension
	double totalConcentration;///< total Surfactant concentration, \f$C_0 (g/cm^3)\f$
	double liquidVolume; ///< Liquid Volume, \f$V_L (cm^3)\f$
	double gammaP;///< Primary layer surfactant concentration, \f$\Gamma_1 (g/cm^2)\f$
	double gammaS;///< Secondary layer surfactant concentration, \f$\Gamma_2 (g/cm^2)\f$
	double bulkConcentration;///< Bulk Surfactant concentration, \f$C (g/cm^3)\f$
	double surfaceArea;///< Surface area of air-liquid interface, \f$A (cm^2)\f$
	double gammaP0;///< Primary layer surfactant concentration at previous time
	double gammaS0;///< Secondary layer surfactant concentration at previous time
	double bulkConcentration0;///< Bulk Surfactant concentration at previous time
	double surfaceArea0;///< Surface area of air-liquid interface at previous time
	//bool reSpreadingFlag;///< Flag for Respreading during Inspiration

	static constexpr double waterSurfaceTension = 72;///< Surface Tension of Water, \f$\gamma_0 (dyens/cm^2)\f$
	static constexpr double gammaPInfinity = 3.1e-7;///< Maximum equilibrium primary layer surfactant concentration, \f$\Gamma_\infty\f$
	static constexpr double gammaPmax = gammaPInfinity * 1.1;///< Maximum dynamic primary layer surfactant concentration, \f$\Gamma_{max1}\f$
	static constexpr double gammaSmax = gammaPmax * 2;///< Maximum dynamic secondary layer surfactant concentration, \f$\Gamma_{max2}\f$
	static constexpr double gammaPmls = gammaPInfinity * 0.85;///< Re-spreading concentration, \f$\Gamma_{mls}\f$
	static constexpr double surfaceElasticity = 0.9 * waterSurfaceTension / gammaPInfinity;///< Surface Elasticity number, \f$E\f$
	static constexpr double rateAbsorpP = 800;///< Absorption rate \f$k_a (cm^{3}\cdot g^{-1}\cdot s^{-1})\f$
	static constexpr double rateDesorpP = 0.00025;///< Primary Layer Desorption rate \f$k_{d1} (s^{-1})\f$
	static constexpr double rateDesorpS = 0.05;///< Secondary Layer Desorption rate \f$k_{d2} (s^{-1})\f$
	static constexpr int timeDivision = 100;///< The number of time division for one time step.
};

#endif /* SURFACTANT_H_ */
