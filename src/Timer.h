/*
 * Timer.h
 *
 *  Created on: Apr 3, 2018
 *      Author: fuji
 */

#ifndef SRC_TIMER_H_
#define SRC_TIMER_H_

#include <iostream>
#include <ctime>
#include <sys/time.h>

class Timer {
public:
	void start(){
		struct timeval tm;
		gettimeofday(&tm, NULL);
		base_sec = tm.tv_sec;
		base_usec = tm.tv_usec;
		elapsedTime = 0.0;
	}
	void stop(){
		struct timeval tm;
		gettimeofday(&tm, NULL);
		elapsedTime = (double) (tm.tv_sec - base_sec) + ((double) (tm.tv_usec - base_usec)) / 1.0e6;
	}
	double getTime(){
		return elapsedTime;
	}
private:
	int base_sec;
	int base_usec;
	double elapsedTime;
};

#endif /* SRC_TIMER_H_ */
