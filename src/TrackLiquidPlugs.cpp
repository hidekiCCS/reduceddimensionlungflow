/*
 * TrackLiquidPlugs.cpp
 *
 *  Created on: Feb 10, 2015
 *      Author: fuji
 */

#include "petsc.h"
#include <mpi.h>
#include "TrackLiquidPlugs.h"

TrackLiquidPlugs::TrackLiquidPlugs(AirwayTreePartitioning *graph, std::vector<Bifurcation*> bifurcationList, std::vector<Airway*> airwayList) : graph(graph),bifurcationList(bifurcationList),airwayList(airwayList){
    MPI_Comm_size(MPI_COMM_WORLD, &numproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
}

TrackLiquidPlugs::~TrackLiquidPlugs() {
	// TODO Auto-generated destructor stub
}

void TrackLiquidPlugs::updateLiquidPlugs(double dt) {
	std::vector<LiquidPlug *> listOfPlugsAtBifurcation;
	std::vector<AirwayWithPlugPropagation *> listOfAirwaysPlugsAtBifurcation;
	/*! - Update plug position by calling LiquidPlug::propagationOfPlug for all plugs
	 	 	 -# LiquidPlug::propagationOfPlug method returns the liquid volume deposited,\f$V_D\f$ within a time step \f$dt\f$.
		     -# \f$V_D\f$ may be negative if the plug gaining mass.
		     -# Adding \f$V_D\f$ to the volume in the airway. Set using Airway::setLiquidVolume()
			 -# If the plug length becomes zero, make it rupture by calling TrackLiquidPlugs::rupturePlug .
			 -# If a plug reaches the end of terminal airway, liquid get into acinus.
			 	 - This version of code does't consider liquid in acinus.
			 -#	If the plug is in a treachea and reaches the end, make it rupture by calling TrackLiquidPlugs::rupturePlug .
	 	     -# If the plug reaches bifurcation point, add to \a listOfPlugsAtBifurcation.
	 */
	for (int i = 0 ; i < airwayList.size() ; i++){
		AirwayWithPlugPropagation *aw = dynamic_cast<AirwayWithPlugPropagation *>(airwayList[i]);
		int numOfPlugs = aw->getNumberOfLiquidPlugs();
		if (numOfPlugs > 0){
			std::vector<LiquidPlug *> lpList;
			lpList.clear();
			for (int j = 0 ; j < numOfPlugs ; j++){
				LiquidPlug *lp = aw->getLiquidPlug(j);
				lpList.push_back(lp);
#ifdef DEBUG
				std::cout << "U=" << aw->getFlowRate() / (M_PI * aw->getRadius() * aw->getRadius())
						<< " PU=" << lp->getPlugVelocity() << std::endl;
#endif
			}
			std::vector<LiquidPlug *>::iterator it = lpList.begin();
			while(it != lpList.end()){
				if ((*it)->getPlugVelocity() *dt / aw->getLength() > 0.1){
					std::cout << "Velocity of Plug in Airway " << aw->getIndex() << " is too large " <<  (*it)->getPlugVelocity() << std::endl;
				}
				double volumeDeposited = (*it)->propagationOfPlug(dt);
				double liquidVolume = aw->getLiquidVolume() + volumeDeposited;
				if (liquidVolume <= 0.0){
					std::cout << "Airway " << aw->getIndex()
							  << " Liquid Film thickness becomes zero: Volume Deposited=" << volumeDeposited << std::endl;
				}
				aw->setLiquidVolume(liquidVolume);
				//aw->computeEpsilon();
				//(*it)->setPrecursorFilmThickness(aw->getEps() * aw->getRadius());

				double po = (*it)->getPosition();
				//std::cout << "po = " << po << " tLength=" << (*it)->getTubeLength() << " tRadius=" << (*it)->getTubeRadius()<< std::endl;
				bool flag = (po >= 0.0) && (po <= 1.0);
				if (flag == false){
					if (((aw->getLowerBifurcationIndex() == -1) && (po > 1.0))  ||
						((aw->getUpperBifurcationIndex() == -1) && (po < 0.0))){
						//std::cout << "po = " << po << std::endl;
						if (aw->getLowerBifurcationIndex() == -1){ // if it is terminal airway
							// When a plug reaches the end of terminal airway, liquid get into acinus.
							// This version of code does't consider liquid in acinus.
							liquidIntoAcinus((*it),aw);
						}else{
							rupturePlug((*it),aw);
						}
						delete (*it);
						it = lpList.erase(it);
					}else{
						//std::cout << "po = " << po << " tLength=" << (*it)->getTubeLength() << " tRadius=" << (*it)->getTubeRadius()<< std::endl;
						listOfPlugsAtBifurcation.push_back((*it));
						listOfAirwaysPlugsAtBifurcation.push_back(aw);
						it++;
					}
				}else{
					if ((*it)->getPlugLength() <= 0.0){
						rupturePlug((*it),aw);
						delete (*it);
						it = lpList.erase(it);
					}else{
						it++;
					}
				}
			}
			//
		}
	}
	///	- Sum up the number of plugs reaching bifurcation (MPI Collective Communication)
	int n = listOfPlugsAtBifurcation.size();
	int total_plungs_inBif = 0;
	MPI_Allreduce(&n, &total_plungs_inBif, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	//if (total_plungs_inBif > 0) std::cout << "Plug in Bifurcation : " << total_plungs_inBif << std::endl;

	/// - If any plug reaches bifurcation, call TrackLiquidPlugs::splitPlugsAtBifurcation
	if (total_plungs_inBif > 0) splitPlugsAtBifurcation(listOfPlugsAtBifurcation,listOfAirwaysPlugsAtBifurcation);
}

void TrackLiquidPlugs::splitPlugsAtBifurcation(std::vector<LiquidPlug *> listOfPlugsAtBifurcation,std::vector<AirwayWithPlugPropagation *> listOfAirwaysPlugsAtBifurcation){
	/// ## Splitting liquid bolus at Bifurcation
	/// - At this point we have a list of LiquidPlug objects that reached bifurcating section.
	std::vector<int> indexOfBifurcationPlugGetIn;
	std::vector<double> volumeOfPlug;
	std::vector<LiquidPlug *>::iterator it = listOfPlugsAtBifurcation.begin();
	std::vector<AirwayWithPlugPropagation *>::iterator ita = listOfAirwaysPlugsAtBifurcation.begin();
	while(it != listOfPlugsAtBifurcation.end()){
		/// - Loop over those plugs at bifurcating section.
		///		+ Get Airway object that the plug was
		AirwayWithPlugPropagation *aw = (*ita);
		/// -
		/// 	+ Find the global index of Bifurcation object
		///			* If the plug position > 1, take lower Bifurcation Index, otherwise take upper Bifurcation Index.
		double pos = (*it)->getPosition();
		int bifurcationIndex;
		if (pos > 1.0){
			bifurcationIndex = aw->getLowerBifurcationIndex();
		}else{
			bifurcationIndex = aw->getUpperBifurcationIndex();
		}
		/// -
		///		+ Get the liquid volume of the plug
		double liquidVolume = (*it)->getLiquidVolume();
		/// -
		/// 	+ Remove plug from the lists and calling AirwayWithPlugPropagation::removeLiquidPlug
		aw->removeLiquidPlug(*it);
		/// -
		///		+ Destroy the LiquidPlug object.
		delete *it;
		it = listOfPlugsAtBifurcation.erase(it);
		ita = listOfAirwaysPlugsAtBifurcation.erase(ita);
		/// -
		///		+ Store the index of bifurcation and the liquid bolus volume to lists
		indexOfBifurcationPlugGetIn.push_back(bifurcationIndex);
		volumeOfPlug.push_back(liquidVolume);
	}

	/// ### Computing the splitting ratio of a liquid bolus at bifurcating section.
	/// <img src="https://docs.google.com/drawings/d/e/2PACX-1vRqVkSEO7zCmo95NWrfxBj07ihezBPDylYO6KzDYrCemj53OhgCuwRA6Gb2Yvdg5rb2d3RlXhLLqIgs/pub?w=367&h=281" width=400>
	/// - We define \f$\mathbf{\Phi}\f$ of \f$n_a \times n_b\f$ matrix, where \f$n_a\f$ and \f$n_b\f$ are the number of Airway and Bifurcation objects.
	///		+ Element \f$\Phi(i,j)\f$ is the positive flow-rate,\f$\left\langle Q_{i}\right\rangle =\max(Q_i,0)\f$ in Airway i coming out of Bifurcation j.
	///			* If the flow in Airway i directs towards Bifurcation j,  the element \f$\Phi(i,j)\f$ is zero.
	/// <img src="https://docs.google.com/drawings/d/e/2PACX-1vSnrhZGCao5Df0ivcjsINggjjc6wlUHKrFxns49mjlE11O_GEnoVHV2e-T3cLnrg-sB3dn7FgLOIYrO/pub?w=566&h=495" width=400>
	/// - For example of tree bifurcation model above, we can define the matrix \f$\mathbf{\Phi}\f$ as
	///
	/// \f$\mathbf{\Phi}=\left[\begin{array}{ccc}
	/// \left\langle -Q_{0}\right\rangle  & 0 & 0\\
	/// \left\langle Q_{1}\right\rangle  & \left\langle -Q_{1}\right\rangle  & 0\\
	/// \left\langle Q_{2}\right\rangle  & 0 & \left\langle -Q_{2}\right\rangle \\
	/// 0 & \left\langle Q_{3}\right\rangle  & 0\\
	/// 0 & \left\langle Q_{4}\right\rangle  & 0\\
	/// 0 & 0 & \left\langle Q_{5}\right\rangle \\
	/// 0 & 0 & \left\langle Q_{6}\right\rangle
	/// \end{array}\right]\f$
	///
	/// With the column \f$l^1\f$ norm which is defined as \f$\phi_{j}=\sum_{i}\Phi_{ij}\f$, the normalized \f$\mathbf{\Phi}\f$ is
	////
	/// \f$\mathbf{\tilde{\Phi}}=\left[\begin{array}{ccc}
	/// \left\langle -Q_{0}\right\rangle /\phi_{0} & 0 & 0\\
	/// \left\langle Q_{1}\right\rangle /\phi_{0} & \left\langle -Q_{1}\right\rangle /\phi_{1} & 0\\
	/// \left\langle Q_{2}\right\rangle /\phi_{0} & 0 & \left\langle -Q_{2}\right\rangle /\phi_{2}\\
	/// 0 & \left\langle Q_{3}\right\rangle /\phi_{1} & 0\\
	/// 0 & \left\langle Q_{4}\right\rangle /\phi_{1} & 0\\
	/// 0 & 0 & \left\langle Q_{5}\right\rangle /\phi_{2}\\
	/// 0 & 0 & \left\langle Q_{6}\right\rangle /\phi_{2}
	/// \end{array}\right]=\left[\begin{array}{ccc}
	/// \left\langle -Q_{0}\right\rangle  & 0 & 0\\
	/// \left\langle Q_{1}\right\rangle  & \left\langle -Q_{1}\right\rangle  & 0\\
	/// \left\langle Q_{2}\right\rangle  & 0 & \left\langle -Q_{2}\right\rangle \\
	/// 0 & \left\langle Q_{3}\right\rangle  & 0\\
	/// 0 & \left\langle Q_{4}\right\rangle  & 0\\
	/// 0 & 0 & \left\langle Q_{5}\right\rangle \\
	/// 0 & 0 & \left\langle Q_{6}\right\rangle
	/// \end{array}\right]\left[\begin{array}{ccc}
	/// 1/\phi_{0} & 0 & 0\\
	/// 0 & 1/\phi_{1} & 0\\
	/// 0 & 0 & 1/\phi_{2}
	/// \end{array}\right]\f$
	///
	/// At the bifurcation 0, the liquid bolus of volume \f$V_{B0}\f$ will be split into
	/// \f$V_{B0}\left\langle -Q_{0}\right\rangle /\phi_{0}\f$,
	/// \f$V_{B0}\left\langle Q_{1}\right\rangle /\phi_{0}\f$ and
	/// \f$V_{B0}\left\langle Q_{2}\right\rangle /\phi_{0}\f$
	/// for the airway 0, 1 and 2 respectively.
	/// So the volume of liquid plug gets into each Airway can be computed by
	///
	/// \f$\left[\begin{array}{c}
	/// V_{P0}\\
	/// V_{P1}\\
	/// V_{P2}\\
	/// V_{P3}\\
	/// V_{P4}\\
	/// V_{P5}\\
	/// V_{P6}
	/// \end{array}\right]=\left[\begin{array}{ccc}
	/// \left\langle -Q_{0}\right\rangle /\phi_{0} & 0 & 0\\
	/// \left\langle Q_{1}\right\rangle /\phi_{0} & \left\langle -Q_{1}\right\rangle /\phi_{1} & 0\\
	/// \left\langle Q_{2}\right\rangle /\phi_{0} & 0 & \left\langle -Q_{2}\right\rangle /\phi_{2}\\
	/// 0 & \left\langle Q_{3}\right\rangle /\phi_{1} & 0\\
	/// 0 & \left\langle Q_{4}\right\rangle /\phi_{1} & 0\\
	/// 0 & 0 & \left\langle Q_{5}\right\rangle /\phi_{2}\\
	/// 0 & 0 & \left\langle Q_{6}\right\rangle /\phi_{2}
	/// \end{array}\right]\left[\begin{array}{c}
	/// V_{B0}\\
	/// V_{B1}\\
	/// V_{B2}
	/// \end{array}\right] \f$
	Mat matAB;
	MatCreateAIJ(PETSC_COMM_WORLD, airwayList.size(), bifurcationList.size(), PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matAB);
	Mat matAB_normal;
	MatCreateAIJ(PETSC_COMM_WORLD, bifurcationList.size(), bifurcationList.size(), PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matAB_normal);
	Vec plugVol;
	VecCreateMPI(PETSC_COMM_WORLD, bifurcationList.size(), PETSC_DETERMINE, &plugVol);
	Vec plugVolSplit;
	VecCreateMPI(PETSC_COMM_WORLD, airwayList.size(), PETSC_DETERMINE, &plugVolSplit);

	/// - Loop over the list of plugs at Bifurcation and set liquid bolus volume in PetSc Vector
	VecSet(plugVol,0);
	for (int i = 0 ; i < indexOfBifurcationPlugGetIn.size() ; i++){
		PetscInt row = graph->getVectorIndexOfBifurcationNode(indexOfBifurcationPlugGetIn[i]);
		VecSetValue(plugVol,row,volumeOfPlug[i],ADD_VALUES);
	}
	VecAssemblyBegin(plugVol);

	// Loop over the Airway objects to set up the Matrix \f$\Phi\f$.
	for (std::vector<Airway*>::iterator it = airwayList.begin() ; it != airwayList.end() ; it++){
		Airway *aw = *it;
		int bif_up = aw->getUpperBifurcationIndex();
		int bif_low = aw->getLowerBifurcationIndex();
		double flowrate = aw->getFlowRate();
		PetscInt row = graph->getVectorIndexOfAirwayNode(aw->getIndex());
		if (bif_up >= 0){
			PetscInt col = graph->getVectorIndexOfBifurcationNode(bif_up);
			MatSetValue(matAB, row, col, std::max(flowrate,0.0), INSERT_VALUES);
		}
		if (bif_low >= 0){
			PetscInt col = graph->getVectorIndexOfBifurcationNode(bif_low);
			MatSetValue(matAB, row, col, std::max(-flowrate,0.0), INSERT_VALUES);
		}
	}

	MatAssemblyBegin(matAB, MAT_FINAL_ASSEMBLY);
	VecAssemblyEnd(plugVol);
	MatAssemblyEnd(matAB, MAT_FINAL_ASSEMBLY);

	PetscScalar *totalQ;
	//PetscMalloc(graph->getNumOfBifurcation() * sizeof(PetscReal),&totalQ);
	PetscMalloc1(graph->getNumOfBifurcation(), &totalQ);
	//std::cout << "BIF=" << graph->getNumOfBifurcation() << std::endl;
	PetscScalar *bolus_volume;
	VecGetArray(plugVol, &bolus_volume);
	MatGetColumnNorms(matAB,NORM_1,totalQ);

	for (int i = 0 ; i < bifurcationList.size() ; i++){
		PetscInt col = graph->getVectorIndexOfBifurcationNode(bifurcationList[i]->getIndex());
		//std::cout << "BIFCOL=" << col << " " << totalQ[col] << std::endl;
		if (totalQ[col] <= 0.0){
			if (bolus_volume[i] > 0.0){
				std::cout << "Stranger things have happened.\n";
				std::cout << "A plug gets into a bifurcating section but no flow comes out from the bifurcation.\n";
				std::cout << "bolus_volume=" << bolus_volume[i] << std::endl;
				std::cout << "totalQ[col]=" << totalQ[col] << std::endl;
				std::cout << "Bif Index = " << bifurcationList[i]->getIndex() << std::endl;
				/// Maybe place on the parent airway???
				throw "no flow comes out from the bifurcation";
			}
			totalQ[col] = 1.0;
		}
		MatSetValue(matAB_normal, col, col, 1.0 / totalQ[col], INSERT_VALUES);
	}
	VecRestoreArray(plugVol, &bolus_volume);
	PetscFree(totalQ);

	MatAssemblyBegin(matAB_normal, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(matAB_normal, MAT_FINAL_ASSEMBLY);
	Mat matAB_scaled;
	MatMatMult(matAB,matAB_normal,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&matAB_scaled);
	MatMult(matAB_scaled, plugVol, plugVolSplit);

	/// ### Create LiquidPlug object
	/// - We obtain the volume of plugs \f$V_Pi\f$ with the procedure above. If zero, do nothing.
	/// - The scaled position of plugs can be determined by the direction of flow in Airway.
	///		+ When Airway::getFlowRate > 0, the plug must come from upper bifurcation, so ``position`` is 0.
	/// 	+ Otherwise the plug must come from lower bifurcation, so ``position`` is 1.
	PetscScalar *plug_volume;
	VecGetArray(plugVolSplit, &plug_volume);
	for (int i = 0 ; i < airwayList.size() ; i++){
		if (plug_volume[i] > 0.0) {
			AirwayWithPlugPropagation *aw = dynamic_cast<AirwayWithPlugPropagation *>(airwayList[i]);
			double rad = aw->getRadius();
			///	- Compute the plug length.
			///	 If the plug length > 0 create a plug.
			if (aw->examinePlugCreation(plug_volume[i])){
				double position = (aw->getFlowRate() > 0.0) ? 0.0:1.0;
				LiquidPlug *lp = aw->addLiquidPlug(plug_volume[i], position);
			}else{
				/// - If the liquid volume is not enough to create a plug, add the liquid volume to the lining liquid.
				double liquidVolume = aw->getLiquidVolume() + plug_volume[i];
				aw->setLiquidVolume(liquidVolume);
			}
		}
	}
	//std::cout << "here\n";
		//while(1);
	VecRestoreArray(plugVolSplit, &plug_volume);
	VecDestroy(&plugVol);
	VecDestroy(&plugVolSplit);
	MatDestroy(&matAB);
	MatDestroy(&matAB_normal);
	MatDestroy(&matAB_scaled);
}

bool TrackLiquidPlugs::exportPHDF5dataspace(std::string pdir, hid_t& file) {
	/// - Set Dataspace Name "./LiquidPlugs"
	std::string dataSpace = pdir + "/LiquidPlugs";
	hid_t gid;
	if (H5Lexists(file, dataSpace.c_str(), H5P_DEFAULT) == 1) {
		gid = H5Gopen(file, dataSpace.c_str(), NULL);
	} else {
		gid = H5Gcreate(file, dataSpace.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	}

	/// - Gather data to store.
	std::vector<int > airwayIndex;
	std::vector<double > position;
	std::vector<double > velocity;
	std::vector<double > volume;
	std::vector<double > length;
	std::vector<double > capillaryNumbers;
	std::vector<double > surfaceTension;
	std::vector<double > resistance;
	std::vector<double > h_precursor;
	std::vector<double > h_trailing;

	for (int i = 0 ; i < airwayList.size() ; i++){
		AirwayWithPlugPropagation *aw = dynamic_cast<AirwayWithPlugPropagation *>(airwayList[i]);
		int numOfPlugs = aw->getNumberOfLiquidPlugs();
		for (int j = 0 ; j < numOfPlugs ; j++){
			airwayIndex.push_back(aw->getIndex());
			//std::cout << "aw=" << aw->getIndex() << std::endl;
			LiquidPlug *lp = aw->getLiquidPlug(j);
			position.push_back(lp->getPosition());
			velocity.push_back(lp->getPlugVelocity());
			volume.push_back(lp->getLiquidVolume());
			length.push_back(lp->getPlugLength());
			capillaryNumbers.push_back(lp->getCapillaryNumber());
			surfaceTension.push_back(lp->getSurfaceTension());
			resistance.push_back(lp->getPlugResistance());
			h_precursor.push_back(lp->getPrecursorFilmThickness());
			h_trailing.push_back(lp->getTrailingFilmThickness());
		}
	}
	int numOfPlugs = airwayIndex.size();
	int totalNumOfPlugs;
	MPI_Allreduce(&numOfPlugs, &totalNumOfPlugs, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	PetscPrintf(PETSC_COMM_WORLD, "Number of Plugs = %d\n", totalNumOfPlugs);

	int offset = 0;
	numOfPlugs = airwayIndex.size();
	if (myid > 0){
		// Receive
		MPI_Status status;
		MPI_Recv(&offset,1,MPI_INT,myid-1,(myid-1)*10,MPI_COMM_WORLD,&status);
	}
	if(myid < numproc - 1){
		// send
		int offsetNext = offset + numOfPlugs;
		MPI_Send(&offsetNext,1,MPI_INT,myid+1,myid*10,MPI_COMM_WORLD);
	}
	/// - Store Total Number Of Plugs
	std::string dataSpaceAirway = dataSpace + "/Count";
	if (myid == 0){
		exportPHDF5dataspaceInteger1d(dataSpaceAirway, file, 1, 1, 0, &totalNumOfPlugs);
	}else{
		exportPHDF5dataspaceInteger1d(dataSpaceAirway, file, 1, 0, 0, &totalNumOfPlugs);
	}

	if (totalNumOfPlugs > 0){
		/// - Store Airway that plug stays.
		std::string dataSpaceAirway = dataSpace + "/AirwayIndex";
		exportPHDF5dataspaceInteger1d(dataSpaceAirway, file, totalNumOfPlugs, numOfPlugs, offset, airwayIndex.data());
		/// - Scaled Position
		std::string dataSpacePosition = dataSpace + "/Position";
		exportPHDF5dataspaceDouble1d(dataSpacePosition, file, totalNumOfPlugs, numOfPlugs, offset, position.data());
		/// - Velocity
		std::string dataSpaceVelocity = dataSpace + "/Velocity";
		exportPHDF5dataspaceDouble1d(dataSpaceVelocity, file, totalNumOfPlugs, numOfPlugs, offset, velocity.data());
		/// - Volume
		std::string dataSpaceVolume = dataSpace + "/Volume";
		exportPHDF5dataspaceDouble1d(dataSpaceVolume, file, totalNumOfPlugs, numOfPlugs, offset, volume.data());
		/// - Length
		std::string dataSpaceLenght = dataSpace + "/Length";
		exportPHDF5dataspaceDouble1d(dataSpaceLenght, file, totalNumOfPlugs, numOfPlugs, offset, length.data());
		/// - Capillary Number
		std::string dataSpaceCa = dataSpace + "/Ca";
		exportPHDF5dataspaceDouble1d(dataSpaceCa, file, totalNumOfPlugs, numOfPlugs, offset, capillaryNumbers.data());
		/// - Surface Tension
		std::string dataSpaceSF = dataSpace + "/SurfaceTension";
		exportPHDF5dataspaceDouble1d(dataSpaceSF, file, totalNumOfPlugs, numOfPlugs, offset, surfaceTension.data());
		/// - Resistance
		std::string dataSpaceResistance = dataSpace + "/Resistance";
		exportPHDF5dataspaceDouble1d(dataSpaceResistance, file, totalNumOfPlugs, numOfPlugs, offset, resistance.data());
		/// - Precursor Film Thickness
		std::string dataSpacePrecursor = dataSpace + "/H_Precursor";
		exportPHDF5dataspaceDouble1d(dataSpacePrecursor, file, totalNumOfPlugs, numOfPlugs, offset, h_precursor.data());
		/// - Trailing Film Thickness
		std::string dataSpaceTrailing = dataSpace + "/H_Trailing";
		exportPHDF5dataspaceDouble1d(dataSpaceTrailing, file, totalNumOfPlugs, numOfPlugs, offset, h_trailing.data());
	}
	/// - Close group
	H5Gclose(gid);
	return true;
}

bool TrackLiquidPlugs::importHDF5dataspace(std::string pdir, hid_t& file) {
	/// - Set Dataspace Name "./LiquidPlugs"
	int totalNumOfPlugs;
	std::string dataSpace = pdir + "/LiquidPlugs";
	std::string dataNumOfPlugs = dataSpace + "/AirwayIndex";
	importHDF5dataspaceInteger1d(dataNumOfPlugs, file, 1,&totalNumOfPlugs);

	if (totalNumOfPlugs > 0){
		std::vector<int > airwayIndex(totalNumOfPlugs);
		std::vector<double > position(totalNumOfPlugs);
		std::vector<double > velocity(totalNumOfPlugs);
		std::vector<double > volume(totalNumOfPlugs);
		std::vector<double > length(totalNumOfPlugs);
		std::vector<double > capillaryNumbers(totalNumOfPlugs);
		std::vector<double > surfaceTension(totalNumOfPlugs);
		std::vector<double > resistance(totalNumOfPlugs);
		std::vector<double > h_precursor(totalNumOfPlugs);
		std::vector<double > h_trailing(totalNumOfPlugs);
		/// - Read Airway that plug stays.
		std::string dataSpaceAirway = dataSpace + "/AirwayIndex";
		importHDF5dataspaceInteger1d(dataSpaceAirway, file, totalNumOfPlugs, airwayIndex.data());
		/// - Scaled Position
		std::string dataSpacePosition = dataSpace + "/Position";
		importHDF5dataspaceDouble1d(dataSpacePosition, file, totalNumOfPlugs, position.data());
		/// - Velocity
		std::string dataSpaceVelocity = dataSpace + "/Velocity";
		importHDF5dataspaceDouble1d(dataSpaceVelocity, file, totalNumOfPlugs, velocity.data());
		/// - Volume
		std::string dataSpaceVolume = dataSpace + "/Volume";
		importHDF5dataspaceDouble1d(dataSpaceVolume, file, totalNumOfPlugs, volume.data());
		/// - Length
		std::string dataSpaceLenght = dataSpace + "/Length";
		importHDF5dataspaceDouble1d(dataSpaceLenght, file, totalNumOfPlugs, length.data());
		/// - Capillary Number
		std::string dataSpaceCa = dataSpace + "/Ca";
		importHDF5dataspaceDouble1d(dataSpaceCa, file, totalNumOfPlugs, capillaryNumbers.data());
		/// - Surface Tension
		std::string dataSpaceSF = dataSpace + "/SurfaceTension";
		importHDF5dataspaceDouble1d(dataSpaceSF, file, totalNumOfPlugs, surfaceTension.data());
		/// - Resistance
		std::string dataSpaceResistance = dataSpace + "/Resistance";
		importHDF5dataspaceDouble1d(dataSpaceResistance, file, totalNumOfPlugs, resistance.data());
		/// - Precursor Film Thickness
		std::string dataSpacePrecursor = dataSpace + "/H_Precursor";
		importHDF5dataspaceDouble1d(dataSpacePrecursor, file, totalNumOfPlugs,  h_precursor.data());
		/// - Trailing Film Thickness
		std::string dataSpaceTrailing = dataSpace + "/H_Trailing";
		importHDF5dataspaceDouble1d(dataSpaceTrailing, file, totalNumOfPlugs, h_trailing.data());

		for (int i = 0 ; i < totalNumOfPlugs ; i++){
			if (graph->part_airway(airwayIndex[i]) == myid) {
				int n;
				for (n = 0 ; n < airwayList.size() ; n++){
					if(airwayList[n]->getIndex() == airwayIndex[i]) break;
				}
				AirwayWithPlugPropagation *aw = dynamic_cast<AirwayWithPlugPropagation *>(airwayList[n]);
				LiquidPlug *lp = aw->addLiquidPlug(volume[n], position[n]);
				lp->setAirwayFlowRate(velocity[n] * M_PI * aw->getRadius() * aw->getRadius());
				lp->setPrecursorFilmThickness(h_precursor[n]);
				lp->setSurfaceTension(surfaceTension[n]);
			}
		}
	}
}

void TrackLiquidPlugs::rupturePlug(LiquidPlug *lp, AirwayWithPlugPropagation *aw) {
	/// * Obtain the volume of plug and add it to the liquid volume in Airway.
	double plugVolume = lp->getLiquidVolume();
	double liquidVolume = aw->getLiquidVolume() + plugVolume;
	aw->setLiquidVolume(liquidVolume);
	/// * Remove the plug.
	aw->removeLiquidPlug(lp);
	/// This method doesn't destroy LiquidPlug object.
}

void TrackLiquidPlugs::liquidIntoAcinus(LiquidPlug *lp, AirwayWithPlugPropagation *aw) {
	/// Under construction
	/// * Remove the plug.
	aw->removeLiquidPlug(lp);
	/// This method doesn't destroy LiquidPlug object.
}
