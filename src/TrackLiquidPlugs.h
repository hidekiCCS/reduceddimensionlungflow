/*
 * TrackLiquidPlugs.h
 *
 *  Created on: Feb 10, 2015
 *      Author: fuji
 */

#ifndef TRACKLIQUIDPLUGS_H_
#define TRACKLIQUIDPLUGS_H_

#include <iostream>
#include <vector>
#include <string>
#include "DataBase.h"
#include "AirwayTreePartitioning.h"
#include "Bifurcation.h"
#include "AirwayWithPlugPropagation.h"
#include "LiquidPlug.h"


/// Tracking liquid plugs in Airway network model.
/// * TrackLiquidPlugs::updateLiquidPlugs updates plug positions.

class TrackLiquidPlugs: public DataBase {
public:
	/// Constructor takes AirwayTreePartitioning object, lists of Bifurcation objects and Airway objects.
	TrackLiquidPlugs(AirwayTreePartitioning *graph, std::vector<Bifurcation *> bifurcationList, std::vector<Airway *> airwayList);
	virtual ~TrackLiquidPlugs();

	/// Update Plug position, must call collectively.
	void updateLiquidPlugs(double dt);

	/// Export Data to Parallel HDF5 data space, must call collectively
	virtual bool exportPHDF5dataspace(std::string pdir, hid_t &file);
	/// Import Data from Parallel HDF5 data space, must call collectively
	virtual bool importHDF5dataspace(std::string pdir, hid_t &file);

private:
	/// Split plugs at Bifurcation
	void splitPlugsAtBifurcation(std::vector<LiquidPlug *> listOfPlugsAtBifurcation, std::vector<AirwayWithPlugPropagation *> listOfAirwaysPlugsAtBifurcation);
	/// Rupture plug
	void rupturePlug(LiquidPlug *lp, AirwayWithPlugPropagation *aw);
	/// plug into acinus
	void liquidIntoAcinus(LiquidPlug *lp, AirwayWithPlugPropagation *aw);

	/// Object of AirwayTreePartitioning
	AirwayTreePartitioning *graph;
	/// List of Bifurcation objects
	std::vector<Bifurcation *> bifurcationList;
	/// List of Airway objects
	std::vector<Airway *> airwayList;
	/// List of Liquid Plugs
	std::vector<LiquidPlug *> liquidPlugList;
	/// List of Airway Index of Liquid Plugs
	std::vector<int> airwayIndexOfliquidPlugList;

	/// process id
	int myid;
	/// number of processes
	int numproc;
};

#endif /* TRACKLIQUIDPLUGS_H_ */
