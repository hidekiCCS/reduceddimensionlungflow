# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 17:11:02 2015

@author: fuji
"""

from pythonModule.xml2VTK import *
import h5py as h5

if (len(sys.argv) < 3):
    print "%s [xml airway Data] [First Count] [Last Count] [Step Size]\n" % (sys.argv[0])
    sys.exit()
if (False == os.path.isfile(sys.argv[1])):
    sys.exit("xml file not exist")
#
lung = xmlLungModel(sys.argv[1])
lung.analyze()
#
baseFileName = lung.baseFileName
print "Base File Name : ", baseFileName
#
firstCount = int(sys.argv[2])
lastCount = firstCount
stepSize = 1
if (len(sys.argv) > 3):
    lastCount = int(sys.argv[3])
if (len(sys.argv) > 4):
    stepSize = int(sys.argv[4])
#h5File = baseFileName + ".h5"
#h5f = h5.File(h5File,'r')
#
#timeCounterList = h5f["TimeSeries"].keys()
#timeSeries = h5f["TimeSeries"]
for i in range(firstCount,lastCount + 1,stepSize):
    h5File = ("%s%06d.h5" % (baseFileName, i))            
    h5f = h5.File(h5File,'r')
    timeC = ("%06d" % (i))
    
    aw = h5f["Airways"]
    prs = aw["Pressure"].value
    eps = aw["EPS"].value
    radius = aw["Radius"].value
    flowR = aw["FlowRate"].value
    cnd = aw["Conductance"].value
    liquidVol = aw["LiquidVolume"].value
    if i == 0:
        lung.addCellDataScalar("Pressure",prs)
        lung.addCellDataScalar("EPS",eps)
        lung.addCellDataScalar("Radius",radius)
        lung.addCellDataScalar("Flowrate",flowR)
        lung.addCellDataScalar("Conductance",cnd)
        lung.addCellDataScalar("LiquidVolume",liquidVol)
    else:
        lung.updateCellDataScalar("Pressure",prs)
        lung.updateCellDataScalar("EPS",eps)
        lung.updateCellDataScalar("Radius",radius)
        lung.updateCellDataScalar("Flowrate",flowR)
        lung.updateCellDataScalar("Conductance",cnd)
        lung.updateCellDataScalar("LiquidVolume",liquidVol)
    lung.export2VTK_Tree(baseFileName + timeC)
    h5f.close()
#
#


