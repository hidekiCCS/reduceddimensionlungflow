# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 11:18:33 2015

@author: fuji
"""

from pythonModule.LungWithPlugs import *
import h5py as h5

if (len(sys.argv) < 3):
    print "%s [xml airway Data] [First Count] [Last Count] [Step Size]\n" % (sys.argv[0])
    sys.exit()
if (False == os.path.isfile(sys.argv[1])):
    sys.exit("xml file not exist")
#
lung = LungWithModel(sys.argv[1])
lung.analyze()
#
baseFileName = lung.baseFileName
print "Base File Name : ", baseFileName
#
firstCount = int(sys.argv[2])
lastCount = firstCount
stepSize = 1
if (len(sys.argv) > 3):
    lastCount = int(sys.argv[3])
if (len(sys.argv) > 4):
    stepSize = int(sys.argv[4])
#
#h5File = baseFileName + ".h5"
#h5f = h5.File(h5File,'r')
#
#timeCounterList = h5f["TimeSeries"].keys()
#timeSeries = h5f["TimeSeries"]
for i in range(firstCount,lastCount + 1,stepSize):
    h5File = ("%s%06d.h5" % (baseFileName, i))            
    h5f = h5.File(h5File,'r')
    timeC = ("P%06d" % (i))
     
    aw = h5f["Airways"]
    prs = aw["Pressure"].value
    eps = aw["EPS"].value
    flowR = aw["FlowRate"].value
    cnd = aw["Conductance"].value
    if i == 0:
        lung.addCellDataScalar("Pressure",prs)
        lung.addCellDataScalar("EPS",eps)
        lung.addCellDataScalar("Flowrate",flowR)
        lung.addCellDataScalar("Conductance",cnd)
    else:
        lung.updateCellDataScalar("Pressure",prs)
        lung.updateCellDataScalar("EPS",eps)
        lung.updateCellDataScalar("Flowrate",flowR)
        lung.updateCellDataScalar("Conductance",cnd)
    lung.export2VTK_Tree(baseFileName + '_' + timeC)
    #
    # Plugs    
    if i != 0:
        lung.clearPlugs()
    plugs = h5f["LiquidPlugs"]
    if len(plugs.keys()) > 0:
        awIdx = plugs["AirwayIndex"].value
        position = plugs["Position"].value
        velocity = plugs["Velocity"].value
        volume = plugs["Volume"].value
        length = plugs["Length"].value
        #
        lung.addPlugs(awIdx,position)
        lung.addPlugDataScalar("Velocity",velocity)
        lung.addPlugDataScalar("Volume",volume)
        lung.addPlugDataScalar("Length",length)
    else: # Dummy
        lung.addPlug(0,0)
        zero = np.array([0.0])
        lung.addPlugDataScalar("Velocity",zero)
        lung.addPlugDataScalar("Volume",zero)
        lung.addPlugDataScalar("Length",zero)
    lung.export2VTK_Plugs(baseFileName + timeC)
    #    
    totalVol = 0.0
    acinus = h5f["Acini"]
    acVol = acinus["Volume"].value
    print "Total Acinus Volume=", acVol.sum()
    h5f.close()
#
#

