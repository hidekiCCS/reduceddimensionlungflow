# -*- coding: utf-8 -*-
"""
Created on Tue Nov 25 2017

@author: fuji
"""

from pythonModule.xml2VTK3D import *
import h5py as h5
import numpy as np

def frontPressureGradient(hf,ca):
    m = 0.5
    k1 = 1.05
    k2 = 2.93
    n1 = 1.90
    n2 = 0
    x = np.power(ca,m) / np.maximum(hf,1.e-5)
    return   formulaEq33(k1,k2,n1,n2,x)
#
def frontShearStress(hf,ca):
    m = 1
    k1 = 0.633
    k2 = 0.107
    n1 = 1.04
    n2 = -0.0299
    x = np.power(ca,m) / np.maximum(hf,1.e-5)
    return   formulaEq33(k1,k2,n1,n2,x)
#
def frontShearStressGradient(hf,ca):
    m = 2./3
    k1 = 0.716
    k2 = 1.86
    n1 = 0.233
    n2 = 1.89
    x = np.power(ca,m) / hf
    return   formulaEq33(k1,k2,n1,n2,x)
#
def rearPressureGradient(ca):
    k1 = 5.57
    k2 = 0.337
    n1 = 0.744
    n2 = -0.299
    return   formulaEq33(k1,k2,n1,n2,ca)
#
def rearShearStress(ca):
    k1 = 1.78
    k2 = 0.498
    n1 = 0.987
    n2 = 0.315
    return   formulaEq33(k1,k2,n1,n2,ca)
#
def rearShearStressGradient(ca):
    k1 = 0.265
    k2 = 2.40
    n1 = 0.0272
    n2 = 0.971
    return   formulaEq33(k1,k2,n1,n2,ca)
#   
def formulaEq33(k1,k2,n1,n2,x):
    return k1 * np.power(x,n1) + k2 * np.power(x,n2)
#
#
def extractAirwayStressData(h5f):
    aw = h5f["Airways"]
    awRadius = aw["Radius"].value
    awLength = aw["Length"].value
    numberOfAirways = len(awRadius)
    numpc = 20
    PG = np.zeros((numberOfAirways, numpc))
    SS = np.zeros((numberOfAirways, numpc))
    SG = np.zeros((numberOfAirways, numpc))
    #
    plugs = h5f["LiquidPlugs"]
    if len(plugs.keys()) > 0:
        awIdx = plugs["AirwayIndex"].value
        position = plugs["Position"].value
        length = plugs["Length"].value
        velocity = plugs["Velocity"].value
        ca = plugs["Ca"].value
        sf = plugs["SurfaceTension"].value
        hf = plugs["H_Precursor"].value
        hr = plugs["H_Trailing"].value
        for idx in range(len(awIdx)):
            #fp = int(numpc * (position[idx] + 0.5 * length[idx] / awLength[awIdx[idx]]))
            #rp = int(numpc * (position[idx] - 0.5 * length[idx] / awLength[awIdx[idx]]))
            fp = int(numpc * position[idx])
            rp = int(numpc * position[idx])
            fp = np.minimum(np.maximum(0,fp),numpc-1)
            rp = np.minimum(np.maximum(0,rp),numpc-1)
            if velocity[idx] < 0:
                fp, rp = rp, fp
            #
            PG[awIdx[idx],fp] = np.maximum(PG[awIdx[idx],fp],frontPressureGradient(hf[idx],ca[idx]) * sf[idx] / (awRadius[awIdx[idx]] * awRadius[awIdx[idx]]))
            SS[awIdx[idx],fp] = np.maximum(SS[awIdx[idx],fp],frontShearStress(hf[idx],ca[idx]) * sf[idx] / awRadius[awIdx[idx]])
            SG[awIdx[idx],fp] = np.maximum(SG[awIdx[idx],fp],frontPressureGradient(hf[idx],ca[idx]) * sf[idx] / (awRadius[awIdx[idx]] * awRadius[awIdx[idx]]))
            PG[awIdx[idx],rp] = np.maximum(PG[awIdx[idx],rp],rearPressureGradient(ca[idx]) * sf[idx] / (awRadius[awIdx[idx]] * awRadius[awIdx[idx]]))
            SS[awIdx[idx],rp] = np.maximum(SS[awIdx[idx],rp],rearShearStress(ca[idx]) * sf[idx] / awRadius[awIdx[idx]])
            SG[awIdx[idx],rp] = np.maximum(SG[awIdx[idx],rp],rearPressureGradient(ca[idx]) * sf[idx] / (awRadius[awIdx[idx]] * awRadius[awIdx[idx]]))
    #
    return PG, SS, SG

#
#

# MAIN
if __name__=="__main__":
    if (len(sys.argv) < 4):
        print "%s [xml airway Data] [First Count] [Last Count] [Step Size]\n" % (sys.argv[0])
        sys.exit()
    if (False == os.path.isfile(sys.argv[1])):
        sys.exit("xml file not exist")
    #
    lung = xmlLungModel3D(sys.argv[1])
    baseFileName = lung.baseFileName
    print "Base File Name : ", baseFileName
    #
    firstCount = int(sys.argv[2])
    lastCount = firstCount
    stepSize = 1
    if (len(sys.argv) > 3):
        lastCount = int(sys.argv[3])
    if (len(sys.argv) > 4):
        stepSize = int(sys.argv[4])
    
    for i in range(firstCount,lastCount + 1,stepSize):
        h5File = ("%s%06d.h5" % (baseFileName, i))            
        h5f = h5.File(h5File,'r')
    
        PG, SS, SG  = extractAirwayStressData(h5f)
        if i == 0:
            mPG = PG
            mSS = SS
            mSG = SG
        else:
            mPG = np.maximum(mPG,PG)
            mSS = np.maximum(mSS,SS)
            mSG = np.maximum(mSG,SG)
        
    #
    lung.addCellDataScalar("Radius",np.array(lung.radius))
    lung.addCellDataScalar("Length",np.array(lung.length))
    lung.addCellDataArray("PressureGradient",mPG)
    lung.addCellDataArray("ShearStress",mSS)
    lung.export2VTK_3DTree(lung.baseFileName)
    